package com.myliveclassroom.eduethics.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.myliveclassroom.eduethics.Models.TimeTableModel;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.classes.TinyDB;

import java.util.List;

public class StudentHealthFeedbackAdapter extends RecyclerView.Adapter<StudentHealthFeedbackAdapter.vh> {

    List<TimeTableModel> studentModelList;
    int of;
    Context mContext;
    TinyDB tinyDB;

    public StudentHealthFeedbackAdapter(Context context, List<TimeTableModel> studentModelList ) {
        this.studentModelList=studentModelList;
        mContext=context;
        tinyDB=new TinyDB(mContext);
    }

    @NonNull
    @Override
    public vh onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new vh(LayoutInflater.from(mContext).inflate(R.layout.student_heath_feedback_adapter, parent, false));
    }

    Boolean isFever=false,isCold=false;
    @Override
    public void onBindViewHolder(@NonNull vh holder, int position) {

        studentModelList.get(position).setFever("N");
        studentModelList.get(position).setCold("N");
        studentModelList.get(position).setSubjectId(
                studentModelList.get(position).getSubjectId()==null?"0":studentModelList.get(position).getSubjectId());

        holder.txtRollNo.setText(studentModelList.get(position).getRollNo());
        holder.txtName.setText(studentModelList.get(position).getStudentName());

        holder.chkFever.setChecked(false);
        holder.chkCold.setChecked(false);

        holder.chkFever.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    studentModelList.get(position).setFever("Y");
                } else {
                    studentModelList.get(position).setFever("N");
                }
            }
        });
        holder.chkCold.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    studentModelList.get(position).setCold("Y");
                } else {
                    studentModelList.get(position).setCold("N");
                }
            }
        });
    }

    public void updateList(List<TimeTableModel> list) {
        studentModelList= list;
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return studentModelList==null?0: studentModelList.size();
    }

    class vh extends RecyclerView.ViewHolder {


        TextView  txtName,   txtRollNo;
        CheckBox chkFever,chkCold;
        public vh(@NonNull View itemView) {
            super(itemView);

            this.txtName = itemView.findViewById(R.id.txtStudentName);
            this.txtRollNo = itemView.findViewById(R.id.txtRollNo);

            this.chkFever = itemView.findViewById(R.id.chkFever);
            this.chkCold = itemView.findViewById(R.id.chkCold);


        }
    }

}
