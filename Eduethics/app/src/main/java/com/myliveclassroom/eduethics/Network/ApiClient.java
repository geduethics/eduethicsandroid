package com.myliveclassroom.eduethics.Network;

import android.content.Context;


import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.API_BASE_URL;

public class ApiClient {
    //public static final String BASE_URL = "http://api.themoviedb.org/3/";
    public static final String BASE_URL = API_BASE_URL;
    private static Retrofit retrofit = null;


    public static Retrofit getClient(Context mContext) {
        if (retrofit == null) {
            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(new NetworkConnectionInterceptor(mContext))
                    .build();


            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
        }
        return retrofit;
    }
}
