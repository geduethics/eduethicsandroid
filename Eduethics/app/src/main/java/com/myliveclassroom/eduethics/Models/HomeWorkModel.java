package com.myliveclassroom.eduethics.Models;

public class HomeWorkModel {

    private String HomeworkId;
    private String WorkDoneId;
    private String ClassId;
    private String SchoolId;
    private String SubjectId;
    private String StudentId;
    private String TeacherId;
    private  String SessionId;

    private String WorkDate;
    private String WorkHead;
    private String WorkDetail;
    private String Attachment;
    private String DoneAttachment;
    private String SubjectName;

    private String StudentName;
    private  String RollNo;
    private  String Status;
    private String MaxMarks;
    private String Score;

    private String Time;
    private  String ProfileType;
    private String Remarks;

    private  String Date;
    public String getTime() {
        return Time;
    }
    private String IsDone;

    public String getIsDone() {
        return IsDone;
    }

    public String getDate() {
        return Date;
    }


    public void setProfileType(String profileType) {
        ProfileType = profileType;
    }

    public String getMaxMarks() {
        return MaxMarks;
    }

    public String getWorkDoneId() {
        return WorkDoneId;
    }

    public void setStudentId(String studentId) {
        StudentId = studentId;
    }

    public void setSchoolId(String schoolId) {
        SchoolId = schoolId;
    }

    public void setClassId(String classId) {
        ClassId = classId;
    }

    public String getStudentId() {
        return StudentId;
    }

    public String getSchoolId() {
        return SchoolId;
    }

    public String getClassId() {
        return ClassId;
    }

    public String getSubjectId() {
        return SubjectId;
    }

    public String getAttachment() {
        return Attachment;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectId(String subjectId) {
        SubjectId = subjectId;
    }

    public String getWorkDate() {
        return WorkDate;
    }

    public String getWorkHead() {
        return WorkHead;
    }

    public String getWorkDetail() {
        return WorkDetail;
    }

    public void setAttachment(String attachment) {
        Attachment = attachment;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public void setWorkDate(String workDate) {
        WorkDate = workDate;
    }

    public void setWorkDetail(String workDetail) {
        WorkDetail = workDetail;
    }

    public void setWorkHead(String workHead) {
        WorkHead = workHead;
    }


    public void setTeacherId(String teacherId) {
        TeacherId = teacherId;
    }

    public String getTeacherId() {
        return TeacherId;
    }

    public String getSessionId() {
        return SessionId;
    }

    public void setSessionId(String sessionId) {
        SessionId = sessionId;
    }

    public String getHomeworkId() {
        return HomeworkId;
    }

    public void setHomeworkId(String homeworkId) {
        HomeworkId = homeworkId;
    }

    public String getDoneAttachment() {
        return DoneAttachment;
    }

    public String getRollNo() {
        return RollNo;
    }

    public String getStudentName() {
        return StudentName;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getScore() {
        return Score;
    }

    public void setScore(String score) {
        Score = score;
    }

    public String getRemarks() {
        return Remarks;
    }

    public void setRemarks(String remarks) {
        Remarks = remarks;
    }
}
