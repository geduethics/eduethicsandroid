package com.myliveclassroom.eduethics.Models;

import android.widget.ImageView;

public class PaymentMethodModel {
   int imgPaymentIcon;
   String PaymentMethod;


    public String getPaymentMethod() {
        return PaymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        PaymentMethod = paymentMethod;
    }

    public void setImgPaymentIcon(int imgPaymentIcon) {
        this.imgPaymentIcon = imgPaymentIcon;
    }

    public int getImgPaymentIcon() {
        return imgPaymentIcon;
    }
}
