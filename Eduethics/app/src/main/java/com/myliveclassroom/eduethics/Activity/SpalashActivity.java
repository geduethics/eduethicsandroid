package com.myliveclassroom.eduethics.Activity;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;


import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.myliveclassroom.eduethics.MyApplication;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Services.GeneralService;
import com.myliveclassroom.eduethics.Utils.GlobalVariables;
import com.myliveclassroom.eduethics.classes.TinyDB;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.HashMap;
import java.util.Map;

import static com.myliveclassroom.eduethics.Utils.HomeConstants.PROFILE;

public class SpalashActivity extends AppCompatActivity {

    SharedPreferences prefs;
    private Thread mSplashThread;
    WebView we;
    TinyDB tinyDB;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spalash);


        tinyDB = new TinyDB(this);
        mContext = this;

        //TODO
        //live class direct entry

        if (getSupportActionBar() != null)
            getSupportActionBar().hide();
        else if (getActionBar() != null) getActionBar().hide();


        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // Toast.makeText(SpalashActivity.this, "Error(220)(Authorize): Please Check your SHA256 on Google Play Console ", Toast.LENGTH_SHORT).show();
                        //   SpalashActivity.this.moveTaskToBack(true);
                    }
                });

            }
        }).start();


        prefs = getApplicationContext().getSharedPreferences("USER_PREF",
                Context.MODE_PRIVATE);


        final Intent[] intent = new Intent[1];
        if (prefs.getString("type", "none").equals("none")) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(1500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            intent[0] = new Intent(SpalashActivity.this, LoginActivity.class);
                            intent[0].setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent[0], ActivityOptions.makeSceneTransitionAnimation(SpalashActivity.this).toBundle());
                        }
                    });
                }
            }).start();


        } else {
            //intent[0] = new Intent(SpalashActivity.this, MainActivity.class);
            //intent[0].setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            //notification(intent[0]);
            checkTinyDB();
        }

        //Foreground service start
        startService();

        }


    private void checkTinyDB() {
        Intent intent;
        if (tinyDB.getString(PROFILE) == null || tinyDB.getString(PROFILE).equalsIgnoreCase("")) {
            intent = new Intent(SpalashActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        } else {
            //Move to complete your profile
            intent = new Intent(SpalashActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }

    private void notification(Intent intent) {


        GlobalVariables.uid = prefs.getString("uid", "none");
        if (FirebaseMessaging.getInstance() != null)
            FirebaseMessaging.getInstance().getToken()
                    .addOnCompleteListener(task -> {
                        if (!task.isSuccessful()) {
                            startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(SpalashActivity.this).toBundle());
                            SpalashActivity.this.finish();
                            return;
                        }

                        Map<String, Object> map = new HashMap<>();
                        map.put("token", task.getResult());

                        FirebaseFirestore.getInstance()
                                .collection(prefs.getString("type", "none").equals("teacher") ? "teacher" : "students")
                                .document(GlobalVariables.uid)
                                .update(map)
                                .addOnSuccessListener(aVoid -> {
                                    startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(SpalashActivity.this).toBundle());
                                    SpalashActivity.this.finish();
                                }).addOnFailureListener(e -> {
                            startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(SpalashActivity.this).toBundle());
                            SpalashActivity.this.finish();
                        });
                    }).addOnFailureListener(e -> {
                startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(SpalashActivity.this).toBundle());
                SpalashActivity.this.finish();
            });
        else {
            startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(SpalashActivity.this).toBundle());
        }
    }

    public void startService() {
        Intent serviceIntent = new Intent(this, GeneralService.class);
        serviceIntent.putExtra("inputExtra", "Foreground Service Example in Android");
        ContextCompat.startForegroundService(this, serviceIntent);
    }

    public static void callAlarmActvity(){
        Intent intent1=new Intent(MyApplication.getAppContext(), AlarmActivity.class);
        MyApplication.getAppContext().startActivity(intent1);
    }

}