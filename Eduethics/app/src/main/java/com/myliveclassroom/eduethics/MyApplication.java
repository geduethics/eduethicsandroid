package com.myliveclassroom.eduethics;
import android.app.Application;
import android.content.Context;


public class MyApplication extends Application {

    AppEnvironment appEnvironment;
    @Override public void onCreate() {
        super.onCreate();
        //ViewTarget.setTagId(R.id.glide_tag);
        appEnvironment = AppEnvironment.PRODUCTION;
        MyApplication.context = getApplicationContext();
    }
    public AppEnvironment getAppEnvironment() {
        return appEnvironment;
    }

    public void setAppEnvironment(AppEnvironment appEnvironment) {
        this.appEnvironment = appEnvironment;
    }

    private static Context context;

    public static Context getAppContext() {
        return MyApplication.context;
    }

}
