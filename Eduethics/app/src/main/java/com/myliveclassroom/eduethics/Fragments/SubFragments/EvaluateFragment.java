package com.myliveclassroom.eduethics.Fragments.SubFragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.myliveclassroom.eduethics.Activity.MainActivity;
import com.myliveclassroom.eduethics.Adapter.UploadImagesAdapter;
import com.myliveclassroom.eduethics.Fragments.HomeClassroomFragment;
import com.myliveclassroom.eduethics.Models.ApiResponseModel;
import com.myliveclassroom.eduethics.Models.HomeWorkModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.google.firebase.firestore.FirebaseFirestore;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;
import static com.myliveclassroom.eduethics.Utils.LocalConstants.FROM.FROM_ASSIGNMENT_FRAG;
import static com.myliveclassroom.eduethics.Utils.LocalConstants.FROM.FROM_EVALUATE_FRAG;
import static com.myliveclassroom.eduethics.Utils.LocalConstants.FROM.FROM_TEST_FRAG;

public class EvaluateFragment extends Fragment {

    HomeWorkModel homeWorkModel;

    ArrayList<String> submissionList = new ArrayList<>();

    UploadImagesAdapter adapter;
    Context mContext;

    int from;

    EditText scoreTxt, totalScoreTxt, feedbackTxt;
    View progressView;


    public EvaluateFragment(Context context, HomeWorkModel homeWorkModel, int from) {
        this.homeWorkModel = homeWorkModel;
        mContext = context;
        this.from = from;
        adapter = new UploadImagesAdapter(submissionList, this, FROM_EVALUATE_FRAG, null);
    }

    View root;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_evaluate, container, false);

        adapter.setProgress(root.findViewById(R.id.progress));
        scoreTxt = root.findViewById(R.id.score);
        totalScoreTxt = root.findViewById(R.id.totalScore);
        feedbackTxt = root.findViewById(R.id.feedbackText);
        progressView=root.findViewById(R.id.progressBar);

        RecyclerView submissionRecyclerView = root.findViewById(R.id.submissionRecyclerView);
        submissionRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        MainActivity.TO_WHICH_FRAG = from == FROM_ASSIGNMENT_FRAG ? LocalConstants.TO_FRAG.TO_ASSIGNMENT_FRAG : LocalConstants.TO_FRAG.TO_TEST_FRAG;
        submissionRecyclerView.setAdapter(adapter);
        MainActivity.TO_WHICH_FRAG = LocalConstants.TO_FRAG.TO_HOME_CLASSROOM;

        scoreTxt.setText(Double.parseDouble(homeWorkModel.getScore())>0?homeWorkModel.getScore():"");
        totalScoreTxt.setText(homeWorkModel.getMaxMarks());

        String[] attachemntss = homeWorkModel.getDoneAttachment().split(",");
        for (String s : attachemntss) {
            submissionList.add(s);
        }
        adapter.notifyDataSetChanged();

        if (!homeWorkModel.getStatus().equalsIgnoreCase(LocalConstants.HOME_WORK_STATUS.UPLOADED)) {
            feedbackTxt.setText(homeWorkModel.getRemarks());
        }

        root.findViewById(R.id.submit).setOnClickListener(v -> {
            boolean allDone = true;
            if (scoreTxt.getText().toString().equals("")) {
                scoreTxt.setError("Required!");
                allDone = false;
            } else if (Double.parseDouble(scoreTxt.getText().toString()) > Double.parseDouble(totalScoreTxt.getText().toString())) {
                Toast.makeText(getContext(), "Give right Score", Toast.LENGTH_SHORT).show();
                allDone = false;
            }
            if (allDone) {
                submitWorkStatus();
            }

            /*if (allDone){
                Map<String, Object> map = new HashMap<>();
                data.put("score", ((EditText) root.findViewById(R.id.score)).getText().toString() + "/" + ((EditText) root.findViewById(R.id.totalScore)).getText().toString());
                if (((EditText) root.findViewById(R.id.feedbackText)).getText().toString().equals("")) {
                    data.put("feedback", "-");
                } else {
                    data.put("feedback", ((EditText) root.findViewById(R.id.feedbackText)).getText().toString());
                }
                data.put("isSeen", true);
                map.put(studentId, data);
                String collection = "";
                if (from == FROM_ASSIGNMENT_FRAG) {
                    collection = "assignmentSub";
                } else if (from == FROM_TEST_FRAG) {
                    collection = "testSub";
                }
                FirebaseFirestore.getInstance()
                        .collection("classroom")
                        .document(HomeClassroomFragment.classId)
                        .collection(collection)
                        .document(docId)
                        .update(map)
                        .addOnSuccessListener(aVoid -> {
                            if (getActivity() == null) return;
                            getActivity().onBackPressed();
                        });
            }
*/
        });

        return root;
    }

    private void submitWorkStatus() {

        homeWorkModel.setStatus(LocalConstants.HOME_WORK_STATUS.GOOD);
        homeWorkModel.setScore(scoreTxt.getText().toString());
        homeWorkModel.setRemarks(feedbackTxt.getText().toString()); //This will go in remarks
        disableView();
        progressView.setVisibility(View.VISIBLE);
        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<ApiResponseModel> call = apiService.HomeWorStausSubmit(homeWorkModel);
        call.enqueue(new Callback<ApiResponseModel>() {
            @Override
            public void onResponse(Call<ApiResponseModel> call, Response<ApiResponseModel> response) {
                try {
                    ApiResponseModel apiResponseModel = response.body();
                    enableView();
                } catch (Exception ex) {
                    Log.e(mTAG, "error =" + ex.getMessage());
                    enableView();
                }
            }

            @Override
            public void onFailure(Call<ApiResponseModel> call, Throwable t) {
                Log.e(mTAG, "error =" + t.toString());
                enableView();
            }
        });
    }

    private void enableView(){
        progressView.setVisibility(View.GONE);
        root.findViewById(R.id.submit).setEnabled(true);
    }

    private void disableView(){
        progressView.setVisibility(View.VISIBLE);
        root.findViewById(R.id.submit).setEnabled(false);
    }
}