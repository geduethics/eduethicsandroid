package com.myliveclassroom.eduethics.Activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;


import com.bumptech.glide.Glide;
import com.myliveclassroom.eduethics.R;
import com.rajat.pdfviewer.PdfViewerActivity;

public class ImagePage extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.MyThemeNoBar);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_image_page);



        String url = getIntent().getStringExtra("urlImage");
        String type = getIntent().getStringExtra("type");


        switch (type) {
            case "pdf":
                Intent intent = new Intent(this, PdfViewerActivity.class);
                intent.putExtra("pdf_file_url", url);
                intent.putExtra("pdf_file_title", url.substring(url.length() - 10));
                intent.putExtra("pdf_file_directory", getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath());
                intent.putExtra("enable_download", true);
                intent.putExtra("isPDFFromPath", false);
                startActivity(intent);
                finish();
                break;
            case "image":
                Glide.with(this).load(Uri.parse(url)).into((ImageView) findViewById(R.id.image));
                break;
            default:
                onBackPressed();
                break;
        }
    }
}