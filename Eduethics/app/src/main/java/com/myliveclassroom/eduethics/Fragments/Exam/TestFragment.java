package com.myliveclassroom.eduethics.Fragments.Exam;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.transition.TransitionInflater;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.myliveclassroom.eduethics.Activity.MainActivity;
import com.myliveclassroom.eduethics.Activity.VideoConfrenceActivity;
import com.myliveclassroom.eduethics.Adapter.TestAdapter;
import com.myliveclassroom.eduethics.Fragments.WebAppFrag.ExamQuestionFragment;
import com.myliveclassroom.eduethics.Fragments.WebLogin;
import com.myliveclassroom.eduethics.Models.ExamModel;
import com.myliveclassroom.eduethics.Models.QuestionModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.TinyDB;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.w3c.dom.Text;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.PROFILE;


public class TestFragment extends Fragment {

    RecyclerView recyclerTest;
    Context context;
    View root;
    TinyDB tinyDB;
    Context mContext;
    List<ExamModel> examModelList = new ArrayList<>();
    TestAdapter testAdapter;
    String mProfile;
    ConstraintLayout header;
    ProgressBar progressBar;


    public TextView submittedText, pendingText;

    public TestFragment(Context context) {
        this.context = context;
        mContext = context;
        tinyDB = new TinyDB(mContext);
        mProfile = tinyDB.getString(PROFILE);
        tinyDB.putBoolean(LocalConstants.Exam_Constants.Show_Ans_Keys, false);
    }

    @Override
    public void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TransitionInflater inflater = TransitionInflater.from(requireContext());
        setEnterTransition(inflater.inflateTransition(R.transition.slide_right));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.fragment_test_student, container, false);
        progressBar = root.findViewById(R.id.progressBar);

        schoolSetting();
        MainActivity.TO_WHICH_FRAG = LocalConstants.TO_FRAG.TO_CLASSROOM;

        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)) {
            ((TextView) root.findViewById(R.id.tit)).setText("Tests scheculed by teacher \nappear here");

            root.findViewById(R.id.createTest).setVisibility(View.GONE);
        } else
            root.findViewById(R.id.createTest).setOnClickListener(v -> getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(
                    R.anim.slide_in,  // enter
                    R.anim.fade_out,  // exit
                    R.anim.fade_in,   // popEnter
                    R.anim.slide_out  // popExit
            ).replace(R.id.fragmentFrame, new CreateTestFragment(mContext)).commit());

        pendingText = root.findViewById(R.id.pendingText);
        submittedText = root.findViewById(R.id.submittedText);

        /*pendingText.setOnClickListener(v -> getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(
                R.anim.slide_in,  // enter
                R.anim.fade_out,  // exit
                R.anim.fade_in,   // popEnter
                R.anim.slide_out  // popExit
        ).replace(R.id.fragmentFrame, new ExamMcqCreateFragment(mContext)).commit());*/

        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER)) {
            //root.findViewById(R.id.pendingText).setVisibility(View.GONE);
            root.findViewById(R.id.submittedText).setVisibility(View.GONE);
        }

      /* if (HomeClassroomFragment.model.getTest().size() == 0) {
            root.findViewById(R.id.empty).setVisibility(View.VISIBLE);
            ((TextView) root.findViewById(R.id.emptyMessage)).setText("No Test Found!");
        } */


        recyclerTest = root.findViewById(R.id.recyclerTest);
        recyclerTest.setLayoutManager(new LinearLayoutManager(context));
        testAdapter = new TestAdapter(TestFragment.this, examModelList, mContext);
        recyclerTest.setAdapter(testAdapter);
        // getExamList();
        return root;
    }

    private void schoolSetting() {
        header = root.findViewById(R.id.head);
        if (tinyDB.getString(LocalConstants.INSTITUTE_TYPE.TYPE).equalsIgnoreCase(LocalConstants.INSTITUTE_TYPE.SCHOOL)) {
            header.setVisibility(View.GONE);
        }
    }

    public void callAddQuestionFragment() {
        getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(
                R.anim.slide_in,  // enter
                R.anim.fade_out,  // exit
                R.anim.fade_in,   // popEnter
                R.anim.slide_out  // popExit
        ).replace(R.id.fragmentFrame, new ExamQuestionFragment(mContext)).commit();
    }

    String mRoomName = "EduEthicsLiveClassRoomOralExal";


    public void startOralTest(ExamModel examModel) {
        String classId = examModel.getClassId();
        tinyDB.putString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID, classId);
        tinyDB.putString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_NAME, examModel.getClassName() == null ? "" : examModel.getClassName());

        String subjectId = examModel.getSubjectId();
        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)) {
            tinyDB.putString(LocalConstants.Jitsi_CONSTANTS.PROFILE, LocalConstants.Jitsi_CONSTANTS.JOIN_AS_STUDENT);
        } else {
            tinyDB.putString(LocalConstants.Jitsi_CONSTANTS.PROFILE, LocalConstants.Jitsi_CONSTANTS.JOIN_AS_TUTOR);
        }
        progressBar.setVisibility(View.VISIBLE);
        mContext.startActivity(new Intent(mContext, VideoConfrenceActivity.class)
                .putExtra("roomId", mRoomName + classId)
                .putExtra("classId", classId)
                .putExtra("mClassType", LocalConstants.Video_TYPE.VideoExam)
        );
    }

    ;

    public void startOralTestAlert(ExamModel examModel) {
        AlertDialog.Builder alertDailogBuilder = new AlertDialog.Builder(mContext);

        final View customLayout = getLayoutInflater().inflate(R.layout.layout_chose_video_mode, null);
        alertDailogBuilder.setView(customLayout);
        alertDailogBuilder.setMessage("Chose exam mode");
        alertDailogBuilder.setCancelable(true);
        AlertDialog alert11 = alertDailogBuilder.create();

        TextView btnStartExamPopUpMobile = customLayout.findViewById(R.id.btnStartExamPopUpMobile);
        TextView btnStartExamPopUpWeb = customLayout.findViewById(R.id.btnStartExamPopUpWeb);

        btnStartExamPopUpMobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startOralTest(examModel);
            }
        });

        btnStartExamPopUpWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tinyDB.putBoolean("IsExam", true);
                tinyDB.putString("ExamId", examModel.getExamId());
                tinyDB.putString("mClassType", LocalConstants.Video_TYPE.VideoExam);
               /* getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(
                        R.anim.slide_in,  // enter
                        R.anim.fade_out,  // exit
                        R.anim.fade_in,   // popEnter
                        R.anim.slide_out  // popExit
                ).replace(R.id.fragmentFrame, new WebLogin(mContext)).commit();*/

                mContext.startActivity(new Intent(mContext, MainActivity.class)
                        .putExtra("roomId", mRoomName + examModel.getClassId())
                        .putExtra("classId", examModel.getClassId())
                        .putExtra("mClassType", LocalConstants.Video_TYPE.VideoExam)
                );

                alert11.dismiss();
            }
        });

        alert11.show();

        /*String classId = examModel.getClassId();
        tinyDB.putString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID, classId);
        tinyDB.putString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_NAME, examModel.getClassName() == null ? "" : examModel.getClassName());

        String subjectId = examModel.getSubjectId();
        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)) {
            tinyDB.putString(LocalConstants.Jitsi_CONSTANTS.PROFILE, LocalConstants.Jitsi_CONSTANTS.JOIN_AS_STUDENT);
        } else {
            tinyDB.putString(LocalConstants.Jitsi_CONSTANTS.PROFILE, LocalConstants.Jitsi_CONSTANTS.JOIN_AS_TUTOR);
        }
        progressBar.setVisibility(View.VISIBLE);
        mContext.startActivity(new Intent(mContext, VideoConfrenceActivity.class)
                .putExtra("roomId", mRoomName + classId)
                .putExtra("classId", classId)
                .putExtra("mClassType", "Exam")
        );*/
    }

    private void getExamList() {
        ExamModel examModel = new ExamModel();
        examModel.setCreatedBy(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
        examModel.setClassId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID));

        if (tinyDB.getString(LocalConstants.INSTITUTE_TYPE.TYPE).equalsIgnoreCase(LocalConstants.INSTITUTE_TYPE.SCHOOL)) {
            examModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
            examModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));
        }
        examModel.setProfileType(mProfile);

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = apiService.ExamListByTeacher(examModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                try {
                    if (response.body() == null) {
                        Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    Type type = new TypeToken<List<ExamModel>>() {
                    }.getType();
                    examModelList = new Gson().fromJson(response.body().getAsJsonArray("Response"), type);
                    testAdapter.updateList(examModelList);
                } catch (Exception ex) {
                    Toast.makeText(mContext, LocalConstants.APP_MESSAGES.ERROR1, Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, LocalConstants.APP_MESSAGES.ERROR1, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void view(int position) {
      /*  getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(
                R.anim.slide_in,  // enter
                R.anim.fade_out,  // exit
                R.anim.fade_in,   // popEnter
                R.anim.slide_out  // popExit
        ).replace(R.id.fragmentFrame, new ViewSubmissionTest(HomeClassroomFragment.model.getTest().get(position).get("resultId").toString(), LocalConstants.FROM.FROM_TEST_FRAG, HomeClassroomFragment.model.getTest().get(position))).commit();
        */
    }

    public void edit(int position) {
        /*
        getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(
                R.anim.slide_in,  // enter
                R.anim.fade_out,  // exit
                R.anim.fade_in,   // popEnter
                R.anim.slide_out  // popExit
        ).replace(R.id.fragmentFrame, new CreateTest(HomeClassroomFragment.model.getTest().get(position), position)).commit();
        */
    }

    public void open(int position, boolean isDead) {
        /*
        getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(
                R.anim.slide_in,  // enter
                R.anim.fade_out,  // exit
                R.anim.fade_in,   // popEnter
                R.anim.slide_out  // popExit
        ).replace(R.id.fragmentFrame, new StartSubmission(HomeClassroomFragment.model.getTest().get(position), LocalConstants.FROM.FROM_TEST_FRAG, isDead)).commit();
        */
    }


    public void solution(int position) {
        /*
        getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(
                R.anim.slide_in,  // enter
                R.anim.fade_out,  // exit
                R.anim.fade_in,   // popEnter
                R.anim.slide_out  // popExit
        ).replace(R.id.fragmentFrame, new AddSolution("test", HomeClassroomFragment.model.getTest().get(position), true)).commit();
        */
    }

    public void makeExamLive(ExamModel examModel) {
        if (Float.parseFloat(examModel.getNoOfQCreated())
                < Float.parseFloat(examModel.getTotalQuestions())) {
            Toast.makeText(mContext, "Only " + examModel.getTotalQuestions() + "question(s) are created yet.", Toast.LENGTH_SHORT).show();
            // return;
        }

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = apiService.ExamMakeLive(examModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.e(mTAG, "userModelList" + response.body().toString());
                try {
                    if (response.body() == null) {
                        Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    String status = new Gson().fromJson(response.body().get("Status"), String.class);
                    if (status.toUpperCase().equalsIgnoreCase(LocalConstants.API_STATUS.SUCCESS.toUpperCase())) {
                        Toast.makeText(mContext, "This is live now", Toast.LENGTH_SHORT).show();
                        getExamList();
                    } else {
                        String mes = new Gson().fromJson(response.body().get("Message"), String.class);
                        Toast.makeText(mContext, mes, Toast.LENGTH_SHORT).show();
                    }


                } catch (Exception ex) {
                    Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        progressBar.setVisibility(View.GONE);
        getExamList();
    }

}