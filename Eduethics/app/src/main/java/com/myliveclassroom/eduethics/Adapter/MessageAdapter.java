package com.myliveclassroom.eduethics.Adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.myliveclassroom.eduethics.Models.MessageModel;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.classes.CommonFunctions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ViewHolder> {

    private Context mContext;
    private List<MessageModel> messageModelList;
    public MessageAdapter(Context context,List<MessageModel> messageModels){
        messageModelList=messageModels;
        mContext=context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.message_adapter, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtMessage.setText(Html.fromHtml(messageModelList.get(position).getMessage()));
        holder.txtSender.setText(Html.fromHtml(CommonFunctions.capitalize(messageModelList.get(position).getTeacherName())));
        holder.txtDate.setText(Html.fromHtml(messageModelList.get(position).getCreatedDate()));
    }

    @Override
    public int getItemCount() {
        return messageModelList==null?0:messageModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imgIcon)
        ImageView imgIcon;

        @BindView(R.id.txtMessage)
        TextView txtMessage;

        @BindView(R.id.txtSender)
        TextView txtSender;

        @BindView(R.id.txtDate)
        TextView txtDate;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
