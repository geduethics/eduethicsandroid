package com.myliveclassroom.eduethics.Activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.myliveclassroom.eduethics.Fragments.WebAppFrag.AttendanceTFragment;
import com.myliveclassroom.eduethics.Models.TimeTableModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.CommonFunctions;
import com.google.android.material.tabs.TabLayout;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;


public class AttendanceTActivity extends BaseActivity {

    @BindView(R.id.txtPageTitle)
    TextView txtPageTitle;

    @BindView(R.id.imgBack)
    ImageView imgBack;

    @BindView(R.id.imgbtnAdd)
    ImageView imgbtnAdd;

    @BindView(R.id.btnAdd)
    TextView btnAdd;

    Fragment fragment;
    public  List<TimeTableModel> timeTableModel;

    private ViewPagerAdapterInner viewPagerAdapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;


    private String mClassId;
    private String mSelectedDate;

    public ImageView imgSubmit()
    {
        return imgbtnAdd;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.MyThemeNoBar);
        setContentView(R.layout.activity_time_table);
        ButterKnife.bind(this);
        _BaseActivity(this);
        getNoDays();
        init();
    }

    private void init() {
        mClassId=getIntent().getExtras().getString("ClassId");
        mSelectedDate=getIntent().getExtras().getString("Date");

        imgbtnAdd.setVisibility(View.VISIBLE);
        imgBack.setVisibility(View.VISIBLE);
        btnAdd.setVisibility(View.GONE);
        btnAdd.setText("");
        imgbtnAdd.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_tick3));
        txtPageTitle.setText("Mark Attendance");
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Back();
            }
        });

        /*imgbtnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitAttendance();
            }
        });*/
    }

    //@OnClick(R.id.imgBack)
    private void Back() {
        super.onBackPressed();
    }

    public void submitAttendance()
    {
        Log.e(mTAG,"clicked submitAttendance");
    }

    private void getNoDays() {
        TimeTableModel listModel = new TimeTableModel();
        listModel.setTeacherId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
        listModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        listModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<List<TimeTableModel>> call = apiService.getTeacherClasses(listModel);
        call.enqueue(new Callback<List<TimeTableModel>>() {
            @Override
            public void onResponse(Call<List<TimeTableModel>> call, Response<List<TimeTableModel>> response) {
                //int statusCode = response.code();
                try {
                    timeTableModel = response.body();
                    tinyDB.putString("ClassName", timeTableModel.get(0).getClassName());
                    tinyDB.putString("ClassId", timeTableModel.get(0).getClassId());
                    fragment = null;
                    viewPagerAdapter = new ViewPagerAdapterInner(getSupportFragmentManager(), timeTableModel.size());
                    viewPager = findViewById(R.id.viewpager);
                    viewPager.setOffscreenPageLimit(1);
                    tinyDB.putString("DayId","1");
                    if (fragment == null) {
                        viewPager.setAdapter(viewPagerAdapter);
                    }
                    tabLayout = findViewById(R.id.tabs);
                    tabLayout.setupWithViewPager(viewPager);
                    tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                        @Override
                        public void onTabSelected(TabLayout.Tab tab) {
                            int i=tab.getPosition();
                            tinyDB.putString("ClassName", timeTableModel.get(tab.getPosition()).getClassName());
                            tinyDB.putString("ClassId", timeTableModel.get(tab.getPosition()).getClassId());
                        }

                        @Override
                        public void onTabUnselected(TabLayout.Tab tab) {

                        }

                        @Override
                        public void onTabReselected(TabLayout.Tab tab) {

                        }
                    });

                    // isAllTabLoaded = true;

                } catch (Exception ex) {
                    Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<List<TimeTableModel>> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public class ViewPagerAdapterInner extends FragmentStatePagerAdapter {
        private int noOfItems;

        public ViewPagerAdapterInner(FragmentManager fm, int noOfItems) {
            super(fm);
            this.noOfItems = noOfItems;
        }

        @Override
        public Fragment getItem(int position) {
            fragment = AttendanceTFragment.newInstance(position);
            return fragment;
        }

        @Override
        public int getCount() {
            return noOfItems;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return CommonFunctions.capitalize(timeTableModel.get(position).getClassName()  );
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

    }
}
