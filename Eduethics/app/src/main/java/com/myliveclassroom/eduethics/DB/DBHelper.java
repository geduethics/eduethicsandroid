package com.myliveclassroom.eduethics.DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;



public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "eduethics.db";

    /*****************************tbNotification**************************************/
    public static final String TABLE_NOTIFICATION = "tbNotification";
    public static final String COLUMN_NOTIFICATION_ID = "Id";
    public static final String COLUMN_NOTIFICATION_DESC = "Notification";
    public static final String COLUMN_NOTIFICATION_DATE = "Date";
    public static final String COLUMN_NOTIFICATION_TYPE = "Type";
    public static final String COLUMN_NOTIFICATION_COUNT = "IsVisible";
    /*****************************tbNotification End***********************************/



    public DBHelper(Context context){
        super(context,DATABASE_NAME,null,4);
    }

    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE_NOT = "CREATE TABLE " + TABLE_NOTIFICATION + "("
                + COLUMN_NOTIFICATION_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COLUMN_NOTIFICATION_DESC + " TEXT, "
                + COLUMN_NOTIFICATION_TYPE + " TEXT, "
                + COLUMN_NOTIFICATION_DATE + " DATETIME DEFAULT CURRENT_TIMESTAMP,"
                +COLUMN_NOTIFICATION_COUNT+ " INTEGER "
                + ")";


        db.execSQL(CREATE_TABLE_NOT);
    }

    public boolean checkIfExistsInTbl(String Query) {
        SQLiteDatabase db = this.getWritableDatabase();
        // String Query = "Select * from " + TableName + " where " + dbfield + " = " + fieldValue;
        Cursor cursor = db.rawQuery(Query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTIFICATION);
        // Create tables again
        onCreate(db);
    }

    /*public boolean addHandler(CircularModel notificationModel) {

        String query = "Select * FROM " + TABLE_NOTIFICATION + " WHERE "
                + COLUMN_NOTIFICATION_DESC + " = " + "'" + notificationModel.getCircularHead() + "' and "
                + COLUMN_NOTIFICATION_DATE + " = " + "'" + notificationModel.getCircularDate() + "' and "
                +COLUMN_NOTIFICATION_TYPE+" = '"+notificationModel.getCircularType()+"'";

        if(!checkIfExistsInTbl(query)) {
            ContentValues values = new ContentValues();
            values.put(COLUMN_NOTIFICATION_DESC, notificationModel.getCircularHead());
            values.put(COLUMN_NOTIFICATION_TYPE, notificationModel.getCircularType());
            values.put(COLUMN_NOTIFICATION_DATE, notificationModel.getCircularDate());
            values.put(COLUMN_NOTIFICATION_COUNT, 1);
            SQLiteDatabase db = this.getWritableDatabase();
            long result = db.insert(TABLE_NOTIFICATION, null, values);
            db.close();
            if (result == -1)
                return false;
            else
                return true;
        }
        return false;
    }*/



    public Cursor getAllData() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLE_NOTIFICATION + " order by Id desc limit 25", null);
        return res;
    }

    public boolean updateHandler(String headType, int updVal) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();
        args.put(COLUMN_NOTIFICATION_COUNT, updVal);
        Boolean b = false;
        try {
            b = db.update(TABLE_NOTIFICATION, args, COLUMN_NOTIFICATION_TYPE + "='" + headType+"'", null)>0;
        } catch (Exception ex) {
            db.close();
            b = false;
            Log.e("update", ex.getMessage());
        }
        if (b) {
            db.close();
            return true;
        } else {
            db.close();
            return false;
        }
    }

    public int countNotifications(String headType){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor mCount= db.rawQuery("select count(*) from "+TABLE_NOTIFICATION+" where "+COLUMN_NOTIFICATION_TYPE+"='" + headType + "' and "+COLUMN_NOTIFICATION_COUNT+"="+1, null);
        mCount.moveToFirst();
        int count= mCount.getInt(0);
        mCount.close();
        return  count;
    }
}
