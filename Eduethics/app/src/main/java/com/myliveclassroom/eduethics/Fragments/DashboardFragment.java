package com.myliveclassroom.eduethics.Fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.transition.TransitionInflater;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.myliveclassroom.eduethics.Activity.AttendanceDashStudentActivity;
import com.myliveclassroom.eduethics.Activity.AttendanceDashTeacherActivity;
import com.myliveclassroom.eduethics.Activity.BirthdayActivity;
import com.myliveclassroom.eduethics.Activity.CircularsActivity;
import com.myliveclassroom.eduethics.Activity.ExamSetupActivity;
import com.myliveclassroom.eduethics.Activity.FeesActivity;
import com.myliveclassroom.eduethics.Activity.GalleryActivity;
import com.myliveclassroom.eduethics.Activity.HealthActivity;
import com.myliveclassroom.eduethics.Activity.HomeWorkActivity;
import com.myliveclassroom.eduethics.Activity.HomeworkDashActivity;
import com.myliveclassroom.eduethics.Activity.MessageActivity;
import com.myliveclassroom.eduethics.Activity.ReportActivity;
import com.myliveclassroom.eduethics.Activity.ResultActivity;
import com.myliveclassroom.eduethics.Activity.TimeTableActivity;
import com.myliveclassroom.eduethics.Activity.TimeTableTActivity;
import com.myliveclassroom.eduethics.Adapter.HomeAdapter;
import com.myliveclassroom.eduethics.Fragments.WebAppFrag.FeeSchoolFragment;
import com.myliveclassroom.eduethics.Models.DashboardModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.HomeConstants;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.ExpandableHeightGridView;
import com.myliveclassroom.eduethics.classes.TinyDB;
import com.google.gson.JsonObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import app.futured.donut.DonutProgressView;
import app.futured.donut.DonutSection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.API_BASE_URL_REPORTS;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.ATTENDANCE;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.ATTENDANCE_TEACHER;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.BIRTHDAY;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.CIRCULARS;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.DIGITAL_LIBRARY;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.EXAMS;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.FEE;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.GALLERY;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.HEALTH_FEEBACK;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.HOMEWORK;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.HOMEWORK_TEACHER;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.ICARD;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.MESSAGES;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.NOTIFICATIONS;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.PROFILE;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.REPORTS;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.RESULTS;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.SCHEDULE_STUDENT;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.SCHEDULE_TEACHER;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.SHARE_APP;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.SYLLABUS;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.TIMETABLE;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.TIMETABLE_TEACHER;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.VIDEO_LECTURES;


public class DashboardFragment extends Fragment {

    Context mContext;
    View root;
    ExpandableHeightGridView gridViewHome;
    TinyDB tinyDB;
    private DonutProgressView dpvChart;
    List<DashboardModel> dashboardModelList;
    CardView cardViewAttendance;

    TextView txtTotalFee, txtTotalFeePaid, txtTotalPending,
            txtTotalTeacher,txtTeacherPresent,txtTeacherAbsent,txtTeacherLeave,
            txtTotalStudent,txtStudentPresent,txtStudentAbsent,txtStudentLeave;
    private BarChart chart;

    public DashboardFragment(Context context, String profileType) {
        mContext = context;
        tinyDB = new TinyDB(mContext);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TransitionInflater inflater = TransitionInflater.from(requireContext());
        setExitTransition(inflater.inflateTransition(R.transition.fade));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.SCHOOL)) {
            root = inflater.inflate(R.layout.home_dashboard_adapter, container, false);
            setHomeAdapterAdmin(root);

            //Show fee details
            cardViewAttendance=root.findViewById(R.id.cardViewAttendance);
            cardViewAttendance.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(
                            R.anim.slide_in,  // enter
                            R.anim.fade_out,  // exit
                            R.anim.fade_in,   // popEnter
                            R.anim.slide_out  // popExit
                    ).replace(R.id.fragmentFrame,new FeeSchoolFragment(mContext, LocalConstants.FEE_VIEWS.ADMIN_SCREEN_1)).commit();
                }
            });

            txtTotalFee = root.findViewById(R.id.txtTotalFee);
            txtTotalFeePaid = root.findViewById(R.id.txtTotalFeePaid);
            txtTotalPending = root.findViewById(R.id.txtTotalPending);

            txtTotalTeacher=root.findViewById(R.id.txtTotalTeacher);
            txtTeacherPresent=root.findViewById(R.id.txtTeacherPresent);
            txtTeacherAbsent=root.findViewById(R.id.txtTeacherAbsent);
            txtTeacherLeave=root.findViewById(R.id.txtTeacherLeave);

            txtTotalStudent=root.findViewById(R.id.txtTotalStudent);
            txtStudentPresent=root.findViewById(R.id.txtStudentPresent);
            txtStudentAbsent=root.findViewById(R.id.txtStudentAbsent);
            txtStudentLeave=root.findViewById(R.id.txtStudentLeave);

            dpvChart = root.findViewById(R.id.dpvChart);
            dashBoardData();
            //barChart();
           /* float totalFee = 18000;
            float feePaid = 9000;
            float pending = totalFee - feePaid;


            DonutSection section = new DonutSection("Section 1 Name", Color.parseColor("#f44336"), (feePaid / totalFee) * 100);
            dpvChart.setCap(100f);
            dpvChart.submitData(new ArrayList<>(Collections.singleton(section)));*/
        } else {
            root = inflater.inflate(R.layout.fragment_dashboard, container, false);
            init(root);
        }

        return root;
    }

    private void init(View v) {
        setHomeAdapter(v);
    }

    private void setHomeAdapterAdmin(View v) {

        //Setting default value in TEMP_SCREEN_NAME
        tinyDB.putString(LocalConstants.TINYDB_KEYS.TEMP_SCREEN_NAME, "");

        gridViewHome = v.findViewById(R.id.gridViewHome);
        gridViewHome.setAdapter(new HomeAdapter(mContext, false));
        gridViewHome.setExpanded(true);
        gridViewHome.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent;
                switch (((TextView) view.getTag(R.id.tvActivity)).getText().toString()) {
                    case ICARD:
                        Toast.makeText(mContext, "Pending", Toast.LENGTH_SHORT).show();
                        //intent = new Intent(mContext, IcardActivity.class);
                        //mContext.startActivity(intent);
                        break;
                    case TIMETABLE:
                        tinyDB.putString(LocalConstants.TINYDB_KEYS.TEMP_SCREEN_NAME, "");
                        intent = new Intent(mContext, TimeTableActivity.class);
                        mContext.startActivity(intent);
                        break;
                    case ATTENDANCE:
                        intent = new Intent(mContext, AttendanceDashStudentActivity.class);
                        mContext.startActivity(intent);
                        break;
                    case ATTENDANCE_TEACHER:
                        intent = new Intent(mContext, AttendanceDashTeacherActivity.class);
                        mContext.startActivity(intent);
                        break;
                    case HEALTH_FEEBACK:
                        intent = new Intent(mContext, HealthActivity.class);
                        mContext.startActivity(intent);
                        break;
                    case FEE:
                        //Toast.makeText(mContext, "Pending", Toast.LENGTH_SHORT).show();
                        intent = new Intent(mContext, FeesActivity.class);
                        tinyDB.putString(LocalConstants.FEE_VIEWS.FEE_SCREEN, LocalConstants.FEE_VIEWS.ADMIN_SCREEN_1+"");
                        mContext.startActivity(intent);
                        break;
                    case HOMEWORK:
                        intent = new Intent(mContext, HomeWorkActivity.class);
                        tinyDB.putString("Screen", HomeConstants.HOMEWORK);
                        mContext.startActivity(intent);
                        // dbHelper.updateHandler(TINY_CONSTANT_HomeworkNotification, 0);
                        break;

                    case HOMEWORK_TEACHER:
                        intent = new Intent(mContext, HomeworkDashActivity.class);
                        tinyDB.putString("Screen", HOMEWORK_TEACHER);
                        mContext.startActivity(intent);
                        break;

                    case SCHEDULE_STUDENT:
                        intent = new Intent(mContext, TimeTableActivity.class);
                        tinyDB.putString(LocalConstants.TINYDB_KEYS.TEMP_SCREEN_NAME, SCHEDULE_STUDENT);
                        mContext.startActivity(intent);
                        break;
                    case SCHEDULE_TEACHER:
                        tinyDB.putString(LocalConstants.TINYDB_KEYS.TEMP_SCREEN_NAME, SCHEDULE_TEACHER);
                        intent = new Intent(mContext, TimeTableTActivity.class);
                        mContext.startActivity(intent);
                        break;

                    case TIMETABLE_TEACHER:
                        tinyDB.putString(LocalConstants.TINYDB_KEYS.TEMP_SCREEN_NAME, "");
                        intent = new Intent(mContext, TimeTableTActivity.class);
                        mContext.startActivity(intent);
                        break;
                    case CIRCULARS:
                        intent = new Intent(mContext, CircularsActivity.class);
                        mContext.startActivity(intent);
                        break;
                    case GALLERY:
                        intent = new Intent(mContext, GalleryActivity.class);
                        mContext.startActivity(intent);
                        break;
                    case NOTIFICATIONS:
                        Toast.makeText(mContext, "Pending", Toast.LENGTH_SHORT).show();
                        //  intent = new Intent(mContext, NotificationActivity.class);
                        //  mContext.startActivity(intent);
                        break;
                    case MESSAGES:
                        intent = new Intent(mContext, MessageActivity.class);
                        mContext.startActivity(intent);
                        break;
                    case BIRTHDAY:
                        intent = new Intent(mContext, BirthdayActivity.class);
                        mContext.startActivity(intent);
                        break;
                    case EXAMS:
                        // intent = new Intent(mContext, ExamSummaryActivity.class);
                        intent = new Intent(mContext, ExamSetupActivity.class);
                        mContext.startActivity(intent);
                        break;
                    case RESULTS:
                        intent = new Intent(mContext, ResultActivity.class);
                        mContext.startActivity(intent);
                        break;
                    case VIDEO_LECTURES:
                        Toast.makeText(mContext, "Pending", Toast.LENGTH_SHORT).show();
                        //myToast(mContext,"Coming Soon!",Toast.LENGTH_SHORT);
                        // intent = new Intent(mContext, VideoLecturesActivity.class);
                        // mContext.startActivity(intent);
                        break;


                    case SYLLABUS:
                        Toast.makeText(mContext, "Pending", Toast.LENGTH_SHORT).show();
                        //intent = new Intent(mContext, DigitalLibrary.class);
                        //intent.putExtra("Activity",SYLLABUS);
                        //intent.putExtra("PageTitle",SYLLABUS);
                        //intent.putExtra("PageTitleAdd","Add Word Of The Day File");
                        //intent.putExtra("Url",api_MainUrlDigitalLibrary);
                        //mContext.startActivity(intent);
                        break;

                    case DIGITAL_LIBRARY:
                        Toast.makeText(mContext, "Pending", Toast.LENGTH_SHORT).show();
                        // intent = new Intent(mContext, DigitalLibrary.class);
                        // intent.putExtra("Activity",DIGITAL_LIBRARY);
                        // intent.putExtra("PageTitle",DIGITAL_LIBRARY);
                        // intent.putExtra("PageTitleAdd","Add Word Of The Day File");
                        // intent.putExtra("Url",api_MainUrlDigitalLibrary);
                        // mContext.startActivity(intent);
                        break;


                    case SHARE_APP:
                        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                        sharingIntent.setType("text/plain");
                        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "e-Sikhya");
                        String text = getResources().getString(R.string.share_app_text_en);
                        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, mShareText);
                        startActivity(Intent.createChooser(sharingIntent, "Share app via"));

                       /* String shareBody=shareText;
                        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Lalaji Grocer");
                        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                        startActivity(Intent.createChooser(sharingIntent, "Share via"));*/
                        break;
                }
            }
        });
    }

    private void setHomeAdapter(View v) {

        //Setting default value in TEMP_SCREEN_NAME
        tinyDB.putString(LocalConstants.TINYDB_KEYS.TEMP_SCREEN_NAME, "");


        gridViewHome = v.findViewById(R.id.gridViewHome);
        gridViewHome.setAdapter(new HomeAdapter(mContext, false));
        gridViewHome.setExpanded(true);
        gridViewHome.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent;
                switch (((TextView) view.getTag(R.id.tvActivity)).getText().toString()) {
                    case ICARD:
                        Toast.makeText(mContext, "Pending", Toast.LENGTH_SHORT).show();
                        //intent = new Intent(mContext, IcardActivity.class);
                        //mContext.startActivity(intent);
                        break;
                    case TIMETABLE:
                        tinyDB.putString(LocalConstants.TINYDB_KEYS.TEMP_SCREEN_NAME, "");
                        intent = new Intent(mContext, TimeTableActivity.class);
                        mContext.startActivity(intent);
                        break;
                    case ATTENDANCE:
                        intent = new Intent(mContext, AttendanceDashStudentActivity.class);
                        mContext.startActivity(intent);
                        break;
                    case ATTENDANCE_TEACHER:
                        intent = new Intent(mContext, AttendanceDashTeacherActivity.class);
                        mContext.startActivity(intent);
                        break;
                    case HEALTH_FEEBACK:
                        intent = new Intent(mContext, HealthActivity.class);
                        mContext.startActivity(intent);
                        break;
                    case FEE:
                        //Toast.makeText(mContext, "Pending", Toast.LENGTH_SHORT).show();
                        intent = new Intent(mContext, FeesActivity.class);
                        tinyDB.putString(LocalConstants.FEE_VIEWS.FEE_SCREEN, LocalConstants.FEE_VIEWS.OTHER_SCREEN+"");
                        mContext.startActivity(intent);
                        break;
                    case HOMEWORK:
                        intent = new Intent(mContext, HomeWorkActivity.class);
                        tinyDB.putString("Screen", HomeConstants.HOMEWORK);
                        mContext.startActivity(intent);
                        // dbHelper.updateHandler(TINY_CONSTANT_HomeworkNotification, 0);
                        break;

                    case HOMEWORK_TEACHER:
                        intent = new Intent(mContext, HomeworkDashActivity.class);
                        tinyDB.putString("Screen", HOMEWORK_TEACHER);
                        mContext.startActivity(intent);
                        break;

                    case SCHEDULE_STUDENT:
                        intent = new Intent(mContext, TimeTableActivity.class);
                        tinyDB.putString(LocalConstants.TINYDB_KEYS.TEMP_SCREEN_NAME, SCHEDULE_STUDENT);
                        mContext.startActivity(intent);
                        break;
                    case SCHEDULE_TEACHER:
                        tinyDB.putString(LocalConstants.TINYDB_KEYS.TEMP_SCREEN_NAME, SCHEDULE_TEACHER);
                        intent = new Intent(mContext, TimeTableTActivity.class);
                        mContext.startActivity(intent);
                        break;

                    case TIMETABLE_TEACHER:
                        tinyDB.putString(LocalConstants.TINYDB_KEYS.TEMP_SCREEN_NAME, "");
                        intent = new Intent(mContext, TimeTableTActivity.class);
                        mContext.startActivity(intent);
                        break;
                    case CIRCULARS:
                        intent = new Intent(mContext, CircularsActivity.class);
                        mContext.startActivity(intent);
                        break;
                    case GALLERY:
                        intent = new Intent(mContext, GalleryActivity.class);
                        mContext.startActivity(intent);
                        break;
                    case NOTIFICATIONS:
                        Toast.makeText(mContext, "Pending", Toast.LENGTH_SHORT).show();
                        //  intent = new Intent(mContext, NotificationActivity.class);
                        //  mContext.startActivity(intent);
                        break;
                    case MESSAGES:
                        intent = new Intent(mContext, MessageActivity.class);
                        mContext.startActivity(intent);
                        break;
                    case BIRTHDAY:
                        intent = new Intent(mContext, BirthdayActivity.class);
                        mContext.startActivity(intent);
                        break;
                    case EXAMS:
                        // intent = new Intent(mContext, ExamSummaryActivity.class);
                        intent = new Intent(mContext, ExamSetupActivity.class);
                        mContext.startActivity(intent);
                        break;
                    case RESULTS:
                        intent = new Intent(mContext, ResultActivity.class);
                        mContext.startActivity(intent);
                        break;

                    case REPORTS:
                        //Toast.makeText(mContext, "Pending", Toast.LENGTH_SHORT).show();
                         intent = new Intent(mContext, ReportActivity.class);
                         intent.putExtra("Activity",REPORTS);
                         intent.putExtra("PageTitle",REPORTS);
                         intent.putExtra("PageTitleAdd","Add Word Of The Day File");
                         intent.putExtra("Url",API_BASE_URL_REPORTS);
                         mContext.startActivity(intent);
                        break;
                    case VIDEO_LECTURES:
                        Toast.makeText(mContext, "Pending", Toast.LENGTH_SHORT).show();
                        //myToast(mContext,"Coming Soon!",Toast.LENGTH_SHORT);
                        // intent = new Intent(mContext, VideoLecturesActivity.class);
                        // mContext.startActivity(intent);
                        break;


                    case SYLLABUS:
                        Toast.makeText(mContext, "Pending", Toast.LENGTH_SHORT).show();
                        //intent = new Intent(mContext, DigitalLibrary.class);
                        //intent.putExtra("Activity",SYLLABUS);
                        //intent.putExtra("PageTitle",SYLLABUS);
                        //intent.putExtra("PageTitleAdd","Add Word Of The Day File");
                        //intent.putExtra("Url",api_MainUrlDigitalLibrary);
                        //mContext.startActivity(intent);
                        break;

                    case DIGITAL_LIBRARY:
                        Toast.makeText(mContext, "Pending", Toast.LENGTH_SHORT).show();
                        // intent = new Intent(mContext, DigitalLibrary.class);
                        // intent.putExtra("Activity",DIGITAL_LIBRARY);
                        // intent.putExtra("PageTitle",DIGITAL_LIBRARY);
                        // intent.putExtra("PageTitleAdd","Add Word Of The Day File");
                        // intent.putExtra("Url",api_MainUrlDigitalLibrary);
                        // mContext.startActivity(intent);
                        break;


                    case SHARE_APP:
                        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                        sharingIntent.setType("text/plain");
                        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "e-Sikhya");
                        String text = getResources().getString(R.string.share_app_text_en);
                        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, mShareText);
                        startActivity(Intent.createChooser(sharingIntent, "Share app via"));

                       /* String shareBody=shareText;
                        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Lalaji Grocer");
                        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                        startActivity(Intent.createChooser(sharingIntent, "Share via"));*/
                        break;
                }
            }
        });
    }

    private void dashBoardData() {
        Calendar c= Calendar.getInstance();
        DashboardModel dashboardModel = new DashboardModel();
        dashboardModel.setMonth(c.get(Calendar.YEAR)+"");
        dashboardModel.setYear(c.get(Calendar.MONTH)+1+"");
        dashboardModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        dashboardModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = apiService.dashboardSelect(dashboardModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.e(mTAG, "userModelList" + response.body().toString());
                try {
                    dashboardModelList = new ArrayList<>();

                    if (response.body() != null) {
                        Type type = new TypeToken<List<DashboardModel>>() {
                        }.getType();
                        dashboardModelList = new Gson().fromJson(response.body().getAsJsonArray("Response"), type);

                        float feePaid = 1f;
                        float pending = 1f;
                        float totalFee = 1f;

                        for (DashboardModel dashboardModel1 : dashboardModelList) {
                            totalFee = Float.parseFloat(dashboardModel1.getPaid()) + Float.parseFloat(dashboardModel1.getUnpaid());
                            feePaid = Float.parseFloat(dashboardModel1.getPaid());
                            pending = Float.parseFloat(dashboardModel1.getUnpaid());

                            txtTotalFee.setText(mContext.getString(R.string.Rs) + "" + totalFee);
                            txtTotalFeePaid.setText(mContext.getString(R.string.Rs) + "" + feePaid);
                            txtTotalPending.setText(mContext.getString(R.string.Rs) + "" + pending);

                            txtTotalTeacher.setText(dashboardModel1.getTotalTeacher());
                            txtTeacherPresent.setText(dashboardModel1.getTeacherPresent());
                            txtTeacherAbsent.setText(dashboardModel1.getTeacherAbsent());
                            txtTeacherLeave.setText(dashboardModel1.getTeacherLeave());

                            txtTotalStudent.setText(dashboardModel1.getTotalStudent());
                            txtStudentPresent.setText(dashboardModel1.getStudentPresent());
                            txtStudentAbsent.setText(dashboardModel1.getStudentAbsent());
                            txtStudentLeave.setText(dashboardModel1.getStudentLeave());
                        }

                        DonutSection sectionTotal = new DonutSection("Total", Color.parseColor("#00476a"), feePaid);
                        DonutSection sectionPaid = new DonutSection("Paid", Color.parseColor("#1D8AE0"), feePaid);
                        DonutSection sectionUnpaid = new DonutSection("Unpaid", Color.parseColor("#f44336"), pending);

                        List<DonutSection> donutSectionList = new ArrayList<>();
                        donutSectionList.add(sectionTotal);
                        donutSectionList.add(sectionPaid);
                        donutSectionList.add(sectionUnpaid);

                        dpvChart.setCap(100f);
                        dpvChart.submitData(donutSectionList);
                    }
                } catch (Exception ex) {
                    Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /*private void barChart(){
        chart =root.findViewById(R.id.chart1);
        ///chart.setDrawBarShadow(false);
        //chart.setDrawValueAboveBar(true);
        //chart.getDescription().setEnabled(false);
        //chart.setPinchZoom(false);
       // chart.setDrawGridBackground(false);

        BarData data = new BarData(getDataSet());

        chart.setData(data);
        chart.animateXY(2000, 2000);
        chart.invalidate();
    }*/

    private ArrayList getDataSet() {
        ArrayList dataSets = null;

        ArrayList valueSet1 = new ArrayList();
        BarEntry v1e1 = new BarEntry(1, 90); // present
        valueSet1.add(v1e1);
        BarEntry v1e2 = new BarEntry(2, 8); // absent
        valueSet1.add(v1e2);
        BarEntry v1e3 = new BarEntry(3, 2); // leave
        valueSet1.add(v1e3);

        BarDataSet barDataSet1 = new BarDataSet(valueSet1, "Attendance");
        barDataSet1.setColor(Color.rgb(0, 155, 0));

        dataSets = new ArrayList();
        dataSets.add(barDataSet1);
        return dataSets;
    }

    String mShareText = "";

    private void getAppShareText() {
        ApiInterface apiService = ApiClient.getClient(mContext).create(ApiInterface.class);
        Call<JsonObject> call = apiService.getAppShareText();
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.e(mTAG, response.body().toString());
                //Toast.makeText()
               /* if (response.isSuccessful()) {
                    if (response.body() != null) {

                        String codeVersion = response.body().get(0).getMessage();
                        if(codeVersion.isEmpty() || codeVersion==null)
                        {
                            mShareText=getResources().getString(R.string.share_app_text_en);
                            mShareText=mShareText+ Uri.parse("http://play.google.com/store/apps/details?id=" + mContext.getPackageName());
                        }
                        else
                        {
                            mShareText=codeVersion+Uri.parse("http://play.google.com/store/apps/details?id=" + mContext.getPackageName());
                        }


                    } else {
                        mShareText=getResources().getString(R.string.share_app_text_en);
                        mShareText=mShareText+Uri.parse("http://play.google.com/store/apps/details?id=" + mContext.getPackageName());
                    }
                }*/
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(mTAG, "Returned empty response");//Toast.makeText(getContext(),"Nothing returned",Toast.LENGTH_LONG).show();

            }
        });

    }

}