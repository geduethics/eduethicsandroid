package com.myliveclassroom.eduethics.Fragments.SubFragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.myliveclassroom.eduethics.Activity.MainActivity;
import com.myliveclassroom.eduethics.Activity.OnlineFeedbackActivity;
import com.myliveclassroom.eduethics.Fragments.HomeClassroomFragment;
import com.myliveclassroom.eduethics.Models.FeeModelTutor;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.ExtraFunctions;
import com.myliveclassroom.eduethics.Utils.GlobalVariables;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.myliveclassroom.eduethics.classes.TinyDB;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;

public class AddFeeEntry extends Fragment {

    View root;

    Context mContext;
    TextView dateText;

    String id, name;

    int type;
    boolean isEdit = false;
    List<FeeModelTutor> data;
    Button btnSaveCreate;
    FeeModelTutor mFeeModelTutor;

    TinyDB tinyDB;
    String mCalDate="";
    Calendar myCalendar = Calendar.getInstance();

    public void isEditState(Context context) {
        this.data = data;
        this.isEdit = true;

    }

    public AddFeeEntry(Context context, FeeModelTutor feeModelTutor) {
        mFeeModelTutor=feeModelTutor;
        mContext=context;
        tinyDB=new TinyDB(mContext);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.fragment_add_entry, container, false);

        MainActivity.TO_WHICH_FRAG = LocalConstants.TO_FRAG.TO_TeachersFeeStudentView;
        dateText = root.findViewById(R.id.dateText);


        SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
        mCalDate=format.format(myCalendar.getTime());

        ((TextView) root.findViewById(R.id.nameStudent)).setText(name);

        if (type == -1) {
            root.findViewById(R.id.modeTypeRadioGroup).setVisibility(View.GONE);
            ((TextInputLayout) root.findViewById(R.id.temp)).setHint("Amount Due");
        }

        if (GlobalVariables.isStudent)
            ((TextInputLayout) root.findViewById(R.id.temp)).setHint("Amount Paid");

       /* if (isEdit) {
            if (data.get("mode").toString().equals("Cash Mode")) {
                ((RadioButton) root.findViewById(R.id.cash)).setChecked(true);
            } else {
                ((RadioButton) root.findViewById(R.id.otherOnline)).setChecked(true);
            }
            ((TextInputEditText) root.findViewById(R.id.amountRec)).setText(Integer.parseInt(data.get("amt").toString()) < 0 ? Integer.parseInt(data.get("amt").toString()) * -1 + "" : data.get("amt").toString());
            ((TextInputEditText) root.findViewById(R.id.description)).setText(data.get("dec").toString());
            d[0] = Integer.parseInt(data.get("date").toString().split("-")[0]);
            d[1] = Integer.parseInt(data.get("date").toString().split("-")[1]);
            d[2] = Integer.parseInt(data.get("date").toString().split("-")[2]);
        }*/

        btnSaveCreate=root.findViewById(R.id.btnSaveCreate);
        btnSaveCreate.setOnClickListener(v -> saveCreate());

        root.findViewById(R.id.date).setOnClickListener(v -> {

            DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                      int dayOfMonth) {
                    // TODO Auto-generated method stub
                    myCalendar.set(Calendar.YEAR, year);
                    myCalendar.set(Calendar.MONTH, monthOfYear);
                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    updateLabel();
                }

            };
            new DatePickerDialog(mContext, date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show();
        });

        dateText.setText(mCalDate);

        return root;
    }

    private void updateLabel() {
        String myFormat = "dd-MMM-yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        dateText.setText(sdf.format(myCalendar.getTime()));
    }

    private void saveCreate() {
        if (((TextInputEditText) root.findViewById(R.id.amountRec)).getText().toString().equals("")) {
            ((TextInputEditText) root.findViewById(R.id.amountRec)).setError("Required !");
        } else if (Integer.parseInt(((TextInputEditText) root.findViewById(R.id.amountRec)).getText().toString()) < 0) {
            ((TextInputEditText) root.findViewById(R.id.amountRec)).setError("Should be in positive !");
        } else {
            root.findViewById(R.id.progress).setVisibility(View.VISIBLE);
            String amount = ((TextInputEditText) root.findViewById(R.id.amountRec)).getText().toString();
            String dec = ((TextInputEditText) root.findViewById(R.id.description)).getText().toString();
            String mode = type == 1 ? ((RadioGroup) root.findViewById(R.id.modeTypeRadioGroup)).getCheckedRadioButtonId() == R.id.cash ? "Cash" : "Online" : "";


            String date =dateText.getText().toString();

            //After all validations
            //Save data
            mFeeModelTutor.setTeacherId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
            mFeeModelTutor.setPaidAmt(amount);
            mFeeModelTutor.setPaymentDate(date);
            mFeeModelTutor.setPaymentMode(mode);
            mFeeModelTutor.setPaymentRemarks(dec);
            mFeeModelTutor.setAmtHead("");


            root.findViewById(R.id.progress).setVisibility(View.VISIBLE);

            ApiInterface apiService =
                    ApiClient.getClient(mContext).create(ApiInterface.class);

            Call<JsonObject> call = apiService.TutorFee_SaveStudentWise(mFeeModelTutor);
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    root.findViewById(R.id.progress).setVisibility(View.GONE);
                    try {
                        if (response.body() != null) {
                            if (new Gson().fromJson(response.body().get("Status"), String.class).toString().equalsIgnoreCase("Success")) {
                                // finish();
                            } else {
                                Toast.makeText(mContext, LocalConstants.APP_MESSAGES.ERROR1, Toast.LENGTH_SHORT).show();
                            }
                            Log.e(mTAG, response.toString());
                        }

                    } catch (Exception ex) {
                        root.findViewById(R.id.progress).setVisibility(View.GONE);
                        Log.e(mTAG, "error =" + ex.getMessage());
                        Toast.makeText(mContext, LocalConstants.APP_MESSAGES.ERROR1, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    root.findViewById(R.id.progress).setVisibility(View.GONE);
                    Log.e(mTAG, "error =" + t.toString());
                    Toast.makeText(mContext, LocalConstants.APP_MESSAGES.ERROR1, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }


    public static void editStudentFee(String idT, View rootT, Activity activity) {

        final int[] advT = {0};
        final int[] recT = {0};

        FirebaseFirestore.getInstance()
                .collection("fees")
                .document(HomeClassroomFragment.classId)
                .collection("record")
                .document(idT)
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        List<Map<String, Object>> lst = (List) documentSnapshot.get("data");
                        for (Map<String, Object> objectMap : lst) {
                            if ((long) objectMap.get("approval") == 1) {
                                if (!objectMap.get("amt").toString().contains("-")) {
                                    recT[0] += Integer.parseInt(objectMap.get("amt").toString());
                                }
                                advT[0] += Integer.parseInt(objectMap.get("amt").toString());
                            }
                        }

                        FirebaseFirestore.getInstance()
                                .collection("fees")
                                .document(HomeClassroomFragment.classId)
                                .get()
                                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                    @Override
                                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                                        List<Map<String, Object>> dataLocal = (List) documentSnapshot.get("students");
                                        for (Map<String, Object> singleData : dataLocal) {
                                            if (singleData.get("id").toString().equals(idT)) {
                                                dataLocal.get(dataLocal.indexOf(singleData)).put("adv", (advT[0] - Integer.parseInt(singleData.get("totalFee").toString())) + "");
                                                dataLocal.get(dataLocal.indexOf(singleData)).put("rec", recT[0] + "");
                                                break;
                                            }
                                        }
                                        FirebaseFirestore.getInstance().collection("teacher")
                                                .document(documentSnapshot.getString("teacherId"))
                                                .get()
                                                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                                    @Override
                                                    public void onSuccess(DocumentSnapshot documentSnapshot) {

                                                    }
                                                });

                                        Map<String, Object> forUp = new HashMap<>();
                                        forUp.put("students", dataLocal);
                                        FirebaseFirestore.getInstance()
                                                .collection("fees")
                                                .document(HomeClassroomFragment.classId)
                                                .update(forUp).addOnSuccessListener(aVoid -> {


                                            rootT.findViewById(R.id.progress).setVisibility(View.GONE);
                                            activity.onBackPressed();
                                        }).addOnFailureListener(e -> rootT.findViewById(R.id.progress).setVisibility(View.GONE));
                                    }
                                });
                    }
                });
    }
}