package com.myliveclassroom.eduethics.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.myliveclassroom.eduethics.R;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Map;

public class HowToUseAdapter extends RecyclerView.Adapter<HowToUseAdapter.vh> {
    List<Map<String, String>> data;
    Fragment fragment;
    public HowToUseAdapter(List<Map<String, String>> data, Fragment fragment) {
        this.data = data;
        this.fragment = fragment;
    }

    @NonNull
    @NotNull
    @Override
    public vh onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new vh(LayoutInflater.from(parent.getContext()).inflate(R.layout.how_to_use_singleitem,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull HowToUseAdapter.vh holder, int position) {

        YouTubePlayerView youTubePlayerView = holder.itemView.findViewById(R.id.youtube_player_view);
        fragment.getLifecycle().addObserver(youTubePlayerView);
        youTubePlayerView.addYouTubePlayerListener(new AbstractYouTubePlayerListener() {
            @Override
            public void onReady(@NonNull YouTubePlayer youTubePlayer) {
                String videoId = data.get(position).get("url");
                youTubePlayer.loadVideo(videoId, 0);
                youTubePlayer.pause();
            }
        }
        );

        ((TextView)holder.itemView.findViewById(R.id.title)).setText(data.get(position).get("title"));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class vh extends RecyclerView.ViewHolder {

        public vh(@NonNull @NotNull View itemView) {
            super(itemView);
        }
    }

}
