package com.myliveclassroom.eduethics.Fragments.HomeFragemtns;

import android.content.Context;
import android.os.Bundle;
import android.transition.TransitionInflater;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.myliveclassroom.eduethics.Activity.MainActivity;
import com.myliveclassroom.eduethics.Adapter.NotieAdapter;
import com.myliveclassroom.eduethics.Fragments.TeacherFragments.CreateNoticeFragment;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.TinyDB;

import static com.myliveclassroom.eduethics.Utils.HomeConstants.PROFILE;


public class NoticeBoardFragment extends Fragment {


    Context context;
    TinyDB tinyDB;
    RecyclerView recyclerviewNotice;

    public  NoticeBoardFragment(Context context){
        this.context = context;
        tinyDB=new TinyDB(context);
    }

    @Override
    public void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TransitionInflater inflater = TransitionInflater.from(requireContext());
        setEnterTransition(inflater.inflateTransition(R.transition.slide_right));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_notice_board, container, false);

        MainActivity.frag = this;

        MainActivity.TO_WHICH_FRAG =  LocalConstants.TO_FRAG.TO_CLASSROOM ;

        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT))
        {
            ((TextView) root.findViewById(R.id.tit)).setText("Important announcements posted\nby the teacher can be \nfound here");
            root.findViewById(R.id.createNotice).setVisibility(View.GONE);
        }

        /*if (HomeClassroomFragment.model.getNotice().size()==0){
            root.findViewById(R.id.empty).setVisibility(View.VISIBLE);
            ((TextView)root.findViewById(R.id.emptyMessage)).setText("No Notice Found!");
        }*/

        recyclerviewNotice = root.findViewById(R.id.recyclerviewNotice);
        //((TextView)root.findViewById(R.id.countText)).setText(HomeClassroomFragment.model.getNotice().size()+" Announcements Posted");
        recyclerviewNotice.setLayoutManager(new LinearLayoutManager(context));
        recyclerviewNotice.setAdapter(new NotieAdapter(this));

        root.findViewById(R.id.createNotice).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(
                        R.anim.slide_in,  // enter
                        R.anim.fade_out,  // exit
                        R.anim.fade_in,   // popEnter
                        R.anim.slide_out  // popExit
                ).replace(R.id.fragmentFrame,new CreateNoticeFragment()).commit();
            }
        });
        return root;
    }


}