package com.myliveclassroom.eduethics.Activity;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.myliveclassroom.eduethics.Adapter.ExamAdapter;
import com.myliveclassroom.eduethics.Models.ExamModel;
import com.myliveclassroom.eduethics.Models.UserModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;


public class ExamSummaryActivity extends BaseActivity {

    @BindView(R.id.txtPageTitle)
    TextView txtPageTitle;

    @BindView(R.id.imgBack)
    ImageView imgBack;

    @BindView(R.id.imgbtnAdd)
    ImageView imgbtnAdd;

    @BindView(R.id.btnAdd)
    TextView btnAdd;

    @BindView(R.id.recyclerExam)
    RecyclerView recyclerExam;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    ExamAdapter examAdapter;
    List<ExamModel> examModelList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.MyThemeNoBar);
        setContentView(R.layout.activity_exam_summary);
        _BaseActivity(this);
        ButterKnife.bind(this);
        init();
    }

    private void Back() {
        super.onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    private void init() {
        progressBar.setVisibility(View.GONE);
        imgbtnAdd.setVisibility(View.INVISIBLE);
        imgBack.setVisibility(View.VISIBLE);
        btnAdd.setVisibility(View.GONE);
        btnAdd.setText("");

        txtPageTitle.setText("e-Sikhya Exams");
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Back();
            }
        });
        initRecyclerView();
    }

    private void initRecyclerView() {
        recyclerExam.setLayoutManager(new LinearLayoutManager(mContext));
        listExams();
        countDownStart();
    }

    private void listExams() {
        progressBar.setVisibility(View.VISIBLE);
        UserModel examModel = new UserModel();
        examModel.setUserId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
        examModel.setClassId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID));
        examModel.setUserType(tinyDB.getString("UserType"));
        examModelList = new ArrayList<>();

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<List<ExamModel>> call = apiService.ExamSummary(examModel);
        call.enqueue(new Callback<List<ExamModel>>() {
            @Override
            public void onResponse(Call<List<ExamModel>> call, Response<List<ExamModel>> response) {
                try {
                    progressBar.setVisibility(View.GONE);
                    if (response.body() != null) {
                        examModelList = response.body();
                        examAdapter = new ExamAdapter(mContext, examModelList);
                        recyclerExam.setAdapter(examAdapter);
                    }
                } catch (Exception ex) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<List<ExamModel>> call, Throwable t) {
                // Log error here since request failed
                progressBar.setVisibility(View.GONE);
                Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private Handler handler = new Handler();
    private Runnable runnable;

    private void countDownStart() {
        runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    handler.postDelayed(this, 10000);
                    listExams();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        handler.postDelayed(runnable, 0);
    }

    @Override
    protected void onStop() {
        super.onStop();
        handler.removeCallbacks(runnable);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runnable);
    }
    @Override
    protected void onResume() {
        super.onResume();
        listExams();
    }
}
