package com.myliveclassroom.eduethics.Activity;

import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.myliveclassroom.eduethics.Fragments.Exam.TestFragment;
import com.myliveclassroom.eduethics.Fragments.Exam.CreateTestFragment;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.myliveclassroom.eduethics.Utils.HomeConstants.PROFILE;

public class ExamSetupActivity extends BaseActivity {

    @BindView(R.id.txtPageTitle)
    TextView txtPageTitle;

    @BindView(R.id.imgBack)
    ImageView imgBack;

    @BindView(R.id.imgbtnAdd)
    ImageView imgbtnAdd;

    @BindView(R.id.btnAdd)
    TextView btnAdd;

    TextView txtBtnOral;
    String mRoomName = "EduEthicsLiveClassRoomExam";

    @BindView(R.id.layoutFragment)
    LinearLayout layoutFragment;


    Fragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.MyThemeNoBar);
        setContentView(R.layout.activity_exam_setup);
        _BaseActivity(this);
        ButterKnife.bind(this);
        init();
        showExamListFragment();
    }

    private void init() {
        imgBack.setVisibility(View.VISIBLE);
        imgbtnAdd.setVisibility(View.GONE);
        btnAdd.setVisibility(View.GONE);
        btnAdd.setText("Create Exam");
        txtPageTitle.setText("Exam Setup");
        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)){
            txtPageTitle.setText("Exam");
            imgbtnAdd.setVisibility(View.GONE);
            btnAdd.setVisibility(View.GONE);
        }
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showExamListFragment();
            }
        });

        imgbtnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showExamListFragment();
            }
        });


        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Back();
            }
        });
    }

    private void Back() {
        super.onBackPressed();
    }

    private void showExamListFragment(){
        layoutFragment.setVisibility(View.VISIBLE);
        getSupportFragmentManager().beginTransaction().setCustomAnimations(
                R.anim.slide_in,  // enter
                R.anim.fade_out,  // exit
                R.anim.fade_in,   // popEnter
                R.anim.slide_out  // popExit
        ).replace(R.id.fragmentFrame, new TestFragment(mContext)).commit();
    }

    private void showFragment(){
        layoutFragment.setVisibility(View.VISIBLE);
        getSupportFragmentManager().beginTransaction().setCustomAnimations(
                R.anim.slide_in,  // enter
                R.anim.fade_out,  // exit
                R.anim.fade_in,   // popEnter
                R.anim.slide_out  // popExit
        ).replace(R.id.fragmentFrame, new CreateTestFragment(mContext)).commit();
    }
}