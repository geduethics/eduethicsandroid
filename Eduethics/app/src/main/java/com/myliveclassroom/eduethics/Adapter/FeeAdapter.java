package com.myliveclassroom.eduethics.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;


import com.myliveclassroom.eduethics.Fragments.SubFragments.FeeDetailStudent;
import com.myliveclassroom.eduethics.Fragments.TeacherFragments.TeachersFeeStudentView;
import com.myliveclassroom.eduethics.Models.FeeModelTutor;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.TinyDB;

import java.util.List;

import static com.myliveclassroom.eduethics.Utils.HomeConstants.PROFILE;

public class FeeAdapter extends RecyclerView.Adapter<FeeAdapter.vh> {

    List<FeeModelTutor> data;
    Fragment fragment;
    TextView totalRec, totalDue, totalAdv;

    float tRec = 0, tAdv = 0, tDue = 0;
    Context mContext;
    TinyDB tinyDB;


    public FeeAdapter(Context context, List<FeeModelTutor> data, Fragment fragment, TextView totalRec, TextView totalDue, TextView totalAdv) {
        this.data = data;
        this.fragment = fragment;
        this.totalAdv = totalAdv;
        this.totalDue = totalDue;
        this.totalRec = totalRec;
        mContext = context;
        tinyDB = new TinyDB(mContext);
    }

    @NonNull
    @Override
    public vh onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new vh(LayoutInflater.from(parent.getContext()).inflate(R.layout.fee_details_single_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull vh holder, int position) {

        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER)) {
            if (fragment.getClass().equals(FeeDetailStudent.class)){
                studentView(holder, position);
            }else {
                teacherView(holder, position);
            }
        } else {
            studentView(holder, position);
        }


        //holder.adv.setTextColor(ResourcesCompat.getColor(fragment.getResources(), Float.parseFloat(data.get(position).getAdvance().toString()) < 0 ? android.R.color.holo_red_dark : android.R.color.holo_green_dark, null));
        //holder.adv.setText(fragment.getString(R.string.Rs) + (Float.parseFloat(data.get(position).getAdvance()) < 0 ? Float.parseFloat(data.get(position).getAdvance().toString()) * -1 : Float.parseFloat(data.get(position).getAdvance().toString())));

        //holder.name.setText(data.get(position).getStudentName() == null ? "" : data.get(position).getStudentName().toString());
        //holder.rec.setText(fragment.getString(R.string.Rs) + data.get(position).getFeeAmt().toString());

        //holder.rec.setTextColor(ResourcesCompat.getColor(fragment.getResources(), Float.parseFloat(data.get(position).getFeeAmt().toString()) < 0 ? android.R.color.holo_red_dark : android.R.color.holo_green_dark, null));

        /////////////////////////////////////////
        //////////////////////////////////////////
        ////////////////////////////////////////////
        //TODO
        //check the screens
        /*if (fragment.getClass().equals(FeeDetailStudent.class)) {
            holder.itemView.setOnClickListener(v -> ((FeeDetailStudent) fragment).click(position));
        }
        if (fragment.getClass().equals(TeachersFeeStudentView.class)) {
            holder.itemView.setOnClickListener(v -> ((TeachersFeeStudentView) fragment).addRec(data.get(position)));
        } else {
            Toast.makeText(mContext, "nthong", Toast.LENGTH_SHORT).show();
        }*/
        /////////////////////////////////////////
        //////////////////////////////////////////
        ////////////////////////////////////////////


        //  tRec += Float.parseFloat(data.get(position).getFeeAmt().toString());
        //  if (Float.parseFloat(data.get(position).getAdvance().toString()) <= 0) {
        //      tDue = Float.parseFloat(data.get(0).getMonthlyFee().toString());
        //  } else {
        //      tAdv += Float.parseFloat(data.get(position).getAdvance().toString());
        //  }
        //  if (position == data.size() - 1) {
        //      totalRec.setText(fragment.getString(R.string.Rs) + (tRec < 0 ? tRec * -1 : tRec));
        //      totalDue.setText(fragment.getString(R.string.Rs) + (tDue < 0 ? tDue * -1 : tDue));
        //      if (totalAdv != null) {
        //          totalAdv.setText(fragment.getString(R.string.Rs) + (tAdv < 0 ? tAdv * -1 : tAdv));
//
        //          totalAdv.setTextColor(ResourcesCompat.getColor(fragment.getResources(), tAdv < 0 ? android.R.color.holo_red_dark : android.R.color.holo_green_light, null));
        //      }
        //      totalRec.setTextColor(ResourcesCompat.getColor(fragment.getResources(), tRec < 0 ? android.R.color.holo_red_dark : android.R.color.holo_green_light, null));
        //      totalDue.setTextColor(ResourcesCompat.getColor(fragment.getResources(), tDue < 0 ? android.R.color.holo_red_dark : android.R.color.holo_green_light, null));
        //  }
    }


    private void teacherView(vh holder, int position) {
        tRec += Float.parseFloat(data.get(position).getPaidAmt().toString());
        tAdv += Float.parseFloat(data.get(position).getAdvance());
        holder.adv.setText(fragment.getString(R.string.Rs) + tAdv);
        holder.adv.setTextColor(ResourcesCompat.getColor(fragment.getResources(), tAdv <= 0 ? android.R.color.holo_red_dark : android.R.color.holo_green_dark, null));

        holder.name.setText(data.get(position).getStudentName() == null ? "" : data.get(position).getStudentName().toString());
        holder.rec.setText(fragment.getString(R.string.Rs) + data.get(position).getPaidAmt().toString());
        holder.rec.setTextColor(ResourcesCompat.getColor(fragment.getResources(), Float.parseFloat(data.get(position).getFeeAmt().toString()) < 0 ? android.R.color.holo_red_dark : android.R.color.holo_green_dark, null));
        totalRec.setText(fragment.getString(R.string.Rs) + (tRec < 0 ? tRec * -1 : tRec));

        holder.imgGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (fragment.getClass().equals(TeachersFeeStudentView.class)) {
                     ((TeachersFeeStudentView) fragment).viewStudentFeeDetail(data.get(position)) ;
                }
            }
        });
    }

    private void studentView(vh holder, int position) {
        holder.name.setText(data.get(position).getPaymentDate());
        holder.rec.setText(fragment.getString(R.string.Rs) + data.get(position).getPaidAmt().toString());
        holder.rec.setTextColor(ResourcesCompat.getColor(fragment.getResources(), Float.parseFloat(data.get(position).getFeeAmt().toString()) < 0 ? android.R.color.holo_red_dark : android.R.color.holo_green_dark, null));
        holder.imgGo.setVisibility(View.INVISIBLE);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    static class vh extends RecyclerView.ViewHolder {

        TextView adv, rec, name;
        ImageView imgGo;

        public vh(@NonNull View itemView) {
            super(itemView);
            this.adv = itemView.findViewById(R.id.adv);
            this.rec = itemView.findViewById(R.id.rec);
            this.name = itemView.findViewById(R.id.name);
            this.imgGo = itemView.findViewById(R.id.imgGo);
        }

    }

}
