package com.myliveclassroom.eduethics.Activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;



import com.myliveclassroom.eduethics.Fragments.LoginFragments.PhoneFragment;
import com.myliveclassroom.eduethics.R;


public class LoginActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.MyThemeNoBar);

        setContentView(R.layout.activity_login);

        setTheme(R.style.MyThemeNoBar);

        SharedPreferences prefs = getApplicationContext().getSharedPreferences("USER_PREF",
                Context.MODE_PRIVATE);


        /*if (!prefs.getString("USER_PREF", "none").equals("none")) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }*/

        getSupportFragmentManager().beginTransaction().replace(R.id.fragmentFrameLogin, new PhoneFragment(LoginActivity.this)).commit();
    }


    @Override
    protected void onStart() {
        super.onStart();
    }



}