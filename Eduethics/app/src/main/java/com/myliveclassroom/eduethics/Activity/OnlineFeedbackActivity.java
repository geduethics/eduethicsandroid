package com.myliveclassroom.eduethics.Activity;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.myliveclassroom.eduethics.Adapter.StudentFeedbackAdapter;
import com.myliveclassroom.eduethics.Models.ApiResponseModel;
import com.myliveclassroom.eduethics.Models.TimeTableModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.myliveclassroom.eduethics.classes.CommonFunctions;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;

public class OnlineFeedbackActivity extends BaseActivity {

    @BindView(R.id.txtPageTitle)
    TextView txtPageTitle;

    @BindView(R.id.imgBack)
    ImageView imgBack;

    @BindView(R.id.imgbtnAdd)
    ImageView imgbtnAdd;

    @BindView(R.id.btnAdd)
    TextView btnAdd;

    @BindView(R.id.txtClass)
    TextView txtClass;

    @BindView(R.id.txtDate)
    TextView txtDate;

    @BindView(R.id.txtBtnMarkAttendance)
    TextView txtBtnMarkAttendance;

    public List<TimeTableModel> timeTableModelList;

    private String mClassId;
    private String mSelectedDate;

    StudentFeedbackAdapter attendanceAdapter;

    ProgressBar progressBar;
    RecyclerView recyclerViewAttendance;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.MyThemeNoBar);
        setContentView(R.layout.activity_online_feedback);

        ButterKnife.bind(this);
        _BaseActivity(this);
        init();
    }

    private void init() {
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        mClassId = tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID);
        mSelectedDate = tinyDB.getString("Date");
        txtClass.setText(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_NAME) );
        txtDate.setText(tinyDB.getString("Date"));
        tinyDB.putString("AttDate", tinyDB.getString("Date"));


        imgbtnAdd.setVisibility(View.GONE);
        imgBack.setVisibility(View.VISIBLE);
        btnAdd.setVisibility(View.GONE);
        btnAdd.setText("");
        imgbtnAdd.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_tick3));
        txtPageTitle.setText("Class Feedback");
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Back();
            }
        });

        imgbtnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(mContext, "test from frag", Toast.LENGTH_SHORT).show();
                Log.e(mTAG, "" + timeTableModelList.size());
            }
        });
        initRecyclerView();
    }

    //@OnClick(R.id.imgBack)
    private void Back() {
        super.onBackPressed();
    }

    private void initRecyclerView() {
        timeTableModelList = new ArrayList<>();
        recyclerViewAttendance = findViewById(R.id.recyclerViewAttendance);
        recyclerViewAttendance.setLayoutManager(new LinearLayoutManager(mContext));
        attendanceAdapter = new StudentFeedbackAdapter(mContext, timeTableModelList, 0);
        recyclerViewAttendance.setAdapter(attendanceAdapter);
        getStudentList();
    }

    private void getStudentList() {
        progressBar.setVisibility(View.VISIBLE);
        TimeTableModel listModel = new TimeTableModel();

        listModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        listModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));
        listModel.setClassId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID));
        listModel.setUserId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
        listModel.setSubjectId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SUBJECT_ID));

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        //Call<List<TimeTableModel>> call = apiService.getClassStudent(listModel);
        Call<JsonObject> call = apiService.StudentListSchool(listModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                progressBar.setVisibility(View.GONE);
                try {
                    Type type = new TypeToken<List<TimeTableModel>>() {
                    }.getType();

                    timeTableModelList = new Gson().fromJson(response.body().getAsJsonArray("Response"), type);

                    attendanceAdapter = new StudentFeedbackAdapter(mContext, timeTableModelList, 0);
                    recyclerViewAttendance.setAdapter(attendanceAdapter);

                } catch (Exception ex) {
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(mTAG, "error =" + t.toString());
            }
        });
    }

    @OnClick(R.id.txtBtnMarkAttendance)
    public void txtBtnMarkAttendance_click(){
        submit_feedback();
    }

    private void submit_feedback(){
        progressBar.setVisibility(View.VISIBLE);

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = apiService.classFeedbackSubmit(timeTableModelList);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                progressBar.setVisibility(View.GONE);
                try {
                    if(response.body()!=null) {
                        if(new Gson().fromJson(response.body().get("Status"),String.class).toString().equalsIgnoreCase("Success")){
                            finish();
                        } else{
                            Toast.makeText(OnlineFeedbackActivity.this, LocalConstants.APP_MESSAGES.ERROR1, Toast.LENGTH_SHORT).show();
                        }
                        Log.e(mTAG, response.toString());
                    }

                } catch (Exception ex) {
                    Log.e(mTAG, "error =" + ex.getMessage());
                    Toast.makeText(OnlineFeedbackActivity.this, LocalConstants.APP_MESSAGES.ERROR1, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(OnlineFeedbackActivity.this, LocalConstants.APP_MESSAGES.ERROR1, Toast.LENGTH_SHORT).show();
            }
        });
    }
}