package com.myliveclassroom.eduethics.Fragments.HomeFragemtns;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.transition.TransitionInflater;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.myliveclassroom.eduethics.Activity.MainActivity;
import com.myliveclassroom.eduethics.Adapter.StudentsAdapter;
import com.myliveclassroom.eduethics.Adapter.StudentsAdapter_old;
import com.myliveclassroom.eduethics.Fragments.HomeClassroomFragment;
import com.myliveclassroom.eduethics.Models.TimeTableModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.AppShareMessage;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.TinyDB;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;


import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.PROFILE;
import static org.webrtc.ContextUtils.getApplicationContext;


public class StudentFragment extends Fragment {

    StudentsAdapter_old reqAdapter;
    StudentsAdapter_old currentAdapter;

    StudentsAdapter studentsAdapterPending;
    StudentsAdapter studentsAdapterEnrolled;

    View progress;
    TinyDB tinyDB;
    Context mContext;
    List<TimeTableModel> studentModelList,studentModelListPending,studentModelListEnrolled;

    View root;

    RecyclerView recyclerViewPendingStudents, recyclerViewEnrolledStudents;

    public StudentFragment(Context context) {
        mContext = context;
        tinyDB = new TinyDB(mContext);
    }

    @Override
    public void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TransitionInflater inflater = TransitionInflater.from(requireContext());
        setEnterTransition(inflater.inflateTransition(R.transition.slide_right));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_student, container, false);

        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)) {
            ((TextView) root.findViewById(R.id.tit)).setText("View all the details of \nstudents in the classroom here");
        }
        initRecycler();
        //runFirst();
        getStudents();
        return root;
    }

    private void initRecycler() {
        //Enrolled students recycler View
        studentModelList=new ArrayList<>();
        recyclerViewEnrolledStudents = root.findViewById(R.id.recyclerViewStudents);
        recyclerViewPendingStudents=root.findViewById(R.id.recyclerReqStudents);
        studentsAdapterEnrolled=new StudentsAdapter(StudentFragment.this,mContext,studentModelList,1);
        recyclerViewEnrolledStudents.setAdapter(studentsAdapterEnrolled);

        //Pending enroll students recycler View
        recyclerViewEnrolledStudents.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewPendingStudents.setLayoutManager(new LinearLayoutManager(getContext()));
        studentsAdapterPending=new StudentsAdapter(StudentFragment.this,mContext,studentModelList,0);
        recyclerViewPendingStudents.setAdapter(studentsAdapterPending);
    }

    private void getStudents() {
        TimeTableModel timeTableModel = new TimeTableModel();

        //timeTableModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        //timeTableModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));
        //timeTableModel.setTeacherId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
        //timeTableModel.setSubjectId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SUBJECT_ID));
        //timeTableModel.setSectionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SECTION_ID));
        timeTableModel.setClassId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID));

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = apiService.listStudentsToTutor(timeTableModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                try {
                    if (response.body() == null) {
                        Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    Type type = new TypeToken<List<TimeTableModel>>() {
                    }.getType();

                    studentModelList = new Gson().fromJson(response.body().getAsJsonArray("Response"), type);

                    studentModelListPending=new ArrayList<>();
                    studentModelListEnrolled=new ArrayList<>();
                    for (TimeTableModel studentModel :studentModelList ){
                        if(studentModel.getIsStudentActive().equalsIgnoreCase("N")  ){
                            studentModelListPending.add(studentModel);
                        }else{
                            studentModelListEnrolled.add(studentModel);
                        }
                    }
                    studentsAdapterEnrolled.updateList(studentModelListEnrolled);
                    studentsAdapterPending.updateList(studentModelListPending);
                    runFirst();
                } catch (Exception ex) {
                    Toast.makeText(mContext, LocalConstants.APP_MESSAGES.ERROR1, Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, LocalConstants.APP_MESSAGES.ERROR1, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void runFirst() {

        //reqAdapter = new StudentsAdapter(this, HomeClassroomFragment.model.getPendingStudents(), 2);
        //currentAdapter = new StudentsAdapter(this, HomeClassroomFragment.model.getCurrentStudents(), 1);

        //RecyclerView recyclerViewStudents = root.findViewById(R.id.recyclerViewStudents);
        //RecyclerView recyclerReqStudents = root.findViewById(R.id.recyclerReqStudents);

        progress = root.findViewById(R.id.progress);
        progress.setVisibility(View.GONE);
        root.findViewById(R.id.inviteStudent).setOnClickListener(v -> {
            Intent intent2 = new Intent();
            intent2.setAction(Intent.ACTION_SEND);
            intent2.setType("text/plain");
            intent2.putExtra(Intent.EXTRA_TEXT,  AppShareMessage.StudentInvite(mContext));;
            startActivity(Intent.createChooser(intent2, "Share via"));
        });

        TextView totalEnrolledStudents = root.findViewById(R.id.countEnrolledStudent);
        TextView totalReqStudents = root.findViewById(R.id.countReqStudent);

        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)) {
            recyclerViewPendingStudents.setVisibility(View.GONE);
            root.findViewById(R.id.temp2).setVisibility(View.GONE);
            totalReqStudents.setVisibility(View.GONE);
        } else {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                long count = studentModelList.stream().filter(animal -> "N".equals(animal.getIsStudentActive())).count();
                totalReqStudents.setText("(" + count + " )");
            }
        }

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            long count = studentModelList.stream().filter(animal -> "Y".equals(animal.getIsStudentActive())).count();
            totalEnrolledStudents.setText("(" + count + " )");
        }
        //totalEnrolledStudents.setText("(" + HomeClassroomFragment.model.getCurrentStudents().size() + ")");

        //recyclerViewStudents.setLayoutManager(new LinearLayoutManager(getContext()));
        //recyclerViewStudents.setAdapter(currentAdapter);

        MainActivity.TO_WHICH_FRAG = LocalConstants.TO_FRAG.TO_CLASSROOM;
    }

    public void action(TimeTableModel timeTableModel,int actionType){
        //actionType=1=APPROVE
        //actionTyope=-1=Delete

        if(actionType==1){
            timeTableModel.setActionType("A");
        }else{
            timeTableModel.setActionType("D");
        }

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = apiService.tutorStudentApproveReject(timeTableModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                try {
                    if (response.body() == null) {
                        Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    getStudents();

                } catch (Exception ex) {
                    Toast.makeText(mContext, LocalConstants.APP_MESSAGES.ERROR1, Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, LocalConstants.APP_MESSAGES.ERROR1, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void action_old(int position, int actionType) {
       /* progress.setVisibility(View.VISIBLE);
       String id ="";// HomeClassroomFragment.model.getPendingStudents().get(position).get("id").toString();

        List<Map<String, Object>> list = HomeClassroomFragment.model.getPendingStudents();
        List<Map<String, Object>> newListPending = new ArrayList<>();
        List<Map<String, Object>> newListCurrent = HomeClassroomFragment.model.getCurrentStudents();

        for (Map<String, Object> stringObjectMap : list) {
            if (!stringObjectMap.get("id").toString().equals(id)) {
                newListPending.add(stringObjectMap);
            } else {
                newListCurrent.add(stringObjectMap);
            }
        }

        Map<String, Object> map = new HashMap<>();
        map.put("pendingStudents", newListPending);
        if (actionType == 1) {
            ExtraFunctions.sendNoti(id, "Request Accpeted", HomeClassroomFragment.model.getClassName() + " Classroom is up now.", getContext());
            map.put("currentStudents", newListCurrent);
        }

        FirebaseFirestore.getInstance()
                .collection("classroom")
                .document(HomeClassroomFragment.classId)
                .update(map)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        FirebaseFirestore.getInstance()
                                .collection("classroom")
                                .document(HomeClassroomFragment.classId)
                                .get()
                                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                    @Override
                                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                                        FirebaseFirestore.getInstance()
                                                .collection("students")
                                                .document(id)
                                                .get()
                                                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                                    @Override
                                                    public void onSuccess(DocumentSnapshot documentSnapshot) {

                                                        List<String> lst = (List) documentSnapshot.get("classes");
                                                        List<String> finalLst = new ArrayList<>();
                                                        for (String s : lst) {
                                                            if (s.endsWith("_p")) {
                                                                if (s.equals(HomeClassroomFragment.classId + "_p")) {
                                                                    finalLst.add(s.split("_")[0]);
                                                                } else {
                                                                    finalLst.add(s);
                                                                }
                                                            } else {
                                                                finalLst.add(s);
                                                            }
                                                        }

                                                        FirebaseFirestore.getInstance()
                                                                .collection("students")
                                                                .document(id)
                                                                .update("classes", finalLst)
                                                                .addOnSuccessListener(aVoid1 -> {
                                                                    HomeClassroomFragment.model = documentSnapshot.toObject(HomeClassroomWholeModel.class);
                                                                    if (getActivity() == null)
                                                                        return;

                                                                    FirebaseFirestore.getInstance()
                                                                            .collection("classroom")
                                                                            .document(HomeClassroomFragment.classId)
                                                                            .get()
                                                                            .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                                                                @Override
                                                                                public void onSuccess(DocumentSnapshot documentSnapshot) {
                                                                                    HomeClassroomFragment.model = documentSnapshot.toObject(HomeClassroomWholeModel.class);
                                                                                    runFirst();
                                                                                }
                                                                            });

                                                                }).addOnFailureListener(e -> progress.setVisibility(View.GONE));
                                                    }
                                                }).addOnFailureListener(e -> progress.setVisibility(View.GONE));
                                    }
                                }).addOnFailureListener(e -> progress.setVisibility(View.GONE));
                    }
                }).addOnFailureListener(e -> progress.setVisibility(View.GONE));

        if (actionType == 1) {
            FirebaseFirestore.getInstance()
                    .collection("fees")
                    .document(HomeClassroomFragment.classId)
                    .get()
                    .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            List<Map<String, Object>> students = (List) documentSnapshot.get("students");
                            if (students == null) return;
                            Map<String, Object> singleData = new HashMap<>();
                            singleData.put("adv", (Integer.parseInt(documentSnapshot.getString("feePerM")) * -1) + "");
                            singleData.put("rec", "0");
                            singleData.put("id", id);
                            singleData.put("name", HomeClassroomFragment.model.getPendingStudents().get(position).get("name").toString());
                            singleData.put("totalFee", documentSnapshot.getString("feePerM"));
                            students.add(singleData);
                            Map<String, Object> map = new HashMap<>();
                            map.put("students", students);
                            FirebaseFirestore.getInstance()
                                    .collection("fees")
                                    .document(HomeClassroomFragment.classId)
                                    .update(map);

                            addRecord((Integer.parseInt(documentSnapshot.getString("feePerM")) * -1) + "", id);
                        }
                    });
        }*/

    }

    private void addRecord(String amt, String id) {
        Map<String, Object> map2 = new HashMap<>();
        List<Map<String, Object>> singleData2 = new ArrayList<>();
        Map<String, Object> studentData = new HashMap<>();
        studentData.put("amt", amt);
        studentData.put("approval", 3);
        studentData.put("dec", "System Added");
        studentData.put("from", "sys");
        studentData.put("mode", "System");
        studentData.put("date", Calendar.getInstance().get(Calendar.DAY_OF_MONTH) + "-" + Calendar.getInstance().get(Calendar.MONTH) + "-" + Calendar.getInstance().get(Calendar.YEAR));
        singleData2.add(studentData);
        map2.put("data", singleData2);
        FirebaseFirestore.getInstance()
                .collection("fees")
                .document(HomeClassroomFragment.classId)
                .collection("record")
                .document(id)
                .set(map2);
    }

    public void deleteStudent(int position) {
       /* Dialog dialog;
        dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.confirmation_dialog);

        ((TextView) dialog.findViewById(R.id.confirmationTitle)).setText("Remove !");
        ((TextView) dialog.findViewById(R.id.confirmationText)).setText("Want to remove this Student from your classroom " + HomeClassroomFragment.model.getClassName() + "?");
        ((TextView) dialog.findViewById(R.id.confirmationBtn)).setTextColor(ResourcesCompat.getColor(getResources(), android.R.color.holo_red_dark, null));
        ((TextView) dialog.findViewById(R.id.confirmationBtn)).setText("Remove");

        dialog.findViewById(R.id.confirmationBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progress.setVisibility(View.VISIBLE);
                List<Map<String, Object>> list = HomeClassroomFragment.model.getCurrentStudents();
                String id = HomeClassroomFragment.model.getCurrentStudents().get(position).get("id").toString();
                List<Map<String, Object>> newCurrentList = new ArrayList<>();
                for (Map<String, Object> stringObjectMap : list) {
                    if (!stringObjectMap.get("id").toString().equals(id)) {
                        newCurrentList.add(stringObjectMap);
                    }
                }

                Map<String, Object> map = new HashMap<>();
                map.put("currentStudents", newCurrentList);

                FirebaseFirestore.getInstance()
                        .collection("classroom")
                        .document(HomeClassroomFragment.classId)
                        .get()
                        .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                List<Map<String, Object>> data = (List) documentSnapshot.get("assignments");
                                List<Map<String, Object>> forUpload = new ArrayList<>();
                                for (Map<String, Object> datum : data) {
                                    if (((List<String>) datum.get("submitted")).contains(id)) {
                                        ((List<String>) datum.get("submitted")).remove(id);
                                    }
                                    forUpload.add(datum);
                                }
                                List<Map<String, Object>> data2 = (List) documentSnapshot.get("test");
                                List<Map<String, Object>> forUpload2 = new ArrayList<>();
                                for (Map<String, Object> datum : data2) {
                                    if (((List<String>) datum.get("submitted")).contains(id)) {
                                        ((List<String>) datum.get("submitted")).remove(id);
                                    }
                                    forUpload2.add(datum);
                                }

                                map.put("assignments", forUpload);
                                map.put("test", forUpload2);

                                FirebaseFirestore.getInstance()
                                        .collection("classroom")
                                        .document(HomeClassroomFragment.classId)
                                        .update(map)
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {

                                                FirebaseFirestore.getInstance()
                                                        .collection("classroom")
                                                        .document(HomeClassroomFragment.classId)
                                                        .collection("assignmentSub")
                                                        .get()
                                                        .addOnSuccessListener(queryDocumentSnapshots -> {
                                                            if (queryDocumentSnapshots != null)
                                                                if (queryDocumentSnapshots.getDocuments() != null)
                                                                    for (DocumentSnapshot document : queryDocumentSnapshots.getDocuments()) {
                                                                        FirebaseFirestore.getInstance()
                                                                                .collection("classroom")
                                                                                .document(HomeClassroomFragment.classId)
                                                                                .collection("assignmentSub")
                                                                                .document(document.getId())
                                                                                .update(id, FieldValue.delete());

                                                                    }

                                                        }).addOnFailureListener(new OnFailureListener() {
                                                    @Override
                                                    public void onFailure(@NonNull Exception e) {
                                                        if (getActivity() == null)
                                                            return;
                                                        progress.setVisibility(View.GONE);
                                                    }
                                                });

                                                FirebaseFirestore.getInstance()
                                                        .collection("classroom")
                                                        .document(HomeClassroomFragment.classId)
                                                        .collection("testSub")
                                                        .get()
                                                        .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                                                            @Override
                                                            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                                                                for (DocumentSnapshot document : queryDocumentSnapshots.getDocuments()) {
                                                                    FirebaseFirestore.getInstance()
                                                                            .collection("classroom")
                                                                            .document(HomeClassroomFragment.classId)
                                                                            .collection("testSub")
                                                                            .document(document.getId())
                                                                            .update(id, FieldValue.delete());

                                                                }
                                                            }
                                                        });
                                                FirebaseFirestore.getInstance().collection("students")
                                                        .document(id)
                                                        .get()
                                                        .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                                            @Override
                                                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                                                List<String> classes = (List) documentSnapshot.get("classes");
                                                                if (classes.contains(HomeClassroomFragment.classId)) {
                                                                    classes.remove(HomeClassroomFragment.classId);
                                                                }
                                                                Map<String, Object> map = new HashMap<>();
                                                                map.put("classes", classes);
                                                                FirebaseFirestore.getInstance().collection("students")
                                                                        .document(id)
                                                                        .update(map);
                                                            }
                                                        }).addOnFailureListener(new OnFailureListener() {
                                                    @Override
                                                    public void onFailure(@NonNull Exception e) {
                                                        if (getActivity() == null)
                                                            return;
                                                        progress.setVisibility(View.GONE);
                                                    }
                                                });

                                                progress.setVisibility(View.GONE);
                                            }
                                            //HERE _________________________-------------->>>>
                                        });


                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        if (getActivity() == null)
                            return;
                        progress.setVisibility(View.GONE);
                    }
                });
                getActivity().onBackPressed();
                dialog.dismiss();
            }
        });


        dialog.findViewById(R.id.cancel).setOnClickListener(v -> dialog.dismiss());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();*/
    }

    public void openFee(int position) {
        /*FirebaseFirestore.getInstance()
                .collection("fees")
                .document(HomeClassroomFragment.classId)
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        List<Map<String, Object>> data = (List) documentSnapshot.get("students");
                        boolean isOpen = false;
                        for (Map<String, Object> objectMap : data) {
                            if (objectMap.get("id").toString().equals(HomeClassroomFragment.model.getCurrentStudents().get(position).get("id").toString())) {
                                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragmentFrame, new TeachersFeeStudentView(objectMap)).commit();
                                isOpen = true;
                                break;
                            }
                        }
                        if (!isOpen)
                            Toast.makeText(getContext(), "No Record Found", Toast.LENGTH_SHORT).show();
                    }
                });

*/
    }

    public void callPhone(String mMobile) {
        /*if(isPermissionGranted) {
            Intent dialIntent = new Intent(Intent.ACTION_CALL);
            dialIntent.setData(Uri.parse("tel:" + Uri.encode(mMobile)));
            dialIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(dialIntent);
        }else{
            checkMultiplePermission();
        }*/
    }

    private boolean isPermissionGranted = false;

    private Boolean checkMultiplePermission() {
        /*isPermissionGranted = false;
        Dexter.withContext(mContext)
                .withPermissions(
                        Manifest.permission.READ_CONTACTS,
                        Manifest.permission.SEND_SMS,
                        Manifest.permission.READ_SMS,
                        Manifest.permission.READ_PHONE_STATE

                ).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                if (report.areAllPermissionsGranted()) {
                    // do you work now
                    isPermissionGranted = true;
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                token.continuePermissionRequest();
                isPermissionGranted = false;
            }
        }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(mContext, "Error occurred! ", Toast.LENGTH_SHORT).show();
                    }
                }).check();*/
        return isPermissionGranted;
    }
}