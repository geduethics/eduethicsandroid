package com.myliveclassroom.eduethics.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.telephony.SmsManager;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.TinyDB;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.API_BASE_URL_REPORTS;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.REPORTS;

public class ReportActivity extends BaseActivity {

    @BindView(R.id.txtPageTitle)
    TextView txtPageTitle;

    @BindView(R.id.imgBack)
    ImageView imgBack;

    @BindView(R.id.imgbtnAdd)
    ImageView imgbtnAdd;

    @BindView(R.id.btnAdd)
    TextView btnAdd;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;


    WebView mWebView;

    String mBaseUrl = API_BASE_URL_REPORTS;
    String mReportUrl = API_BASE_URL_REPORTS;

    TinyDB tinyDB;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.MyThemeNoBar);
        setContentView(R.layout.activity_report);
        ButterKnife.bind(this);
        tinyDB = new TinyDB(this);
        init();
        initWebView();
    }

    private void init() {

        imgbtnAdd.setVisibility(View.INVISIBLE);
        imgBack.setVisibility(View.VISIBLE);
        btnAdd.setVisibility(View.GONE);
        btnAdd.setText("");
        txtPageTitle.setText(REPORTS);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Back();
            }
        });

        setReportUrl();
    }

    private void setReportUrl() {
        if (tinyDB.getString(LocalConstants.URL.REPORT_TYPE).equalsIgnoreCase(LocalConstants.URL.REPORT_EXAM)) {

        } else if (tinyDB.getString(LocalConstants.URL.REPORT_TYPE).equalsIgnoreCase(LocalConstants.URL.REPORT_ATTENDANCE_TEACHER)) {
            mReportUrl = LocalConstants.URL.URL_REPORTS_Attendance_Teacher;
        } else if (tinyDB.getString(LocalConstants.URL.REPORT_TYPE).equalsIgnoreCase(LocalConstants.URL.REPORT_ATTENDANCE_STUDENT)) {
            mReportUrl = LocalConstants.URL.URL_REPORTS_Attendance_Student+"?s="+tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID)+"&cid="+tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID);
        }
        else if (tinyDB.getString(LocalConstants.URL.REPORT_TYPE).equalsIgnoreCase(LocalConstants.URL.REPORT_FEE)) {

        }
    }


    private void initWebView() {
        progressBar.setVisibility(View.VISIBLE);
        mWebView = (WebView) findViewById(R.id.webviewMain);
        mWebView.getSettings().setJavaScriptEnabled(true);

        mWebView.setHorizontalScrollBarEnabled(false);

        WebSettings webSettings = mWebView.getSettings();
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setAllowFileAccess(true);
        mWebView.getSettings().setAppCacheEnabled(false);
        mWebView.clearCache(true);
        //webView.setInitialScale(1);
        mWebView.addJavascriptInterface(new JavaScriptInterface (this), "Android");
        //webView.getSettings().setUseWideViewPort(true);
        mWebView.setWebViewClient(new Client());

        //Passing Class value
        // mUrl = mUrl + "?classid=" + tinyDB.getString("ClassId") + "&UserType=" + tinyDB.getString("UserType") + "&FileType=" + mFileType;
        mWebView.loadUrl(mReportUrl);
    }

    Boolean isBackPressed = false;

    private void Back() {
        // super.onBackPressed();
        if (mWebView.canGoBack()) {
            mWebView.goBack();
        } else {
            finish();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    if (mWebView.canGoBack()) {
                        mWebView.goBack();
                    } else {
                        finish();
                    }
                    return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }

    public class Client extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            // super.onPageStarted(view, url, favicon);
            progressBar.setVisibility(View.VISIBLE);
            super.onPageStarted(view, url, favicon);

        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.startsWith("tel:")) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                startActivity(intent);
                return true;
            }
            if (!url.toLowerCase().contains(mBaseUrl.toLowerCase())) {
                return true;
            } else {
                view.loadUrl(url);
                return true;
            }
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            //swipe.setRefreshing(false);
            progressBar.setVisibility(View.GONE);
        }
    }

    public class JavaScriptInterface {
        Context mContext;

        /**
         * Instantiate the interface and set the context
         */
        JavaScriptInterface(Context c) {
            mContext = c;
        }

        /**
         * Show a toast from the web page
         */
        @JavascriptInterface
        public void showToast(String toast) {
            Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
        }

        @JavascriptInterface
        public void SavePreferences(String key, String value) {
            tinyDB.putString(key, value);
        }


        @JavascriptInterface
        public String LoadPreferences(String key) {
            return tinyDB.getString(key);
        }

        @JavascriptInterface
        public void vibrate(long milliseconds) {
            Vibrator v = (Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(milliseconds);
        }

        @JavascriptInterface
        public void SendSMS(String phoneNumber, String message) {
            SmsManager sms = SmsManager.getDefault();
            sms.sendTextMessage(phoneNumber, null, message, null, null);
        }
    }

}