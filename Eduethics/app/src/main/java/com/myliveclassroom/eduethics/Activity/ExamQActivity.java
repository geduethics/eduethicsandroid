package com.myliveclassroom.eduethics.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.myliveclassroom.eduethics.Adapter.QuestionAdapter;
import com.myliveclassroom.eduethics.Models.ApiResponseModel;
import com.myliveclassroom.eduethics.Models.QuestionModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.PROFILE;


public class ExamQActivity extends BaseActivity {

    @BindView(R.id.recyclerQuestions)
    RecyclerView recyclerQuestions;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.txtStudentName)
    TextView txtStudentName;

    @BindView(R.id.txtClass)
    TextView txtClass;

    @BindView(R.id.txtExamSubject)
    TextView txtExamSubject;

    @BindView(R.id.txtTotalTime)
    TextView txtTotalTime;

    @BindView(R.id.txtTotalMarks)
    TextView txtTotalMarks;

    @BindView(R.id.txtTotalQuestions)
    TextView txtTotalQuestions;

    @BindView(R.id.txtTotalQAttempt)
    TextView txtTotalQAttempt;

    @BindView(R.id.txtMarquee)
    TextView txtMarquee;

    @BindView(R.id.txtBtnSubmitExam)
    TextView txtBtnSubmitExam;

    @BindView(R.id.frameTimer)
    FrameLayout frameTiming;


    QuestionAdapter questionAdapter;
    List<QuestionModel> questionModelList;
    List<QuestionModel> questionList;
    List<String> qIds;

    CircleImageView imgStudentPhoto;
    LinearLayout layoutToolbarInner;

    String mEndDateStr = "";
    String mExamId = "";
    String mQuestionId = "";
    String mAnswer = "";

    TextView txtPageTitle1;
    TextView txtPageTitle;
    TextView txtTimer;
    TextView txtTimer1,txtTimer1Lbl;

    String studentName;
    String mExamClass;
    String mSubject;
    String mExamTime;
    String mExamMaxMarks;
    String mExamTotalQ;
    String mExamTotalQAtt;

    @BindView(R.id.btnAddQuestions)
    Button btnAddQuestions;

    @BindView(R.id.txtExamEndTime)
    TextView txtExamEndTime;

    @BindView(R.id.txtMarkPerCorrect)
    TextView txtMarkPerCorrect;

    @BindView(R.id.txtMarkPerInCorrect)
    TextView txtMarkPerInCorrect;

    Boolean isExamStarted=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.MyThemeNoBar);
        setContentView(R.layout.activity_exam_q);
        _BaseActivity(this);
        ButterKnife.bind(this);

        init();

        layoutToolbarInner = findViewById(R.id.layoutToolbarInner);
        //imgStudentPhoto = findViewById(R.id.imgStudentPhoto);
        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.CollapsingToolbarLayout1);
        AppBarLayout appbar = (AppBarLayout) findViewById(R.id.app_bar_layout);
        collapsingToolbarLayout.setTitle(tinyDB.getString("SchoolNameRegional"));

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);

        appbar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isVisible = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    //toolbar.setTitle("Student Name");
                    isVisible = true;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // imgStudentPhoto.setVisibility(View.INVISIBLE);
                            layoutToolbarInner.setVisibility(View.VISIBLE);
                            toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                        }
                    });
                } else if (isVisible) {
                    //toolbar.setTitle("");
                    isVisible = false;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //  imgStudentPhoto.setVisibility(View.VISIBLE);
                            layoutToolbarInner.setVisibility(View.INVISIBLE);
                            toolbar.setBackgroundColor(getResources().getColor(R.color.caldroid_transparent));
                        }
                    });

                }
            }
        });
    }

    private void init() {
        txtPageTitle = findViewById(R.id.txtPageTitle);
        txtPageTitle1 = findViewById(R.id.txtPageTitle1);

        txtTimer = findViewById(R.id.txtTimer);
        txtTimer1 = findViewById(R.id.txtTimer1);
        txtTimer1Lbl= findViewById(R.id.txtTimer1Lbl);

        mExamId = tinyDB.getString(LocalConstants.Exam_Constants.ExamId);
        mEndDateStr = tinyDB.getString(LocalConstants.Exam_Constants.EndDate);

        studentName = tinyDB.getString(LocalConstants.Exam_Constants.STUDENT_NAME);
        mExamClass = tinyDB.getString(LocalConstants.Exam_Constants.ExamClass);
        mSubject = tinyDB.getString(LocalConstants.Exam_Constants.ExamSubject);
        mExamTime = tinyDB.getString(LocalConstants.Exam_Constants.ExamTime);
        mExamMaxMarks = tinyDB.getString(LocalConstants.Exam_Constants.ExamMaxMarks);
        mExamTotalQ = tinyDB.getString(LocalConstants.Exam_Constants.ExamTotalQ);
        mExamTotalQAtt = tinyDB.getString(LocalConstants.Exam_Constants.ExamTotalQAtt);
        txtMarkPerCorrect.setText(": " + tinyDB.getString(LocalConstants.Exam_Constants.ExamPerCorrect));
        txtMarkPerInCorrect.setText(": " + tinyDB.getString(LocalConstants.Exam_Constants.ExamNegativePerIncorrect));
        txtExamEndTime.setText(": " + tinyDB.getString(LocalConstants.Exam_Constants.EndDate));


        txtStudentName.setText(": " + studentName);
        txtClass.setText(": " + mExamClass);
        txtExamSubject.setText(": " + mSubject);
        txtTotalTime.setText(": " + mExamTime);
        txtTotalMarks.setText(": " + mExamMaxMarks);
        txtTotalQuestions.setText(": " + mExamTotalQ);
        txtTotalQAttempt.setText(": " + mExamTotalQAtt);

        txtPageTitle.setText("Exam");
        txtPageTitle1.setText("Exam");

        txtTimer.setText("00:00:00");
        txtTimer1.setText("00:00:00");


        progressBar.setVisibility(View.GONE);
        initRecyclerView();
        txtMarquee.setText("Exam timing : " + mExamTime + ", You will get alert message after time is over and you will be asked to submit your exam or you can submit your exam by pressing 'Submit' button");
        txtMarquee.setSelected(true);

        txtBtnSubmitExam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitExam();
            }
        });
        txtBtnSubmitExam.setVisibility(View.VISIBLE);
        btnAddQuestions.setText("Start Exam");
        btnAddQuestions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counterExamEnd();
                countDownStart();
                isExamStarted=true;
                tinyDB.putBoolean(LocalConstants.Exam_Constants.Exam_IsStarted,isExamStarted);
                btnAddQuestions.setVisibility(View.GONE);
                Toast.makeText(mContext, "Exam started", Toast.LENGTH_SHORT).show();
            }
        });

        if (tinyDB.getBoolean(LocalConstants.Exam_Constants.Show_Ans_Keys)) {
            txtBtnSubmitExam.setVisibility(View.GONE);
            btnAddQuestions.setVisibility(View.GONE);
            frameTiming.setVisibility(View.GONE);
            txtTimer1Lbl.setVisibility(View.GONE);
            txtTimer1.setVisibility(View.GONE);
        }
    }

    private void initRecyclerView() {
        recyclerQuestions.setLayoutManager(new LinearLayoutManager(mContext));
        listQuestions();
    }

    private void listQuestions() { 
        progressBar.setVisibility(View.VISIBLE);
        QuestionModel questionModel = new QuestionModel();
        questionModel.setExamId(mExamId);
        questionModel.setCreatedBy(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
        questionModel.setProfileType(tinyDB.getString(PROFILE));
        if(tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)){
            questionModel.setStudentId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
        }
        //questionModel.setClassName(mExamClass);
        //questionModel.setSubjectName(mSubject);

        questionModel.setProfileType(tinyDB.getString(PROFILE));
        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<List<QuestionModel>> call = apiService.QuestionList(questionModel);
        call.enqueue(new Callback<List<QuestionModel>>() {
            @Override
            public void onResponse(Call<List<QuestionModel>> call, Response<List<QuestionModel>> response) {
                try {
                    progressBar.setVisibility(View.GONE);
                    if (response.body() != null) {
                        questionModelList = response.body();
                        qIds = new ArrayList<>();
                        for (int j = 0; j < questionModelList.size(); j++) {
                            if (!qIds.contains(questionModelList.get(j).getQuestionId())) {
                                qIds.add(questionModelList.get(j).getQuestionId());
                            }
                        }
                        questionAdapter = new QuestionAdapter(mContext, qIds, questionModelList);
                        recyclerQuestions.setAdapter(questionAdapter);
                    }
                } catch (Exception ex) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<List<QuestionModel>> call, Throwable t) {
                // Log error here since request failed
                progressBar.setVisibility(View.GONE);
                Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private String mExamEndTime = "2020-06-08 18:00:00";
    private String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private TextView tv_days, tv_hour, tv_minute, tv_second;
    private Handler handler = new Handler();
    private Runnable runnable;

    private void countDownStart() {
        runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    Calendar datenow = Calendar.getInstance();
                    String dateTodayStr = dateTimeformat.format(datenow.getTime());
                    dateNow = dateTimeformat.parse(dateTodayStr);
                    endDate = dateTimeformat.parse(mEndDateStr);

                    handler.postDelayed(this, 1000);
                    //SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
                    //Date event_date = dateFormat.parse(mExamEndTime);

                    //Date current_date = new Date();
                    if (!dateNow.after(endDate)) {
                        long diff = endDate.getTime() - dateNow.getTime();
                        long Days = diff / (24 * 60 * 60 * 1000);
                        long Hours = diff / (60 * 60 * 1000) % 24;
                        long Minutes = diff / (60 * 1000) % 60;
                        long Seconds = diff / 1000 % 60;
                        //
                        //tv_days.setText(String.format("%02d", Days));
                        txtTimer.setText(String.format("%02d", Hours) + ":" + String.format("%02d", Minutes) + ":" + String.format("%02d", Seconds));
                        txtTimer1.setText(String.format("%02d", Hours) + ":" + String.format("%02d", Minutes) + ":" + String.format("%02d", Seconds));
                    } else {
                        handler.removeCallbacks(runnable);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        handler.postDelayed(runnable, 0);
    }

    private Handler handlerExamEnd = new Handler();
    private Runnable runnableExamEnd;
    SimpleDateFormat dateTimeformat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
    Date endDate;
    Date dateNow;

    private void counterExamEnd() {
        runnableExamEnd = new Runnable() {
            @Override
            public void run() {
                try {
                    try {
                        Calendar datenow = Calendar.getInstance();
                        String dateTodayStr = dateTimeformat.format(datenow.getTime());
                        dateNow = dateTimeformat.parse(dateTodayStr);
                        endDate = dateTimeformat.parse(mEndDateStr);

                        if (endDate.before(dateNow)) {
                            Toast.makeText(mContext, "Exam ends", Toast.LENGTH_SHORT).show();
                            Log.e(mTAG, "Exam ends");
                            openExamEndAlert();
                        } else {
                            Log.e(mTAG, "Exam not end");
                            handlerExamEnd.postDelayed(this, 5000);
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        handlerExamEnd.postDelayed(runnableExamEnd, 0);
    }

    private void openExamEndAlert() {
        handlerExamEnd.removeCallbacks(runnableExamEnd);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Time over. Kindly click submit.");
        alertDialogBuilder.setPositiveButton("Submit",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        //Toast.makeText(mContext, "Submited", Toast.LENGTH_SHORT).show();
                        submitExam();
                        finish();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    private void alertBackPressed() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Exam in progress, Do you want to submit it now?");
        alertDialogBuilder.setPositiveButton("Submit",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        //Toast.makeText(mContext, "Submited", Toast.LENGTH_SHORT).show();
                        submitExam();
                        finish();
                    }
                });
        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    public void clickAnsSubmit(String qid, String answerId) {

        QuestionModel questionModel = new QuestionModel();
        questionModel.setQuestionId(qid);
        questionModel.setAnswerId(answerId);
        questionModel.setStudentId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
        questionModel.setExamId(mExamId);

        ApiInterface apiService = ApiClient.getClient(mContext).create(ApiInterface.class);
        Call<ApiResponseModel> call = apiService.ExamSubmitSingleAns(questionModel);
        call.enqueue(new Callback<ApiResponseModel>() {
            @Override
            public void onResponse(Call<ApiResponseModel> call, Response<ApiResponseModel> response) {
                //int statusCode = response.code();
                try {
                    Log.e("res", response.body().toString());

                    // myProgressDialog.hideProgress();
                } catch (Exception ex) {
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ApiResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
            }
        });
    }


    private void submitExam() {
        tinyDB.putBoolean(LocalConstants.Exam_Constants.Exam_IsStarted,false);
        QuestionModel questionModel = new QuestionModel();
        questionModel.setExamId(mExamId);
        questionModel.setStudentId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
        questionModel.setClassName(mExamClass);
        questionModel.setSubjectName(mSubject);

        ApiInterface apiService = ApiClient.getClient(mContext).create(ApiInterface.class);
        Call<ApiResponseModel> call = apiService.ExamFinalSubmit(questionModel);
        call.enqueue(new Callback<ApiResponseModel>() {
            @Override
            public void onResponse(Call<ApiResponseModel> call, Response<ApiResponseModel> response) {
                //int statusCode = response.code();
                try {
                    if (response.body() != null) {
                        Log.e("res", response.body().toString());
                        if (response.body().getMessage().equalsIgnoreCase(""))
                            openDialog("Exam Submited successfully");
                        else {
                            myToast(mContext, "Please submit again!", Toast.LENGTH_SHORT);
                        }
                    }
                    //finish();
                    // myProgressDialog.hideProgress();
                } catch (Exception ex) {
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ApiResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
            }
        });
    }

    public void openDialog(String title) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(title);
        alertDialogBuilder.setPositiveButton("Go to Result Screen",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        //Toast.makeText(mContext, "Submited", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(mContext, ResultActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });
        alertDialogBuilder.setNegativeButton("Go to Main Screen",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        //Toast.makeText(mContext, "Submited", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(mContext, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        handler.removeCallbacks(runnable);
        handlerExamEnd.removeCallbacks(runnableExamEnd);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runnable);
        handlerExamEnd.removeCallbacks(runnableExamEnd);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        /*if(tinyDB.getBoolean(LocalConstants.Exam_Constants.Exam_IsStarted)) {
            alertBackPressed();
            return;
        }*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(tinyDB.getBoolean(LocalConstants.Exam_Constants.Exam_IsStarted)) {
            countDownStart();
            counterExamEnd();
        }
    }
}
