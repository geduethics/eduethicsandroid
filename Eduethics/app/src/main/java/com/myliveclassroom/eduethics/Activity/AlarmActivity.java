package com.myliveclassroom.eduethics.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.myliveclassroom.eduethics.R;

public class AlarmActivity extends AppCompatActivity {

    ImageView imgCloseAlarm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);

        imgCloseAlarm=findViewById(R.id.imgCloseAlarm);
        imgCloseAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}