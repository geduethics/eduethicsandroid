package com.myliveclassroom.eduethics.Activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.myliveclassroom.eduethics.R;


public class ImageFullViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.MyThemeNoBar);
        setContentView(R.layout.activity_image_full_view);
    }
}
