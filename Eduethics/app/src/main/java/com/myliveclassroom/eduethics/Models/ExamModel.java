package com.myliveclassroom.eduethics.Models;

import com.google.gson.annotations.SerializedName;

public class ExamModel {

    @SerializedName("StudentId")
    private String StudentId;

    @SerializedName("StudentName")
    private String StudentName;

    private String ExamId;
    private String ExamIdDone;
    private String ExamTitle;
    private String StartDate;
    private String EndDate;
    private String StartTime;
    private String EndTime;
    private String StartTimeStr;
    private String EndTimeStr;
    private String ClassName;
    private String SubjectName;
    private String ClassId;
    private String SubjectId;
    private String MaxMarks;
    private String TotalQuestions,NoOfQCreated;

    private String ToAttempt;
    private String ExamType;
    private  String ExamSubmitDate;
    private String Certificate;

    private String Description;
    private String  MarksPerCorrect;
    private String NegativePerIncorrect;

    private String CorrectCount,InCorrectCount;
    private String MarksObtained;
    private String NegativeMarks;
    private String Attempted;
    private  String CreatedBy;
    private  String ProfileType;
    private String SchoolId;
    private String SessionId;
    private String IsLive;
    private  String ExamClassSubjectId;

    public String getInCorrectCount() {
        return InCorrectCount;
    }

    public String getNegativeMarks() {
        return NegativeMarks;
    }


    public String getExamClassSubjectId() {
        return ExamClassSubjectId;
    }

    public void setExamClassSubjectId(String examClassSubjectId) {
        ExamClassSubjectId = examClassSubjectId;
    }

    public String getNoOfQCreated() {
        return NoOfQCreated;
    }

    public String getIsLive() {
        return IsLive;
    }

    public void setIsLive(String isLive) {
        IsLive = isLive;
    }

    private String Duration;

    public String getDuration() {
        return Duration;
    }


    public void setDuration(String duration) {
        Duration = duration;
    }

    private Boolean IsDead;

    public Boolean getIsDead() {
        return IsDead;
    }

    public void setIsDead(Boolean dead) {
        IsDead = dead;
    }

    public void setSessionId(String sessionId) {
        SessionId = sessionId;
    }

    public void setSchoolId(String schoolId) {
        SchoolId = schoolId;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }


    public String getProfileType() {
        return ProfileType;
    }

    public void setProfileType(String profileType) {
        ProfileType = profileType;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public void setClassId(String classId) {
        ClassId = classId;
    }

    public void setSubjectId(String subjectId) {
        SubjectId = subjectId;
    }

    public void setClassName(String className) {
        ClassName = className;
    }

    public String getClassName() {
        return ClassName;
    }

    public String getEndDate() {
        return EndDate;
    }

    public String getEndTime() {
        return EndTime;
    }

    public String getExamId() {
        return ExamId;
    }

    public String getExamTitle() {
        return ExamTitle;
    }

    public String getExamType() {
        return ExamType;
    }

    public String getMaxMarks() {
        return MaxMarks;
    }

    public String getStartDate() {
        return StartDate;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setEndTime(String endTime) {
        EndTime = endTime;
    }

    public void setExamId(String examId) {
        ExamId = examId;
    }

    public String getToAttempt() {
        return ToAttempt;
    }

    public String getTotalQuestions() {
        return TotalQuestions;
    }

    public void setExamTitle(String examTitle) {
        ExamTitle = examTitle;
    }

    public void setMaxMarks(String maxMarks) {
        MaxMarks = maxMarks;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public void setExamType(String examType) {
        ExamType = examType;
    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public void setToAttempt(String toAttempt) {
        ToAttempt = toAttempt;
    }

    public void setTotalQuestions(String totalQuestions) {
        TotalQuestions = totalQuestions;
    }

    public String getEndTimeStr() {
        return EndTimeStr;
    }

    public String getStartTimeStr() {
        return StartTimeStr;
    }

    public void setEndTimeStr(String endTimeStr) {
        EndTimeStr = endTimeStr;
    }

    public void setStartTimeStr(String startTimeStr) {
        StartTimeStr = startTimeStr;
    }

    public String getExamIdDone() {
        return ExamIdDone;
    }

    public void setExamIdDone(String examIdDone) {
        ExamIdDone = examIdDone;
    }

    public String getExamSubmitDate() {
        return ExamSubmitDate;
    }

    public String getCertificate() {
        return Certificate;
    }

    public void setCertificate(String certificate) {
        Certificate = certificate;
    }

    public void setStudentName(String studentName) {
        StudentName = studentName;
    }

    public void setStudentId(String studentId) {
        StudentId = studentId;
    }

    public String getCorrectCount() {
        return CorrectCount;
    }

    public String getAttempted() {
        return Attempted;
    }

    public String getMarksObtained() {
        return MarksObtained;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getNegativePerIncorrect() {
        return NegativePerIncorrect;
    }

    public String getDescription() {
        return Description;
    }

    public String getStudentId() {
        return StudentId;
    }

    public String getClassId() {
        return ClassId;
    }

    public String getMarksPerCorrect() {
        return MarksPerCorrect;
    }

    public String getStudentName() {
        return StudentName;
    }


    public String getSubjectId() {
        return SubjectId;
    }

    public void setAttempted(String attempted) {
        Attempted = attempted;
    }

    public void setCorrectCount(String correctCount) {
        CorrectCount = correctCount;
    }

    public void setExamSubmitDate(String examSubmitDate) {
        ExamSubmitDate = examSubmitDate;
    }

    public void setMarksObtained(String marksObtained) {
        MarksObtained = marksObtained;
    }

    public void setMarksPerCorrect(String marksPerCorrect) {
        MarksPerCorrect = marksPerCorrect;
    }

    public void setNegativePerIncorrect(String negativePerIncorrect) {
        NegativePerIncorrect = negativePerIncorrect;
    }
}
