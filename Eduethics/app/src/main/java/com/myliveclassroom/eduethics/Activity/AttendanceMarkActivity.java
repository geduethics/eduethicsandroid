package com.myliveclassroom.eduethics.Activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.myliveclassroom.eduethics.Adapter.AttendanceAdapterWebApp;
import com.myliveclassroom.eduethics.Models.ApiResponseModel;
import com.myliveclassroom.eduethics.Models.TimeTableModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;

public class AttendanceMarkActivity extends BaseActivity {


    @BindView(R.id.txtPageTitle)
    TextView txtPageTitle;

    @BindView(R.id.imgBack)
    ImageView imgBack;

    @BindView(R.id.imgbtnAdd)
    ImageView imgbtnAdd;

    @BindView(R.id.btnAdd)
    TextView btnAdd;

    @BindView(R.id.txtClass)
    TextView txtClass;

    @BindView(R.id.txtDate)
    TextView txtDate;

    @BindView(R.id.txtBtnMarkAttendance)
    TextView txtBtnMarkAttendance;


    Fragment fragment;
    public List<TimeTableModel> timeTableModelList;

    private String mClassId,mPeriodId;
    private String mSelectedDate;

    AttendanceAdapterWebApp attendanceAdapter;

    ProgressBar progressBar;
    RecyclerView recyclerViewAttendance;

    public ImageView imgSubmit() {
        return imgbtnAdd;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.MyThemeNoBar);
        setContentView(R.layout.activity_attendance_mark);
        ButterKnife.bind(this);
        _BaseActivity(this);
        init();
    }

    private void init() {
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        mPeriodId=getIntent().getExtras().getString("PeriodId");
        mClassId = getIntent().getExtras().getString("ClassId");
        mSelectedDate = getIntent().getExtras().getString("Date");
        txtClass.setText(getIntent().getExtras().getString("ClassName"));
        txtDate.setText(getIntent().getExtras().getString("Date"));
        tinyDB.putString("AttDate", getIntent().getExtras().getString("Date"));


        imgbtnAdd.setVisibility(View.GONE);
        imgBack.setVisibility(View.VISIBLE);
        btnAdd.setVisibility(View.GONE);
        btnAdd.setText("");
        imgbtnAdd.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_tick3));
        txtPageTitle.setText("Mark Attendance");
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Back();
            }
        });

        imgbtnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(mContext, "test from frag", Toast.LENGTH_SHORT).show();
                Log.e(mTAG, "" + timeTableModelList.size());
                submitAttendance();
            }
        });

        txtBtnMarkAttendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(mContext, "test from frag", Toast.LENGTH_SHORT).show();
                Log.e(mTAG, "" + timeTableModelList.size());
                submitAttendance();
            }
        });
        initRecyclerView();
    }

    //@OnClick(R.id.imgBack)
    private void Back() {
        super.onBackPressed();
    }

    private void initRecyclerView() {
        timeTableModelList = new ArrayList<>();
        recyclerViewAttendance = findViewById(R.id.recyclerViewAttendance);
        recyclerViewAttendance.setLayoutManager(new LinearLayoutManager(mContext));
        attendanceAdapter = new AttendanceAdapterWebApp(mContext, timeTableModelList);
        recyclerViewAttendance.setAdapter(attendanceAdapter);
        getStudentList();
    }

    private void getStudentList() {
        progressBar.setVisibility(View.VISIBLE);
        TimeTableModel listModel = new TimeTableModel();

        listModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        listModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));
        //listModel.setStudentId("0");
        listModel.setClassId(mClassId);
        listModel.setAttDate(tinyDB.getString("AttDate"));
        listModel.setTimeTableId(mPeriodId);

        Log.e(mTAG, tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_NAME) + mClassId);


        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        //Call<List<TimeTableModel>> call = apiService.getClassStudent(listModel);
        Call<List<TimeTableModel>> call = apiService.AttendanceSelectOnDate(listModel);
        call.enqueue(new Callback<List<TimeTableModel>>() {
            @Override
            public void onResponse(Call<List<TimeTableModel>> call, Response<List<TimeTableModel>> response) {
                progressBar.setVisibility(View.GONE);
                try {
                    timeTableModelList = response.body();
                    attendanceAdapter = new AttendanceAdapterWebApp(mContext, timeTableModelList);
                    recyclerViewAttendance.setAdapter(attendanceAdapter);

                } catch (Exception ex) {
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<List<TimeTableModel>> call, Throwable t) {
                Log.e(mTAG, "error =" + t.toString());
            }
        });
    }

    Date dateToday;
    String dateTodayStr;
    Date attDate;
    SimpleDateFormat dateformat = new SimpleDateFormat("dd-MMM-yyyy");
    private void submitAttendance()  {
        try {
            Calendar startDate = Calendar.getInstance();
            dateTodayStr = dateformat.format(startDate.getTime());
            dateToday = dateformat.parse(dateTodayStr);
            attDate = dateformat.parse(tinyDB.getString("AttDate"));
        }
        catch (Exception ex){

        }
        if (!attDate.equals(dateToday)) {
            Toast.makeText(mContext, "Attendance can not be submitted on other date!", Toast.LENGTH_SHORT).show();
            return;
        }

        for (TimeTableModel timeTableModel : timeTableModelList) {
            if (timeTableModel.getUserId() == null) {
                timeTableModel.setUserId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID).trim());
            }
            if (timeTableModel.getAttDate() == null) {
                timeTableModel.setAttDate(tinyDB.getString("AttDate").trim());
            }

            if (timeTableModel.getSchoolId() == null) {
                timeTableModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID).trim());
            }
            timeTableModel.setTimeTableId(mPeriodId);
        }
        progressBar.setVisibility(View.VISIBLE);
        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = apiService.attendanceSubmit(timeTableModelList);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                progressBar.setVisibility(View.GONE);

                try {
                    if (response.body() != null) {
                        //ApiResponseModel apiResponseModel = response.body();
                        //Toast.makeText(AttendanceMarkActivity.this, response.body().getMessage()==null?"Done":response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        if(new Gson().fromJson(response.body().get("Success"), String.class).equalsIgnoreCase("true")){
                            Log.e(mTAG, "Attendance marked successfully");
                            Toast.makeText(AttendanceMarkActivity.this, "Attendance marked successfully", Toast.LENGTH_SHORT).show();
                        }else{
                            Log.e(mTAG, new Gson().fromJson(response.body().get("Message"), String.class));
                        }

                        finish();
                        //Log.e(mTAG, apiResponseModel.getMessage());
                    }

                } catch (Exception ex) {
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(mTAG, "error =" + t.toString());
            }
        });
    }
}
