package com.myliveclassroom.eduethics.Fragments.WebAppFrag;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.myliveclassroom.eduethics.Adapter.ClassroomTeachersAdapter2;
import com.myliveclassroom.eduethics.Models.TimeTableModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.TinyDB;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;

public class CreateTimeTableSchoolFragment extends Fragment {

    List<TimeTableModel> classSubjectModelList;
    ClassroomTeachersAdapter2 classroomTeachersAdapter;
    RecyclerView classroomRecyclerView, scheduleRecycler;
    Context mContext;
    TinyDB tinyDB;
            View root;


    public CreateTimeTableSchoolFragment(Context context) {
        mContext=context;
        tinyDB=new TinyDB(mContext);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root= inflater.inflate(R.layout.fragment_create_time_table_school, container, false);
        init();
        return root;
    }

    private void init(){
        classroomRecyclerView = root.findViewById(R.id.recyclerViewClassroom);
        classroomRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        classSubjectModelList=new ArrayList<>();
        classroomTeachersAdapter=new ClassroomTeachersAdapter2(mContext,classSubjectModelList, CreateTimeTableSchoolFragment.this);
        classroomRecyclerView.setAdapter(classroomTeachersAdapter);
        classListWithSubjects();

        //timeTableAll();
    }

    private void classListWithSubjects(){

        TimeTableModel listModel = new TimeTableModel();
        listModel.setTeacherId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
        listModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        listModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<List<TimeTableModel>> call = apiService.getTeacherClasses(listModel);
        call.enqueue(new Callback<List<TimeTableModel>>() {
            @Override
            public void onResponse(Call<List<TimeTableModel>> call, Response<List<TimeTableModel>> response) {
                //int statusCode = response.code();
                try {
                    classSubjectModelList = response.body();
                    classroomTeachersAdapter.updateList(classSubjectModelList);

                } catch (Exception ex) {
                    Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<List<TimeTableModel>> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void createTimeTableFragment(){
        return;
    }
}