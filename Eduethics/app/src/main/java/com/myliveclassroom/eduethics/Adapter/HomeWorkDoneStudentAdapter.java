package com.myliveclassroom.eduethics.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.myliveclassroom.eduethics.Activity.HomeWorkAsignActivity;
import com.myliveclassroom.eduethics.Models.HomeWorkModel;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.classes.CommonFunctions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeWorkDoneStudentAdapter extends RecyclerView.Adapter<HomeWorkDoneStudentAdapter.ViewHolder> {
    Context mContext;
    List<HomeWorkModel> homeWorkModelList;
    int rowNo=0;
    public HomeWorkDoneStudentAdapter(Context context,List<HomeWorkModel> userModels) {
        homeWorkModelList=userModels;
        mContext=context;
        rowNo=0;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_work_done_adapter, parent, false);
        return new ViewHolder(view);
    }

    private boolean enableClick=false;
    private String enableMsg="";
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        enableClick=false;
        enableMsg="";
        holder.txtBtnWorkStatus.setVisibility(View.INVISIBLE);
        if(!homeWorkModelList.get(position).getWorkDoneId().equalsIgnoreCase("0")){
            holder.txtBtnWorkStatus.setVisibility(View.VISIBLE);
            if(homeWorkModelList.get(position).getStatus().equalsIgnoreCase("Uploaded")) {
                holder.txtBtnWorkStatus.setText("Check");
                holder.txtBtnWorkStatus.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
                holder.txtBtnWorkStatus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                            if (mContext instanceof HomeWorkAsignActivity) {
                                ((HomeWorkAsignActivity) mContext).viewUploadedWork(homeWorkModelList.get(position));
                            }
                    }
                });
            }
            if(homeWorkModelList.get(position).getStatus().equalsIgnoreCase("Excellent") || homeWorkModelList.get(position).getStatus().equalsIgnoreCase("Good")) {
                holder.txtBtnWorkStatus.setText("Done");
                holder.txtBtnWorkStatus.setTextColor(mContext.getResources().getColor(R.color.green));
                enableClick=false;
                enableMsg="Work checked and marked completed.";
            }
            if(homeWorkModelList.get(position).getStatus().equalsIgnoreCase("Correct it")) {
                holder.txtBtnWorkStatus.setText("Done");
                holder.txtBtnWorkStatus.setTextColor(mContext.getResources().getColor(R.color.blue));
                enableClick=false;
                enableMsg="Work checked and marked Correct it.";
            }
        }
        holder.txtRollNo.setText(CommonFunctions.capitalize(homeWorkModelList.get(position).getRollNo()));
        holder.txtStudentName.setText(CommonFunctions.capitalize(homeWorkModelList.get(position).getStudentName()));


    }

    @Override
    public int getItemCount() {
        return homeWorkModelList==null?0:homeWorkModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtRollNo)
        TextView txtRollNo;

        @BindView(R.id.txtStudentName)
        TextView txtStudentName;

        @BindView(R.id.txtBtnWorkStatus)
        TextView txtBtnWorkStatus;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
