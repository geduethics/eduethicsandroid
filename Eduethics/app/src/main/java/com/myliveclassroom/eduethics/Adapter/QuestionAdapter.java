package com.myliveclassroom.eduethics.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.myliveclassroom.eduethics.Activity.ExamQActivity;
import com.myliveclassroom.eduethics.Models.QuestionModel;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.TinyDB;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.stream.Collectors;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.API_BASE_URL;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mcqAnsSrArray;


public class QuestionAdapter extends RecyclerView.Adapter<QuestionAdapter.ViewHolder> {

    List<String> questionList;
    List<QuestionModel> questionWithAnsList;
    Context mContext;
    TinyDB tinyDB;

    public QuestionAdapter(Context context, List<String> questionModel, List<QuestionModel> questionModelWithAns) {
        mContext = context;
        tinyDB = new TinyDB(mContext);
        questionList = questionModel;
        questionWithAnsList = questionModelWithAns;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.exam_quest_adapter, parent, false);
        return new ViewHolder(view);
    }

    Boolean isRadio = false;
    Boolean isChkBox = false;
    String qId = "0";
    List<QuestionModel> innerList;
    int rowSr;

    @SuppressLint("ResourceType")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        isRadio = false;
        isChkBox = false;
        rowSr = position;
        qId = questionList.get(position);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            innerList = questionWithAnsList.stream().filter(f -> f.getQuestionId().equalsIgnoreCase(qId)).collect(Collectors.toList());
            Log.e("", innerList.toString());
        }

        if (innerList.size() > 0) {

            holder.txtqid.setText(innerList.get(0).getQuestionId());
            holder.txtQ.setText(innerList.get(0).getQuestion());
            holder.txtPoints.setText("Marks : " + innerList.get(0).getPoints());
            if (innerList.get(0).getChkChoice().toLowerCase().equalsIgnoreCase("y")) {
                isChkBox = true;
                isRadio = false;
            } else if (innerList.get(0).getRadioChoice().toLowerCase().equalsIgnoreCase("y")) {
                isRadio = true;
                isChkBox = false;
            }

            if (isRadio) {
                holder.imgChk1.setVisibility(View.GONE);
                holder.imgChk2.setVisibility(View.GONE);
                holder.imgChk3.setVisibility(View.GONE);
                holder.imgChk4.setVisibility(View.GONE);

                holder.imgRdo1.setVisibility(View.VISIBLE);
                holder.imgRdo2.setVisibility(View.VISIBLE);
                holder.imgRdo3.setVisibility(View.VISIBLE);
                holder.imgRdo4.setVisibility(View.VISIBLE);
            }
            if (isChkBox) {
                holder.imgRdo1.setVisibility(View.GONE);
                holder.imgRdo2.setVisibility(View.GONE);
                holder.imgRdo3.setVisibility(View.GONE);
                holder.imgRdo4.setVisibility(View.GONE);

                holder.imgChk1.setVisibility(View.VISIBLE);
                holder.imgChk2.setVisibility(View.VISIBLE);
                holder.imgChk3.setVisibility(View.VISIBLE);
                holder.imgChk4.setVisibility(View.VISIBLE);
            }
            //holder.imgQ.setBackground(mContext.getResources().getDrawable(R.drawable.ic_att_camera));

            if (!innerList.get(0).getQuestionImg().equalsIgnoreCase("")) {
                Glide.with(mContext)
                        .asBitmap()
                        .load(API_BASE_URL + "qImages/" + innerList.get(0).getQuestionImg())
                        .into(holder.imgQ);
                holder.imgQ.setVisibility(View.VISIBLE);
            } else {
                holder.imgQ.setVisibility(View.GONE);
            }

            holder.txtQno.setText("Q.No. : " + (position + 1));
            if (isRadio) {
                if (!innerList.get(0).getAnsImg().equalsIgnoreCase("")) {
                    holder.layoutAnsRdo.setVisibility(View.VISIBLE);
                    if (innerList.size() > 0) {
                        if (!innerList.get(0).getAnsImg().equalsIgnoreCase("")) {
                            Glide.with(mContext)
                                    .asBitmap()
                                    .load(API_BASE_URL + "qImages/" + innerList.get(0).getAnsImg())
                                    .into(holder.img1);
                        }
                        if (innerList.get(0).getAnswerId().equalsIgnoreCase(innerList.get(0).getAnswerIdS())) {
                            holder.imgRdo1.setChecked(true);
                        }
                        holder.txt1.setText(innerList.get(0).getAns());
                        holder.txtRdo1.setText(innerList.get(0).getAnswerId());
                        holder.img1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                holder.imgRdo1.setChecked(true);
                                holder.imgRdo2.setChecked(false);
                                holder.imgRdo3.setChecked(false);
                                holder.imgRdo4.setChecked(false);
                                if (mContext instanceof ExamQActivity) {
                                    ((ExamQActivity) mContext).clickAnsSubmit(holder.txtqid.getText().toString(), holder.txtRdo1.getText().toString());
                                }
                            }
                        });
                    }

                    if (innerList.size() > 1) {
                        if (!innerList.get(1).getAnsImg().equalsIgnoreCase("")) {
                            Glide.with(mContext)
                                    .asBitmap()
                                    .load(API_BASE_URL + "qImages/" + innerList.get(1).getAnsImg())
                                    .into(holder.img2);
                        }
                        if (innerList.get(1).getAnswerId().equalsIgnoreCase(innerList.get(1).getAnswerIdS())) {
                            holder.imgRdo2.setChecked(true);
                        }
                        holder.txt2.setText(innerList.get(1).getAns());
                        holder.txtRdo2.setText(innerList.get(1).getAnswerId());
                        holder.img2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                holder.imgRdo1.setChecked(false);
                                holder.imgRdo2.setChecked(true);
                                holder.imgRdo3.setChecked(false);
                                holder.imgRdo4.setChecked(false);
                                if (mContext instanceof ExamQActivity) {
                                    ((ExamQActivity) mContext).clickAnsSubmit(holder.txtqid.getText().toString(), holder.txtRdo2.getText().toString());
                                }
                            }
                        });


                    }
                    if (innerList.size() > 2) {
                        if (!innerList.get(2).getAnsImg().equalsIgnoreCase("")) {
                            Glide.with(mContext)
                                    .asBitmap()
                                    .load(API_BASE_URL + "qImages/" + innerList.get(2).getAnsImg())
                                    .into(holder.img3);
                        }
                        if (innerList.get(2).getAnswerId().equalsIgnoreCase(innerList.get(2).getAnswerIdS())) {
                            holder.imgRdo3.setChecked(true);
                        }
                        holder.txt3.setText(innerList.get(2).getAns());
                        holder.txtRdo3.setText(innerList.get(2).getAnswerId());
                        holder.img3.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                holder.imgRdo1.setChecked(false);
                                holder.imgRdo2.setChecked(false);
                                holder.imgRdo3.setChecked(true);
                                holder.imgRdo4.setChecked(false);
                                if (mContext instanceof ExamQActivity) {
                                    ((ExamQActivity) mContext).clickAnsSubmit(holder.txtqid.getText().toString(), holder.txtRdo3.getText().toString());
                                }
                            }
                        });
                    }
                    if (innerList.size() > 3) {
                        if (!innerList.get(3).getAnsImg().equalsIgnoreCase("")) {
                            Glide.with(mContext)
                                    .asBitmap()
                                    .load(API_BASE_URL + "qImages/" + innerList.get(3).getAnsImg())
                                    .into(holder.img4);
                        }

                        if (innerList.get(3).getAnswerId().equalsIgnoreCase(innerList.get(3).getAnswerIdS())) {
                            holder.imgRdo4.setChecked(true);
                        }
                        holder.txt4.setText(innerList.get(3).getAns());
                        holder.txtRdo4.setText(innerList.get(3).getAnswerId());
                        holder.img4.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                holder.imgRdo1.setChecked(false);
                                holder.imgRdo2.setChecked(false);
                                holder.imgRdo3.setChecked(false);
                                holder.imgRdo4.setChecked(true);

                                if (mContext instanceof ExamQActivity) {
                                    ((ExamQActivity) mContext).clickAnsSubmit(holder.txtqid.getText().toString(), holder.txtRdo4.getText().toString());
                                }
                            }
                        });
                    }
                } else {
                    holder.layoutAnsRdo.setVisibility(View.GONE);
                    RadioGroup radioGroup = new RadioGroup(mContext);
                    radioGroup.setOrientation(RadioGroup.VERTICAL);
                    RadioGroup.LayoutParams rprms = new RadioGroup.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    holder.layoutAns.addView(radioGroup);

                    for (int i = 0; i < innerList.size(); i++) {
                        if (!innerList.get(i).getAnswerId().equalsIgnoreCase("0")) {
                            RadioButton radioButton = new RadioButton(mContext);
                            radioButton.setText(innerList.get(i).getAns());
                            radioButton.setId(Integer.valueOf(innerList.get(i).getAnswerId()));

                            showAnsKeys(radioButton, i);
                            if (!innerList.get(i).getAnswerIdS().equalsIgnoreCase("0")) {
                                radioButton.setChecked(true);
                            }
                            radioGroup.addView(radioButton);
                        }
                        position = position + 1;
                    }
                    radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(RadioGroup group, int checkedId) {
                            Toast.makeText(mContext, "" + checkedId, Toast.LENGTH_SHORT).show();
                            if (mContext instanceof ExamQActivity) {
                                ((ExamQActivity) mContext).clickAnsSubmit(holder.txtqid.getText().toString(), checkedId + "");
                            }
                        }
                    });
                }
            }

            if (isChkBox) {
                if (!innerList.get(0).getAnsImg().equalsIgnoreCase("")) {
                    holder.layoutAnsRdo.setVisibility(View.VISIBLE);
                    if (innerList.size() > 0) {
                        if (!innerList.get(0).getAnsImg().equalsIgnoreCase("")) {
                            Glide.with(mContext)
                                    .asBitmap()
                                    .load(API_BASE_URL + "qImages/" + innerList.get(0).getAnsImg())
                                    .into(holder.img1);
                        }
                        if (innerList.get(0).getAnswerId().equalsIgnoreCase(innerList.get(0).getAnswerIdS())) {
                            holder.imgChk1.setChecked(true);
                        }
                        holder.txt1.setText(innerList.get(0).getAns());
                        holder.imgChk1.setText(innerList.get(0).getAnswerId());
                        holder.img1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                holder.imgChk1.setChecked(true);
                                holder.imgChk2.setChecked(false);
                                holder.imgChk3.setChecked(false);
                                holder.imgChk4.setChecked(false);
                                if (mContext instanceof ExamQActivity) {
                                    ((ExamQActivity) mContext).clickAnsSubmit(holder.txtqid.getText().toString(), holder.txtRdo1.getText().toString());
                                }
                            }
                        });
                    }

                    if (innerList.size() > 1) {
                        if (!innerList.get(1).getAnsImg().equalsIgnoreCase("")) {
                            Glide.with(mContext)
                                    .asBitmap()
                                    .load(API_BASE_URL + "qImages/" + innerList.get(1).getAnsImg())
                                    .into(holder.img2);
                        }
                        if (innerList.get(1).getAnswerId().equalsIgnoreCase(innerList.get(1).getAnswerIdS())) {
                            holder.imgChk2.setChecked(true);
                        }
                        holder.txt2.setText(innerList.get(1).getAns());
                        holder.imgChk2.setText(innerList.get(1).getAnswerId());
                        holder.img2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                holder.imgChk1.setChecked(false);
                                holder.imgChk2.setChecked(true);
                                holder.imgChk3.setChecked(false);
                                holder.imgChk4.setChecked(false);
                                if (mContext instanceof ExamQActivity) {
                                    ((ExamQActivity) mContext).clickAnsSubmit(holder.txtqid.getText().toString(), holder.txtRdo2.getText().toString());
                                }
                            }
                        });


                    }
                    if (innerList.size() > 2) {
                        if (!innerList.get(2).getAnsImg().equalsIgnoreCase("")) {
                            Glide.with(mContext)
                                    .asBitmap()
                                    .load(API_BASE_URL + "qImages/" + innerList.get(2).getAnsImg())
                                    .into(holder.img3);
                        }
                        if (innerList.get(2).getAnswerId().equalsIgnoreCase(innerList.get(2).getAnswerIdS())) {
                            holder.imgChk3.setChecked(true);
                        }
                        holder.txt3.setText(innerList.get(2).getAns());
                        holder.imgChk3.setText(innerList.get(2).getAnswerId());
                        holder.img3.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                holder.imgChk1.setChecked(false);
                                holder.imgChk2.setChecked(false);
                                holder.imgChk3.setChecked(true);
                                holder.imgChk4.setChecked(false);
                                if (mContext instanceof ExamQActivity) {
                                    ((ExamQActivity) mContext).clickAnsSubmit(holder.txtqid.getText().toString(), holder.txtRdo3.getText().toString());
                                }
                            }
                        });
                    }
                    if (innerList.size() > 3) {
                        if (!innerList.get(3).getAnsImg().equalsIgnoreCase("")) {
                            Glide.with(mContext)
                                    .asBitmap()
                                    .load(API_BASE_URL + "qImages/" + innerList.get(3).getAnsImg())
                                    .into(holder.img4);
                        }

                        if (innerList.get(3).getAnswerId().equalsIgnoreCase(innerList.get(3).getAnswerIdS())) {
                            holder.imgChk4.setChecked(true);
                        }
                        holder.txt4.setText(innerList.get(3).getAns());
                        holder.imgChk4.setText(innerList.get(3).getAnswerId());
                        holder.img4.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                holder.imgChk1.setChecked(false);
                                holder.imgChk2.setChecked(false);
                                holder.imgChk3.setChecked(false);
                                holder.imgChk4.setChecked(true);

                                if (mContext instanceof ExamQActivity) {
                                    ((ExamQActivity) mContext).clickAnsSubmit(holder.txtqid.getText().toString(), holder.txtRdo4.getText().toString());
                                }
                            }
                        });
                    }
                } else {
                    holder.layoutAnsRdo.setVisibility(View.GONE);
                    RadioGroup radioGroup = new RadioGroup(mContext);
                    radioGroup.setOrientation(RadioGroup.VERTICAL);
                    RadioGroup.LayoutParams rprms = new RadioGroup.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    holder.layoutAns.addView(radioGroup);

                    for (int i = 0; i < innerList.size(); i++) {
                        if (!innerList.get(i).getAnswerId().equalsIgnoreCase("0")) {
                            CheckBox chkBox = new CheckBox(mContext);
                            chkBox.setText("(" + mcqAnsSrArray[i] + ") " + innerList.get(i).getAns());
                            chkBox.setId(Integer.valueOf(innerList.get(i).getAnswerId()));

                            if (innerList.get(i).getAnswerId().equalsIgnoreCase(innerList.get(i).getAnswerIdS())) {
                                chkBox.setChecked(true);
                            }
                            radioGroup.addView(chkBox);
                        }
                        position = position + 1;
                    }

                    radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(RadioGroup group, int checkedId) {
                            Toast.makeText(mContext, "" + checkedId, Toast.LENGTH_SHORT).show();
                            if (mContext instanceof ExamQActivity) {
                                ((ExamQActivity) mContext).clickAnsSubmit(holder.txtqid.getText().toString(), checkedId + "");
                            }
                        }
                    });
                }
            }
                /*PresetRadioGroup presetRadioGroup=new PresetRadioGroup(mContext);
                holder.layoutAns.addView(presetRadioGroup);
                for (int i = 0; i < innerList.size(); i++) {
                    if(!innerList.get(i).getAnswerId().equalsIgnoreCase("0")) {
                        PresetValueButton presetValueButton=new PresetValueButton(mContext);
                        ImageView rdoImg = presetValueButton.findViewById(R.id.rdoImg);

                        Glide.with(mContext)
                                .asBitmap()
                                .load(api_MainUrl + "qImages/" + innerList.get(0).getQuestionImg())
                                .into(rdoImg);

                        presetRadioGroup.addView(presetValueButton);



                    }
                    position = position + 1;
                }*/

            // }
        }
    }

    private void showAnsKeys(RadioButton radioButton, int i) {
        if (!tinyDB.getBoolean(LocalConstants.Exam_Constants.Show_Ans_Keys)) {
            return;
        }
        radioButton.setEnabled(false);

        if (innerList.get(i).getCorrectAnsId().equalsIgnoreCase(innerList.get(i).getAnswerIdS())) {
            radioButton.setTextColor(mContext.getColor(R.color.green2));
            radioButton.setTypeface(null, Typeface.BOLD_ITALIC);
            radioButton.setChecked(true);
            radioButton.setText(innerList.get(i).getAns() + " [Correct]");
        }   else if(!innerList.get(i).getAnswerIdS().equalsIgnoreCase("0")){
            radioButton.setText(innerList.get(i).getAns() + " [Wrong]");
            radioButton.setTypeface(null, Typeface.BOLD_ITALIC);
            radioButton.setTextColor(mContext.getColor(R.color.red3));
            radioButton.setChecked(true);
        }
        else
        if (innerList.get(i).getIsAns().equalsIgnoreCase("Y")) {
            radioButton.setTextColor(mContext.getColor(R.color.green2));
            radioButton.setTypeface(null, Typeface.BOLD_ITALIC);
            radioButton.setText(innerList.get(i).getAns() + " [Correct Ans]");
        }
    }


    public static Drawable drawableFromUrl(String url) throws IOException {
        Bitmap x;

        HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
        connection.connect();
        InputStream input = connection.getInputStream();

        x = BitmapFactory.decodeStream(input);
        return new BitmapDrawable(Resources.getSystem(), x);
       /* InputStream iStream = (InputStream) new URL(url).getContent();
        Drawable drawable = Drawable.createFromStream(iStream, "src name");
        return drawable;*/
    }

    @Override
    public int getItemCount() {
        return questionList == null ? 0 : questionList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtQno)
        TextView txtQno;

        @BindView(R.id.txtQ)
        TextView txtQ;

        @BindView(R.id.txtPoints)
        TextView txtPoints;

        @BindView(R.id.layoutAns)
        LinearLayout layoutAns;

        @BindView(R.id.txtqid)
        TextView txtqid;

        @BindView(R.id.imgQ)
        ImageView imgQ;

        @BindView(R.id.imgRdo1)
        RadioButton imgRdo1;

        @BindView(R.id.imgRdo2)
        RadioButton imgRdo2;

        @BindView(R.id.imgRdo3)
        RadioButton imgRdo3;

        @BindView(R.id.imgRdo4)
        RadioButton imgRdo4;

        @BindView(R.id.imgChk1)
        CheckBox imgChk1;

        @BindView(R.id.imgChk2)
        CheckBox imgChk2;

        @BindView(R.id.imgChk3)
        CheckBox imgChk3;

        @BindView(R.id.imgChk4)
        CheckBox imgChk4;

        @BindView(R.id.img1)
        ImageView img1;

        @BindView(R.id.img2)
        ImageView img2;

        @BindView(R.id.img3)
        ImageView img3;

        @BindView(R.id.img4)
        ImageView img4;

        @BindView(R.id.layoutAnsRdo)
        LinearLayout layoutAnsRdo;

        @BindView(R.id.txtRdo1)
        TextView txtRdo1;

        @BindView(R.id.txtRdo2)
        TextView txtRdo2;

        @BindView(R.id.txtRdo3)
        TextView txtRdo3;

        @BindView(R.id.txtRdo4)
        TextView txtRdo4;

        @BindView(R.id.txt1)
        TextView txt1;

        @BindView(R.id.txt2)
        TextView txt2;

        @BindView(R.id.txt3)
        TextView txt3;

        @BindView(R.id.txt4)
        TextView txt4;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
