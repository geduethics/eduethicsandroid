package com.myliveclassroom.eduethics.Fragments.WebAppFrag;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.myliveclassroom.eduethics.Activity.AttendanceMarkActivity;
import com.myliveclassroom.eduethics.Activity.AttendanceTActivity;
import com.myliveclassroom.eduethics.Adapter.AttendanceAdapterWebApp;
import com.myliveclassroom.eduethics.Models.ApiResponseModel;
import com.myliveclassroom.eduethics.Models.TimeTableModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.CommonFunctions;
import com.myliveclassroom.eduethics.classes.TinyDB;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;

public class AttendanceTFragment extends Fragment {


    private static final String ARG_SECTION_NUMBER = "section_number";
    private int sectionNumber;


    ProgressBar progressBar;

    TinyDB tinyDB;
    Context mContext;
    private String mDayId = "0";
    ImageView imgSubmit;
    String  mClassId="0";


    List<TimeTableModel> timeTableModelList;
    AttendanceAdapterWebApp timetableAdapter;

    RecyclerView recyclerViewTimeTable;


    public AttendanceTFragment() {
    }

    public static AttendanceTFragment newInstance(int sectionNumber) {
        AttendanceTFragment fragment = new AttendanceTFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setHasOptionsMenu(true);
        sectionNumber = getArguments() != null ? getArguments().getInt(ARG_SECTION_NUMBER) : 1;
        Log.e(mTAG,"sectionNumber="+sectionNumber);

        mContext = getContext();
        tinyDB = new TinyDB(getContext());
        mDayId= String.valueOf(sectionNumber+1);


        imgSubmit=((AttendanceTActivity)getActivity()).imgSubmit();

        imgSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(mContext, "test from frag", Toast.LENGTH_SHORT).show();
                Log.e(mTAG,""+timeTableModelList.size());

                submitAttendance();
            }
        });
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_one_one, container, false);
        ButterKnife.bind(this, view);

        Log.e(mTAG,"onCreateView"+mDayId);
        initRecyclerView(view);
        progressBar=view.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        return view;
    }

    private void initRecyclerView(View view) {
        timeTableModelList=new ArrayList<>();
        recyclerViewTimeTable=view.findViewById(R.id.recyclerViewTimeTable);
        recyclerViewTimeTable.setLayoutManager(new LinearLayoutManager(mContext));
        timetableAdapter=new AttendanceAdapterWebApp(mContext,timeTableModelList);
        recyclerViewTimeTable.setAdapter(timetableAdapter);
        getStudentList();
    }

    private void getStudentList() {
        mClassId= ((AttendanceTActivity) getActivity()).timeTableModel.get(sectionNumber).getClassId();

        TimeTableModel listModel = new TimeTableModel();

        listModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        listModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));
        listModel.setClassId(mClassId);

        Log.e(mTAG,tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_NAME)+mClassId);

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<List<TimeTableModel>> call = apiService.getClassStudent2(listModel);
        call.enqueue(new Callback<List<TimeTableModel>>() {
            @Override
            public void onResponse(Call<List<TimeTableModel>> call, Response<List<TimeTableModel>> response) {
                try {
                    timeTableModelList = response.body();
                    timetableAdapter=new AttendanceAdapterWebApp(mContext,timeTableModelList);
                    recyclerViewTimeTable.setAdapter(timetableAdapter);

                } catch (Exception ex) {
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<List<TimeTableModel>> call, Throwable t) {
                Log.e(mTAG, "error =" + t.toString());
            }
        });
    }


    private void submitAttendance(){
        progressBar.setVisibility(View.VISIBLE);
        imgSubmit.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_wait));
        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = apiService.attendanceSubmit(timeTableModelList);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                progressBar.setVisibility(View.GONE);
                imgSubmit.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_tick3));
                try {
                    if(response.body()!=null) {
                        //ApiResponseModel apiResponseModel = response.body();
                        //CommonFunctions.myToast(mContext,apiResponseModel.getMessage());
                        //Log.e(mTAG, apiResponseModel.getMessage());
                        if(new Gson().fromJson(response.body().get("Success"), String.class).equalsIgnoreCase("true")){
                            Log.e(mTAG, "Attendance marked successfully");
                            Toast.makeText(mContext, "Attendance marked successfully", Toast.LENGTH_SHORT).show();
                        }else{
                            Log.e(mTAG, new Gson().fromJson(response.body().get("Message"), String.class));
                        }
                    }

                } catch (Exception ex) {
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(mTAG, "error =" + t.toString());
            }
        });
    }



    @Override
    public void onResume() {
        super.onResume();
    }

}