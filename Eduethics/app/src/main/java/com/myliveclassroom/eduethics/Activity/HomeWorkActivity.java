package com.myliveclassroom.eduethics.Activity;


import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonObject;
import com.myliveclassroom.eduethics.Adapter.HomeWorkAdapter;
import com.myliveclassroom.eduethics.Adapter.UploadImagesAdapter;
import com.myliveclassroom.eduethics.Models.ApiResponseModel;
import com.myliveclassroom.eduethics.Models.HomeWorkModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.HomeConstants;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.HorizontalCalendarView;
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.API_BASE_URL;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.APP_PACKAGE_NAME;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.ATTACHMENT_ARRAY;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.CAMERA_METHOD;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.MAIN_FOLDER;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.PICK_CAMERA_REQUEST_CODE;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.PICK_PDF_REQUEST_CODE;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;
import static com.myliveclassroom.eduethics.classes.CommonFunctions.createImageFileNew;


public class HomeWorkActivity extends BaseActivity {

    ProgressBar progressBar;
    TextView mtxtAttachment;
    final Handler handler = new Handler();

    @BindView(R.id.txtPageTitle)
    TextView txtPageTitle;

    @BindView(R.id.imgBack)
    ImageView imgBack;

    @BindView(R.id.imgbtnAdd)
    ImageView imgbtnAdd;

    @BindView(R.id.btnAdd)
    TextView btnAdd;

    @BindView(R.id.txtDateToday)
    TextView txtDateToday;

    String dateToday = "";

    List<HomeWorkModel> subjectList;
    HomeWorkAdapter homeWorkAdapter;

    @BindView(R.id.recyclerViewHomeWork)
    RecyclerView recyclerViewHomeWork;

    SimpleDateFormat dateformat = new SimpleDateFormat("dd-MMM-yyyy");

    RelativeLayout rootView;
    HorizontalCalendar horizontalCalendar;
    LinearLayout layoutCalendar;
    int firstTimeDiff = 0;
    Calendar startDate;
    List<String> homeWorkSubmitHeadList = new ArrayList<String>();

    ArrayList<String> documentsPathsArray = new ArrayList<>(); // this is the object where files stored
    ArrayList<String> attachmentArray = new ArrayList<>();
    UploadImagesAdapter imagesadapter = new UploadImagesAdapter(documentsPathsArray, HomeWorkActivity.this, LocalConstants.FROM.FROM_HOMEWORK_ACTVITY_STUDENT, null);


    //@OnClick(R.id.imgBack)
    private void Back() {
        super.onBackPressed();
    }

    int expand = 0;

    private DownloadManager downloadManager;
    private long refid;
    private Uri Download_Uri;
    ArrayList<Long> downloadList = new ArrayList<>();
    RecyclerView uploadRecyclerView;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.MyThemeNoBar);
        setContentView(R.layout.activity_home_work);


        _BaseActivity(this);
        ButterKnife.bind(this);
        init();
        tinyDB.putString(ATTACHMENT_ARRAY, "");
        startDate = Calendar.getInstance();
        dateToday = dateformat.format(startDate.getTime());

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                initHorizontalCalendar();
                listSubjects();
            }
        }, 100);

        initRecyclerView();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        /*downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        IntentFilter filter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        registerReceiver(downloadReceiver, filter);*/

        if (!permissioncheck(mContext, 1010)) {
            Toast.makeText(mContext, "Some permissions are missing ", Toast.LENGTH_SHORT).show();
        }
    }

    private void init() {
        //tinyDB.putString("Screen",HomeConstants.HOMEWORK);
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        layoutCalendar = findViewById(R.id.layoutCalendar);
        imgbtnAdd.setVisibility(View.INVISIBLE);
        imgBack.setVisibility(View.VISIBLE);
        btnAdd.setVisibility(View.GONE);
        btnAdd.setText("");
        imgbtnAdd.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_tick3));

        imagesadapter.setProgress(findViewById(R.id.progress));


        if (tinyDB.getString("Screen").equalsIgnoreCase(HomeConstants.HOMEWORK_TEACHER)) {
            txtPageTitle.setText("Assign Home Work - " + tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_NAME));
        } else {
            txtPageTitle.setText("Home Work - " + tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_NAME));
        }
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Back();
            }
        });

        rootView = (RelativeLayout) findViewById(R.id.root_view);
        rootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int heightDiff = rootView.getRootView().getHeight() - rootView.getHeight();
                if (firstTimeDiff == 0) {
                    firstTimeDiff = heightDiff;
                }
                if (heightDiff > firstTimeDiff + 50) {
                    layoutCalendar.setVisibility(View.GONE);
                    Log.e("MyActivity", "keyboard opened");
                } else if (heightDiff == firstTimeDiff) {

                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 100ms
                            layoutCalendar.setVisibility(View.VISIBLE);
                        }
                    }, 100);

                    Log.e("MyActivity", "keyboard closed");
                }
            }
        });


    }

    private void initRecyclerView() {
        subjectList = new ArrayList<>();
        recyclerViewHomeWork = findViewById(R.id.recyclerViewHomeWork);
        recyclerViewHomeWork.setLayoutManager(new LinearLayoutManager(mContext));
    }

    private void initHorizontalCalendar() {
        startDate.add(Calendar.MONTH, -3);
        txtDateToday.setText(dateToday);//+" - "+ tinyDB.getString(TINYDB_LOGIN_KEYS.CLASS_NAME));

        /* ends after 1 month from now */
        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.DATE, 7);

        horizontalCalendar = new HorizontalCalendar.Builder(this, R.id.calendarView)
                .range(startDate, endDate)
                .datesNumberOnScreen(7)
                .build();

        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Calendar date, int position) {
                dateToday = dateformat.format(date.getTime());
                txtDateToday.setText(dateToday);
                Log.e(mTAG, "" + dateformat.format(date.getTime()));
                listSubjects();
            }

            @Override
            public void onCalendarScroll(HorizontalCalendarView calendarView,
                                         int dx, int dy) {

            }

            @Override
            public boolean onDateLongClicked(Calendar date, int position) {
                return true;
            }
        });
    }

    public void listSubjects() {
        HomeWorkModel timeTableModel = new HomeWorkModel();
        timeTableModel.setWorkDate(dateToday);

        timeTableModel.setStudentId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
        timeTableModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));
        timeTableModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        timeTableModel.setClassId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID));
        if (tinyDB.getString("Screen").equalsIgnoreCase(HomeConstants.HOMEWORK_TEACHER)) {
            timeTableModel.setSubjectId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SUBJECT_ID));
        }
        showProgress();
        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<List<HomeWorkModel>> call = apiService.HomeWorkList(timeTableModel);
        call.enqueue(new Callback<List<HomeWorkModel>>() {
            @Override
            public void onResponse(Call<List<HomeWorkModel>> call, Response<List<HomeWorkModel>> response) {
                try {
                    hideProgress();
                    if (response.body() != null) {
                        subjectList = response.body();
                        homeWorkAdapter = new HomeWorkAdapter(mContext, subjectList);
                        recyclerViewHomeWork.setAdapter(homeWorkAdapter);
                    }
                } catch (Exception ex) {
                    Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<List<HomeWorkModel>> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                hideProgress();
                Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void openCamera(String method) {
        //String photoPath = openCam(null, method);
       /* CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(HomeWorkActivity.this);*/

        if (ContextCompat.checkSelfPermission(HomeWorkActivity.this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED)
            ActivityCompat.requestPermissions(HomeWorkActivity.this, new String[]{Manifest.permission.CAMERA}, PICK_CAMERA_REQUEST_CODE);
        else {
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            /***************************************
             *      Custom file name for Camera file
             ***************************************/
            File photoFile = null;
            try {
                photoFile = createImageFileNew(LocalConstants.FILE_TYPE.JPG);
                mCurrentPhotoPath = photoFile.getAbsolutePath();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Log.i(mTAG, "IOException");
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri imageUri = FileProvider.getUriForFile(
                        mContext,
                        APP_PACKAGE_NAME + ".provider",
                        photoFile);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                startActivityForResult(cameraIntent, PICK_CAMERA_REQUEST_CODE);
            }
        }
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public void openDocExplorer() {
        Intent intent = new Intent();
        intent.setType("application/pdf");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        startActivityForResult(Intent.createChooser(intent, "Select Pdf"), PICK_PDF_REQUEST_CODE);
    }


    List<String> selectFileList = new ArrayList<>();


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_CANCELED) {
            //Write your code if there's no result
            /*mtxtAttachment.setText("No attachment");
            mtxtAttachment.setTextColor(mContext.getResources().getColor(R.color.blue));*/
            return;
        }
        if (requestCode == PICK_PDF_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            documentsPathsArray = new ArrayList<>();

            //Single file upload code --OK
            if (data.getData() != null) {
                Uri docUri = data.getData();
                if (!selectFileList.contains(docUri.getPath())) {
                    selectFileList.add(docUri.getPath());
                    File f = null;
                    try {
                        f = comfun.saveFile(mContext, docUri, "", MAIN_FOLDER, "doc_");
                        mCurrentPhotoPath = f.getAbsolutePath();
                        if (!documentsPathsArray.contains(mCurrentPhotoPath)) {
                            documentsPathsArray.add(mCurrentPhotoPath);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            } else {
                documentsPathsArray = new ArrayList<>();
                ClipData clipData = data.getClipData();
                if (clipData != null) {
                    for (int i = 0; i < clipData.getItemCount(); i++) {
                        ClipData.Item item = clipData.getItemAt(i);
                        Uri uri = item.getUri();

                        if (!selectFileList.contains(uri.getPath())) {
                            selectFileList.add(uri.getPath());

                            /*******OK if rename and loction change required*******/
                            File file;
                            try {
                                file = comfun.saveFile(mContext, uri, "", MAIN_FOLDER, "doc_");
                                String path = file.getAbsolutePath();
                                if (!documentsPathsArray.contains(path)) {
                                    documentsPathsArray.add(path);
                                }
                            } catch (IOException e) {
                                Log.e(mTAG, e.getMessage());
                                e.printStackTrace();
                            }
                        }
                    }

                }
                Log.e(mTAG, "PICK_PDF_REQUEST -> Enter 2" + documentsPathsArray.toString());
            }


        } else if (requestCode == PICK_CAMERA_REQUEST_CODE) {

            /*Uri sourceUri = Uri.parse(mCurrentPhotoPath);
            comfun.openCropActivity(mContext, sourceUri, sourceUri, null);*/

            Bitmap mImageBitmap = null;
            Uri tempUri = Uri.parse(mCurrentPhotoPath);
            File finalFile = new File(tempUri.getPath());
            mCurrentPhotoPath = comfun.compressImage(mContext, finalFile.getAbsolutePath(), MAIN_FOLDER + "/Images/");
            documentsPathsArray.add(finalFile.getName());
            imagesadapter.notifyItemInserted(documentsPathsArray.size() - 1);

        } else if (requestCode == PICK_IMAGE) {
            Uri sourceUri = data.getData();
            comfun.openCropActivity(mContext, sourceUri, sourceUri, null);
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                File finalFile = new File(resultUri.getPath());
                documentsPathsArray = new ArrayList<>();
                documentsPathsArray.add(finalFile.getAbsolutePath());

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
        //attachFiles();
    }

    public void deleteImage(int position) {
        if (documentsPathsArray.size() > 0) {
            try {
                documentsPathsArray.remove(position);
                imagesadapter.notifyItemRemoved(position);
            } catch (Exception ex) {
            }
        }

    }

    public void clearFiles() {
        attachmentArray = new ArrayList<>();
    }


    private void attachFiles() {
        for (int i = 0; i < documentsPathsArray.size(); i++) {
            if (!attachmentArray.contains(documentsPathsArray.get(i))) {
                attachmentArray.add(documentsPathsArray.get(i));
            }
        }

        String[] tinyArray = tinyDB.getString(ATTACHMENT_ARRAY).split(",");
        for (String str : tinyArray) {
            if (!attachmentArray.contains(str)) {
                attachmentArray.add(str);
            }
        }

        String fileAttachmentArray = "";
        for (String str : attachmentArray) {
            if (!fileAttachmentArray.contains(str)) {
                fileAttachmentArray = str + "," + fileAttachmentArray;
            }
        }

        //String fileAttachmentArray = tinyDB.getString(ATTACHMENT_ARRAY);
        //fileAttachmentArray = (fileAttachmentArray.equalsIgnoreCase("") ? "" : fileAttachmentArray + ",") + documentsPathsArray.get(i);
        tinyDB.putString(ATTACHMENT_ARRAY, fileAttachmentArray);
        if (mtxtAttachment != null) {
            mtxtAttachment.setText(fileAttachmentArray.split(",").length + " attachments");
            mtxtAttachment.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
        }
    }

    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    String downloadedFileListArray[];

    public void downloadFiles(String[] files, TextView txtPercentage) {


        downloadedFileListArray = files;

        //progressBarStyleHorizontal
        mtxtPercentage = txtPercentage;
        progressBar.setProgress(0);
        fileDownloaded = 0;
        mTotalFiles = files.length;
        for (int i = 0; i < files.length; i++) {
            new DownloadFileFromURL().execute(files[i]);
        }
    }


    TextView mtxtPercentage;
    int fileDownloaded = 0;

    private class DownloadFileFromURL extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //uploadNotification(notificationId,"Download starting","0% downloaded");
            //progressBar.setProgress(0);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mtxtPercentage.setText("Downloading " + (fileDownloaded == 0 ? 1 : fileDownloaded + 1) + " of " + mTotalFiles + ", 0%");
                }
            });


        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(API_BASE_URL + "uploads/emails/" + f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // this will be useful so that you can show a tipical 0-100% progress bar
                int lenghtOfFile = conection.getContentLength();
                Log.e(mTAG, "lenghtOfFile =" + lenghtOfFile);
                // download the file
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                // Output stream
                OutputStream output = new FileOutputStream(Environment.getExternalStorageDirectory().toString() + "/" + Environment.DIRECTORY_DOWNLOADS.toString() + "/" + f_url[0]);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }
            return null;
        }

        /**
         * Updating progress bar
         */
        @Override
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            Log.e(mTAG, "progress=" + progress[0]);
            //notifyUploadProgress(notificationId,"Downloading 1 of "+mTotalFiles,progress[0]+"%",100,Integer.valueOf(progress[0]));
            //progressBar.setProgress(Integer.valueOf(progress[0]));
            //progressBar.setVisibility(View.VISIBLE);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mtxtPercentage.setText("Downloading " + (fileDownloaded == 0 ? 1 : fileDownloaded + 1) + " of " + mTotalFiles + ", " + progress[0] + "%");
                }
            });

        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String str) {

            // dismiss the dialog after the file was downloaded
            //Log.e(mTAG,""+file_url);
            //progressBar.setVisibility(View.GONE);
            //notificationManager.cancel(notificationId);
            fileDownloaded = fileDownloaded + 1;

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //Toast.makeText(mContext, "File(s) downloaded successfully", Toast.LENGTH_SHORT).show();
                    if (fileDownloaded == mTotalFiles) {
                        mtxtPercentage.setText("");
                        openDialog(null);
                    }
                }
            });
        }
    }

    int mTotalFiles = 0;

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    LinearLayout layoutGallery, layoutDocument, layoutCamera;
    AlertDialog alertDialog;

    public void openDialog(String[] arr) {
        alertDialog = null;
        if (downloadedFileListArray == null) {
            downloadedFileListArray = arr;
        }
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        dialogBuilder.setTitle("Open file");
        ListView list = new ListView(mContext);
        list.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, downloadedFileListArray));
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View view, int pos, long id) {
                final String path = Environment.getExternalStorageDirectory().toString() + "/" + Environment.DIRECTORY_DOWNLOADS.toString() + "/" + downloadedFileListArray[pos];
                File file = new File(path);
                //Uri photoURI = FileProvider.getUriForFile(mContext, getApplicationContext().getPackageName() + ".provider", file);
                Uri photoURI = FileProvider.getUriForFile(mContext, APP_PACKAGE_NAME + ".provider", file);
                String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension
                        (MimeTypeMap.getFileExtensionFromUrl(path));
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setType(mimeType);
                intent.setDataAndType(photoURI, mimeType);
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    // handle no application here....
                    Toast.makeText(mContext, "Could not open this file", Toast.LENGTH_SHORT).show();
                }
            }
        });
        dialogBuilder.setView(list);

        alertDialog = dialogBuilder.create();
        alertDialog.show();

    }

    private boolean doubleBackToExitPressedOnce = false;

    public void deleteEntry(HomeWorkModel homeWorkModel) {
        if (doubleBackToExitPressedOnce) {
            showProgress();
            ApiInterface apiService =
                    ApiClient.getClient(mContext).create(ApiInterface.class);

            Call<ApiResponseModel> call = apiService.deleteHomework(homeWorkModel);
            call.enqueue(new Callback<ApiResponseModel>() {
                @Override
                public void onResponse(Call<ApiResponseModel> call, Response<ApiResponseModel> response) {
                    hideProgress();
                    try {
                        //Toast.makeText(mContext, "Deleted!", Toast.LENGTH_SHORT).show();
                        if (!response.body().getStatus().toLowerCase().contains("fail")) {
                            Toast.makeText(mContext, "Deleted!", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(mContext, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                        listSubjects();

                    } catch (Exception ex) {

                    }
                }

                @Override
                public void onFailure(Call<ApiResponseModel> call, Throwable t) {
                    // Log error here since request failed
                    Log.e(mTAG, "error =" + t.toString());
                    Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                }
            });
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        myToast(HomeWorkActivity.this, "double click to delete", Toast.LENGTH_SHORT);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);


    }

    Spinner spinnerWorkHead;
    TextView txtBtnSubmit;
    TextView txtBtnCancel;
    EditText editTextDesc;

    public void openHomeWorkUploadDialog(HomeWorkModel homeWorkModel) {
        alertDialog = null;

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        View customTitleView = getLayoutInflater().inflate(R.layout.title_bar, null);
        TextView title = (TextView) customTitleView.findViewById(R.id.title);
        title.setText("Submit Homework");
        dialogBuilder.setCustomTitle(customTitleView);

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View dialogView = inflater.inflate(R.layout.home_work_assign_layout, null);
        TextView txtHeader = dialogView.findViewById(R.id.txtHeader);

        uploadRecyclerView = dialogView.findViewById(R.id.uploadRecyclerView);
        uploadRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        uploadRecyclerView.setAdapter(imagesadapter);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            txtHeader.setText(Html.fromHtml("Submit Home Work - <font color=#cc0029>" + homeWorkModel.getSubjectName() + "</font>", Html.FROM_HTML_MODE_LEGACY));
        } else {
            txtHeader.setText(Html.fromHtml("Submit Home Work - <font color=#cc0029>" + homeWorkModel.getSubjectName() + "</font>"));
        }


        spinnerWorkHead = dialogView.findViewById(R.id.spinnerWorkHead);
        bindSpinner();

        ImageView imgAttachFile = dialogView.findViewById(R.id.imgAttachFile);
        imgAttachFile.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_att_camera));
        TextView txtAttachment = dialogView.findViewById(R.id.txtAttachment);
        imgAttachFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCamera(CAMERA_METHOD);
            }
        });
        txtAttachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCamera(CAMERA_METHOD);
            }
        });

        mtxtAttachment = txtAttachment;

        TextView txtClearAttachments = dialogView.findViewById(R.id.txtClearAttachments);
        txtClearAttachments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mtxtAttachment.setText("Click here to select files");
                selectFileList = new ArrayList<>();
                tinyDB.putString(ATTACHMENT_ARRAY, "");
                attachmentArray = new ArrayList<>();
                documentsPathsArray = new ArrayList<>();
            }
        });

        editTextDesc = dialogView.findViewById(R.id.editTextDesc);
        //editTextDesc.setVisibility(View.GONE);
        //editTextDesc.setHint("");

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(0, 40, 0, 40);
        LinearLayout layoutAttachment = dialogView.findViewById(R.id.layoutAttachment);
        layoutAttachment.setLayoutParams(params);

        txtBtnSubmit = dialogView.findViewById(R.id.txtBtnSubmit);
        txtBtnCancel = dialogView.findViewById(R.id.txtBtnCancel);

        txtBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        txtBtnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadHomeWork(homeWorkModel);
            }
        });

        dialogBuilder.setView(dialogView);

        alertDialog = dialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();

    }

    String mWorkHead = "";

    private void bindSpinner() {

        homeWorkSubmitHeadList.add("Work Completed");
        //homeWorkSubmitHeadList.add("I want to ask ");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(mContext,
                android.R.layout.simple_spinner_item, homeWorkSubmitHeadList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerWorkHead.setAdapter(dataAdapter);

        spinnerWorkHead.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mWorkHead = String.valueOf(spinnerWorkHead.getSelectedItem());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void uploadHomeWork(HomeWorkModel homeWorkModel) {

        /*if (editTextDesc.getText().toString().trim().length() == 0) {
            myToast(mContext, "Please enter Work Description", Toast.LENGTH_SHORT);
            return;
        }*/

        disableSubmitBtn();
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("CreatedBy", tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
            jsonObject.put("SchoolId", tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
            jsonObject.put("SessionId", tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));
            jsonObject.put("HomeworkId", homeWorkModel.getHomeworkId());
            jsonObject.put("StudentId", tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));

            jsonObject.put("ClassId", homeWorkModel.getClassId());
            jsonObject.put("SubjectId", homeWorkModel.getSubjectId());
            jsonObject.put("SubjectName", homeWorkModel.getSubjectName());
            jsonObject.put("FcmToken", tinyDB.getString("FcmToken"));

        } catch (JSONException e) {
            e.printStackTrace();
        }


        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);


        String ClaimImages = tinyDB.getString(ATTACHMENT_ARRAY);
        String[] claimImagesArry = ClaimImages.split(",");
        MultipartBody.Part[] body;
        //MultipartBody.Part[] body = new MultipartBody.Part[claimImagesArry.length];
        body = new MultipartBody.Part[documentsPathsArray.size()];

        /*int j = 0;
        for (int i = 0; i < claimImagesArry.length; i++) {
            String path = claimImagesArry[i].replace("file:", "");
            if (!path.equalsIgnoreCase("")) {
                File file1 = new File(path);
                RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file1);
                try {
                    body[j] = MultipartBody.Part.createFormData("uploaded_file_" + String.valueOf(j), file1.getName(), requestFile);
                    j++;
                } catch (Exception ex) {
                    Log.e(mTAG, "" + ex.getMessage());
                }
            }
        }*/

        for (int i = 0; i < documentsPathsArray.size(); i++) {
            File file = null;
            if (documentsPathsArray.get(i).toLowerCase().endsWith(".pdf")) {
                file = new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_DOCUMENTS + "/" + MAIN_FOLDER + "/" + "Pdf") + "/" + documentsPathsArray.get(i));
            } else {
                file = new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES + "/" + MAIN_FOLDER + "/" + "Images") + "/" + documentsPathsArray.get(i));
            }

            RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
            try {
                body[i] = MultipartBody.Part.createFormData("uploaded_file_" + i, file.getName(), requestFile);
            } catch (Exception ex) {
                Log.e(mTAG, "" + ex.getMessage());
            }
        }

        RequestBody modelParm = RequestBody.create(MediaType.parse("text/plain"), jsonObject.toString());

        Call<JsonObject> call = apiService.HomeWorkDoneSubmitWithFile(body, modelParm);

        // Call<ApiResponse> call = apiService.PostSave(Desc,CreatedBy);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                //int statusCode = response.code();
                enableSubmitBtn();
                try {
                    Log.e("res", response.body().toString());
                    tinyDB.putString(ATTACHMENT_ARRAY, "");
                    selectFileList = new ArrayList<>();
                    tinyDB.putString(ATTACHMENT_ARRAY, "");
                    attachmentArray = new ArrayList<>();
                    documentsPathsArray = new ArrayList<>();

                    mtxtAttachment.setText("No attachment");

                    alertDialog.dismiss();
                    listSubjects();
                    /*new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            finish();
                        }
                    }, 1000);*/


                } catch (Exception ex) {
                    Log.e(mTAG, "error =" + ex.getMessage());
                    Toast.makeText(mContext, "Something went wrong2!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                // myProgressDialog.hideProgress();
                Toast.makeText(mContext, "Something went wrong1!", Toast.LENGTH_SHORT).show();
                enableSubmitBtn();
            }
        });
    }

    private void enableSubmitBtn() {
        hideProgress();
        txtBtnSubmit.setEnabled(true);
        txtBtnSubmit.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
        txtBtnSubmit.setText("Submit");
    }

    private void disableSubmitBtn() {
        showProgress();
        txtBtnSubmit.setEnabled(false);
        txtBtnSubmit.setText("Please wait");
        txtBtnSubmit.setBackgroundColor(mContext.getResources().getColor(R.color.grey_att));
    }
}
