package com.myliveclassroom.eduethics.Models;

public class FeeModel {

    private String SchoolId;
    private String SessionId;
    private String FeeId;
    private String ClassId;



    private String RollNo;
    private String StudentId;
    private String StudentName;
    private String Mobile;
    private String FatherName;
    private String ClassName;
    private String TotalFee;
    private String TotalPaid;
    private String TotalPending;

    private String Month;
    private String Year;
    private String DueDate;
    private String PaidDate;
    private String IsPaid;
    private String Screen;
    private String TxnId;
    private String TxnStatus;
    private String PaidBy;
    private String Email;

    public void setEmail(String email) {
        Email = email;
    }

    public void setPaidBy(String paidBy) {
        PaidBy = paidBy;
    }

    public String getPaidBy() {
        return PaidBy;
    }

    public String getTxnStatus() {
        return TxnStatus;
    }

    public void setFeeId(String feeId) {
        FeeId = feeId;
    }

    public void setPaidDate(String paidDate) {
        PaidDate = paidDate;
    }


    public void setTxnStatus(String txnStatus) {
        TxnStatus = txnStatus;
    }


    public String getEmail() {
        return Email;
    }

    public void setTxnId(String txnId) {
        TxnId = txnId;
    }

    public String getTxnId() {
        return TxnId;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public String getMobile() {
        return Mobile;
    }

    public String getFatherName() {
        return FatherName;
    }

    public void setScreen(String screen) {
        Screen = screen;
    }

    public String getTotalPaid() {
        return TotalPaid;
    }

    public String getTotalPending() {
        return TotalPending;
    }

    public void setTotalPaid(String totalPaid) {
        TotalPaid = totalPaid;
    }

    public void setTotalPending(String totalPending) {
        TotalPending = totalPending;
    }

    public void setSessionId(String sessionId) {
        SessionId = sessionId;
    }

    public String getFeeId() {
        return FeeId;
    }

    public String getStudentName() {
        return StudentName;
    }

    public String getRollNo() {
        return RollNo;
    }

    public String getClassName() {
        return ClassName;
    }


    public void setClassName(String className) {
        ClassName = className;
    }

    public String getDueDate() {
        return DueDate;
    }

    public String getIsPaid() {
        return IsPaid;
    }

    public String getMonth() {
        return Month;
    }

    public String getTotalFee() {
        return TotalFee;
    }

    public String getYear() {
        return Year;
    }

    public void setDueDate(String dueDate) {
        DueDate = dueDate;
    }

    public void setIsPaid(String isPaid) {
        IsPaid = isPaid;
    }

    public void setMonth(String month) {
        Month = month;
    }

    public void setRollNo(String rollNo) {
        RollNo = rollNo;
    }


    public void setStudentName(String studentName) {
        StudentName = studentName;
    }

    public void setTotalFee(String totalFee) {
        TotalFee = totalFee;
    }

    public void setYear(String year) {
        Year = year;
    }

    public void setSchoolId(String schoolId) {
        SchoolId = schoolId;
    }

    public String getClassId() {
        return ClassId;
    }

    public void setClassId(String classId) {
        ClassId = classId;
    }





    public String getSchoolId() {
        return SchoolId;
    }

    public String getStudentId() {
        return StudentId;
    }

    public void setStudentId(String studentId) {
        StudentId = studentId;
    }

    public String getPaidDate() {
        return PaidDate;
    }
}
