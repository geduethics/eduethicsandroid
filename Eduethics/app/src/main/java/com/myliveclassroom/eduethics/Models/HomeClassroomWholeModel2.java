package com.myliveclassroom.eduethics.Models;

import java.util.List;

public class HomeClassroomWholeModel2 {

    String aboutClass;
    String aboutInstructor;
    String className;
    String subject;
    List<Object> assignments;
    List<Object> studyMaterial;
    List<Object> test;
    List<Object> notice;
    List<Object> timetable;
    List<Object> currentStudents;
    List<Object> pendingStudents;
    String fee;
    String id;
    String teacherId;
    String runningClass = "false";
    boolean onlyTeacherMessage = false;


    public HomeClassroomWholeModel2() {

    }


    public String getRunningClass() {
        return runningClass;
    }

    public void setRunningClass(String runningClass) {
        this.runningClass = runningClass;
    }

    public boolean isOnlyTeacherMessage() {
        return onlyTeacherMessage;
    }

    public void setOnlyTeacherMessage(boolean onlyTeacherMessage) {
        this.onlyTeacherMessage = onlyTeacherMessage;
    }


    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAboutClass() {
        return aboutClass;
    }

    public void setAboutClass(String aboutClass) {
        this.aboutClass = aboutClass;
    }

    public String getAboutInstructor() {
        return aboutInstructor;
    }

    public void setAboutInstructor(String aboutInstructor) {
        this.aboutInstructor = aboutInstructor;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }


    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public List<Object> getAssignments() {
        return assignments;
    }

    public void setAssignments(List<Object> assignments) {
        this.assignments = assignments;
    }

    public List<Object> getStudyMaterial() {
        return studyMaterial;
    }

    public void setStudyMaterial(List<Object> studyMaterial) {
        this.studyMaterial = studyMaterial;
    }

    public List<Object> getTest() {
        return test;
    }

    public void setTest(List<Object> test) {
        this.test = test;
    }

    public List<Object> getNotice() {
        return notice;
    }

    public void setNotice(List<Object> notice) {
        this.notice = notice;
    }

    public List<Object> getTimetable() {
        return timetable;
    }

    public void setTimetable(List<Object> timeTable) {
        this.timetable = timeTable;
    }

    public List<Object> getCurrentStudents() {
        return currentStudents;
    }

    public void setCurrentStudents(List<Object> currentStudents) {
        this.currentStudents = currentStudents;
    }

    public List<Object> getPendingStudents() {
        return pendingStudents;
    }

    public void setPendingStudents(List<Object> pendingStudents) {
        this.pendingStudents = pendingStudents;
    }
}
