package com.myliveclassroom.eduethics.Activity;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.myliveclassroom.eduethics.Adapter.NotificationAdapter;
import com.myliveclassroom.eduethics.DB.DBHelper;
import com.myliveclassroom.eduethics.Models.CircularModel;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.classes.TinyDB;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationActivity extends BaseActivity {

    @BindView(R.id.txtPageTitle)
    TextView txtPageTitle;

    @BindView(R.id.imgBack)
    ImageView imgBack;

    @BindView(R.id.imgbtnAdd)
    ImageView imgbtnAdd;

    @BindView(R.id.btnAdd)
    TextView btnAdd;


    List<CircularModel> circularModelList;
    NotificationAdapter circularAdapter;
    Context mContext;
    RecyclerView recyclerNotification;
    TinyDB tinyDB;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.MyThemeNoBar);
        setContentView(R.layout.activity_notification);

        mContext = this;
        dbHelper = new DBHelper(mContext);
        tinyDB = new TinyDB(this);
        ButterKnife.bind(this);
        init();
    }

    private void Back() {
        super.onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void init() {
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        imgbtnAdd.setVisibility(View.INVISIBLE);
        imgBack.setVisibility(View.VISIBLE);
        btnAdd.setVisibility(View.GONE);
        btnAdd.setText("");


        txtPageTitle.setText(mContext.getString(R.string.home_notifications));
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Back();
            }
        });

        initRecyclerView();
    }

    private void initRecyclerView() {
        circularModelList = new ArrayList<>();
        recyclerNotification = findViewById(R.id.recyclerNotification);
        recyclerNotification.setLayoutManager(new LinearLayoutManager(mContext));
        //getCirculars();
    }

    DBHelper dbHelper;
    private void getNotifications() {
        circularModelList = new ArrayList<>();
        Cursor cursor = dbHelper.getAllData();
        if (cursor.getCount() == 0) {
            return;
        }
        showProgress();
        cursor.moveToFirst();

        while (cursor.isAfterLast() == false) {
            CircularModel circularModel = new CircularModel();
            int totalColumn = cursor.getColumnCount();
            for (int i = 0; i < totalColumn; i++) {
                if (cursor.getColumnName(i) != null) {
                    try {
                        if (cursor.getString(i) != null) {
                            //Log.d("TAG_NAME", cursor.getString(i));
                            //rowObject.put(cursor.getColumnName(i), cursor.getString(i));
                            if (cursor.getColumnName(i).equals("Notification")) {
                                circularModel.setCircularHead(cursor.getString(i));
                            }
                            if (cursor.getColumnName(i).equals("Type")) {
                                circularModel.setCircularType(cursor.getString(i));
                            }
                            if (cursor.getColumnName(i).equals("Date")) {
                                circularModel.setCircularDate(cursor.getString(i));
                            }
                        }
                    } catch (Exception ex) {

                    }
                }
            }
            cursor.moveToNext();
            circularModelList.add(circularModel);
        }
        cursor.close();
        circularAdapter = new NotificationAdapter(mContext, circularModelList);
        recyclerNotification.setAdapter(circularAdapter);
        hideProgress();
    }

    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
        getNotifications();
    }
}
