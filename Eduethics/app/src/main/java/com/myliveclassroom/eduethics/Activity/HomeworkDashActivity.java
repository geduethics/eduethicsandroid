package com.myliveclassroom.eduethics.Activity;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.FileProvider;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.myliveclassroom.eduethics.Adapter.HomeworkDashAdapter;
import com.myliveclassroom.eduethics.Adapter.TimetableAdapterWebApp;
import com.myliveclassroom.eduethics.Models.ApiResponseModel;
import com.myliveclassroom.eduethics.Models.HomeWorkModel;
import com.myliveclassroom.eduethics.Models.TimeTableModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.CommonFunctions;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.API_BASE_URL;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.APP_PACKAGE_NAME;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;

public class HomeworkDashActivity extends BaseActivity {

    List<TimeTableModel> timeTableModelList;
    TimetableAdapterWebApp timetableAdapter;
    Spinner spinnerTimeTable;

    @BindView(R.id.txtPageTitle)
    TextView txtPageTitle;

    @BindView(R.id.imgBack)
    ImageView imgBack;

    @BindView(R.id.imgbtnAdd)
    ImageView imgbtnAdd;

    @BindView(R.id.btnAdd)
    TextView btnAdd;

    @BindView(R.id.txtBtnAssignHomeWork1)
    Button txtBtnAssignHomeWork;

    @BindView(R.id.txtBtnCheckHomeWork)
    Button txtBtnCheckHomeWork;


    @BindView(R.id.recyclerHomeWork)
    RecyclerView recyclerHomeWork;

    @BindView(R.id.scrollView)
    ScrollView scrollView;


    private String mMethodAssign = "Assign";
    private String mMethodView = "View";
    private String mMethod = mMethodAssign;

    private String mClassId, mClassName;
    private Date mSelectedDate;
    private String mDate;
    private String mSubjectId, mSubjectName;
    private ProgressBar progressBar;

    private CaldroidFragment caldroidFragment;
    private CaldroidFragment dialogCaldroidFragment;
    /*List<CalendarModel> calendarList;*/
    SimpleDateFormat formatter, formatter1;
    List<String> disableDates;
    List<HomeWorkModel> homeWorkModelList;
    List<String> timeTableHeads;

    HomeworkDashAdapter homeworkDashAdapter;

    ColorDrawable green;
    ColorDrawable red;
    ColorDrawable blue;
    ColorDrawable grey;
    ColorDrawable yellow;
    ColorDrawable brown;
    ColorDrawable black3;
    ColorDrawable white;

    Date dateToday;
    String dateTodayStr;
    String mDayId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.MyThemeNoBar);
        setContentView(R.layout.activity_homework_dash);

        _BaseActivity(this);
        ButterKnife.bind(this);
        init();

        setAdapterCalendarList();
    }

    private void init() {
        formatter = new SimpleDateFormat("dd MMM yyyy");
        formatter1 = new SimpleDateFormat("dd-MMM-yyyy");
        mSelectedDate = new Date();
        mDate = formatter1.format(mSelectedDate);

        dateToday= new Date();;
        Calendar startDate = Calendar.getInstance();
        dateTodayStr = formatter1.format(startDate.getTime());

        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        white = new ColorDrawable(mContext.getResources().getColor(R.color.white));
        green = new ColorDrawable(mContext.getResources().getColor(R.color.green_att));
        red = new ColorDrawable(mContext.getResources().getColor(R.color.red_att));
        blue = new ColorDrawable(mContext.getResources().getColor(R.color.blue_att));
        grey = new ColorDrawable(mContext.getResources().getColor(R.color.grey_att));
        yellow = new ColorDrawable(mContext.getResources().getColor(R.color.yellow_att));
        brown = new ColorDrawable(mContext.getResources().getColor(R.color.brown_att));
        black3 = new ColorDrawable(mContext.getResources().getColor(R.color.black3_att));

        spinnerTimeTable = findViewById(R.id.spinnerTimeTable);
        imgbtnAdd.setVisibility(View.GONE);
        imgBack.setVisibility(View.VISIBLE);
        btnAdd.setVisibility(View.GONE);
        btnAdd.setText("Add");

        txtPageTitle.setText(R.string.pagetitle_homework);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Back();
            }
        });

        caldroidFragment = new CaldroidFragment();

        Bundle args = new Bundle();
        Calendar cal = Calendar.getInstance();
        args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
        args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
        args.putBoolean(CaldroidFragment.ENABLE_SWIPE, true);
        args.putBoolean(CaldroidFragment.SIX_WEEKS_IN_CALENDAR, true);
        caldroidFragment.setArguments(args);

        // Set todays day
        Calendar c= Calendar.getInstance();
        mDayId=c.get(Calendar.DATE)+"" ;

        initRecyclerView();
        getTeacherTimeTable();
    }

    private void initRecyclerView() {
        homeWorkModelList = new ArrayList<>();
        recyclerHomeWork.setLayoutManager(new LinearLayoutManager(mContext));
        homeworkDashAdapter = new HomeworkDashAdapter(mContext, homeWorkModelList);
        recyclerHomeWork.setAdapter(homeworkDashAdapter);
    }

    //@OnClick(R.id.imgBack)
    private void Back() {
        super.onBackPressed();
    }


    private void setAdapterCalendarList() {
        // Attach to the activity
        disableDates = new ArrayList<>();
        homeWorkModelList = new ArrayList<>();
        //setCustomResourceForDates();

        FragmentTransaction t = getSupportFragmentManager().beginTransaction();
        t.replace(R.id.calendar1, caldroidFragment);
        t.commitAllowingStateLoss();


        // Setup listener
        final CaldroidListener listener = new CaldroidListener() {

            @Override
            public void onSelectDate(Date date, View view) {
                // attendanceList(date);
                //Toast.makeText(getApplicationContext(), formatter.format(date),Toast.LENGTH_SHORT).show();

                caldroidFragment.setBackgroundDrawableForDate(white, mSelectedDate);
                caldroidFragment.setTextColorForDate(R.color.black3, mSelectedDate);

                caldroidFragment.setBackgroundDrawableForDate(blue, date);
                caldroidFragment.setTextColorForDate(R.color.white, date);
                caldroidFragment.refreshView();
                mSelectedDate = date;
                mDate = formatter1.format(date);
                //txtAttendanceStatusHead.setText("Attendance Status : "+mDate);

                Calendar c = Calendar.getInstance();
                c.setTime(mSelectedDate); // yourdate is an object of type Date
                mDayId = c.get(Calendar.DAY_OF_WEEK)+"";

                dateTodayStr=mDate;

                getTeacherTimeTable();
                homeworkList();
            }

            @SuppressLint("WrongConstant")
            @Override
            public void onChangeMonth(int month, int year) {
                //attendanceList(month,year);
            }


            @Override
            public void onLongClickDate(Date date, View view) {
                Toast.makeText(getApplicationContext(),
                        formatter.format(date),
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCaldroidViewCreated() {

                Button leftButton = caldroidFragment.getLeftArrowButton();
                Button rightButton = caldroidFragment.getRightArrowButton();
                TextView textView = caldroidFragment.getMonthTitleTextView();
                // Do customization here
            }
        };
        // Setup Caldroid
        caldroidFragment.setCaldroidListener(listener);
    }

    String mSpinnerSelectedText;

    private void getTeacherTimeTable() { //To list classes

        progressBar.setVisibility(View.VISIBLE);
        TimeTableModel listModel = new TimeTableModel();
        listModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        listModel.setTeacherId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
        listModel.setScheduleDate(dateTodayStr);
        listModel.setDayId("0");

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = apiService.getTeacherTimeTable(listModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                progressBar.setVisibility(View.GONE);
                try {
                    Type type = new TypeToken<List<TimeTableModel>>() {
                    }.getType();

                    timeTableModelList = new Gson().fromJson(response.body().getAsJsonArray("Response"), type);
                    timetableAdapter = new TimetableAdapterWebApp(mContext, timeTableModelList);
                    timeTableHeads = new ArrayList<>();

                    for (TimeTableModel timeTableModel : timeTableModelList) {
                        timeTableHeads.add(timeTableModel.getClassName() +   " - " + CommonFunctions.capitalize(timeTableModel.getSubjectName()) + "  -  " + timeTableModel.getTimeFrom() + "-" + timeTableModel.getTimeTo());
                    }

                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(mContext,
                            android.R.layout.simple_spinner_item, timeTableHeads);
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerTimeTable.setAdapter(dataAdapter);

                    spinnerTimeTable.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            mSpinnerSelectedText = String.valueOf(spinnerTimeTable.getSelectedItem());
                            mClassId = timeTableModelList.get(i).getClassId();
                            mClassName = timeTableModelList.get(i).getClassName();
                            mSubjectId = timeTableModelList.get(i).getSubjectId();
                            mSubjectName = timeTableModelList.get(i).getSubjectName();
                            TextView tv = (TextView) spinnerTimeTable.getSelectedView();
                            tv.setTypeface(Typeface.DEFAULT_BOLD); //to make text bold
                            //tv.setTypeface(Typeface.DEFAULT); //to make text normal
                            homeworkList();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (Exception ex) {
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(mTAG, "error =" + t.toString());
            }
        });
    }

    private void homeworkList() {
        if (mClassId == null   || mDate == null) return;
         progressBar.setVisibility(View.VISIBLE);
        homeWorkModelList = new ArrayList<>();

        HomeWorkModel listModel = new HomeWorkModel();
        listModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));
        listModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        listModel.setClassId(mClassId);
        listModel.setWorkDate(mDate);
        listModel.setSubjectId(mSubjectId);
        listModel.setStudentId("0");

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<List<HomeWorkModel>> call = apiService.HomeWorkList(listModel);
        call.enqueue(new Callback<List<HomeWorkModel>>() {
            @Override
            public void onResponse(Call<List<HomeWorkModel>> call, Response<List<HomeWorkModel>> response) {
                //int statusCode = response.code();
                progressBar.setVisibility(View.GONE);
                try {
                    Log.e(mTAG, "error =" + response.toString());
                    homeWorkModelList.clear();
                    homeWorkModelList = response.body();
                    homeworkDashAdapter = new HomeworkDashAdapter(mContext, homeWorkModelList);
                    recyclerHomeWork.setAdapter(homeworkDashAdapter);
                    scrollView.post(new Runnable() {
                        @Override
                        public void run() {
                            scrollView.scrollTo(0, scrollView.getBottom());
                        }
                    });

                   // txtBtnAssignHomeWork.setVisibility(View.INVISIBLE);
                    txtBtnAssignHomeWork.setEnabled(true);
                    //txtBtnAssignHomeWork.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
                    //txtBtnAssignHomeWork.setTextColor(mContext.getResources().getColor(R.color.white));
                    if (homeWorkModelList.size() > 0) {
                        if (!homeWorkModelList.get(0).getWorkHead().toLowerCase().contains("no work")) {
                            //txtBtnAssignHomeWork.setText("View Homework Done by Students");
                            mMethod = mMethodView;
                        }
                        dateWiseButtonSetting();
                        //TODO
                        //Disable previous date
                        /*else  {

                            mMethod = mMethodAssign;
                        }*/
                    }


                } catch (Exception ex) {
                    Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<List<HomeWorkModel>> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void dateWiseButtonSetting(){
        if(!mDate.equalsIgnoreCase(dateTodayStr) ){

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
            try {
                Date d = sdf.parse(mDate);
                Date datetoday=sdf.parse(dateTodayStr);
                if(d.before(datetoday)){
                    txtBtnAssignHomeWork.setEnabled(false);
                    //txtBtnAssignHomeWork.setText("Can not assign on other day");
                    //txtBtnAssignHomeWork.setBackgroundColor(mContext.getResources().getColor(R.color.grey_color));
                    //txtBtnAssignHomeWork.setTextColor(mContext.getResources().getColor(R.color.black3));
                } else if(d.after(datetoday)){
                    txtBtnAssignHomeWork.setEnabled(true);
                    txtBtnAssignHomeWork.setText("Assign Homework");
                    //txtBtnAssignHomeWork.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
                    //txtBtnAssignHomeWork.setTextColor(mContext.getResources().getColor(R.color.white));
                }
            } catch (ParseException ex) {
                Log.v("Exception", ex.getLocalizedMessage());
            }
        }
        else {
            txtBtnAssignHomeWork.setText("Assign Homework");
        }
    }

    @OnClick(R.id.txtBtnAssignHomeWork1)
    public void txtBtnAssignHomeWork_click() {
        if (mClassId == null || mDate == null) {
            Toast.makeText(mContext, "Please select date", Toast.LENGTH_SHORT).show();
            return;
        }
        mMethod="Assign";
        Intent intent = new Intent(mContext, HomeWorkAsignActivity.class);
        intent.putExtra("ClassId", mClassId);
        intent.putExtra("ClassName", mClassName);
        intent.putExtra("SubjectId", mSubjectId);
        intent.putExtra("SubjectName", mSubjectName);
        intent.putExtra("Date", mDate);
        intent.putExtra("Method", mMethod);
        startActivity(intent);
    }

    @OnClick(R.id.txtBtnCheckHomeWork)
    public void txtBtnCheckHomeWork_click() {
        if (mClassId == null || mDate == null) {
            Toast.makeText(mContext, "Please select date", Toast.LENGTH_SHORT).show();
            return;
        }
        mMethod="View";
        Intent intent = new Intent(mContext, HomeWorkAsignActivity.class);
        intent.putExtra("ClassId", mClassId);
        intent.putExtra("ClassName", mClassName);
        intent.putExtra("SubjectId", mSubjectId);
        intent.putExtra("SubjectName", mSubjectName);
        intent.putExtra("Date", mDate);
        intent.putExtra("Method", mMethod);
        startActivity(intent);
    }


    private boolean doubleBackToExitPressedOnce = false;

    public void deleteEntry(HomeWorkModel homeWorkModel) {
        if (doubleBackToExitPressedOnce) {
            ApiInterface apiService =
                    ApiClient.getClient(mContext).create(ApiInterface.class);

            Call<ApiResponseModel> call = apiService.deleteHomework(homeWorkModel);
            call.enqueue(new Callback<ApiResponseModel>() {
                @Override
                public void onResponse(Call<ApiResponseModel> call, Response<ApiResponseModel> response) {
                    try {
                        //Toast.makeText(mContext, "Deleted!", Toast.LENGTH_SHORT).show();
                        if (!response.body().getStatus().toLowerCase().contains("fail")) {
                            {
                                if (!response.body().getStatus().toLowerCase().contains("error")) {
                                    Toast.makeText(mContext, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                }else{
                                    Toast.makeText(mContext, "Deleted!", Toast.LENGTH_SHORT).show();
                                }
                            }
                            homeworkList();
                        } else {
                            Toast.makeText(mContext, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception ex) {

                    }
                }

                @Override
                public void onFailure(Call<ApiResponseModel> call, Throwable t) {
                    // Log error here since request failed
                    Log.e(mTAG, "error =" + t.toString());
                    Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                }
            });
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        //myToast(mContext, "double click to delete", Toast.LENGTH_SHORT);
        Toast.makeText(mContext, "double click to delete", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);


    }

    @Override
    public void onResume() {
        super.onResume();
        homeworkList();
    }

    @Override
    protected void onDestroy() {
        try {
            super.onDestroy();
        } catch (Exception ex) {

        }
    }


    String downloadedFileListArray[];

    public void downloadFiles(String[] files, TextView txtPercentage) {


        downloadedFileListArray = files;

        //progressBarStyleHorizontal
        mtxtPercentage = txtPercentage;
        progressBar.setProgress(0);
        fileDownloaded = 0;
        mTotalFiles = files.length;
        for (int i = 0; i < files.length; i++) {
            new DownloadFileFromURL().execute(files[i]);
        }
    }


    TextView mtxtPercentage;
    int fileDownloaded = 0;

    private class DownloadFileFromURL extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //uploadNotification(notificationId,"Download starting","0% downloaded");
            //progressBar.setProgress(0);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mtxtPercentage.setText("Downloading " + (fileDownloaded == 0 ? 1 : fileDownloaded + 1) + " of " + mTotalFiles + ", 0%");
                }
            });


        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(API_BASE_URL + "uploads/emails/" + f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // this will be useful so that you can show a tipical 0-100% progress bar
                int lenghtOfFile = conection.getContentLength();
                Log.e(mTAG, "lenghtOfFile =" + lenghtOfFile);
                // download the file
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                // Output stream
                OutputStream output = new FileOutputStream(Environment.getExternalStorageDirectory().toString() + "/" + Environment.DIRECTORY_DOWNLOADS.toString() + "/" + f_url[0]);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }
            return null;
        }

        /**
         * Updating progress bar
         */
        @Override
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            Log.e(mTAG, "progress=" + progress[0]);
            //notifyUploadProgress(notificationId,"Downloading 1 of "+mTotalFiles,progress[0]+"%",100,Integer.valueOf(progress[0]));
            //progressBar.setProgress(Integer.valueOf(progress[0]));
            //progressBar.setVisibility(View.VISIBLE);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mtxtPercentage.setText("Downloading " + (fileDownloaded == 0 ? 1 : fileDownloaded + 1) + " of " + mTotalFiles + ", " + progress[0] + "%");
                }
            });

        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String str) {

            // dismiss the dialog after the file was downloaded
            //Log.e(mTAG,""+file_url);
            //progressBar.setVisibility(View.GONE);
            //notificationManager.cancel(notificationId);
            fileDownloaded = fileDownloaded + 1;

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //Toast.makeText(mContext, "File(s) downloaded successfully", Toast.LENGTH_SHORT).show();
                    if (fileDownloaded == mTotalFiles) {
                        mtxtPercentage.setText("");
                        openDialog(null);
                    }
                }
            });
        }
    }

    int mTotalFiles = 0;

    AlertDialog alertDialog;

    public void openDialog(String[] arr) {
        alertDialog = null;
        if (downloadedFileListArray == null) {
            downloadedFileListArray = arr;
        }
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        dialogBuilder.setTitle("Open file");
        ListView list = new ListView(mContext);
        list.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, downloadedFileListArray));
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View view, int pos, long id) {
                final String path = Environment.getExternalStorageDirectory().toString() + "/" + Environment.DIRECTORY_DOWNLOADS.toString() + "/" + downloadedFileListArray[pos];
                File file = new File(path);
                //Uri photoURI = FileProvider.getUriForFile(mContext, getApplicationContext().getPackageName() + ".provider", file);
                Uri photoURI = FileProvider.getUriForFile(mContext, APP_PACKAGE_NAME+".provider", file);
                String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension
                        (MimeTypeMap.getFileExtensionFromUrl(path));
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setType(mimeType);
                intent.setDataAndType(photoURI, mimeType);
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    // handle no application here....
                    Toast.makeText(mContext, "Could not open this file", Toast.LENGTH_SHORT).show();
                }
            }
        });
        dialogBuilder.setView(list);

        alertDialog = dialogBuilder.create();
        alertDialog.show();

    }
}
