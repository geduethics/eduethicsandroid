package com.myliveclassroom.eduethics.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;


import com.myliveclassroom.eduethics.Fragments.Exam.ViewSubmissionTest;
import com.myliveclassroom.eduethics.Models.HomeWorkModel;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;

import java.util.List;

public class SubmissionAdapter extends RecyclerView.Adapter<SubmissionAdapter.vh> {


    List<HomeWorkModel>  homeWorkModelList;
    Fragment fragment;
    Context mContext;

    public SubmissionAdapter(Fragment fragment, Context context, List<HomeWorkModel>  homeWorkModelList) {
        this.fragment = fragment;
        this.homeWorkModelList=homeWorkModelList;
        mContext=context;
    }

    @NonNull
    @Override
    public vh onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new vh(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_submission, parent, false));
    }

    public void updateList(List<HomeWorkModel> list) {
        homeWorkModelList = list;
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return homeWorkModelList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull vh holder, int position) {
        ((TextView) holder.itemView.findViewById(R.id.name)).setText(homeWorkModelList.get(position).getStudentName());
         if (homeWorkModelList.get(position).getStatus().equalsIgnoreCase(LocalConstants.HOME_WORK_STATUS.UPLOADED)) {
            ((TextView) holder.itemView.findViewById(R.id.feedbackIndication)).setText("Feedback not given");
             holder.itemView.findViewById(R.id.score).setVisibility(View.GONE);
        } else {
            ((TextView) holder.itemView.findViewById(R.id.feedbackIndication)).setText("Feedback Submitted");
             ((TextView) holder.itemView.findViewById(R.id.score)).setText("Score : " + homeWorkModelList.get(position).getScore());
        }

        holder.itemView.findViewById(R.id.evaluateBtn).setOnClickListener(v -> ((ViewSubmissionTest) fragment).evaluate(homeWorkModelList.get(position)));
    }

    class vh extends RecyclerView.ViewHolder {
        public vh(@NonNull View itemView) {
            super(itemView);
        }
    }

}
