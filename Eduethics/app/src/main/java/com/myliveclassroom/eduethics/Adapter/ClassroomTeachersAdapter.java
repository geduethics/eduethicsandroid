package com.myliveclassroom.eduethics.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;


import com.myliveclassroom.eduethics.Activity.MainActivity;
import com.myliveclassroom.eduethics.Fragments.ClassroomTeacherFragment;
import com.myliveclassroom.eduethics.R;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ClassroomTeachersAdapter extends RecyclerView.Adapter<ClassroomTeachersAdapter.vh> {

    List<Map<String, Object>> classrooms;
    Fragment fragment;

    public ClassroomTeachersAdapter(Fragment fragment, List<Map<String, Object>> classrooms) {
        this.classrooms = classrooms;
        this.fragment = fragment;
        MainActivity.ids = null;
        MainActivity.ids = new HashMap<>();
    }

    @NonNull
    @Override
    public vh onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new vh(LayoutInflater.from(fragment.getContext()).inflate(R.layout.classroom_teacher_single_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull vh holder, int position) {
        Map<String, Object> classroom = classrooms.get(position);
        holder.subject.setText(classroom.get("subject").toString());
        holder.classTV.setText(classroom.get("name").toString());
        FirebaseFirestore.getInstance()
                .collection("classroom")
                .document(classrooms.get(position).get(("id")).toString()).get().addOnSuccessListener(documentSnapshot -> {
                    try {
                        holder.totalStudents.setText(((List) documentSnapshot.get("currentStudents")).size() + " Student(s)");
                    } catch (Exception ignored) { }
                });

        int id = View.generateViewId();
        MainActivity.ids.put(id, "cls;"+classrooms.get(position).get(("id"))+";"+classrooms.get(position).get(("name"))+";"+classrooms.get(position).get(("subject")));
        holder.itemView.setId(id);
        fragment.getActivity().registerForContextMenu(holder.itemView);
        holder.itemView.setOnClickListener(v -> ((ClassroomTeacherFragment) fragment).click(position));
    }

    @Override
    public int getItemCount() {
        return classrooms.size();
    }

    static class vh extends RecyclerView.ViewHolder {
        TextView subject, classTV, totalStudents;

        public vh(@NonNull View itemView) {
            super(itemView);
            this.classTV = itemView.findViewById(R.id.classSingleItem);
            this.subject = itemView.findViewById(R.id.subjectSingleItem);
            this.totalStudents = itemView.findViewById(R.id.totalStudents);
        }
    }
}



