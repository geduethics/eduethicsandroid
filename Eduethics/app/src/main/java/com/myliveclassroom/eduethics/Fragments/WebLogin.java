package com.myliveclassroom.eduethics.Fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.myliveclassroom.eduethics.Activity.CapActivity;
import com.myliveclassroom.eduethics.Activity.MainActivity;
import com.myliveclassroom.eduethics.Network.SignalRSingleton;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.google.zxing.integration.android.IntentIntegrator;
import com.microsoft.signalr.HubConnection;
import com.myliveclassroom.eduethics.classes.TinyDB;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;


public class WebLogin extends Fragment {

    private SignalRSingleton mSignalRInstance = new SignalRSingleton();
    HubConnection mHubConnection;
    String mConnectionId;

    Context mContext;
    TinyDB tinyDB;

    public WebLogin( ){
    }
    public WebLogin(Context context){
        mContext=context;
        tinyDB=new TinyDB(mContext);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View root = inflater.inflate(R.layout.fragment_web_login, container, false);
        MainActivity.TO_WHICH_FRAG = LocalConstants.TO_FRAG.TO_DASHBOARD;


        //mHubConnection = mSignalRInstance.getmHubConnection();

        if(tinyDB!=null){
            if(tinyDB.getBoolean("IsExam")) {
                IntentIntegrator scanIntegrator = new IntentIntegrator(getActivity());
                scanIntegrator.setPrompt("Scan QR From Website");
                scanIntegrator.setBeepEnabled(true);

                //scanIntegrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
                scanIntegrator.setCaptureActivity(CapActivity.class);
                scanIntegrator.setOrientationLocked(true);
                scanIntegrator.setBarcodeImageEnabled(true);
                scanIntegrator.initiateScan();
            }
        }

        root.findViewById(R.id.scan).setOnClickListener(v -> {
           IntentIntegrator scanIntegrator = new IntentIntegrator(getActivity());
            scanIntegrator.setPrompt("Scan QR From Website");
            scanIntegrator.setBeepEnabled(true);

            //scanIntegrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
            scanIntegrator.setCaptureActivity(CapActivity.class);
            scanIntegrator.setOrientationLocked(true);
            scanIntegrator.setBarcodeImageEnabled(true);
            scanIntegrator.initiateScan();
           /* if (mHubConnection != null) {
                mHubConnection = mSignalRInstance.getmHubConnection();
                try {
                     mHubConnection.send("Send",  "User1","mesage1");
                    Log.e(mTAG,"SignalR mesage");
                } catch (Exception e) {
                    Log.e(mTAG,"SignalR"+e.getMessage());
                    e.printStackTrace();
                }
            }*/
        });
        //recieveMsg();
        return root;
    }

    private void recieveMsg() {
        try {
            mHubConnection.on("ReceiveMessage", (user, message) ->
            {
                mConnectionId = message;
                Log.e(mTAG, "SignalR | " + message);

            }, String.class, String.class);
        } catch (Exception ex) {
            Log.e(mTAG, ex.getMessage());
        }
    }

}