package com.myliveclassroom.eduethics.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.myliveclassroom.eduethics.Models.UserModel;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.classes.CommonFunctions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BirthdayAdapter extends RecyclerView.Adapter<BirthdayAdapter.ViewHolder> {

    Context mContext;
    List<UserModel> userModelList;

    public BirthdayAdapter(Context context, List<UserModel> userModel) {
        mContext = context;
        userModelList = userModel;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.birthday_adapter, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtStudentName.setText(CommonFunctions.capitalize(userModelList.get(position).getName()));
        holder.txtStudentDetail.setText(userModelList.get(position).getClassName());
        holder.txtBirthdayDate.setText(userModelList.get(position).getDOB());
    }

    @Override
    public int getItemCount() {
        return (userModelList == null ? 0 : userModelList.size());
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        /*@BindView(R.id.imgIcon)
        ImageView imgIcon;*/

        @BindView(R.id.txtStudentName)
        TextView txtStudentName;

        @BindView(R.id.txtStudentDetail)
        TextView txtStudentDetail;

        @BindView(R.id.txtBirthdayDate)
        TextView txtBirthdayDate;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }
}

// extends RecyclerView.Adapter<CircularAdapter.ViewHolder> {