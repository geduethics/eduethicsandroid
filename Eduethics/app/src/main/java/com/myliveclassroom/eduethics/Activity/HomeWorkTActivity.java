package com.myliveclassroom.eduethics.Activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.myliveclassroom.eduethics.Adapter.TimetableAdapterWebApp;
import com.myliveclassroom.eduethics.Models.TimeTableModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.HomeConstants;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;


public class HomeWorkTActivity extends BaseActivity {

    @BindView(R.id.txtPageTitle)
    TextView txtPageTitle;

    @BindView(R.id.imgBack)
    ImageView imgBack;

    @BindView(R.id.imgbtnAdd)
    ImageView imgbtnAdd;

    @BindView(R.id.btnAdd)
    TextView btnAdd;

    RecyclerView recyclerViewTimeTable;
    List<TimeTableModel> timeTableModelList;
    TimetableAdapterWebApp timetableAdapter;


    //@OnClick(R.id.imgBack)
    private void Back() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.MyThemeNoBar);
        setContentView(R.layout.activity_home_work_t);

        ButterKnife.bind(this);
        _BaseActivity(this);
        tinyDB.putString("Screen", HomeConstants.HOMEWORK_TEACHER);

        init();
        initRecyclerView();
    }

    private void init() {
        imgbtnAdd.setVisibility(View.INVISIBLE);
        imgBack.setVisibility(View.VISIBLE);
        btnAdd.setVisibility(View.GONE);
        btnAdd.setText("");
        imgbtnAdd.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_tick3));
        txtPageTitle.setText("Home Work");
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Back();
            }
        });
    }

    private void initRecyclerView() {
        timeTableModelList = new ArrayList<>();
        recyclerViewTimeTable = findViewById(R.id.recyclerViewTimeTable);
        recyclerViewTimeTable.setLayoutManager(new LinearLayoutManager(mContext));
        timetableAdapter = new TimetableAdapterWebApp(mContext, timeTableModelList);
        recyclerViewTimeTable.setAdapter(timetableAdapter);
        getTimeTable();
    }

    private void getTimeTable() {
        TimeTableModel listModel = new TimeTableModel();
        listModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        listModel.setTeacherId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = apiService.getTeacherTimeTable(listModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                try {
                    Type type = new TypeToken<List<TimeTableModel>>() {
                    }.getType();

                    timeTableModelList = new Gson().fromJson(response.body().getAsJsonArray("Response"), type);
                    timetableAdapter = new TimetableAdapterWebApp(mContext, timeTableModelList);
                    recyclerViewTimeTable.setAdapter(timetableAdapter);

                } catch (Exception ex) {
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(mTAG, "error =" + t.toString());
            }
        });
    }


}
