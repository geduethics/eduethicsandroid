package com.myliveclassroom.eduethics.Activity;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.myliveclassroom.eduethics.Adapter.HomeWorkDoneStudentAdapter;
import com.myliveclassroom.eduethics.Adapter.UploadImagesAdapter;
import com.myliveclassroom.eduethics.Models.ApiResponseModel;
import com.myliveclassroom.eduethics.Models.ExamModel;
import com.myliveclassroom.eduethics.Models.HomeWorkModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.CommonFunctions;
import com.google.gson.JsonObject;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.API_BASE_URL;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.APP_PACKAGE_NAME;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.ATTACHMENT_ARRAY;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.CAMERA_METHOD;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.GALLERY_METHOD;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.MAIN_FOLDER;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.PICK_CAMERA_REQUEST_CODE;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.PICK_GALLERY_REQUEST_CODE;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.PICK_PDF_REQUEST_CODE;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.TINY_CONSTANT_HOMEWORK_DONE_STATUS;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.TINY_CONSTANT_HomeworkHead;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.PROFILE;
import static com.myliveclassroom.eduethics.classes.CommonFunctions.createImageFileNew;
import static com.myliveclassroom.eduethics.classes.CommonFunctions.getBytes;
import static com.myliveclassroom.eduethics.classes.CommonFunctions.getImageUri;
import static com.myliveclassroom.eduethics.classes.CommonFunctions.getPath;
import static com.myliveclassroom.eduethics.classes.CommonFunctions.saveFile;
import static com.myliveclassroom.eduethics.classes.CommonFunctions.writeToFile;


public class HomeWorkAsignActivity extends BaseActivity {
    //NotificationCenter notificationCenter;


    @BindView(R.id.txtPageTitle)
    TextView txtPageTitle;

    @BindView(R.id.imgBack)
    ImageView imgBack;

    @BindView(R.id.imgbtnAdd)
    ImageView imgbtnAdd;

    @BindView(R.id.btnAdd)
    TextView btnAdd;

    @BindView(R.id.txtClass)
    TextView txtClass;

    @BindView(R.id.txtSubject)
    TextView txtSubject;

    @BindView(R.id.txtDate)
    TextView txtDate;

    ProgressBar progressBar;
    ProgressBar progressBarAlert;

    @BindView(R.id.layoutAssignWork)
    RelativeLayout layoutAssignWork;

    @BindView(R.id.layoutHeader)
    LinearLayout layoutHeader;

    @BindView(R.id.recyclerViewHomeWork)
    RecyclerView recyclerViewHomeWork;


    private String mClassId, mSubjectId, mSubjectName, mClassName;
    private String mDate;
    String mWorkHead;
    String mMethod;
    private String mMethodAssign = "Assign";
    private String mMethodView = "View";

    List<String> homeWorkHeadList = new ArrayList<String>();
    List<String> homeWorkDoneStatusList = new ArrayList<String>();
    List<HomeWorkModel> homeWorkModelList;

    ImageView imgAttachFile;
    TextView txtAttachment, txtClearAttachments, txtBtnCancel, txtBtnSubmit;
    EditText editTextDesc;

    LinearLayout layoutDocument, layoutCamera,layoutGallery;
    AlertDialog alertDialog;

    HomeWorkDoneStudentAdapter homeWorkDoneStudentAdapter;


    List<String> selectFileList = new ArrayList<>();
    ArrayList<String> documentsPathsArray=new ArrayList<>(); // this is the object where files stored
    ArrayList<String> attachmentArray = new ArrayList<>();
    UploadImagesAdapter imagesadapter = new UploadImagesAdapter(documentsPathsArray, HomeWorkAsignActivity.this, LocalConstants.FROM.FROM_HOMEWORK_ACTVITY, null);
    RecyclerView uploadRecyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.MyThemeNoBar);
        setContentView(R.layout.activity_home_work_asign);
        ButterKnife.bind(this);
        _BaseActivity(this);
        init();
    }

    private void init() {
        //notificationCenter = new NotificationCenter(mContext);


        imagesadapter.setProgress(findViewById(R.id.progress));

        uploadRecyclerView = findViewById(R.id.uploadRecyclerView);
        uploadRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        uploadRecyclerView.setAdapter(imagesadapter);


        tinyDB.putString(ATTACHMENT_ARRAY, "");
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        mClassId = getIntent().getExtras().getString("ClassId");
        mClassName = getIntent().getExtras().getString("ClassName");
        mDate = getIntent().getExtras().getString("Date");
        mSubjectId = getIntent().getExtras().getString("SubjectId");
        mSubjectName = getIntent().getExtras().getString("SubjectName");
        mMethod = getIntent().getExtras().getString("Method");

        txtClass.setText(getIntent().getExtras().getString("ClassName") );
        txtSubject.setText(CommonFunctions.capitalize(mSubjectName));

        txtDate.setText(getIntent().getExtras().getString("Date"));
        tinyDB.putString("AttDate", getIntent().getExtras().getString("Date"));


        imgbtnAdd.setVisibility(View.GONE);
        imgBack.setVisibility(View.VISIBLE);
        btnAdd.setVisibility(View.GONE);
        btnAdd.setText("");
        //imgbtnAdd.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_tick3));
        txtPageTitle.setText(mContext.getResources().getString(R.string.pagetitle_homeworkAssign));
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Back();
            }
        });

        imgbtnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(mContext, "test from frag", Toast.LENGTH_SHORT).show();
            }
        });

        String[] heads = tinyDB.getString(TINY_CONSTANT_HomeworkHead).split(",");
        for (String s : heads) {
            homeWorkHeadList.add(s);
        }
        String[] headStatus = tinyDB.getString(TINY_CONSTANT_HOMEWORK_DONE_STATUS).split(",");
        for (String s : headStatus) {
            homeWorkDoneStatusList.add(s);
        }


        //Included layout
        if (mMethod.equalsIgnoreCase(mMethodAssign)) {
            layoutAssignWork.setVisibility(View.VISIBLE);
        } else {
            layoutAssignWork.setVisibility(View.GONE);
        }
        txtClearAttachments = findViewById(R.id.txtClearAttachments);
        txtAttachment = findViewById(R.id.txtAttachment);
        imgAttachFile = findViewById(R.id.imgAttachFile);
        txtBtnCancel = findViewById(R.id.txtBtnCancel);
        txtBtnSubmit = findViewById(R.id.txtBtnSubmit);
        editTextDesc = findViewById(R.id.editTextDesc);
        bindSpinner();

        imgAttachFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //comfun.open_camera(mContext,101);
                popWindowAtt();
            }
        });

        txtAttachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //comfun.open_camera(mContext,101);
                popWindowAtt();
            }
        });

        txtBtnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(mContext)
                        .setTitle("Homework assign confirmation")
                        .setMessage("Are you sure to assign homework?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                uploadHomeWork();
                            }})
                        .setNegativeButton(android.R.string.no, null).show();
            }
        });
        txtBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (mMethod.equalsIgnoreCase(mMethodView)) {
            layoutHeader.setVisibility(View.VISIBLE);
            initRecyclerView();
        }
    }

    private void initRecyclerView() {
        homeWorkModelList = new ArrayList<>();
        recyclerViewHomeWork.setLayoutManager(new LinearLayoutManager(mContext));
        homeWorkDoneStudentAdapter = new HomeWorkDoneStudentAdapter(mContext, homeWorkModelList);
        recyclerViewHomeWork.setAdapter(homeWorkDoneStudentAdapter);

        getStudentList();
    }

    private void bindSpinner() {
        Spinner spinnerWorkHead = findViewById(R.id.spinnerWorkHead);

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(mContext,
                android.R.layout.simple_spinner_item, homeWorkHeadList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerWorkHead.setAdapter(dataAdapter);

        spinnerWorkHead.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mWorkHead = String.valueOf(spinnerWorkHead.getSelectedItem());

                if (mWorkHead.toLowerCase().contains("youtube")) {

                    editTextDesc.setHint("Paste Youtube video link here");
                } else {
                    editTextDesc.setHint("Description");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    //@OnClick(R.id.imgBack)
    private void Back() {
        super.onBackPressed();
    }

    public void popWindowAtt() {
        alertDialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View dialogView = inflater.inflate(R.layout.file_attachment_layout, null);
        dialogBuilder.setView(dialogView);


        layoutGallery = (LinearLayout) dialogView.findViewById(R.id.layoutGallery);
        layoutCamera = (LinearLayout) dialogView.findViewById(R.id.layoutCamera);
        layoutDocument = (LinearLayout) dialogView.findViewById(R.id.layoutDocument);
        ImageView imgBtnCloseDialog = dialogView.findViewById(R.id.imgBtnCloseDialog);

        imgBtnCloseDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        layoutCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //openCamera(CAMERA_METHOD);

                if (ContextCompat.checkSelfPermission(HomeWorkAsignActivity.this, Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_DENIED)
                    ActivityCompat.requestPermissions(HomeWorkAsignActivity.this, new String[]{Manifest.permission.CAMERA}, PICK_CAMERA_REQUEST_CODE);
                else {
                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    /***************************************
                     *      Custom file name for Camera file
                     ***************************************/
                    File photoFile = null;
                    try {
                        photoFile = createImageFileNew(LocalConstants.FILE_TYPE.JPG);
                        mCurrentPhotoPath = photoFile.getAbsolutePath();
                    } catch (IOException ex) {
                        // Error occurred while creating the File
                        Log.i(mTAG, "IOException");
                    }
                    // Continue only if the File was successfully created
                    if (photoFile != null) {
                        Uri imageUri = FileProvider.getUriForFile(
                                mContext,
                                APP_PACKAGE_NAME + ".provider",
                                photoFile);
                        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                        startActivityForResult(cameraIntent, PICK_CAMERA_REQUEST_CODE);
                    }
                }
                alertDialog.dismiss();
            }
        });

        layoutGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* openCamera(GALLERY_METHOD);*/
                //Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                Intent intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(intent, PICK_GALLERY_REQUEST_CODE);
                alertDialog.dismiss();

            }
        });

        layoutDocument.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDocExplorer();
                alertDialog.dismiss();

            }
        });

        alertDialog = dialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    public void openCamera(String method) {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public void openDocExplorer() {
        Intent intent = new Intent();
        intent.setType("application/pdf");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        startActivityForResult(Intent.createChooser(intent, "Select Pdf"), PICK_PDF_REQUEST_CODE);
    }




    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case PICK_PDF_REQUEST_CODE:
                if (data == null) return;
                File pdfFile = null;
                try {
                    pdfFile = createImageFileNew(LocalConstants.FILE_TYPE.PDF);
                } catch (IOException ex) {
                    // Error occurred while creating the File
                    Log.i(mTAG, "IOException");
                }

                Uri uri = data.getData();
                byte[] inputData = null;
                InputStream iStream = null;
                try {
                    iStream = mContext.getContentResolver().openInputStream(uri);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                try {
                    inputData = getBytes(iStream);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    writeToFile(inputData, pdfFile.getAbsolutePath());
                } catch (IOException e) {
                    e.printStackTrace();
                }

                documentsPathsArray.add(pdfFile.getName());
                imagesadapter.notifyItemInserted(documentsPathsArray.size() - 1);
                break;
            case PICK_GALLERY_REQUEST_CODE:
                if (data == null) return;
                //*************OK**************
                if (requestCode == PICK_GALLERY_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
                    if (data == null) {
                        return;
                    }
                    Uri selectedImageUri = data.getData();
                    if (null != selectedImageUri) {
                        Uri imageUri = data.getData();
                        Bitmap bitmap = null;
                        try {
                            bitmap = MediaStore.Images.Media.getBitmap(mContext.getContentResolver(), imageUri);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        Uri u = getImageUri(mContext, bitmap);
                        File finalFile = null;
                        File file = new File(getPath(mContext, u));

                        file = saveFile(file, LocalConstants.FILE_TYPE.JPG);

                        documentsPathsArray.add(file.getName());
                        imagesadapter.notifyItemInserted(documentsPathsArray.size() - 1);
                    }
                }

                break;
            case PICK_CAMERA_REQUEST_CODE:
                try {
                    Bitmap mImageBitmap = null;
                    Uri tempUri = Uri.parse(mCurrentPhotoPath);
                    File finalFile = new File(tempUri.getPath());
                    mCurrentPhotoPath = comfun.compressImage(mContext, finalFile.getAbsolutePath(), MAIN_FOLDER + "/Images/");
                    documentsPathsArray.add(finalFile.getName());
                    imagesadapter.notifyItemInserted(documentsPathsArray.size() - 1);
                } catch (Exception ex) {
                    Log.e(mTAG, ex.getMessage());
                }
                break;
        }
    }

    public void deleteImage(int position) {
        documentsPathsArray.remove(position);
        imagesadapter.notifyItemRemoved(position);
    }

   /* @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_CANCELED) {
            return;
        }
        if (requestCode == PICK_PDF_REQUEST_CODE && resultCode == Activity.RESULT_OK) {


            File pdfFile = null;
            try {
                pdfFile = createImageFileNew(LocalConstants.FILE_TYPE.PDF);
            } catch (IOException ex) {
                // Error occurred while creating the File
                Log.i(mTAG, "IOException");
            }

            Uri uri = data.getData();
            byte[] inputData = null;
            InputStream iStream = null;
            try {
                iStream = mContext.getContentResolver().openInputStream(uri);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            try {
                inputData = getBytes(iStream);
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                writeToFile(inputData, pdfFile.getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (!documentsPathsArray.contains(pdfFile.getName())) {
                documentsPathsArray.add(pdfFile.getName());
            }

            //Single file upload code --OK
            *//*if (data.getData() != null) {
                Uri docUri = data.getData();
                if (!selectFileList.contains(docUri.getPath())) {
                    selectFileList.add(docUri.getPath());
                    File f = null;
                    try {
                        f = comfun.saveFile(mContext, docUri, "", MAIN_FOLDER, "doc_");
                        mCurrentPhotoPath = f.getAbsolutePath();
                        if (!documentsPathsArray.contains(mCurrentPhotoPath)) {
                            documentsPathsArray.add(mCurrentPhotoPath);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            } else {
                documentsPathsArray = new ArrayList<>();
                ClipData clipData = data.getClipData();
                if (clipData != null) {
                    for (int i = 0; i < clipData.getItemCount(); i++) {
                        ClipData.Item item = clipData.getItemAt(i);
                        Uri uri = item.getUri();

                        if (!selectFileList.contains(uri.getPath())) {
                            selectFileList.add(uri.getPath());

                            *//**//*******OK if rename and loction change required*******//**//*
                            File file;
                            try {
                                file = comfun.saveFile(mContext, uri, "", MAIN_FOLDER, "doc_");
                                String path = file.getAbsolutePath();
                                if (!documentsPathsArray.contains(path)) {
                                    documentsPathsArray.add(path);
                                }
                            } catch (IOException e) {
                                Log.e(mTAG, e.getMessage());
                                e.printStackTrace();
                            }
                        }
                    }
                }
                Log.e(mTAG, "PICK_PDF_REQUEST -> Enter 2" + documentsPathsArray.toString());
            }*//*


        } else if (requestCode == MY_CAMERA_PERMISSION_CODE) {
            Uri sourceUri = Uri.parse(mCurrentPhotoPath);
            comfun.openCropActivity(mContext, sourceUri, sourceUri, null);
        } else if (requestCode == PICK_IMAGE) {
            Uri sourceUri = data.getData();
            comfun.openCropActivity(mContext, sourceUri, sourceUri, null);
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                File finalFile = new File(resultUri.getPath());

                finalFile = saveFile(finalFile, LocalConstants.FILE_TYPE.JPG);

                if (!documentsPathsArray.contains(finalFile.getName())) {
                    documentsPathsArray.add(finalFile.getName());
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
         attachFiles();
    }*/

    public void clearFiles() {
        attachmentArray = new ArrayList<>();
    }

    private void attachFiles() {
        for (int i = 0; i < documentsPathsArray.size(); i++) {
            if (!attachmentArray.contains(documentsPathsArray.get(i))) {
                attachmentArray.add(documentsPathsArray.get(i));
            }
        }
        String[] tinyArray = tinyDB.getString(ATTACHMENT_ARRAY).split(",");
        for (String str : tinyArray) {
            if (!attachmentArray.contains(str)) {
                attachmentArray.add(str);
            }
        }
        String fileAttachmentArray = "";
        for (String str : attachmentArray) {
            if (!fileAttachmentArray.contains(str)) {
                fileAttachmentArray = str + "," + fileAttachmentArray;
            }
        }
        //String fileAttachmentArray = tinyDB.getString(ATTACHMENT_ARRAY);
        //fileAttachmentArray = (fileAttachmentArray.equalsIgnoreCase("") ? "" : fileAttachmentArray + ",") + documentsPathsArray.get(i);
        tinyDB.putString(ATTACHMENT_ARRAY, fileAttachmentArray);

        txtAttachment.setText(fileAttachmentArray.split(",").length + " attachments");
        txtAttachment.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
    }

    private boolean mContinue=false;
    private void uploadHomeWork() {

        if (editTextDesc.getText().toString().trim().length() == 0) {
            myToast(mContext, "Please enter Work Description", Toast.LENGTH_SHORT);
            return;
        }

        disableSubmitBtn();

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("CreatedBy", tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
            jsonObject.put("SchoolId", tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
            jsonObject.put("SessionId", tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));
            jsonObject.put("ClassId", mClassId);
            jsonObject.put("SubjectId", mSubjectId);
            jsonObject.put("TeacherId", tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));

            jsonObject.put("WorkHead", mWorkHead);
            jsonObject.put("WorkDetail", editTextDesc.getText().toString());
            jsonObject.put("WorkDate", mDate);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);


        String ClaimImages = tinyDB.getString(ATTACHMENT_ARRAY);
        String[] claimImagesArry = ClaimImages.split(",");
        MultipartBody.Part[] body;//= new MultipartBody.Part[claimImagesArry.length];

       // int j = 0;
        /*for (int i = 0; i < claimImagesArry.length; i++) {
            String path = claimImagesArry[i].replace("file:", "");
            if (!path.equalsIgnoreCase("")) {
                File file1 = new File(path);
                RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file1);
                try {
                    body[j] = MultipartBody.Part.createFormData("uploaded_file_" + String.valueOf(j), file1.getName(), requestFile);
                    j++;
                } catch (Exception ex) {
                    Log.e(mTAG, "" + ex.getMessage());
                }
            }
        }*/

        body = new MultipartBody.Part[documentsPathsArray.size()];
        //attaching images to multipart
        for (int i = 0; i < documentsPathsArray.size(); i++) {
            File file =null;
            if (documentsPathsArray.get(i).toLowerCase().endsWith(".pdf")) {
                file=new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_DOCUMENTS + "/" + MAIN_FOLDER + "/" + "Pdf")+"/"+documentsPathsArray.get(i));
            }
            else{
                file=new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES + "/" + MAIN_FOLDER + "/" + "Images")+"/"+documentsPathsArray.get(i));
            }

            RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
            try {
                body[i] = MultipartBody.Part.createFormData("uploaded_file_" +i, file.getName(), requestFile);
            } catch (Exception ex) {
                Log.e(mTAG, "" + ex.getMessage());
            }
        }

        RequestBody modelParm = RequestBody.create(MediaType.parse("text/plain"), jsonObject.toString());

        Call<JsonObject> call = apiService.HomeWorkSubmitWithFile(body, modelParm);

        // Call<ApiResponse> call = apiService.PostSave(Desc,CreatedBy);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                //int statusCode = response.code();
                enableSubmitBtn();
                try {
                    Log.e("res", response.body().toString());
                    tinyDB.putString(ATTACHMENT_ARRAY, "");
                    txtAttachment.setText("No attachment");

                   new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            finish();
                        }
                    }, 1000);


                } catch (Exception ex) {
                    Log.e(mTAG, "error =" + ex.getMessage());
                    Toast.makeText(mContext, "Something went wrong2!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                // myProgressDialog.hideProgress();
                Toast.makeText(mContext, "Something went wrong1!", Toast.LENGTH_SHORT).show();
                enableSubmitBtn();
            }
        });
    }

    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    private void enableSubmitBtn() {
        hideProgress();
        txtBtnSubmit.setEnabled(true);
        txtBtnSubmit.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
        txtBtnSubmit.setText("Submit");
    }

    private void disableSubmitBtn() {
        showProgress();
        txtBtnSubmit.setEnabled(false);
        txtBtnSubmit.setText("Please wait");
        txtBtnSubmit.setBackgroundColor(mContext.getResources().getColor(R.color.grey_att));
    }

    private void getStudentList() {
        progressBar.setVisibility(View.VISIBLE);
        HomeWorkModel homeWorkModel = new HomeWorkModel();

        homeWorkModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        homeWorkModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));
        homeWorkModel.setClassId(mClassId);
        homeWorkModel.setWorkDate(mDate);
        homeWorkModel.setSubjectId(mSubjectId);
        homeWorkModel.setProfileType(tinyDB.getString(PROFILE));

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = apiService.HomeWorkDoneList(homeWorkModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                progressBar.setVisibility(View.GONE);
                try {
                    //TODO
                    // get data from json object is pending
                    //
                    //homeWorkModelList = response.body();

                    Type type = new TypeToken<List<HomeWorkModel>>() {
                    }.getType();
                    homeWorkModelList = new Gson().fromJson(response.body().getAsJsonArray("Response"), type);

                    homeWorkDoneStudentAdapter = new HomeWorkDoneStudentAdapter(mContext, homeWorkModelList);
                    recyclerViewHomeWork.setAdapter(homeWorkDoneStudentAdapter);

                } catch (Exception ex) {
                    Log.e(mTAG, "error =" + ex.getMessage());
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(mTAG, "error =" + t.toString());
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    String doneattachments;
    String doneattachmentArray[] = {};
    Spinner spinnerWorkStatus;
    String mWorkStatus = "";
    EditText editTextDescStatus;
    TextView txtBtnSubmitAlert;
    TextView txtBtnCancelAlert;


    LinearLayout layoutImages;

    public void viewUploadedWork(HomeWorkModel homeWorkModel) {
        alertDialog = null;

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View dialogView = inflater.inflate(R.layout.home_work_check_layout, null);
        doneattachments = homeWorkModel.getDoneAttachment();
        if (doneattachments != null && !doneattachments.isEmpty()) {
            doneattachmentArray = doneattachments.split(",");
        }
        layoutImages = dialogView.findViewById(R.id.layoutImages);
        TableLayout tableAttachments = dialogView.findViewById(R.id.tableAttachments);
        int sr = 0;
        for (String str : doneattachmentArray) {
            sr = sr + 1;
            final View rowLayout = inflater.inflate(R.layout.file_table_row2, null);
            TextView txtSr = rowLayout.findViewById(R.id.txtSr);
            txtSr.setText(sr + ".");
            TextView txtFileName = rowLayout.findViewById(R.id.txtFileName);
            txtFileName.setText(str);
            TextView txtDownloadFile=rowLayout.findViewById(R.id.txtDownloadFile);
            ImageView imgDownload = rowLayout.findViewById(R.id.imgDownload);
            /*Glide.with(mContext)
                    .asBitmap()
                    .load(api_MainUrl + "Uploads/" + str)
                    .into(imgDownload);*/
            imgDownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    downloadFiles(str, imgDownload,txtDownloadFile);
                }
            });

            TableRow trAttachment = (TableRow) rowLayout;
            tableAttachments.addView(trAttachment);
        }
        //******************Attachment List End*********************

        spinnerWorkStatus = dialogView.findViewById(R.id.spinnerWorkHead);
        bindStatusSpinner();

        editTextDescStatus = dialogView.findViewById(R.id.editTextDesc);
        txtBtnSubmitAlert = dialogView.findViewById(R.id.txtBtnSubmit);
        txtBtnCancelAlert = dialogView.findViewById(R.id.txtBtnCancel);

        txtBtnSubmitAlert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitWorkStatus(homeWorkModel);
            }
        });

        txtBtnCancelAlert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        progressBarAlert = dialogView.findViewById(R.id.progressBar);
        progressBarAlert.setVisibility(View.GONE);
        dialogBuilder.setView(dialogView);
        alertDialog = dialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    private void bindStatusSpinner() {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(mContext,
                android.R.layout.simple_spinner_item, homeWorkDoneStatusList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerWorkStatus.setAdapter(dataAdapter);

        spinnerWorkStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mWorkStatus = String.valueOf(spinnerWorkStatus.getSelectedItem());

                if (i == 0 || i == 1) {
                    editTextDescStatus.setVisibility(View.GONE);
                } else {
                    editTextDescStatus.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void submitWorkStatus(HomeWorkModel homeWorkModel) {
        disableAlertSubmit();
        homeWorkModel.setStatus(mWorkStatus);
        homeWorkModel.setWorkDetail(editTextDescStatus.getText().toString()); //This will go in remarks

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<ApiResponseModel> call = apiService.HomeWorStausSubmit(homeWorkModel);
        call.enqueue(new Callback<ApiResponseModel>() {
            @Override
            public void onResponse(Call<ApiResponseModel> call, Response<ApiResponseModel> response) {
                enableAlertSubmit();
                try {
                    ApiResponseModel apiResponseModel = response.body();
                    alertDialog.dismiss();
                    getStudentList();

                } catch (Exception ex) {
                    Log.e(mTAG, "error =" + ex.getMessage());
                    enableAlertSubmit();
                }
            }

            @Override
            public void onFailure(Call<ApiResponseModel> call, Throwable t) {
                Log.e(mTAG, "error =" + t.toString());
                enableAlertSubmit();
            }
        });
    }

    private void enableAlertSubmit() {
        progressBarAlert.setVisibility(View.GONE);
        txtBtnSubmitAlert.setBackgroundColor(mContext.getResources().getColor(R.color.design_default_color_primary_dark));
        txtBtnSubmitAlert.setTextColor(mContext.getResources().getColor(R.color.white));
        txtBtnSubmitAlert.setEnabled(false);
        txtBtnCancelAlert.setEnabled(false);
    }

    private void disableAlertSubmit() {
        txtBtnSubmitAlert.setBackgroundColor(mContext.getResources().getColor(R.color.grey_color));
        txtBtnSubmitAlert.setTextColor(mContext.getResources().getColor(R.color.black3));
        txtBtnSubmitAlert.setEnabled(false);
        txtBtnCancelAlert.setEnabled(false);
        progressBarAlert.setVisibility(View.VISIBLE);
    }

    String downloadedFileListArray[];

    public void downloadFiles(String files, ImageView destination,TextView txtDownloadFile) {
        new DownloadFileFromURL(destination,txtDownloadFile).execute(files);
    }

    private class DownloadFileFromURL extends AsyncTask<String, String, String> {

        ImageView _imgDestination;
        TextView txtDownloadFile;
        String _imgSrc;

        public DownloadFileFromURL(ImageView imgDestination,TextView _txtDownloadFile) {
            _imgDestination = imgDestination;
            txtDownloadFile=_txtDownloadFile;
        }

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //uploadNotification(notificationId,"Download starting","0% downloaded");
            //progressBar.setProgress(0);
            progressBarAlert.setVisibility(View.VISIBLE);
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                _imgSrc=f_url[0];
                URL url = new URL(API_BASE_URL + "uploads/emails/" + f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // this will be useful so that you can show a tipical 0-100% progress bar
                int lenghtOfFile = conection.getContentLength();
                Log.e(mTAG, "lenghtOfFile =" + lenghtOfFile);
                // download the file
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                // Output stream
                OutputStream output = new FileOutputStream(Environment.getExternalStorageDirectory().toString() + "/" + Environment.DIRECTORY_DOWNLOADS.toString() + "/" + f_url[0]);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }
            return null;
        }

        /**
         * Updating progress bar
         */
        @Override
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            Log.e(mTAG, "progress=" + progress[0]);
        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String str) {
            progressBarAlert.setVisibility(View.GONE);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //Toast.makeText(mContext, "File(s) downloaded successfully", Toast.LENGTH_SHORT).show();
                    //Insert into Sqlite

                    Glide.with(mContext)
                            .load(Environment.getExternalStorageDirectory().toString() + "/" + Environment.DIRECTORY_DOWNLOADS.toString() + "/" + _imgSrc)
                            .override(200, 200)
                            .into(_imgDestination);
                    _imgDestination.setVisibility(View.GONE);
                    txtDownloadFile.setVisibility(View.VISIBLE);
                    txtDownloadFile.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final String path = Environment.getExternalStorageDirectory().toString() + "/" + Environment.DIRECTORY_DOWNLOADS.toString() + "/" + _imgSrc;
                            File file = new File(path);
                            //Uri photoURI = FileProvider.getUriForFile(mContext, getApplicationContext().getPackageName() + ".provider", file);
                            Uri photoURI = FileProvider.getUriForFile(mContext, APP_PACKAGE_NAME+".provider", file);
                            String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension
                                    (MimeTypeMap.getFileExtensionFromUrl(path));
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setType(mimeType);
                            intent.setDataAndType(photoURI, mimeType);
                            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            try {
                                startActivity(intent);
                            } catch (ActivityNotFoundException e) {
                                // handle no application here....
                                Toast.makeText(mContext, "Could not open this file", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                }
            });
        }
    }
}
