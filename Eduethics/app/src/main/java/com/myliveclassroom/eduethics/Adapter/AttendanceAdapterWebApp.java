package com.myliveclassroom.eduethics.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.myliveclassroom.eduethics.Models.TimeTableModel;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.CommonFunctions;
import com.myliveclassroom.eduethics.classes.TinyDB;

import java.util.ArrayList;
import java.util.List;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;


public class AttendanceAdapterWebApp extends RecyclerView.Adapter<AttendanceAdapterWebApp.ViewHolder> {

    List<TimeTableModel> timeTableModelList;
    Context mContext;
    List<String> listAttTags = new ArrayList<String>();
    TinyDB tinyDB;

    public AttendanceAdapterWebApp(Context context, List<TimeTableModel> list) {
        timeTableModelList = list;
        mContext = context;
        tinyDB=new TinyDB(mContext);

        listAttTags.add("P");
        listAttTags.add("A");
        listAttTags.add("L");
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.attendance_mark_adp_view, parent, false);
        return new AttendanceAdapterWebApp.ViewHolder(view);
    }

    boolean isHeaderIncluded=false;
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
       /* if(!isHeaderIncluded) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View rowHeadLayout = inflater.inflate(R.layout.attendance_mark_adp_view_heading, null);
            LinearLayout trHead = (LinearLayout) rowHeadLayout;
            holder.layoutAttendance.addView(trHead);
            isHeaderIncluded=true;
        }*/
        holder.txtStudentName.setText(CommonFunctions.capitalize(timeTableModelList.get(position).getStudentName()));

        //timeTableModelList.get(position).setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID).trim());
        //timeTableModelList.get(position).setSessionId(tinyDB.getString(TINYDB_LOGIN_KEYS.SESSION_ID).trim());

        timeTableModelList.get(position).setUserId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID).trim());
        timeTableModelList.get(position).setAttDate(tinyDB.getString("AttDate").trim());

        holder.txtRollNo.setText(timeTableModelList.get(position).getRollNo());
        if(position==0) {
            Log.e(mTAG,timeTableModelList.get(position).getAttTag() == null ? "abc" : timeTableModelList.get(position).getAttTag());
        }
        holder.txtAttTag.setText(timeTableModelList.get(position).getAttTag() == null ? "P" : timeTableModelList.get(position).getAttTag().trim());
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(mContext,
                android.R.layout.simple_spinner_item, listAttTags);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        holder.spinnerAttTags.setAdapter(dataAdapter);
        if(timeTableModelList.get(position).getAttTag()!=null) {
            holder.spinnerAttTags.setSelection(listAttTags.indexOf(timeTableModelList.get(position).getAttTag().trim()));
        }
        else
        {
            timeTableModelList.get(position).setAttTag(listAttTags.get(0).trim());
        }
        holder.spinnerAttTags.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                /*Toast.makeText(mContext,
                        "OnClickListener : " +
                                "\nSpinner 1 : "+ String.valueOf(holder.spinnerAttTags.getSelectedItem()) ,

                        Toast.LENGTH_SHORT).show();*/

                    timeTableModelList.get(position).setAttTag(String.valueOf(holder.spinnerAttTags.getSelectedItem()).trim());
                    if (String.valueOf(holder.spinnerAttTags.getSelectedItem()).trim().contains("P")) {
                        ((TextView) adapterView.getChildAt(0)).setTextColor(mContext.getResources().getColor(R.color.black3));
                        ((TextView) adapterView.getChildAt(0)).setTypeface(null, Typeface.BOLD);
                    } else if (String.valueOf(holder.spinnerAttTags.getSelectedItem()).trim().contains("L")) {
                        ((TextView) adapterView.getChildAt(0)).setTextColor(mContext.getResources().getColor(R.color.blue));
                        ((TextView) adapterView.getChildAt(0)).setTypeface(null, Typeface.BOLD);
                    } else if (String.valueOf(holder.spinnerAttTags.getSelectedItem()).trim().contains("A")) {
                        ((TextView) adapterView.getChildAt(0)).setTextColor(mContext.getResources().getColor(R.color.red));
                        ((TextView) adapterView.getChildAt(0)).setTypeface(null, Typeface.BOLD);
                    }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



    }

    @Override
    public int getItemCount() {
        return (timeTableModelList==null?0: timeTableModelList.size());
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout layoutAttendance;
        TextView txtRollNo;
        TextView txtStudentName;
        TextView txtAttTag;
        CheckBox chkTag;
        Spinner spinnerAttTags;
        public ViewHolder(@NonNull View view) {
            super(view);

            txtRollNo=view.findViewById(R.id.txtRollNo);
            txtStudentName=view.findViewById(R.id.txtStudentName);
            txtAttTag=view.findViewById(R.id.txtAttTag);
            chkTag=view.findViewById(R.id.chkTag);
            spinnerAttTags=view.findViewById(R.id.spinnerAttTags);
            layoutAttendance=view.findViewById(R.id.layoutAttendance);
        }
    }
}

