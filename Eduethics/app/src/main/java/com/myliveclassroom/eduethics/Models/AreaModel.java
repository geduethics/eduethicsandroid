package com.myliveclassroom.eduethics.Models;

public class AreaModel {

    private String DistrictNamePA;
    private String DistrictId;
    private String DistrictName;

    public String getDistrictId() {
        return DistrictId;
    }

    public void setDistrictId(String districtId) {
        DistrictId = districtId;
    }

    public String getDistrictName() {
        return DistrictName;
    }

    public String getDistrictNamePA() {
        return DistrictNamePA;
    }

    public void setDistrictName(String districtName) {
        DistrictName = districtName;
    }

    public void setDistrictNamePA(String districtNamePA) {
        DistrictNamePA = districtNamePA;
    }
}
