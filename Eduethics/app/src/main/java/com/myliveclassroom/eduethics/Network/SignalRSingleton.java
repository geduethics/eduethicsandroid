package com.myliveclassroom.eduethics.Network;

import com.microsoft.signalr.HubConnection;
import com.microsoft.signalr.HubConnectionBuilder;
import com.microsoft.signalr.HubConnectionState;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.SignalR_BASE_URL;

public class SignalRSingleton {
    public HubConnection mHubConnection;
    private static SignalRSingleton mInstance = null;

    public static SignalRSingleton getInstance(){
        if(mInstance == null)
        {
            mInstance = new SignalRSingleton();
        }
        return mInstance;
    }

    public HubConnection getmHubConnection()
    {
         if(mHubConnection==null)
         {
             mHubConnection = HubConnectionBuilder.create(SignalR_BASE_URL)
                     .build();
             mHubConnection.start().blockingAwait();
         }
        if(mHubConnection.getConnectionState()== HubConnectionState.DISCONNECTED) {
            mHubConnection = HubConnectionBuilder.create(SignalR_BASE_URL)
                    .build();
            mHubConnection.start().blockingAwait();
        }
        return mHubConnection;
    }

   /* public void sendMsgToServer(ChatModel chatModel)
    {
        getmHubConnection().send(
                "SendMsgGrp",
                chatModel.getGroupId(),
                chatModel.getCreatedBy(),
                chatModel.getDataType(),
                chatModel.getMessage(),
                chatModel.getFileUrlLocal(),
                chatModel.getFileUrlOnline()
                );
    }*/
}