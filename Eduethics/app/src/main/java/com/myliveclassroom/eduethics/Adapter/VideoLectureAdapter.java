package com.myliveclassroom.eduethics.Adapter;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.myliveclassroom.eduethics.Models.VideoLectureModel;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.classes.TinyDB;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoLectureAdapter extends RecyclerView.Adapter<VideoLectureAdapter.ViewHolder> {
    List<VideoLectureModel> videoLectureModelList;
    Context mContext;
    TinyDB tinyDB;

    public VideoLectureAdapter(Context context, List<VideoLectureModel> videoLectureModels) {
        tinyDB = new TinyDB(context);
        mContext = context;
        videoLectureModelList = videoLectureModels;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.video_lecture_adapter, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtTitle.setText(videoLectureModelList.get(position).getVideoTitle());
        holder.txtClass.setText(videoLectureModelList.get(position).getClassName());
        holder.txtSubject.setText(videoLectureModelList.get(position).getSubjectName());

      String []videoId=videoLectureModelList.get(position).getVideoLink().split("/");
       /*   if(videoId.length>0) {
            Glide.with(mContext)
                    .load("http://img.youtube.com/vi/"+videoId[videoId.length-1]+"/hqdefault.jpg")
                    .into(holder.imgPreview);
        }*/

        String k1="AIzaSyB3Qg";
        String k2="BsPfbezCo3XyY";
        String k3="O5ZoAPCTioEyqpOM";
       final YouTubeThumbnailView youTubeThumbnailView = (YouTubeThumbnailView) holder.youtubeThumbView;
        youTubeThumbnailView.initialize(k1+k2+k3, new YouTubeThumbnailView.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubeThumbnailView youTubeThumbnailView, final YouTubeThumbnailLoader youTubeThumbnailLoader) {
                youTubeThumbnailLoader.setVideo(videoId[videoId.length-1]);
                youTubeThumbnailLoader.setOnThumbnailLoadedListener(new YouTubeThumbnailLoader.OnThumbnailLoadedListener() {
                    @Override
                    public void onThumbnailLoaded(YouTubeThumbnailView youTubeThumbnailView, String s) {
                        youTubeThumbnailLoader.release();
                        //youTubeThumbnailLoader.setVideo(s);
                    }

                    @Override
                    public void onThumbnailError(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader.ErrorReason errorReason) {

                    }
                });
            }

            @Override
            public void onInitializationFailure(YouTubeThumbnailView youTubeThumbnailView, YouTubeInitializationResult youTubeInitializationResult) {

            }
        });

        youTubeThumbnailView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                watchYoutubeVideo(mContext,videoId[videoId.length-1]);
            }
        });
    }

    public static void watchYoutubeVideo(Context context, String id) {
        Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + id));
        Intent webIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://www.youtube.com/watch?v=" + id));
        try {
            context.startActivity(appIntent);
        } catch (ActivityNotFoundException ex) {
            context.startActivity(webIntent);
        }
    }

    @Override
    public int getItemCount() {
        return (videoLectureModelList == null ? 0 : videoLectureModelList.size());
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtTitle)
        TextView txtTitle;

        @BindView(R.id.txtClass)
        TextView txtClass;

        @BindView(R.id.txtSubject)
        TextView txtSubject;

          @BindView(R.id.imgPreview)
        ImageView imgPreview;

        @BindView(R.id.youtubeThumbView)
        YouTubeThumbnailView youtubeThumbView;;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
