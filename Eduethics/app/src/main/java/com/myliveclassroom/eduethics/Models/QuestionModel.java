package com.myliveclassroom.eduethics.Models;

import com.google.gson.annotations.SerializedName;

public class QuestionModel {

    private String ProfileType;

    @SerializedName("ExamId")
    private String ExamId;

    @SerializedName("SchoolId")
    private String SchoolId;

    @SerializedName("StudentId")
    private String StudentId;

    @SerializedName("StudentName")
    private String StudentName;

    @SerializedName("SessionId")
    private String SessionId;

    @SerializedName("ClassId")
    private String ClassId;

    @SerializedName("ClassName")
    private String ClassName;

    @SerializedName("SubjectName")
    private String SubjectName;

    @SerializedName("QuestionId")
    private String QuestionId;

    @SerializedName("Question")
    private String Question;

    @SerializedName("QuestionImg")
    private String QuestionImg;

    @SerializedName("AnswerId")
    private String AnswerId;

    @SerializedName("AnswerIdS")
    private String AnswerIdS;

    @SerializedName("Ans")
    private String Ans;

    @SerializedName("AnsImg")
    private String AnsImg;


    @SerializedName("Points")
    private String Points;

    @SerializedName("CheckBox")
    private String ChkChoice;

    @SerializedName("Radio")
    private String RadioChoice;

    @SerializedName("IsAns")
    private String IsAns;

    private  String CorrectAnsId;

    public String getCorrectAnsId() {
        return CorrectAnsId;
    }

    private String InCorrectCount,CorrectCount;
    private String         NegativeMarks;
    private String MarksObtained;

    public String getInCorrectCount() {
        return InCorrectCount;
    }

    public String getMarksObtained() {
        return MarksObtained;
    }

    public String getNegativeMarks() {
        return NegativeMarks;
    }

    public String getCorrectCount() {
        return CorrectCount;
    }

    private String CreatedBy;

    public void setProfileType(String profileType) {
        ProfileType = profileType;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public void setIsAns(String isAns) {
        IsAns = isAns;
    }

    public String getIsAns() {
        return IsAns;
    }

    public String getExamId() {
        return ExamId;
    }

    public void setExamId(String examId) {
        ExamId = examId;
    }

    public String getSchoolId() {
        return SchoolId;
    }

    public void setSchoolId(String schoolId) {
        SchoolId = schoolId;
    }

    public void setSessionId(String sessionId) {
        SessionId = sessionId;
    }

    public String getSessionId() {
        return SessionId;
    }

    public String getClassId() {
        return ClassId;
    }

    public void setClassId(String classId) {
        ClassId = classId;
    }

    public String getQuestion() {
        return Question;
    }

    public String getQuestionId() {
        return QuestionId;
    }

    public String getAns() {
        return Ans;
    }

    public void setQuestionId(String questionId) {
        QuestionId = questionId;
    }

    public String getChkChoice() {
        return ChkChoice;
    }

    public String getPoints() {
        return Points;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    public String getRadioChoice() {
        return RadioChoice;
    }

    public void setAns(String ans) {
        Ans = ans;
    }

    public void setChkChoice(String chkChoice) {
        ChkChoice = chkChoice;
    }

    public void setPoints(String points) {
        Points = points;
    }

    public void setRadioChoice(String radioChoice) {
        RadioChoice = radioChoice;
    }

    public String getAnswerId() {
        return AnswerId;
    }

    public void setAnswerId(String answerId) {
        AnswerId = answerId;
    }

    public String getAnswerIdS() {
        return AnswerIdS;
    }

    public void setAnswerIdS(String answerIdS) {
        AnswerIdS = answerIdS;
    }

    public String getStudentId() {
        return StudentId;
    }

    public void setStudentId(String studentId) {
        StudentId = studentId;
    }

    public String getQuestionImg() {
        return QuestionImg;
    }

    public void setQuestionImg(String questionImg) {
        QuestionImg = questionImg;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public void setClassName(String className) {
        ClassName = className;
    }

    public String getAnsImg() {
        return AnsImg;
    }

    public void setStudentName(String studentName) {
        StudentName = studentName;
    }
}
