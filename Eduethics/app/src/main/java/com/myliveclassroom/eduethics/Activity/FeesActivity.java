package com.myliveclassroom.eduethics.Activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.FragmentContainerView;
import androidx.recyclerview.widget.RecyclerView;

import com.myliveclassroom.eduethics.Fragments.WebAppFrag.FeeSchoolFragment;
import com.myliveclassroom.eduethics.Models.FeesModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Activity.MainActivity.TO_WHICH_FRAG;
import static com.myliveclassroom.eduethics.Utils.LocalConstants.FEE_VIEWS.TEACHER_SCREEN_1;
import static com.myliveclassroom.eduethics.Utils.LocalConstants.TO_FRAG.TO_FEES_SUMMARY;


public class FeesActivity extends BaseActivity {

    int i = 0;
    List<FeesModel> alFeesModel;

    @BindView(R.id.txtPageTitle)
    TextView txtPageTitle;

    @BindView(R.id.imgBack)
    ImageView imgBack;

    @BindView(R.id.imgbtnAdd)
    ImageView imgbtnAdd;

    @BindView(R.id.btnAdd)
    TextView btnAdd;

    @BindView(R.id.ivUp)
    ImageView ivUp;

    @BindView(R.id.ivDown)
    ImageView ivDown;

    @BindView(R.id.fragmentFrame)
    FragmentContainerView fragmentFrame;


    List<FeesModel> feesModelList;
    List<FeesModel> feesModelListUnique;
    List<String> feesModelListTemp;

    RecyclerView fee_recycler_view;
    int mFeeScreen = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.MyThemeNoBar);
        setContentView(R.layout.activity_fees);
        setTitle("Fee");
        _BaseActivity(this);
        ButterKnife.bind(this);

        if (tinyDB.getString(LocalConstants.FEE_VIEWS.FEE_SCREEN).equalsIgnoreCase(LocalConstants.FEE_VIEWS.OTHER_SCREEN + "")) {
            mFeeScreen = TEACHER_SCREEN_1;
        } else {
            mFeeScreen = LocalConstants.FEE_VIEWS.ADMIN_SCREEN_1;
        }

        init();
        //fee_recycler_view= (RecyclerView) findViewById(R.id.fee_recycler_view);
        //fee_recycler_view.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        // create the layout with inflater

        // getFeeData();
        showStudentFeeFragment();
    }


    private void init() {
        imgbtnAdd.setVisibility(View.GONE);
        imgBack.setVisibility(View.VISIBLE);
        btnAdd.setVisibility(View.GONE);
        btnAdd.setText("Create");
        //imgbtnAdd.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_tick3));
        txtPageTitle.setText("Fee");
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Back();
            }
        });


    }

    private void Back() {
        super.onBackPressed();
    }

    private void showStudentFeeFragment() {
            getSupportFragmentManager().beginTransaction().setCustomAnimations(
                    R.anim.slide_in,  // enter
                    R.anim.fade_out,  // exit
                    R.anim.fade_in,   // popEnter
                    R.anim.slide_out  // popExit
            ).replace(R.id.fragmentFrame, new FeeSchoolFragment(mContext, mFeeScreen)).commit();
    }

    private void getFeeData() {

        feesModelList = new ArrayList<>();
        feesModelListUnique = new ArrayList<>();
        feesModelListTemp = new ArrayList<>();

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);
        Call call = apiService.feeData();
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response.isSuccessful()) {
                    feesModelList = (List<FeesModel>) response.body();

                    for (int j = 0; j < feesModelList.size(); j++) {
                        if (!feesModelListTemp.contains(feesModelList.get(j).getRecieptNo())) {
                            feesModelListTemp.add(feesModelList.get(j).getRecieptNo());
                        }
                    }
                    for (String element : feesModelListTemp) {
                        FeesModel feesModel = new FeesModel();
                        feesModel.setRecieptNo(element);
                        if (!feesModelListUnique.contains(element)) {
                            feesModelListUnique.add(feesModel);
                        }
                    }

                    /*FeeAdapter adapter = new FeeAdapter(FeesActivity.this, feesModelListUnique, feesModelList);
                    fee_recycler_view.setVisibility(View.GONE);
                    fee_recycler_view.setAdapter(adapter);*/
                } else {
                    Log.e("", "error =");
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.e("", "error =" + t.toString());
            }
        });
    }

    @OnClick(R.id.ivDown)
    public void clickToDownImage(View view) {
        fee_recycler_view.setVisibility(View.VISIBLE);
        ivDown.setVisibility(View.GONE);
        ivUp.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.ivUp)
    public void clickToUpImage(View view) {
        fee_recycler_view.setVisibility(View.GONE);
        ivUp.setVisibility(View.GONE);
        ivDown.setVisibility(View.VISIBLE);
    }


    @Override
    public void onBackPressed() {
        /*if (tinyDB.getString(LocalConstants.INSTITUTE_TYPE.TYPE).equalsIgnoreCase(LocalConstants.INSTITUTE_TYPE.TUTION)) {
            TO_WHICH_FRAG = TO_CLASSROOM;
        } */
        switch (TO_WHICH_FRAG) {
            case TO_FEES_SUMMARY:
                getSupportFragmentManager().beginTransaction().setCustomAnimations(
                        R.anim.slide_in,  // enter
                        R.anim.fade_out,  // exit
                        R.anim.fade_in,   // popEnter
                        R.anim.slide_out  // popExit
                ).replace(R.id.fragmentFrame, new FeeSchoolFragment(mContext, mFeeScreen)).commit();
                break;

            default:
                super.onBackPressed();
        }
    }


}
///