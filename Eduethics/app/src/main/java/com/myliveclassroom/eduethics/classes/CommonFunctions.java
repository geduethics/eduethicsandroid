package com.myliveclassroom.eduethics.classes;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.location.Location;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import com.myliveclassroom.eduethics.R;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.myliveclassroom.eduethics.Utils.LocalConstants;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.channels.FileChannel;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.APP_PACKAGE_NAME;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.MAIN_FOLDER;


public class CommonFunctions {

    private ProgressDialog dialog;
    private String TAG;
    Context mContext;
    TinyDB tinyDB;

    public static boolean isEmpty(CharSequence str) {
        if (str == null || str.length() == 0)
            return true;
        else
            return false;
    }


    public String getAppVersion(Context context) {
        String version = "";
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            version = "0";
        }
        return version;
    }

    public Boolean isAppAvailable(String address, Double lat, Double lng) {
        if (address.toLowerCase().contains("160001")
                || address.toLowerCase().contains("160002")
                || address.toLowerCase().contains("160003")
                || address.toLowerCase().contains("160004")
                || address.toLowerCase().contains("160008")
                || address.toLowerCase().contains("160009")
                || address.toLowerCase().contains("160010")
                || address.toLowerCase().contains("160011")
                || address.toLowerCase().contains("160012")
                || address.toLowerCase().contains("160014")
                || address.toLowerCase().contains("160015")
                || address.toLowerCase().contains("160017")
                || address.toLowerCase().contains("160018")
                || address.toLowerCase().contains("160019")
                || address.toLowerCase().contains("160020")
                || address.toLowerCase().contains("160022")
                || address.toLowerCase().contains("160023")
                || address.toLowerCase().contains("160025")
                || address.toLowerCase().contains("160026")
                || address.toLowerCase().contains("160027")
                || address.toLowerCase().contains("160028")
                || address.toLowerCase().contains("160029")
                || address.toLowerCase().contains("160030")
                || address.toLowerCase().contains("160031")
                || address.toLowerCase().contains("160033")
                || address.toLowerCase().contains("160034")
                || address.toLowerCase().contains("160035")
                || address.toLowerCase().contains("160036")
                || address.toLowerCase().contains("160041")
                || address.toLowerCase().contains("160043")
                || address.toLowerCase().contains("160044")
                || address.toLowerCase().contains("160045")
                || address.toLowerCase().contains("160047")
                || address.toLowerCase().contains("160050")
                || address.toLowerCase().contains("160052")
                || address.toLowerCase().contains("160054")
                || address.toLowerCase().contains("160055")
                || address.toLowerCase().contains("160056")) {

            return true;
        } else {
            double distance = distanceBetweenLatLng(lat, lng);
            if (distance < 10000) {
                return true;
            } else {
                return false;
            }
        }
    }

    private double distanceBetweenLatLng(double lat, double lng) {
        //30.7495331	76.6576063
        Location startPoint = new Location("locationA");
        startPoint.setLatitude(30.7495331);
        startPoint.setLongitude(76.6576063);

        Location endPoint = new Location("locationA");
        endPoint.setLatitude(lat);
        endPoint.setLongitude(lng);

        double distance = startPoint.distanceTo(endPoint);
        return distance;
    }

    public String getDateTime(String format) {
        DateFormat dateFormat = new SimpleDateFormat(format);//"yyyyMMddHHmmssSS");
        Date date = new Date();
        return dateFormat.format(date);
    }


    public String parseDateToddMMyyyyTime(String time) {

        SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy  hh:mm a");
        String str = null;
        try {
            str = format.format(Date.parse(time));
        } catch (Exception e) {
            str = e.getMessage();
        }
        return str;
    }

    public String parseDateToddMMyyyyDateOnly(String time) {

        SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
        String str = null;
        try {
            str = format.format(Date.parse(time));
        } catch (Exception e) {
            str = e.getMessage();
        }
        return str;
    }


    public static void myToast(Context context, String msg) {
        Toast toast = Toast.makeText(context, msg, Toast.LENGTH_SHORT);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        v.setTextColor(context.getResources().getColor(R.color.colorAccent));
        toast.show();
    }

    public void showProgress(Context context) {
        dialog = new ProgressDialog(context);
        dialog.setCancelable(false);
        dialog.setMessage("Please wait...");
        dialog.show();
    }

    public void hideProgress() {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    Boolean _permisson = false;
    String mCurrentPhotoPath;


    private String _open_camera(Context context, int request_code) {
        Activity activity = (Activity) context;
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(activity.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile("temp_capture");
                mCurrentPhotoPath = "file:" + photoFile.getAbsolutePath();
            } catch (IOException ex) {
                Log.i(TAG, "IOException");
            }

            if (photoFile != null) {
                try {
                    Uri imageUri = FileProvider.getUriForFile(
                            context,
                            APP_PACKAGE_NAME+".provider", //(use your app signature + ".provider" )
                            photoFile);

                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                } catch (Exception ex) {
                    Log.e(TAG, ex.getMessage());
                }
            }
            try {
                activity.startActivityForResult(cameraIntent, request_code);
            } catch (Exception ex) {
                Toast.makeText(context, ex.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
        return mCurrentPhotoPath;
    }

    private String _open_gallery(Context context, int request_code) {
        Activity activity = (Activity) context;


        Intent pickIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickIntent.setType("image/*");
        Intent chooserIntent = Intent.createChooser(pickIntent, "Select Image");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{pickIntent});


      /* Intent intent = new Intent();
       intent.setType("image/*");
       intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
       intent.setAction(Intent.ACTION_GET_CONTENT);
       //startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_IMAGE_MULTIPLE);
*/
        try {
            activity.startActivityForResult(chooserIntent, request_code);
            //activity.startActivityForResult(Intent.createChooser(intent,"Select Picture"), request_code);
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
        return mCurrentPhotoPath;
    }

    public static File createImageFile(String folderName) throws IOException {

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES) + "/" + folderName + "/");

        if (!mediaStorageDir.exists()) {
            mediaStorageDir.mkdirs();
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        String dateString = sdf.format(new Date(System.currentTimeMillis()));
        dateString = dateString.replace("-", "");
        dateString = dateString.replace(" ", "");
        String mImageName = "IMG_" + dateString;// String.valueOf(System.currentTimeMillis());

        // File parent = new File(System.getProperty("java.io.tmpdir"));
        // File image = new File(parent, mImageName + ".jpg");

        File image = File.createTempFile(
                mImageName,  // prefix
                ".jpg",         // suffix
                mediaStorageDir      // directory
        );

        return image;
    }

    public static File createImageFileNew(String fileType) throws IOException {

        File storageDir = null;
        File tempfile = null;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        String dateString = sdf.format(new Date(System.currentTimeMillis()));
        dateString = dateString.replace("-", "");
        dateString = dateString.replace(" ", "");
        String mImageName = "IMG_" + dateString+"_";

        //String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "";
        //img file
        if (fileType.equalsIgnoreCase(LocalConstants.FILE_TYPE.JPG)) {
            imageFileName = "JPEG_" + dateString + "_";
            storageDir = createDirectory("Images", fileType);

            tempfile = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );
        }
        //pdf
        else if (fileType.equalsIgnoreCase(LocalConstants.FILE_TYPE.PDF)) {
            imageFileName = "PDF_" + dateString + "_";
            storageDir = createDirectory("Pdf", fileType);

            tempfile = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".pdf",         /* suffix */
                    storageDir      /* directory */
            );
        }
        return tempfile;
    }

    public static File saveFile(File source, String fileType) {
        File destination = null;
        try {

            destination = createImageFileNew(fileType);

            //File source = new File(sourceUri.getPath());
            FileChannel src = new FileInputStream(source).getChannel();
            FileChannel dst = new FileOutputStream(destination).getChannel();
            dst.transferFrom(src, 0, src.size());
            src.close();
            dst.close();
        } catch (
                IOException ex) {
            ex.printStackTrace();
        }
        return destination;
    }

    public static Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public static void writeToFile(byte[] data, String fileName) throws IOException{
        FileOutputStream out = new FileOutputStream(fileName);
        out.write(data);
        out.close();
    }

    public static byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    public static String getPath(Context mContext ,Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = mContext.getContentResolver().query(uri, projection, null, null, null);
        if (cursor == null) return null;
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String s = cursor.getString(column_index);
        cursor.close();
        return s;
    }

    public Bitmap getBitMapFromPath(Context conext, String path) {
        Bitmap photo = null;
        //mCurrentPhotoPath="file:" +mCurrentPhotoPath;
        if (!path.equalsIgnoreCase("")) {
            mCurrentPhotoPath = path;
            if (!mCurrentPhotoPath.startsWith("file:")) {
                mCurrentPhotoPath = "file:" + mCurrentPhotoPath;
            }
        }
        try {
            //photo = MediaStore.Images.Media.getBitmap(conext.getContentResolver(), Uri.parse(mCurrentPhotoPath));
            photo = handleSamplingAndRotationBitmap(conext, Uri.parse(mCurrentPhotoPath));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return photo;
    }

    public static Bitmap handleSamplingAndRotationBitmap(Context context, Uri selectedImage)
            throws IOException {
        int MAX_HEIGHT = 4096 * 2;
        int MAX_WIDTH = 4096 * 2;

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        InputStream imageStream = context.getContentResolver().openInputStream(selectedImage);
        BitmapFactory.decodeStream(imageStream, null, options);
        imageStream.close();

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, MAX_WIDTH, MAX_HEIGHT);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        imageStream = context.getContentResolver().openInputStream(selectedImage);
        Bitmap img = BitmapFactory.decodeStream(imageStream, null, options);

        img = rotateImageIfRequired(context, img, selectedImage);
        return img;
    }

    private static int calculateInSampleSize(BitmapFactory.Options options,
                                             int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;


            final float totalPixels = width * height;

            // Anything more than 2x the requested pixels we'll sample down further
            final float totalReqPixelsCap = reqWidth * reqHeight * 2;

            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++;
            }
        }
        return inSampleSize;
    }

    public static Bitmap rotateImageIfRequired(Context context, Bitmap img, Uri selectedImage) throws IOException {

        InputStream input = context.getContentResolver().openInputStream(selectedImage);
        ExifInterface ei;
        if (Build.VERSION.SDK_INT > 23)
            ei = new ExifInterface(input);
        else
            ei = new ExifInterface(selectedImage.getPath());

        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
       /* if(orientation==0) {
            orientation= getOrientation(context,selectedImage);
        }*/

        switch (orientation) {
            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                return flip(img, true, false);

            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                return flip(img, false, true);
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);

            default:
                return img;
        }
    }

    public static int getOrientation(Context context, Uri photoUri) {

        Cursor cursor = context.getContentResolver().query(photoUri,
                new String[]{MediaStore.Images.ImageColumns.ORIENTATION}, null, null, null);

        if (cursor == null || cursor.getCount() != 1) {
            return 6;  //Assuming it was taken portrait 90
        }

        cursor.moveToFirst();
        return cursor.getInt(0);
    }


    private static Bitmap flip(Bitmap bitmap, boolean horizontal, boolean vertical) {
        Matrix matrix = new Matrix();
        matrix.preScale(horizontal ? -1 : 1, vertical ? -1 : 1);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }

    public void error(Context context, String error) {
        if (error.toLowerCase().contains("unauthorized")) {
            //login(context);
        }
    }

    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }


    public static SecretKey generateKey(String password)
            throws NoSuchAlgorithmException, InvalidKeySpecException {
        return new SecretKeySpec(password.getBytes(), "AES");
    }

    public static byte[] encryptMsg(String message, SecretKey secret)
            throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidParameterSpecException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
        /* Encrypt the message. */
        Cipher cipher = null;
        cipher = Cipher.getInstance("Blowfish/CBC/ISO10126Padding");
        cipher.init(Cipher.ENCRYPT_MODE, secret);
        byte[] cipherText = cipher.doFinal(message.getBytes("UTF-8"));
        return cipherText;
    }

    public static String decryptMsg(byte[] cipherText, SecretKey secret)
            throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidParameterSpecException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, UnsupportedEncodingException {
        /* Decrypt the message, given derived encContentValues and initialization vector. */
        Cipher cipher = null;
        cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, secret);
        String decryptString = new String(cipher.doFinal(cipherText), "UTF-8");
        return decryptString;
    }


    AlertDialog.Builder alertDialogSuccess = null;
    AlertDialog dialogSuccess = null;



   /* private static File createDirectory(String directory)
    {
        String file_path = Environment.getExternalStorageDirectory().getAbsolutePath() +
                "/"+ FileConstants.FILE_APP_MAIN_FOLDER +"/"+directory;
        File dir = new File(file_path);
        if(!dir.exists())
            dir.mkdirs();
        return dir;
    }*/

    private static String createRandomFileName(String prefix) //IMG / DOC
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        String dateString = sdf.format(new Date(System.currentTimeMillis()));
        dateString = dateString.replace("-", "");
        dateString = dateString.replace(" ", "");
        Random r = new Random();
        int i1 = r.nextInt(99 - 9) + 65;
        String mImageName = prefix + dateString + "_" + i1;
        return mImageName;
    }


    /*public static File saveBitmap(Context context,Bitmap bmp,Uri uri) throws IOException {
        String fileName=createRandomFileName("Img_");
        String extention=getfileExtensionFromUri(context, uri);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 60, bytes);
        File f=createDirectory("temp");
        f = new File(f +"/"+fileName +"."+extention);
        f.createNewFile();
        FileOutputStream fo = new FileOutputStream(f);
        fo.write(bytes.toByteArray());
        fo.close();
        return f;
    }*/

    /*public static File saveFile(Context context, Uri uri,String extention,String folder,String prefix) throws IOException {

        if(extention.equalsIgnoreCase(""))
        {
           extention= getfileExtensionFromUri(context,uri);
        }
        InputStream inputStream =context.getContentResolver().openInputStream(uri) ;
        FileOutputStream output = null;
        File f=createDirectory(folder);
        String fileName=createRandomFileName(prefix);
        byte[] buffer = new byte[1024 * 20];
        f = new File(f +"/"+fileName +"."+extention);
        f.createNewFile();
        output = new FileOutputStream(f);
        int len;
        while ((len = inputStream.read(buffer)) != -1) {
            output.write(buffer, 0, len);
        }
        return f;
    }*/

    public static String fixFileName(String fileName) {
        if (fileName != null) {
            fileName = fileName.replaceAll("[\u0001-\u001f<>\u202E:\"/\\\\|?*\u007f]+", "").trim();
        }
        return fileName;
    }

    public static String getExt(String filePath) {
        int strLength = filePath.lastIndexOf(".");
        if (strLength > 0)
            return filePath.substring(strLength + 1).toLowerCase();
        return null;
    }

    private static String getfileExtensionFromUri(Context context, Uri uri) {
        String extension;
        ContentResolver contentResolver = context.getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        extension = mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));
        return extension;
    }

    public String open_camera(Context context, int request_code) {

        if (permissioncheck(context, request_code)) {
            msg = _open_camera(context, request_code);
        } else {
            //go to check permission
            _permisson = openSettingsDialog(context, request_code);
            if (_permisson) {
                msg = _open_camera(context, request_code);
            } else {
                msg = "Error: permissino denied";
            }
        }
        return msg;
    }


    public String open_gallery(Context context, int request_code) {
        msg = "";
        if (permissioncheck(context, request_code)) {
            msg = _open_gallery(context, request_code);
        } else {
            //go to check permission
            _permisson = openSettingsDialog(context, request_code);
            if (_permisson) {
                msg = _open_gallery(context, request_code);
            } else {
                msg = "Error: permissino denied";
            }
        }
        return msg;
    }

    public Boolean openSettingsDialog(final Context context, final int requestCode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Required Permissions");
        builder.setMessage("This app require permission to use awesome feature. Grant them in app settings.");
        builder.setPositiveButton("Take Me To SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", context.getPackageName(), null);
                intent.setData(uri);
                Activity activity = (Activity) context;

                activity.startActivityForResult(intent, requestCode);
                _permisson = true;
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                _permisson = false;
            }
        });
        builder.show();
        return _permisson;
    }


    public boolean permissioncheck(final Context context, final int requestCode) {
        _permisson = false;
        Activity activity = (Activity) context;
        Dexter.withActivity(activity)
                .withPermissions(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // _permisson=true;
                        if (report.areAllPermissionsGranted()) {
                            _permisson = true;
                        }
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            _permisson = false;
                            openSettingsDialog(context, requestCode);
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                        _permisson = false;
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        _permisson = false;
                    }
                })
                .onSameThread()
                .check();
        return _permisson;
    }

    String msg;

    public void openCropActivity(Context context, Uri sourceUri, Uri destinationUri, Fragment fragment) {
        //int defaultWidth=4096*2;
        int defaultWidth = 512 * 2;
        Activity activity = (Activity) context;
        File photoFile = null;
        try {
            photoFile = createImageFile("");
            mCurrentPhotoPath = "file:" + photoFile.getAbsolutePath();
        } catch (IOException ex) {
            Log.i(TAG, "IOException");
        }
        destinationUri = Uri.fromFile(photoFile);

       /*options.setCompressionQuality(60);

        if (fragment != null) {
            UCrop.of(sourceUri, destinationUri)
                    .withMaxResultSize(defaultWidth, defaultWidth)
                    .withOptions(options)
                    //.withAspectRatio(16, 9)
                    .start(activity, fragment, UCrop.REQUEST_CROP);
        } else {
            UCrop.of(sourceUri, destinationUri)
                    .withOptions(options)
                    .withMaxResultSize(defaultWidth, defaultWidth)
                    //.withAspectRatio(16, 9)

                    .start(activity, UCrop.REQUEST_CROP);
        }*/
    }

    public static Date getDateFormated(String date) {

        String[] dateTemp = date.contains("/")?date.split(" ")[0].split("/"):date.split(" ")[0].split("-");
        int month = getMonthFromString(dateTemp[1]);
        int year = (Integer.parseInt(dateTemp[2])- 1900);
        Date currentDate = new Date();
        currentDate.setDate(Integer.parseInt(dateTemp[0]));
        currentDate.setMonth(month);
        currentDate.setYear(year);
        return currentDate;
    }

    private static int getMonthFromString(String month) {
        if (month.equalsIgnoreCase("jan")) {
            return 0;
        } else if (month.equalsIgnoreCase("feb")) {
            return 1;
        }
        if (month.equalsIgnoreCase("mar")) {
            return 2;
        } else if (month.equalsIgnoreCase("apr")) {
            return 3;
        }
        if (month.equalsIgnoreCase("may")) {
            return 4;
        } else if (month.equalsIgnoreCase("jun")) {
            return 5;
        }
        if (month.equalsIgnoreCase("jul")) {
            return 6;
        } else if (month.equalsIgnoreCase("aug")) {
            return 7;
        }
        if (month.equalsIgnoreCase("sep")) {
            return 8;
        } else if (month.equalsIgnoreCase("oct")) {
            return 9;
        }
        if (month.equalsIgnoreCase("nov")) {
            return 10;
        } else if (month.equalsIgnoreCase("dec")) {
            return 11;
        }

        return 0;
    }

    public String compressImage(Context context, String imageUri, String childDir) {

        File originalFile = new File(imageUri);
        mContext = context;
        String filePath = getRealPathFromURI(imageUri);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 1200.0f;
        float maxWidth = 1200.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = originalFile.getAbsolutePath();
        //filename = getFilename(childDir);

        try {
            out = new FileOutputStream(filename);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

//          write the compressed bitmap at the destination specified by filename.
        scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);


        return filename;

    }

    public String getFilename(String childDir) {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), childDir);
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;
    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;
    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = mContext.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public static File saveFile(Context context, Uri uri, String extention, String folder, String prefix) throws IOException {

        if (extention.equalsIgnoreCase("")) {
            extention = getfileExtensionFromUri(context, uri);
        }
        InputStream inputStream = context.getContentResolver().openInputStream(uri);
        FileOutputStream output = null;
        File f = createDirectory(folder);
        String fileName = createRandomFileName(prefix);
        byte[] buffer = new byte[1024 * 20];
        f = new File(f + "/" + fileName + "." + extention);
        f.createNewFile();
        output = new FileOutputStream(f);
        int len;
        while ((len = inputStream.read(buffer)) != -1) {
            output.write(buffer, 0, len);
        }
        return f;
    }

    public static File createDirectory(String directory) {
        /*String file_path = Environment.getExternalStorageDirectory().getAbsolutePath() +
                "/"+ MAIN_FOLDER +"/"+directory;*/

        File file_path = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES + "/" + MAIN_FOLDER + "/" + directory);

        File dir = new File(file_path.getAbsolutePath());
        if (!dir.exists())
            dir.mkdirs();
        return dir;
    }

    public static File createDirectory(String directory, String fileType) {
        /*String file_path = Environment.getExternalStorageDirectory().getAbsolutePath() +
                "/"+ MAIN_FOLDER +"/"+directory;*/
        File file_path = null;

        if(fileType.equalsIgnoreCase(LocalConstants.FILE_TYPE.JPG)) {
            file_path = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES + "/" + MAIN_FOLDER + "/" + directory);
        } else if(fileType.equalsIgnoreCase(LocalConstants.FILE_TYPE.PDF)) {
            file_path = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOCUMENTS + "/" + MAIN_FOLDER + "/" + directory);
        }
        File dir = new File(file_path.getAbsolutePath());
        if (!dir.exists())
            dir.mkdirs();
        return dir;
    }

    public static String capitalize(final String str) {
        if (str != null) {
            String[] words = str.trim().split(" ");
            StringBuilder ret = new StringBuilder();
            for (int i = 0; i < words.length; i++) {
                if (words[i].trim().length() > 0) {
                    ret.append(Character.toUpperCase(words[i].trim().charAt(0)));
                    ret.append(words[i].trim().substring(1).toLowerCase());
                    if (i < words.length - 1) {
                        ret.append(' ');
                    }
                }
            }
            return ret.toString();
        } else {
            return "";
        }
    }

    public static File saveBitmap(Context context, Bitmap bmp, Uri uri) throws IOException {
        String fileName = createRandomFileName("Img_");
        String extention = getfileExtensionFromUri(context, uri);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File f = createDirectory("temp");
        f = new File(f + "/" + fileName + "." + extention);
        f.createNewFile();
        FileOutputStream fo = new FileOutputStream(f);
        fo.write(bytes.toByteArray());
        fo.close();
        return f;
    }


}
