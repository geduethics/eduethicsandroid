package com.myliveclassroom.eduethics.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.myliveclassroom.eduethics.Activity.MainActivity;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;


public class HowToUseFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        MainActivity.TO_WHICH_FRAG = LocalConstants.TO_FRAG.TO_DASHBOARD;
        View root =  inflater.inflate(R.layout.fragment_how_to_use, container, false);
        MainActivity.actionBarTitle.setText("How To Use");
        RecyclerView recyclerView = root.findViewById(R.id.recyclerHowToUse);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));


       /* FirebaseFirestore.getInstance().collection("utilData").document("appData").get().addOnSuccessListener(documentSnapshot -> {
            List<Map<String,String>> data =  (List)documentSnapshot.get("howtouse");
            recyclerView.setAdapter(new HowToUseAdapter(data,HowToUseFragment.this));
        });*/



        return root;
    }
}