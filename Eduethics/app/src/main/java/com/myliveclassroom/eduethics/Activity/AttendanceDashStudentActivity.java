package com.myliveclassroom.eduethics.Activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.myliveclassroom.eduethics.Adapter.AttendanceMonthlySummaryAdapter;
import com.myliveclassroom.eduethics.Adapter.AttendanceSubjectWiseAdapter;
import com.myliveclassroom.eduethics.Adapter.AttendanceYearlySummaryAdapter;
import com.myliveclassroom.eduethics.Models.TimeTableModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.CommonFunctions;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.monthArray;


public class AttendanceDashStudentActivity extends BaseActivity {

    @BindView(R.id.txtPageTitle)
    TextView txtPageTitle;

    @BindView(R.id.imgBack)
    ImageView imgBack;

    @BindView(R.id.imgbtnAdd)
    ImageView imgbtnAdd;

    @BindView(R.id.btnAdd)
    TextView btnAdd;

    @BindView(R.id.txtPresentCount)
    TextView txtPresentCount;

    @BindView(R.id.txtAbsentCount)
    TextView txtAbsentCount;

    @BindView(R.id.txtLeaveCount)
    TextView txtLeaveCount;

    @BindView(R.id.txtHolidayCount)
    TextView txtHolidayCount;

    @BindView(R.id.txtAttendanceStatusHead)
    TextView txtAttendanceStatusHead;

    @BindView(R.id.txtRecyclerViewEmpty)
    TextView txtRecyclerViewEmpty;

    @BindView(R.id.recyclerAttendanceDayDetail)
    RecyclerView recyclerAttendanceDayDetail;

    @BindView(R.id.recyclerMonthlySummary)
    RecyclerView recyclerMonthlySummary;

    @BindView(R.id.recyclerMonthlySummarysubjectWise)
    RecyclerView recyclerMonthlySummarysubjectWise;

    @BindView(R.id.recyclerAnnualSummary)
    RecyclerView recyclerAnnualSummary;


    AttendanceSubjectWiseAdapter attendanceAdapterSubjectWise;
    AttendanceSubjectWiseAdapter attendanceAdapterSubjectWiseMonthly;
    AttendanceMonthlySummaryAdapter attendanceMonthlySummaryAdapter;
    AttendanceYearlySummaryAdapter attendanceYearlySummaryAdapter;

    private CaldroidFragment caldroidFragment;
    private CaldroidFragment dialogCaldroidFragment;
    /*List<CalendarModel> calendarList;*/
    SimpleDateFormat formatter, formatter1;
    List<String> disableDates;
    List<TimeTableModel> attendanceList;
    List<TimeTableModel> attendanceList2;

    List<TimeTableModel> attendanceListMonthly;
    List<TimeTableModel> attendanceListMonthlySubjectWise;
    List<TimeTableModel> attendanceListAnnual;

    int mMonth, mYear;

    @SuppressLint("WrongConstant")
    private void setCustomResourceForDates() {

        if (caldroidFragment != null) {

            ColorDrawable green = new ColorDrawable(getColor(R.color.green_att));
            ColorDrawable red = new ColorDrawable(getColor(R.color.red_att));
            ColorDrawable blue = new ColorDrawable(getColor(R.color.blue_att));
            ColorDrawable grey = new ColorDrawable(getColor(R.color.grey_att));
            ColorDrawable yellow = new ColorDrawable(getColor(R.color.yellow_att));
            ColorDrawable brown = new ColorDrawable(getColor(R.color.brown_att));
            ColorDrawable black3 = new ColorDrawable(getColor(R.color.black3_att));
            if (disableDates != null) {
                //this is for sundays
                for (int j = 0; j < disableDates.size(); j++) {
                    Log.e("tag", "disable" + disableDates.get(j));
                    caldroidFragment.setBackgroundDrawableForDate(grey, CommonFunctions.getDateFormated(disableDates.get(j)));
                    caldroidFragment.setTextColorForDate(R.color.black3, CommonFunctions.getDateFormated(disableDates.get(j)));
                }
            }
            int presentCount = 0;
            int absentCount = 0;
            int leaveCount = 0;
            int holidayCount = 0;

            for (int i = 0; i < attendanceList.size(); i++) {
                String curDate = attendanceList.get(i).getAttDate();
                Date date = null;
                try {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
                    if (curDate.contains("-")) {
                        dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
                    }
                    date = dateFormat.parse(curDate.split(" ")[0]);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if (attendanceList.get(i).getAttTag().equalsIgnoreCase("P")) {

                    caldroidFragment.setBackgroundDrawableForDate(green, date);
                    caldroidFragment.setTextColorForDate(R.color.black3, date);
                    presentCount = presentCount + 1;
                } else if (attendanceList.get(i).getAttTag().equalsIgnoreCase("A")) {
                    caldroidFragment.setBackgroundDrawableForDate(red, date);
                    caldroidFragment.setTextColorForDate(R.color.black3, date);
                    absentCount = absentCount + 1;
                } else if (attendanceList.get(i).getAttTag().equalsIgnoreCase("H")) {
                    caldroidFragment.setBackgroundDrawableForDate(brown, date);// CommonFunctions.getDateFormated(curDate));
                    caldroidFragment.setTextColorForDate(R.color.black, date);
                    holidayCount = holidayCount + 1;
                } else if (attendanceList.get(i).getAttTag().equalsIgnoreCase("L")) {
                    caldroidFragment.setBackgroundDrawableForDate(blue, date);
                    caldroidFragment.setTextColorForDate(R.color.black3, date);
                    leaveCount = leaveCount + 1;
                }
            }

            txtPresentCount.setText("" + presentCount);
            txtAbsentCount.setText("" + absentCount);
            txtLeaveCount.setText("" + leaveCount);
            txtHolidayCount.setText("" + holidayCount);

            caldroidFragment.refreshView();
        }
    }

    public void countWeekendDays() {
        disableDates = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        // Note that month is 0-based in calendar, bizarrely.
        calendar.set(mYear, mMonth - 1, 1);
        int daysInMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        int count = 0;
        for (int day = 1; day <= daysInMonth; day++) {
            calendar.set(mYear, mMonth - 1, day);
            int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
            if (dayOfWeek == Calendar.SUNDAY) {

                Date date = null;
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
                //String curDate = calendar.getTime()+"";
                //date=dateFormat.parse(curDate);
                disableDates.add(dateFormat.format(calendar.getTime()));
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.MyThemeNoBar);
        setContentView(R.layout.activity_attendance);
        _BaseActivity(this);
        ButterKnife.bind(this);
        init();
        formatter = new SimpleDateFormat("dd MMM yyyy");
        formatter1 = new SimpleDateFormat("dd-MMM-yyyy");

        caldroidFragment = new CaldroidFragment();

        Bundle args = new Bundle();
        Calendar cal = Calendar.getInstance();
        args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
        args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
        args.putBoolean(CaldroidFragment.ENABLE_SWIPE, true);
        args.putBoolean(CaldroidFragment.SIX_WEEKS_IN_CALENDAR, true);
        caldroidFragment.setArguments(args);
        setAdapterCalendarList();
    }

    private void init() {
        imgbtnAdd.setVisibility(View.INVISIBLE);
        imgBack.setVisibility(View.VISIBLE);
        btnAdd.setVisibility(View.VISIBLE);
        btnAdd.setText("");
        imgbtnAdd.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_tick3));
        txtPageTitle.setText(R.string.pagetitle_attendance);

        btnAdd.setText("Attendance Report");

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tinyDB.putString(LocalConstants.URL.REPORT_TYPE, LocalConstants.URL.REPORT_ATTENDANCE_STUDENT);
                Intent intent = new Intent(mContext, ReportActivity.class);
                startActivity(intent);
            }
        });


        initRecyclerView();

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Back();
            }
        });
    }

    private void initRecyclerView() {
        //Daily Subject wise
        recyclerAttendanceDayDetail.setLayoutManager(new LinearLayoutManager(mContext));
        attendanceAdapterSubjectWise = new AttendanceSubjectWiseAdapter(mContext, attendanceList2);
        recyclerAttendanceDayDetail.setAdapter(attendanceAdapterSubjectWise);

        //Monthly
        recyclerMonthlySummary.setLayoutManager(new LinearLayoutManager(mContext));
        attendanceMonthlySummaryAdapter = new AttendanceMonthlySummaryAdapter(mContext, attendanceListMonthly);
        recyclerMonthlySummary.setAdapter(attendanceMonthlySummaryAdapter);

        //Monthly Subject Wise
        recyclerMonthlySummarysubjectWise.setLayoutManager(new LinearLayoutManager(mContext));
        attendanceAdapterSubjectWiseMonthly = new AttendanceSubjectWiseAdapter(mContext, attendanceListMonthlySubjectWise);
        recyclerMonthlySummarysubjectWise.setAdapter(attendanceAdapterSubjectWiseMonthly);

        //Annual
        recyclerAnnualSummary.setLayoutManager(new LinearLayoutManager(mContext));
        attendanceYearlySummaryAdapter = new AttendanceYearlySummaryAdapter(mContext, attendanceListAnnual);
        recyclerAnnualSummary.setAdapter(attendanceYearlySummaryAdapter);
        attendanceAnnualSummary();
    }

    //@OnClick(R.id.imgBack)
    private void Back() {
        super.onBackPressed();
    }


    private void setAdapterCalendarList() {
        // Attach to the activity
        disableDates = new ArrayList<>();
        attendanceList = new ArrayList<>();
        disableDates.add("02-feb-2020");
        disableDates.add("09-feb-2020");
        disableDates.add("16-feb-2020");
        disableDates.add("23-feb-2020");
        //setCustomResourceForDates();

        FragmentTransaction t = getSupportFragmentManager().beginTransaction();
        t.replace(R.id.calendar1, caldroidFragment);
        t.commitAllowingStateLoss();


        // Setup listener
        final CaldroidListener listener = new CaldroidListener() {

            @Override
            public void onSelectDate(Date date, View view) {

                Toast.makeText(mContext, formatter1.format(date) + "", Toast.LENGTH_SHORT).show();
                attendanceListDailySubjectWise(formatter1.format(date) + "");
            }

            @SuppressLint("WrongConstant")
            @Override
            public void onChangeMonth(int month, int year) {
                mMonth = month;
                mYear = year;
                countWeekendDays();

                tinyDB.putString("mMonth", CommonFunctions.capitalize(monthArray[month - 1]));
                tinyDB.putString("mYear", mYear + "");

                //Monthly Calendar refresh
                attendanceListMonthlySubjectWise();
                attendanceMonthlySummary();
                attendanceList(month, year);
                attendanceList2 = new ArrayList<>();
                attendanceAdapterSubjectWise.updateList(attendanceList2);

                //Monthly Summary refresh
            }


            @Override
            public void onLongClickDate(Date date, View view) {
                Toast.makeText(getApplicationContext(),
                        "Long click " + formatter.format(date),
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCaldroidViewCreated() {

                Button leftButton = caldroidFragment.getLeftArrowButton();
                Button rightButton = caldroidFragment.getRightArrowButton();
                TextView textView = caldroidFragment.getMonthTitleTextView();
                // Do customization here
            }
        };
        // Setup Caldroid
        caldroidFragment.setCaldroidListener(listener);
    }

    private void attendanceList(int month, int year) {
        attendanceList = new ArrayList<>();
        TimeTableModel listModel = new TimeTableModel();

        txtAttendanceStatusHead.setText("Attendance : " + CommonFunctions.capitalize(monthArray[month - 1]) + "-" + year);
        listModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));
        listModel.setStudentId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
        listModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        listModel.setClassId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID));
        //listModel.setAttDate("1-"+monthArray[month-1]+"-"+year);

        listModel.setYear(year + "");
        listModel.setMonth(month + "");

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        //Call<List<TimeTableModel>> call = apiService.AttendanceList(listModel);
        Call<JsonObject> call = apiService.AttendanceReportStudentDateWise(listModel);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                //int statusCode = response.code();
                try {
                    Log.e(mTAG, "error =" + response.toString());

                    Type type = new TypeToken<List<TimeTableModel>>() {
                    }.getType();

                    attendanceList = new Gson().fromJson(response.body().getAsJsonArray("Response"), type);
                    setCustomResourceForDates();

                } catch (Exception ex) {
                    Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void attendanceListDailySubjectWise(String date) {
        attendanceList2 = new ArrayList<>();
        TimeTableModel listModel = new TimeTableModel();
        listModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));
        listModel.setStudentId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
        listModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        listModel.setClassId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID));
        listModel.setAttDate(date);
        listModel.setYear(mYear + "");
        listModel.setMonth(mMonth + "");

        txtAttendanceStatusHead.setText("Attendance : " + date);

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        //Call<List<TimeTableModel>> call = apiService.AttendanceList(listModel);
        Call<JsonObject> call = apiService.AttendanceReportStudentDateSubjectWise(listModel);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                //int statusCode = response.code();
                try {
                    Log.e(mTAG, "error =" + response.toString());

                    Type type = new TypeToken<List<TimeTableModel>>() {
                    }.getType();

                    attendanceList2 = new Gson().fromJson(response.body().getAsJsonArray("Response"), type);
                    if (attendanceList2.size() == 0) {
                        txtRecyclerViewEmpty.setVisibility(View.VISIBLE);
                        recyclerAttendanceDayDetail.setVisibility(View.GONE);
                    } else {
                        recyclerAttendanceDayDetail.setVisibility(View.VISIBLE);
                        txtRecyclerViewEmpty.setVisibility(View.GONE);
                        attendanceAdapterSubjectWise.updateList(attendanceList2);
                    }

                } catch (Exception ex) {
                    Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void attendanceListMonthlySubjectWise() {
        attendanceListMonthlySubjectWise = new ArrayList<>();
        TimeTableModel listModel = new TimeTableModel();
        listModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));
        listModel.setStudentId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
        listModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        listModel.setClassId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID));
        listModel.setYear(mYear + "");
        listModel.setMonth(mMonth + "");
        listModel.setAttDate(null);

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        //Call<List<TimeTableModel>> call = apiService.AttendanceList(listModel);
        Call<JsonObject> call = apiService.AttendanceReportStudentDateSubjectWise(listModel);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                //int statusCode = response.code();
                try {
                    Log.e(mTAG, "error =" + response.toString());

                    Type type = new TypeToken<List<TimeTableModel>>() {
                    }.getType();

                    attendanceListMonthlySubjectWise = new Gson().fromJson(response.body().getAsJsonArray("Response"), type);
                    attendanceAdapterSubjectWiseMonthly.updateList(attendanceListMonthlySubjectWise);

                } catch (Exception ex) {
                    Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void attendanceMonthlySummary() {
        attendanceListAnnual = new ArrayList<>();
        TimeTableModel listModel = new TimeTableModel();
        listModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));
        listModel.setStudentId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
        listModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        listModel.setClassId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID));
        listModel.setMonth(mMonth + "");
        listModel.setYear(mYear + "");

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        //Call<List<TimeTableModel>> call = apiService.AttendanceList(listModel);
        Call<JsonObject> call = apiService.AttendanceReport(listModel);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                //int statusCode = response.code();
                try {
                    Log.e(mTAG, "error =" + response.toString());

                    Type type = new TypeToken<List<TimeTableModel>>() {
                    }.getType();

                    attendanceListMonthly = new Gson().fromJson(response.body().getAsJsonArray("Response"), type);
                    attendanceMonthlySummaryAdapter.updateList(attendanceListMonthly);

                } catch (Exception ex) {
                    Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void attendanceAnnualSummary() {
        attendanceListAnnual = new ArrayList<>();
        TimeTableModel listModel = new TimeTableModel();
        listModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));
        listModel.setStudentId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
        listModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        listModel.setClassId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID));
        listModel.setYear(mYear + "");

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        //Call<List<TimeTableModel>> call = apiService.AttendanceList(listModel);
        Call<JsonObject> call = apiService.AttendanceReport(listModel);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                //int statusCode = response.code();
                try {
                    Log.e(mTAG, "error =" + response.toString());

                    Type type = new TypeToken<List<TimeTableModel>>() {
                    }.getType();

                    attendanceListAnnual = new Gson().fromJson(response.body().getAsJsonArray("Response"), type);
                    attendanceYearlySummaryAdapter.updateList(attendanceListAnnual);

                } catch (Exception ex) {
                    Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
