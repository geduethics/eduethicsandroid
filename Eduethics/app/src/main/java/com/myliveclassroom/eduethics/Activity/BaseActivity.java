package com.myliveclassroom.eduethics.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.Settings;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.classes.CommonFunctions;
import com.myliveclassroom.eduethics.classes.TinyDB;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;


import java.io.ByteArrayOutputStream;
import java.util.List;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.CAMERA_METHOD;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.GALLERY_METHOD;


public abstract class BaseActivity extends AppCompatActivity {

    Context mContext;
    TinyDB tinyDB;
    String mCurrentPhotoPath;
    int MY_CAMERA_PERMISSION_CODE = 100;
    int PICK_IMAGE = 101;
    CommonFunctions comfun=new CommonFunctions();

    public void _BaseActivity(Context context) {
        mContext = context;
        tinyDB = new TinyDB(context);
       /* tinyDB.putString("UserId", "1");
        tinyDB.putString("SchoolId", "1");
        tinyDB.putString("SessionId", "1");
        tinyDB.putString("ClassId", "1");
        tinyDB.putString("StudentId", "1");*/
    }

    public String openCam( ImageView imgView, String method) {

        if (method.equalsIgnoreCase(CAMERA_METHOD)) {
            mCurrentPhotoPath = comfun.open_camera(mContext, MY_CAMERA_PERMISSION_CODE);
        }

        if (method.equalsIgnoreCase(GALLERY_METHOD)) {
            mCurrentPhotoPath = comfun.open_gallery(mContext, PICK_IMAGE);
        }

        if (mCurrentPhotoPath != null && mCurrentPhotoPath.contains("Error")) {
            Toast.makeText(this, mCurrentPhotoPath, Toast.LENGTH_SHORT).show();
        }
        return mCurrentPhotoPath;
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor =getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    Boolean _permisson=false;
    public boolean permissioncheck(final Context context,final int requestCode) {
        _permisson = false;
        Activity activity = (Activity) context;
        Dexter.withActivity(activity)
                .withPermissions(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // _permisson=true;
                        if (report.areAllPermissionsGranted()) {
                            _permisson = true;
                        }
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            _permisson = false;
                            openSettingsDialog(context,requestCode);
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                        _permisson = false;
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        _permisson = false;
                    }
                })
                .onSameThread()
                .check();
        return _permisson;
    }

    public Boolean openSettingsDialog(final Context context,final int requestCode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Required Permissions");
        builder.setMessage("This app require permission to use awesome feature. Grant them in app settings.");
        builder.setPositiveButton("Take Me To SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", context.getPackageName(), null);
                intent.setData(uri);
                Activity activity = (Activity) context;

                activity.startActivityForResult(intent, requestCode);
                _permisson = true;
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                _permisson = false;
            }
        });
        builder.show();
        return _permisson;
    }

    public void myToast(Context context,String message,int duration){
        Toast toast = Toast.makeText(context, message, duration);
        View view = toast.getView();

//Gets the actual oval background of the Toast then sets the colour filter
        view.setBackgroundColor(getResources().getColor(R.color.colorAccent));

//Gets the TextView from the Toast so it can be editted
        TextView text = view.findViewById(android.R.id.message);
        text.setTextColor(Color.WHITE);
        toast.show();
    }
}
