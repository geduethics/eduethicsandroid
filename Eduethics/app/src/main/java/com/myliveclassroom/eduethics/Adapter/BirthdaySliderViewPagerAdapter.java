package com.myliveclassroom.eduethics.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.myliveclassroom.eduethics.Models.UserModel;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.classes.CommonFunctions;

import java.util.List;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.API_BASE_URL;

public class BirthdaySliderViewPagerAdapter extends PagerAdapter {
    Context mContext;
    List<UserModel> userModelList;
    LayoutInflater layoutInflater;


    public BirthdaySliderViewPagerAdapter(Context context, List<UserModel> userModels) {
        this.mContext = context;
        this.userModelList =userModels;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return userModelList==null?0:userModelList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = layoutInflater.inflate(R.layout.birthday_slider, container, false);

        ImageView imgStudent = (ImageView) itemView.findViewById(R.id.imgStudent);
        TextView txtStudentName=(TextView)itemView.findViewById(R.id.txtStudentName);
        TextView txtClassName=(TextView)itemView.findViewById(R.id.txtClassName);
        TextView txtDOB=(TextView)itemView.findViewById(R.id.txtDOB);

        txtDOB.setText(userModelList.get(position).getDOB());
        txtStudentName.setText(CommonFunctions.capitalize(userModelList.get(position).getName()));
        txtClassName.setText(userModelList.get(position).getClassName() );
        if(userModelList.get(position).getProfilePic()!=null) {
            Glide.with(mContext)
                    .asBitmap()
                    .load(API_BASE_URL+ "ProfilePic/" + userModelList.get(position).getProfilePic())
                    .into(imgStudent);
        }

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}