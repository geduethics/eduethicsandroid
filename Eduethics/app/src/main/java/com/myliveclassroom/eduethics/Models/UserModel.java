package com.myliveclassroom.eduethics.Models;

import com.google.gson.annotations.SerializedName;

public class UserModel {


    @SerializedName("UserId")
    private String UserId;

    @SerializedName("SchoolId")
    private String SchoolId;

    @SerializedName("StudentId")
    private String StudentId;

    @SerializedName("SessionId")
    private String SessionId;

    @SerializedName("ClassId")
    private String ClassId;

    @SerializedName("ClassName")
    private String ClassName;




    @SerializedName("Name")
    private String Name;


    @SerializedName("Mobile")
    private String Mobile;


    @SerializedName("UserType")
    private String UserType;

    @SerializedName("InstituteType")
    private String InstituteType;


    @SerializedName("FcmToken")
    private String FcmToken;

    @SerializedName("ProfilePic")
    private String ProfilePic;

    public String getProfilePic() {
        return ProfilePic;
    }

    public String getClassName() {
        return ClassName;
    }

    @SerializedName("RollNo")
    private String RollNo;

    public String getRollNo() {
        return RollNo;
    }

    public void setName(String name) {
        Name = name;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public void setFcmToken(String fcmToken) {
        FcmToken = fcmToken;
    }

    public String getClassId() {
        return ClassId;
    }



    public String getSchoolId() {
        return SchoolId;
    }

    public String getMobile() {
        return Mobile;
    }

    public String getName() {
        return Name;
    }

    public String getSessionId() {
        return SessionId;
    }

    public String getUserId() {
        return UserId;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public void setUserType(String userType) {
        UserType = userType;
    }



    public String getUserType() {
        return UserType;
    }

    public String getInstituteType() {
        return InstituteType;
    }

    public void setClassId(String classId) {
        ClassId = classId;
    }

    public void setSchoolId(String schoolId) {
        SchoolId = schoolId;
    }

    public void setSessionId(String sessionId) {
        SessionId = sessionId;
    }


    public void setClassName(String className) {
        ClassName = className;
    }


    @SerializedName("DOB")
    private String DOB;

    public String getDOB() {
        return DOB;
    }

    public String getStudentId() {
        return StudentId;
    }

    public void setStudentId(String studentId) {
        StudentId = studentId;
    }
}


