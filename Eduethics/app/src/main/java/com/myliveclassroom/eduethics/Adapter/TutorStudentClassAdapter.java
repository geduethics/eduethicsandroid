package com.myliveclassroom.eduethics.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.myliveclassroom.eduethics.Fragments.ClassroomStudentFragment;
import com.myliveclassroom.eduethics.Models.TimeTableModel;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.TinyDB;

import java.util.List;

public class TutorStudentClassAdapter extends RecyclerView.Adapter<TutorStudentClassAdapter.vh> {

    Fragment fragment;
    List<TimeTableModel> classModelList;
    Context mContext;
    TinyDB tinyDB;

    public TutorStudentClassAdapter(ClassroomStudentFragment fragment, Context context, List<TimeTableModel> classModelList) {
        this.fragment = fragment;
        mContext=context;
        this.classModelList=classModelList;
        tinyDB=new TinyDB(mContext);
    }

    @NonNull
    @Override
    public vh onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new vh(LayoutInflater.from(fragment.getContext()).inflate(R.layout.single_item_classroom_classroom, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull vh holder, int position) {
        holder.subjectName.setText(classModelList.get(position).getSubjectName());
        holder.className.setText(classModelList.get(position).getClassName());
        holder.indication.setBackgroundColor(ResourcesCompat.getColor(fragment.getResources(),R.color.customPrimary, null));

         if (classModelList.get(position).getIsClassActive().equalsIgnoreCase("Y")){
             holder.itemView.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     tinyDB.putString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID,classModelList.get(position).getClassId());
                     tinyDB.putString(LocalConstants.TINYDB_LOGIN_KEYS.SUBJECT_NAME,classModelList.get(position).getSubjectName());
                     ((ClassroomStudentFragment)fragment).visitClassroom(position);
                 }
             });
            //holder.itemView.setOnClickListener(v -> );
            holder.isConfirm.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return classModelList.size();
    }

    static class vh extends RecyclerView.ViewHolder {

        View indication;
        TextView subjectName, className, time,isConfirm;

        public vh(@NonNull View itemView) {
            super(itemView);
            this.isConfirm = itemView.findViewById(R.id.isConfirm);
            this.indication = itemView.findViewById(R.id.indicationOfClassroom_singleItem);
            this.subjectName = itemView.findViewById(R.id.subject_single_item);
            this.className = itemView.findViewById(R.id.class_single_item);
            this.time = itemView.findViewById(R.id.dateTimeStarted_single_item);

        }
    }

}
