package com.myliveclassroom.eduethics.Adapter;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.myliveclassroom.eduethics.Fragments.SubFragments.FeeDetailStudent;
import com.myliveclassroom.eduethics.Fragments.WebAppFrag.FeeSchoolFragment;
import com.myliveclassroom.eduethics.Fragments.WebAppFrag.FeeStudentListFragment;
import com.myliveclassroom.eduethics.Models.FeeModel;
import com.myliveclassroom.eduethics.Models.FeesModel;
import com.myliveclassroom.eduethics.Models.TimeTableModel;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.TinyDB;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import static com.myliveclassroom.eduethics.Utils.HomeConstants.PROFILE;

public class FeeSchoolAdapter extends RecyclerView.Adapter<FeeSchoolAdapter.ViewHolder> {
    List<FeeModel> feesModelList;
    Context mContext;
    TinyDB tinyDB;
    Fragment fragment;
    int mScreenID;

    public FeeSchoolAdapter(Fragment fragment, Context context, List<FeeModel> feesModels, int screenID) {
        this.fragment = fragment;
        mContext = context;
        feesModelList = feesModels;
        tinyDB = new TinyDB(mContext);
        mScreenID = screenID;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
      if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.SCHOOL)) {
          if (fragment.getClass().equals(FeeStudentListFragment.class)) {}
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_fee_view, parent, false));
        }
      else if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER_SCHOOL) || tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER)) {
          return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_fee_view, parent, false));
      }
      else {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.school_fee_adapter, parent, false));
        }
       /* if (mScreenID == LocalConstants.FEE_VIEWS.ADMIN_SCREEN_1) {
            if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)){
                return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.school_fee_adapter, parent, false));
            } else {
                return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_fee_view, parent, false));
            }} else {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.school_fee_adapter, parent, false));
        } */
    }

    String mLastClass = "", mLastMonth = "", mLastYear = "";

    @Override
    public void onBindViewHolder(@NonNull FeeSchoolAdapter.ViewHolder holder, int position) {

        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.SCHOOL)) {
            /*if (fragment.getClass().equals(FeeStudentListFragment.class)) {
                listStudents(holder, position);
            } else {
                setSchoolView(holder, position);
            }*/
            setSchoolView(holder, position);
        } else if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)) {
            setStudentView(holder, position);
        } else if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER)) {
            setSchoolView(holder, position);
        }
    }

    Boolean isMainHeadVisible = true;

    private void listStudents(FeeSchoolAdapter.ViewHolder holder, int position) {
        if (isMainHeadVisible) {
            holder.relativeHead.setVisibility(View.VISIBLE);
            isMainHeadVisible = false;
        } else {
            holder.relativeHead.setVisibility(View.GONE);
        }
        //holder.txtPaidDate.setVisibility(View.GONE);
        //holder.txtDueDate.setVisibility(View.GONE);
        //holder.txtMonthYear.setVisibility(View.GONE);
        
        holder.txtBtnPay.setText("Reminder");
        holder.txtBtnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext, "Pending", Toast.LENGTH_SHORT).show();
            }
        });

        holder.txtHead.setText(feesModelList.get(position).getStudentName());
        holder.txtHead.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        holder.txtMonthYear.setText("Admission No: "+feesModelList.get(position).getRollNo());
        holder.txtDueDate.setText(""+ feesModelList.get(position).getFatherName());

        holder.txtMainDateHead.setText("Class: "+feesModelList.get(position).getClassName());
        holder.txtMonthHead.setText("Month: "+(Integer.parseInt(feesModelList.get(position).getMonth())<10?"0"+feesModelList.get(position).getMonth():feesModelList.get(position).getMonth())
                +"-"+feesModelList.get(position).getYear());

        if(feesModelList.get(position).getIsPaid().toUpperCase().equalsIgnoreCase("N")) {
            holder.txtPaidDate.setText("Not paid  " + feesModelList.get(position).getMobile() );
            holder.txtPaidDate.setTextColor(ResourcesCompat.getColor(fragment.getResources(), android.R.color.holo_red_dark ,null));
            holder.txtPaidDate.setVisibility(View.VISIBLE); 
        }else{
            holder.txtPaidDate.setText("Paid");
            holder.txtPaidDate.setVisibility(View.VISIBLE);
        }

        holder.txtOrderAmt.setText(fragment.getString(R.string.Rs) + feesModelList.get(position).getTotalFee());
        holder.txtOrderAmt.setVisibility(View.VISIBLE);
    }

    private void setSchoolView(FeeSchoolAdapter.ViewHolder holder, int position) {

        if (isMainHeadVisible) {
            if( holder.layoutHead!=null) {
                holder.layoutHead.setVisibility(View.VISIBLE);
                isMainHeadVisible = false;
            }
        } else {
            if( holder.layoutHead!=null) {
            holder.layoutHead.setVisibility(View.GONE);
        }}

        holder.txtClass.setText(feesModelList.get(position).getClassName());
        holder.txtTotal.setText(fragment.getString(R.string.Rs) + feesModelList.get(position).getTotalFee());
        holder.txtPaid.setText(fragment.getString(R.string.Rs) + feesModelList.get(position).getTotalPaid());
        holder.txtPending.setText(fragment.getString(R.string.Rs) + feesModelList.get(position).getTotalPending());

        holder.btnViewDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fragment.getClass().equals(FeeSchoolFragment.class)) {
                    ((FeeSchoolFragment) fragment).viewFeeDetail_btnclick(feesModelList.get(position));
                }
            }
        });

        // holder.imgDetail.setText(feesModelList.get(position).getClassName());

        /*holder.txtPaidDate.setVisibility(View.GONE);
        holder.txtDueDate.setVisibility(View.GONE);

        if (!mLastClass.equalsIgnoreCase(feesModelList.get(position).getClassName())
        ) {
            mLastClass = feesModelList.get(position).getClassName();
            mLastMonth = feesModelList.get(position).getMonth();
            mLastYear = feesModelList.get(position).getYear();
            holder.txtMainDateHead.setVisibility(View.VISIBLE);
            holder.relativeHead.setVisibility(View.VISIBLE);
            holder.txtMonthHead.setVisibility(View.GONE);
        } else {
            holder.txtMainDateHead.setVisibility(View.GONE);
            holder.relativeHead.setVisibility(View.GONE);
        }

        holder.txtMainDateHead.setText("Month: " +
                (Integer.parseInt(feesModelList.get(position).getMonth()) < 10 ? "0" + feesModelList.get(position).getMonth() : feesModelList.get(position).getMonth())
                + "-" + feesModelList.get(position).getYear()

                + "    " + feesModelList.get(position).getClassName()
        );


        if (feesModelList.get(position).getIsPaid().equalsIgnoreCase("Y")) {
            holder.txtBtnPay.setVisibility(View.GONE);
            holder.txtHead.setText("Paid");
            holder.txtPaidDate.setVisibility(View.GONE);
        } else {
            holder.txtBtnPay.setVisibility(View.VISIBLE);
            holder.txtBtnPay.setText(LocalConstants.PAYMENT_BTN_TXT.VIEW_DETAIL);

            holder.txtHead.setText("Due");
            holder.txtPaidDate.setVisibility(View.GONE);

            holder.txtBtnPay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(mContext, "hello", Toast.LENGTH_SHORT).show();
                    ((FeeSchoolFragment) fragment).viewFeeDetail_btnclick(feesModelList.get(position));
                }
            });
        }
        holder.txtOrderAmt.setText(feesModelList.get(position).getTotalFee());*/
    }

    private void setStudentView(FeeSchoolAdapter.ViewHolder holder, int position) {

        if(holder.txtMainDateHead!=null) {
            holder.txtMainDateHead.setText(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.NAME));

        holder.txtMonthHead.setText("Month: " + (Integer.parseInt(feesModelList.get(position).getMonth()) < 10 ? "0" + feesModelList.get(position).getMonth() : feesModelList.get(position).getMonth())
                + "-" + feesModelList.get(position).getYear());
            holder.txtMainDateHead.setVisibility(View.VISIBLE);
        }

        if (feesModelList.get(position).getIsPaid().equalsIgnoreCase("Y")) {
            holder.txtBtnPay.setVisibility(View.GONE);
            holder.txtHead.setText("Paid");
            holder.txtPaidDate.setText("Paid On: " + feesModelList.get(position).getPaidDate());
        } else {
            holder.txtBtnPay.setVisibility(View.VISIBLE);
            holder.txtHead.setText("Due");
            holder.txtPaidDate.setText("Paid On: ");
        }

        holder.txtDueDate.setText("Due Date: " + feesModelList.get(position).getDueDate());
        holder.txtOrderAmt.setText(mContext.getResources().getString(R.string.Rs) + feesModelList.get(position).getTotalFee());
        holder.txtMonthYear.setText("Class: " + feesModelList.get(position).getClassName());

        if (holder.txtBtnPay.getText().toString().equalsIgnoreCase(LocalConstants.PAYMENT_BTN_TXT.PAY_NOW)) {
            holder.txtBtnPay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((FeeSchoolFragment) fragment).paynow_btnclick(feesModelList.get(position));
                }
            });
        }
    }

    public void updateList(List<FeeModel> list) {
        feesModelList = list;
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return feesModelList == null ? 0 : feesModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtHead, txtMonthYear, txtDueDate, txtOrderAmt, txtBtnPay, txtPaidDate, txtMainDateHead, txtMonthHead;
        RelativeLayout relativeHead;

        TextView txtClass;
        TextView txtTotal;
        TextView txtPaid;
        TextView txtPending, btnViewDetail;
        ImageView imgDetail;
        LinearLayout layoutHead;
        RelativeLayout rrViewDetail;

        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);

                 txtClass = itemView.findViewById(R.id.txtClass)==null?null:itemView.findViewById(R.id.txtClass);
                txtTotal = itemView.findViewById(R.id.txtTotal)==null?null:itemView.findViewById(R.id.txtTotal);
                txtPaid = itemView.findViewById(R.id.txtPaid)==null?null:itemView.findViewById(R.id.txtPaid);
                txtPending = itemView.findViewById(R.id.txtPending)==null?null:itemView.findViewById(R.id.txtPending);
                //imgDetail= itemView.findViewById(R.id.imgDetail);
                layoutHead = itemView.findViewById(R.id.layoutHead)==null?null:itemView.findViewById(R.id.layoutHead);
                rrViewDetail = itemView.findViewById(R.id.rrViewDetail)==null?null:itemView.findViewById(R.id.rrViewDetail);
                btnViewDetail = itemView.findViewById(R.id.btnViewDetail)==null?null:itemView.findViewById(R.id.btnViewDetail);

                txtHead = itemView.findViewById(R.id.txtHead)==null?null:itemView.findViewById(R.id.txtHead);
                txtMonthYear = itemView.findViewById(R.id.txtMonthYear)==null?null:itemView.findViewById(R.id.txtMonthYear);
                txtDueDate = itemView.findViewById(R.id.txtDueDate)==null?null:itemView.findViewById(R.id.txtDueDate);
                txtOrderAmt = itemView.findViewById(R.id.txtOrderAmt)==null?null:itemView.findViewById(R.id.txtOrderAmt);
                txtBtnPay = itemView.findViewById(R.id.txtBtnPay)==null?null:itemView.findViewById(R.id.txtBtnPay);
                txtPaidDate = itemView.findViewById(R.id.txtPaidDate)==null?null:itemView.findViewById(R.id.txtPaidDate);
                txtMainDateHead = itemView.findViewById(R.id.txtDateHead)==null?null:itemView.findViewById(R.id.txtDateHead);
                relativeHead = itemView.findViewById(R.id.relativeHead)==null?null:itemView.findViewById(R.id.relativeHead);
                txtMonthHead = itemView.findViewById(R.id.txtMonthHead)==null?null:itemView.findViewById(R.id.txtMonthHead);
                if(txtBtnPay!=null)
                txtBtnPay.setText(LocalConstants.PAYMENT_BTN_TXT.PAY_NOW);

        }
    }
}
