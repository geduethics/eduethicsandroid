package com.myliveclassroom.eduethics.Adapter;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.myliveclassroom.eduethics.Activity.HomeworkDashActivity;
import com.myliveclassroom.eduethics.Models.HomeWorkModel;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.classes.CommonFunctions;
import com.myliveclassroom.eduethics.classes.TinyDB;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeworkDashAdapter extends RecyclerView.Adapter<HomeworkDashAdapter.ViewHolder> {

    List<HomeWorkModel> homeWorkModelList;
    Context mContext;
    TinyDB tinyDB;

    Date dateToday;
    String dateTodayStr;
    Date workDate;
    SimpleDateFormat dateformat = new SimpleDateFormat("dd-MMM-yyyy");

    public HomeworkDashAdapter(Context context, List<HomeWorkModel> list) {
        homeWorkModelList = list;
        mContext = context;
        tinyDB = new TinyDB(mContext);

        Calendar startDate = Calendar.getInstance();
        dateTodayStr = dateformat.format(startDate.getTime());

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_work_adp_layout, parent, false);
        return new ViewHolder(view);
    }

    String mVideoLink;
    String[] mVideoLinkArray;
    private long mLastClickTime = 0;
    String attachments;
    String attachmentArray[] = {};
    TextView txtBtnDownload;
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        //holder.txtBtnUploadHomeWork.setVisibility(View.GONE);
        //holder.txtHead.setText(CommonFunctions.capitalize(homeWorkModelList.get(position).getWorkHead()));
        //holder.txtBtnUploadHomeWork.setText("Edit");

        holder.txtBtnUploadHomeWork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        holder.txtHead.setText(homeWorkModelList.get(position).getWorkHead());
        holder.txtDesc.setText(homeWorkModelList.get(position).getWorkDetail());

        mVideoLink = homeWorkModelList.get(position).getWorkDetail();
        if (mVideoLink.toLowerCase().contains("youtu") && mVideoLink.toLowerCase().contains("/")) {
            mVideoLinkArray = mVideoLink.split("/");
        }

        attachments = homeWorkModelList.get(position).getAttachment();
        if (attachments != null && !attachments.isEmpty()) {
            attachmentArray = attachments.split(",");
        }

        //******************Attachment List*********************
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        int sr = 0;
        for (String str : attachmentArray) {
            sr = sr + 1;
            final View rowLayout = inflater.inflate(R.layout.file_table_row, null);
            TextView txtSr = rowLayout.findViewById(R.id.txtSr);
            txtSr.setText(sr + ".");
            TextView txtFileName = rowLayout.findViewById(R.id.txtFileName);
            txtFileName.setText(str);
            txtBtnDownload = rowLayout.findViewById(R.id.txtBtnDownload);
            if(sr<attachmentArray.length){
                txtBtnDownload.setVisibility(View.GONE);
            }
            TableRow trAttachment = (TableRow) rowLayout;
            holder.tableAttachments.addView(trAttachment);
        }
        //******************Attachment List End*********************

        if (homeWorkModelList.get(position).getWorkHead().toLowerCase().contains("youtube")) {
            holder.imgWatchVideo.setVisibility(View.VISIBLE);

            holder.imgWatchVideo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    watchYoutubeVideo(mContext, mVideoLinkArray[mVideoLinkArray.length - 1]);
                }
            });
        }

        try {
            dateToday = dateformat.parse(dateTodayStr);
            workDate = dateformat.parse(homeWorkModelList.get(position).getWorkDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (workDate.equals(dateToday)) {
            if(!homeWorkModelList.get(position).getWorkHead().toLowerCase().contains("no work assigned")){
                holder.imgBtnDelete.setVisibility(View.VISIBLE);
                holder.imgBtnDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mContext instanceof HomeworkDashActivity) {
                            ((HomeworkDashActivity) mContext).deleteEntry(homeWorkModelList.get(position));
                        }
                    }
                });
            }
        }
        if (txtBtnDownload != null) {
            txtBtnDownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                        return;
                    }
                    mLastClickTime = SystemClock.elapsedRealtime();
                    if (mContext instanceof HomeworkDashActivity) {

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    // Your implementation goes here
                                    ((HomeworkDashActivity) mContext).downloadFiles(attachmentArray, holder.txtPercentage);
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            }
                        }).start();
                    }
                }
            });
        }
    }

    public static void watchYoutubeVideo(Context context, String id) {
        Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + id));
        Intent webIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://www.youtube.com/watch?v=" + id));
        try {
            context.startActivity(appIntent);
        } catch (ActivityNotFoundException ex) {
            context.startActivity(webIntent);
        }
    }

    @Override
    public int getItemCount() {
        return homeWorkModelList == null ? 0 : homeWorkModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.layoutHeader)
        LinearLayout layoutHeader;

        @BindView(R.id.txtSubjectHead)
        TextView txtSubjectHead;

        @BindView(R.id.txtHead)
        TextView txtHead;

        @BindView(R.id.txtDesc)
        TextView txtDesc;

        @BindView(R.id.txtBtnAssignHomeWork)
        TextView txtBtnAssignHomeWork;

        @BindView(R.id.layoutAssignWork)
        LinearLayout layoutAssignWork;

        @BindView(R.id.imgBtnAssign)
        ImageView imgBtnAssign;

        @BindView(R.id.imgBtnDelete)
        ImageView imgBtnDelete;

        @BindView(R.id.imgAttIcon)
        ImageView imgAttIcon;

        @BindView(R.id.txtAttachCount)
        TextView txtAttachCount;

        @BindView(R.id.imgDownload)
        ImageView imgDownload;

        @BindView(R.id.imgWatchVideo)
        ImageView imgWatchVideo;

        @BindView(R.id.txtPercentage)
        TextView txtPercentage;

        @BindView(R.id.tableAttachments)
        TableLayout tableAttachments;

        @BindView(R.id.txtBtnUploadHomeWork)
        TextView  txtBtnUploadHomeWork;


        public ViewHolder(@NonNull View view) {
            super(view);
            ButterKnife.bind(this, view);
            layoutHeader.setVisibility(View.GONE);
        }
    }
}
