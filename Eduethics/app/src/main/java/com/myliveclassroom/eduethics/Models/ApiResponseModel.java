package com.myliveclassroom.eduethics.Models;

import com.google.gson.annotations.SerializedName;

public class ApiResponseModel {

    @SerializedName("Status")
    private String Status;

    @SerializedName("Message")
    private String Message;

    @SerializedName("Response")
    private Object Response;

    @SerializedName("CodeVersion")
    private String CodeVersion;

    @SerializedName("NotificationURL")
    private String NotificationURL;

    @SerializedName("AppVersion")
    private String AppVersion;

    @SerializedName("UnderMaintenance")
    private String UnderMaintenance;

    public String getAppVersion() {
        return AppVersion;
    }

    public String getNotificationURL() {
        return NotificationURL;
    }

    public String getMessage() {
        return Message;
    }

    public String getStatus() {
        return Status;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getCodeVersion() {
        return CodeVersion;
    }

    public String getUnderMaintenance() {
        return UnderMaintenance;
    }

    public Object getResponse() {
        return Response;
    }
}
