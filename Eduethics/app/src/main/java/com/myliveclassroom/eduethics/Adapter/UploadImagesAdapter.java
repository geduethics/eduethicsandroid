package com.myliveclassroom.eduethics.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.myliveclassroom.eduethics.Activity.HomeWorkActivity;
import com.myliveclassroom.eduethics.Activity.HomeWorkAsignActivity;
import com.myliveclassroom.eduethics.Fragments.Exam.AddSolution;
import com.myliveclassroom.eduethics.Fragments.Exam.StartSubmission;
import com.myliveclassroom.eduethics.Fragments.Exam.CreateAssignmentFragment;
import com.myliveclassroom.eduethics.Fragments.Exam.CreateTestFragment;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.ExtraFunctions;

import java.util.List;

import static com.myliveclassroom.eduethics.Utils.LocalConstants.FROM.FROM_ADD_SOLUTION;
import static com.myliveclassroom.eduethics.Utils.LocalConstants.FROM.FROM_ASSIGNMENT_FRAG;
import static com.myliveclassroom.eduethics.Utils.LocalConstants.FROM.FROM_CREATE_ASSIGNMENT;
import static com.myliveclassroom.eduethics.Utils.LocalConstants.FROM.FROM_CREATE_TEST;
import static com.myliveclassroom.eduethics.Utils.LocalConstants.FROM.FROM_HOMEWORK_ACTVITY;
import static com.myliveclassroom.eduethics.Utils.LocalConstants.FROM.FROM_HOMEWORK_ACTVITY_STUDENT;
import static com.myliveclassroom.eduethics.Utils.LocalConstants.FROM.FROM_SUMBIT_TEST;
import static com.myliveclassroom.eduethics.Utils.LocalConstants.FROM.FROM_SUMBIT_TEST_ATT;
import static com.myliveclassroom.eduethics.Utils.LocalConstants.FROM.FROM_TEST_FRAG;

public class UploadImagesAdapter extends RecyclerView.Adapter<UploadImagesAdapter.vh> {

    List<String> data;
    Fragment fragment;
    public int from;
    View progress;
    Context mContext;


    public void setProgress(View progress){
        this.progress = progress;
    }

    public UploadImagesAdapter(List<String> data, Fragment fragment, int from, View progress) {
        this.fragment = fragment;
        this.data = data;
        this.from = from;
        this.progress = progress;
        mContext=fragment.getContext();
    }

    public UploadImagesAdapter(List<String> data, Activity fragment, int from, View progress) {
        mContext = fragment;
        this.data = data;
        this.from = from;
        this.progress = progress;
    }

    @NonNull
    @Override
    public vh onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new vh(LayoutInflater.from(parent.getContext()).inflate(R.layout.upload_images_single_item, parent, false));
    }


    @Override
    public void onBindViewHolder(@NonNull vh holder, int position) {

        /*if (((CreateTest) fragment).uploadRecyclerView.getBackground() == null)
            ((CreateTest) fragment).uploadRecyclerView.setBackground(ResourcesCompat.getDrawable(fragment.getResources(),R.drawable.background_popup_custom,null));*/

        holder.itemView.setOnClickListener(v -> {
            if(fragment==null){
                ExtraFunctions.downloadImageAndShow(progress,data.get(position),(Activity) mContext);
            }
            else {
                ExtraFunctions.downloadImageAndShow(progress,data.get(position),fragment.getActivity());
            }
        });

        if (from == FROM_ASSIGNMENT_FRAG || from == FROM_TEST_FRAG || from == FROM_SUMBIT_TEST_ATT  ) {
            Glide.with(mContext).load(R.drawable.ic_download).into(holder.deleteImage);
        } else {
            holder.deleteImage.setOnClickListener(v -> {
                if (from == FROM_CREATE_ASSIGNMENT) {
                    ((CreateAssignmentFragment) fragment).deleteImage(position);
                } else if (from == FROM_CREATE_TEST) {
                    ((CreateTestFragment) fragment).deleteImage(position);
                } else if (from == FROM_SUMBIT_TEST) {
                    ((StartSubmission) fragment).deleteImage(position);
                } else if (from == FROM_ADD_SOLUTION) {
                    ((AddSolution) fragment).deleteImage(position);
                } else if(from ==FROM_HOMEWORK_ACTVITY){
                    ((HomeWorkAsignActivity) mContext).deleteImage(position);
                }
                else if(from ==FROM_HOMEWORK_ACTVITY_STUDENT){
                    ((HomeWorkActivity) mContext).deleteImage(position);
                }
            });

        }


        ((TextView) holder.itemView.findViewById(R.id.nameImage)).setText(data.get(position).startsWith("314") ? data.get(position).substring(3) : data.get(position));
        if (data.get(position).endsWith("pdf")) {
            Glide.with(mContext).load(R.drawable.ic_pdf).centerCrop().fitCenter().into((ImageView) holder.itemView.findViewById(R.id.imageType));
        } else if (data.get(position).endsWith("jpg")) {
            Glide.with(mContext).load(R.drawable.ic_image).centerCrop().fitCenter().into((ImageView) holder.itemView.findViewById(R.id.imageType));
        } else {
            Glide.with(mContext).load(R.drawable.ic_attachment_pin).centerCrop().fitCenter().into((ImageView) holder.itemView.findViewById(R.id.imageType));
        }

    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    class vh extends RecyclerView.ViewHolder {
        ImageView deleteImage;

        public vh(@NonNull View itemView) {
            super(itemView);
            this.deleteImage = itemView.findViewById(R.id.deleteBtnImage);
        }
    }
}
