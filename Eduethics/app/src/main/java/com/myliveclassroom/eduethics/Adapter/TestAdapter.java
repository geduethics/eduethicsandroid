package com.myliveclassroom.eduethics.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;


import com.myliveclassroom.eduethics.Activity.ExamQActivity;
import com.myliveclassroom.eduethics.Activity.MainActivity;
import com.myliveclassroom.eduethics.Activity.ResultActivity;
import com.myliveclassroom.eduethics.Fragments.Exam.TestFragment;
import com.myliveclassroom.eduethics.Models.ExamModel;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.CommonFunctions;
import com.myliveclassroom.eduethics.classes.TinyDB;

import java.util.HashMap;
import java.util.List;

import static com.myliveclassroom.eduethics.Utils.HomeConstants.PROFILE;

public class TestAdapter extends RecyclerView.Adapter<TestAdapter.vh> {


    List<ExamModel> examModelList;
    Fragment fragment;
    int submitted = 0;
    TinyDB tinyDB;
    Context mContext;
    String mProfile = "";

    public TestAdapter(Fragment fragment, List<ExamModel> examModelList, Context context) {
        this.fragment = fragment;
        //this.tests = HomeClassroomFragment.model.getTest();
        MainActivity.ids = null;
        MainActivity.ids = new HashMap<>();
        mContext = context;
        tinyDB = new TinyDB(mContext);
        mProfile = tinyDB.getString(PROFILE);
        this.examModelList = examModelList;
    }


    @NonNull
    @Override
    public vh onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new vh(LayoutInflater.from(fragment.getContext()).inflate(R.layout.test_single_item, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull vh holder, int position) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            holder.txtTitle.setText(Html.fromHtml("<font color='#cc0029'>"
                    + examModelList.get(position).getExamType() + "</font> - " + CommonFunctions.capitalize(examModelList.get(position).getExamTitle())
                    , Html.FROM_HTML_MODE_COMPACT));
        } else {
            holder.txtTitle.setText(Html.fromHtml("<font color='#cc0029'>"
                    + examModelList.get(position).getExamType() + " - " + CommonFunctions.capitalize(examModelList.get(position).getExamTitle())
                    + "\nClass: " + examModelList.get(position).getExamType() + " - " + examModelList.get(position).getClassName()
                    + " - " + "Subject: " + examModelList.get(position).getExamType() + " - " + examModelList.get(position).getSubjectName() + "</font>"));
        }


        //.setText(examModelList.get(position).getStartTime().split("-")[0]+"-"+examModelList.get(position).getStartTime().split("-")[1]+"-"+examModelList.get(position).getStartTime().split("-")[2]);
        // "\n(" + ExtraFunctions.getReadableTime(Integer.parseInt(examModelList.get(position).getStartTime().split(":")[0]), Integer.parseInt(examModelList.get(position).getStartTime().split(":")[1])) + ")");

        examStatus(holder, position);
        studentOptions(holder, position);
        teacherOptions(holder, position);

        /*Map<String, Object> data = tests.get(position);*//*
        if (mProfile.equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)) {
            holder.itemView.findViewById(R.id.teacherSection).setVisibility(View.GONE);
            holder.btnStartExam.setVisibility(View.VISIBLE);
            holder.btnStartExam.setText("View");
            holder.btnStartExam.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tinyDB.putString(LocalConstants.Exam_Constants.ExamId, examModelList.get(position).getExamId());

                    tinyDB.putString(LocalConstants.Exam_Constants.STUDENT_NAME, tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.NAME));
                    tinyDB.putString(LocalConstants.Exam_Constants.ExamClass, examModelList.get(position).getClassName());
                    tinyDB.putString(LocalConstants.Exam_Constants.ExamSubject, examModelList.get(position).getExamTitle());
                    tinyDB.putString(LocalConstants.Exam_Constants.ExamTime, examModelList.get(position).getStartTime());
                    tinyDB.putString(LocalConstants.Exam_Constants.EndDate, examModelList.get(position).getEndTime());
                    tinyDB.putString(LocalConstants.Exam_Constants.ExamMaxMarks, examModelList.get(position).getMaxMarks());
                    //tinyDB.putString(LocalConstants.Exam_Constants.ExamTotalQ,examModelList.get(position).get);
                    //tinyDB.putString(LocalConstants.Exam_Constants.ExamTotalQAtt);
                    tinyDB.putString(LocalConstants.Exam_Constants.ExamPerCorrect, examModelList.get(position).getMarksPerCorrect());
                    tinyDB.putString(LocalConstants.Exam_Constants.ExamNegativePerIncorrect, examModelList.get(position).getNegativePerIncorrect());
                    Intent intent = new Intent(mContext, ExamQActivity.class);
                    mContext.startActivity(intent);
                }
            });
        } else {
            int id = View.generateViewId();
            //MainActivity.ids.put(id, "test;" + data.get("resultId").toString());
            // holder.itemView.setId(id);
            //fragment.getActivity().registerForContextMenu(holder.itemView);
        }

        if (examModelList.get(position).getExamType().equalsIgnoreCase(LocalConstants.Exam_TYPE.ORAL)) {
            if (mProfile.equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)) {
                holder.btnStartExamGoLive.setText("Join");
            } else {

            }
            holder.btnAddQuestions.setVisibility(View.GONE);
            holder.btnStartExamGoLive.setVisibility(View.VISIBLE);
            holder.btnStartExamGoLive.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((TestFragment) fragment).startOralTest(examModelList.get(position));
                }
            });
        }




        holder.txtBtnAddEditSolution.setOnClickListener(v -> ((TestFragment) fragment).solution(position));
        holder.btnAddQuestions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tinyDB.putString(LocalConstants.Exam_Constants.ExamId, examModelList.get(position).getExamId());

                //tinyDB.putString(LocalConstants.Exam_Constants.STUDENT_NAME);
                //tinyDB.putString(LocalConstants.Exam_Constants.ExamClass);
                //tinyDB.putString(LocalConstants.Exam_Constants.ExamSubject);
                //tinyDB.putString(LocalConstants.Exam_Constants.ExamTime);
                //tinyDB.putString(LocalConstants.Exam_Constants.ExamMaxMarks);
                //tinyDB.putString(LocalConstants.Exam_Constants.ExamTotalQ);
                //tinyDB.putString(LocalConstants.Exam_Constants.ExamTotalQAtt);

                //Intent intent = new Intent(mContext, ExamQActivity.class);
                //mContext.startActivity(intent);
                ((TestFragment) fragment).callAddQuestionFragment();
            }
        });

        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER)) {

            //holder.submittedStudents.setText(((List) data.get("submitted")).size() + "/" + HomeClassroomFragment.model.getCurrentStudents().size() + " Submitted");
            holder.txtStatus.setVisibility(View.GONE);
            holder.imgBtnEdit.setOnClickListener(v -> ((TestFragment) fragment).edit(position));
            holder.txtBtnViewSubmission.setOnClickListener(v -> ((TestFragment) fragment).view(position));

        } else {

            Calendar calendar = Calendar.getInstance();
            *//*calendar.set(
                    Integer.parseInt(((String) data.get("date")).split("-")[0]),
                    Integer.parseInt(((String) data.get("date")).split("-")[1]) - 1,
                    Integer.parseInt(((String) data.get("date")).split("-")[2]),
                    Integer.parseInt(((String) data.get("time")).split(":")[0]),
                    Integer.parseInt(((String) data.get("time")).split(":")[1])
            );*//*

            //TODO
            *//*calendar.set(
                    Integer.parseInt(((String) examModelList.get(position).getStartDate()).split("-")[0]),
                    Integer.parseInt(((String) examModelList.get(position).getStartDate()).split("-")[1]) - 1,
                    Integer.parseInt(((String) examModelList.get(position).getStartDate()).split("-")[2]),
                    Integer.parseInt(((String) examModelList.get(position).getStartTime()).split(":")[0]),
                    Integer.parseInt(((String) examModelList.get(position).getStartTime()).split(":")[1])
            );*//*


            if (examModelList.get(position).getExamIdDone().equalsIgnoreCase("0")) {
                //holder.status.setText(((List) data.get("submitted")).contains(GlobalVariables.uid) ? "Completed" : "Not Started");
                examModelList.get(position).setIsDead(false);
            } else {
                //TODO
                //set Status
                holder.txtStatus.setText("Submitted");
                examModelList.get(position).setIsDead(true);
                holder.btnStartExam.setVisibility(View.GONE);
            }


           *//* if (((List) data.get("submitted")).contains(GlobalVariables.uid)) {
                submitted++;
            }*//*
         *//*if (position == tests.size() - 1) {
                ((TestFragment) fragment).submittedText.setText(submitted + "/" + tests.size() + " Submitted");
                ((TestFragment) fragment).pendingText.setText((tests.size() - submitted) + " Pending");
            }*//*
            //TODO
            //IsExam Expires
            // holder.itemView.setOnClickListener(v -> ((TestFragment) fragment).open(position, true));
        }*/
    }

    private void examStatus(vh holder, int position) {
        //HTML EXAMPLE
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            txtHeader.setText(Html.fromHtml("Submit Home Work - <font color=#cc0029>" + homeWorkModel.getSubjectName() + "</font>", Html.FROM_HTML_MODE_LEGACY));
        } else {
            txtHeader.setText(Html.fromHtml("Submit Home Work - <font color=#cc0029>" + homeWorkModel.getSubjectName() + "</font>"));
        }*/

        //Status
        if (examModelList.get(position).getIsLive().equalsIgnoreCase("Y")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                holder.txtStatus.setText(Html.fromHtml("Status: <font color=#cc0029>Live</font>", Html.FROM_HTML_MODE_LEGACY));
            } else {
                holder.txtStatus.setText(Html.fromHtml("Status: <font color=#cc0029>Live</font>"));
            }
        } else if (examModelList.get(position).getIsLive().equalsIgnoreCase("N")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                holder.txtStatus.setText(Html.fromHtml("Status: <font color=#cc0029>Not Live</font>", Html.FROM_HTML_MODE_LEGACY));
            } else {
                holder.txtStatus.setText(Html.fromHtml("Status: <font color=#cc0029>Not Live</font>"));
            }
        }

        if (Integer.parseInt(examModelList.get(position).getExamIdDone())>0) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                holder.txtStatus.setText(Html.fromHtml("Status: <font color=#cc0029>Submitted</font>", Html.FROM_HTML_MODE_LEGACY));
                holder.btnResult.setVisibility(View.VISIBLE);
                holder.btnResult.setText("Result");
            }
        }
    }

    private void studentOptions(vh holder, int position) {
        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER)) {
            return;
        }

        holder.txtMaxMarks.setText(
                 "Class :  "+examModelList.get(position).getClassName()+""
                                + "\nSubject :  "+ examModelList.get(position).getSubjectName()+""
                                + "\n\nTotal Questions :  "+examModelList.get(position).getTotalQuestions()+""
                                + "\nQuesetions to Attemp :  "+examModelList.get(position).getToAttempt()+""
                                + "\n\nMax Marks :  "+(examModelList.get(position).getMaxMarks() != null || examModelList.get(position).getMaxMarks().equalsIgnoreCase("") ? Math.round(Double.parseDouble(examModelList.get(position).getMaxMarks())) : "")+""


            );


        holder.txtDuration.setText( "Duration :  " + examModelList.get(position).getDuration() + " min(s)" );

        holder.txtTime.setText( "Start Date / Time :  \n"+examModelList.get(position).getStartTime()+""
                + "\n\nSubmit Date :  " + examModelList.get(position).getExamSubmitDate()+""
                + "\nQuestion(s) Attempted :  " + examModelList.get(position).getAttempted()+""
                + "\n\nCorrect :  " + examModelList.get(position).getCorrectCount()+""
                + "\nInCorrect :  " + examModelList.get(position).getInCorrectCount()+""
                + "\n\nNegative Marks :  " + examModelList.get(position).getNegativeMarks()+""
                + "\nMarks Obtained :  " + examModelList.get(position).getMarksObtained()+""
        );

         /*      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            holder.txtMaxMarks.setText(
                    Html.fromHtml( "Class :  <b>"+examModelList.get(position).getClassName()+"</b>"
                            + "<br>Subject :  <b>"+ examModelList.get(position).getSubjectName()+"</b>"
                            + "<br><br>Total Questions :  <b>"+examModelList.get(position).getTotalQuestions()+"</b>"
                            + "<br>Quesetions to Attemp :  <b>"+examModelList.get(position).getToAttempt()+"</b>"
                            + "<br><br>Max Marks :  <b>"+(examModelList.get(position).getMaxMarks() != null || examModelList.get(position).getMaxMarks().equalsIgnoreCase("") ? Math.round(Double.parseDouble(examModelList.get(position).getMaxMarks())) : "")+"</b>"
                            ,Html.FROM_HTML_MODE_COMPACT
                    )
                    );


            holder.txtDuration.setText(Html.fromHtml( "Duration :  <b>" + examModelList.get(position).getDuration() + " min(s)</b>",Html.FROM_HTML_MODE_COMPACT));

            holder.txtTime.setText(Html.fromHtml( "Start Date / Time :  <b>"+examModelList.get(position).getStartTime()+"</b>"
                    + "<br><br>Submit Date :  <b>" + examModelList.get(position).getExamSubmitDate()+"</b>"
                    + "<br>Question(s) Attempted :  <b>" + examModelList.get(position).getAttempted()+"</b>"
                    + "<br><br>Correct :  <b>" + examModelList.get(position).getCorrectCount()+"</b>"
                    + "<br>InCorrect :  <b>" + examModelList.get(position).getInCorrectCount()+"</b>"
                    + "<br><br>Negative Marks :  <b>" + examModelList.get(position).getNegativeMarks()+"</b>"
                    + "<br>Marks Obtained :  <b>" + examModelList.get(position).getMarksObtained()+"</b>"
            ));

        } else {
            holder.txtMaxMarks.setText(
                    Html.fromHtml( "Class :  <b>  "+examModelList.get(position).getClassName()+"</b>"
                                    + "<br>Subject :   <b>  "+ examModelList.get(position).getSubjectName()+"</b>"
                                    + "<br><br>Total Questions :  <b>"+examModelList.get(position).getTotalQuestions()+"</b>"
                                    + "<br>Quesetions to Attemp :  <b>"+examModelList.get(position).getToAttempt()+"</b>"
                                    + "<br>Max Marks :  <b>"+(examModelList.get(position).getMaxMarks() != null || examModelList.get(position).getMaxMarks().equalsIgnoreCase("") ? Math.round(Double.parseDouble(examModelList.get(position).getMaxMarks())) : "")+"</b>"

                    )
            );

            holder.txtDuration.setText(Html.fromHtml( "Duration : <b>" + examModelList.get(position).getDuration() + " min(s)</b>" ));

            holder.txtTime.setText("Start Date / Time: "+examModelList.get(position).getStartTime()
                    + "\n\nSubmit Date: " + examModelList.get(position).getExamSubmitDate()
                    + "\nQuestion(s) Attempted: " + examModelList.get(position).getAttempted()
                    + "\n\nCorrect: " + examModelList.get(position).getCorrectCount()
                    + "\nInCorrect: " + examModelList.get(position).getInCorrectCount()
                    + "\n\nNegative Marks: " + examModelList.get(position).getNegativeMarks()
                    + "\nMarks Obtained: " + examModelList.get(position).getMarksObtained()
            );
        }

        */



        holder.btnStartExamGoLive.setEnabled(false);
        holder.btnStartExamGoLive.setText(LocalConstants.Exam_Constants.btnText_StartExam);

        if (examModelList.get(position).getExamType().equalsIgnoreCase(LocalConstants.Exam_TYPE.ORAL)) {
            //show start button in case of MCQ & Subjective
            if (examModelList.get(position).getIsLive().toUpperCase().equalsIgnoreCase("Y")) {
                //TODO
                //Check time also before this button
                holder.btnStartExamGoLive.setText(LocalConstants.Exam_Constants.btnText_JoinExam);
                holder.btnStartExamGoLive.setEnabled(true);
            }
        }

        if (!examModelList.get(position).getExamType().equalsIgnoreCase(LocalConstants.Exam_TYPE.ORAL)) {
            //show start button in case of MCQ & Subjective
            if (examModelList.get(position).getIsLive().toUpperCase().equalsIgnoreCase("Y")) {
                //TODO
                //Check time also before this button
                holder.btnStartExamGoLive.setEnabled(true);
            }
        }

        //Not live
        if (!examModelList.get(position).getIsLive().toUpperCase().equalsIgnoreCase("Y")) {
            holder.btnStartExamGoLive.setEnabled(false);
            holder.btnStartExamGoLive.setBackground(mContext.getDrawable(R.drawable.background_blocks_12dp_disabled));
        }

        //btn click functions according to Text
        btnClickFunctionSettingsStudents(holder, position);

        if (Integer.parseInt(examModelList.get(position).getExamIdDone()) > 0) {
            holder.btnStartExamGoLive.setVisibility(View.GONE);
            holder.btnStartExamGoLive.setEnabled(false);
            holder.btnStartExamGoLive.setBackground(mContext.getDrawable(R.drawable.background_blocks_12dp_disabled));

            holder.btnAnsKey.setVisibility(View.VISIBLE);
        }
    }

    private void teacherOptions(vh holder, int position) {
        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)) {
            return;
        }
        holder.txtTime.setText("Start Date / Time: \n"+examModelList.get(position).getStartTime());

        holder.txtMaxMarks.setText(
                "Class: " + examModelList.get(position).getClassName()
                        + "\nSubject: " + examModelList.get(position).getSubjectName()
                        + "\n\nTotal Questions: " + examModelList.get(position).getTotalQuestions()
                        + "\nQuesetions to Attemp: " + examModelList.get(position).getToAttempt()
                        + "\nMax Marks : " + (examModelList.get(position).getMaxMarks() != null || examModelList.get(position).getMaxMarks().equalsIgnoreCase("") ? Math.round(Double.parseDouble(examModelList.get(position).getMaxMarks())) : ""));
        holder.txtDuration.setText("Duration : " + examModelList.get(position).getDuration() + " min(s)"
                + (mProfile.equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER) ? " \n Question(s) Created: " + examModelList.get(position).getNoOfQCreated() : "")
                + (mProfile.equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER) ? " \n Marks Created: " + Float.parseFloat(examModelList.get(position).getNoOfQCreated()) * Float.parseFloat(examModelList.get(position).getMarksPerCorrect()) : "")
        );

        if (examModelList.get(position).getExamType().equalsIgnoreCase(LocalConstants.Exam_TYPE.ORAL)) {
            holder.btnMakeLive.setText(LocalConstants.Exam_Constants.btnText_StartExam);
            holder.btnMakeLive.setVisibility(View.VISIBLE);
            holder.btnMakeLive.setEnabled(true);
            holder.btnAddQuestions.setVisibility(View.GONE);
        }

        if (!examModelList.get(position).getExamType().equalsIgnoreCase(LocalConstants.Exam_TYPE.ORAL)) {
            holder.btnAddQuestions.setVisibility(View.VISIBLE);
        }

        //Set options when exam submitted
        holder.txtBtnViewSubmission.setVisibility(View.VISIBLE);

        //set buttons when exam is live / Not Live
        if (examModelList.get(position).getIsLive().toUpperCase().equalsIgnoreCase("Y")) {
            //TODO
            //Check time also before this button
            //holder.btnMakeLive.setEnabled(false);
            //holder.btnMakeLive.setBackground(mContext.getDrawable(R.drawable.background_blocks_12dp_disabled));

            //holder.btnStartExamGoLive.setText(LocalConstants.Exam_Constants.btnText_StartExam);

            holder.btnAddQuestions.setEnabled(false);
            holder.btnAddQuestions.setBackground(mContext.getDrawable(R.drawable.background_blocks_12dp_disabled));
            holder.imgBtnEdit.setVisibility(View.GONE);
            holder.txtBtnViewSubmission.setEnabled(false);

           /* holder.btnStartExamGoLive.setText(LocalConstants.Exam_Constants.btnText_GoLive);
            holder.btnStartExamGoLive.setEnabled(true); */

        } else {
            holder.btnStartExamGoLive.setText(LocalConstants.Exam_Constants.btnText_MakeLive);
            holder.btnStartExamGoLive.setEnabled(true);

            holder.btnAddQuestions.setEnabled(true);
            holder.imgBtnEdit.setVisibility(View.VISIBLE);
            holder.txtBtnViewSubmission.setEnabled(false);
            holder.txtBtnViewSubmission.setBackground(mContext.getDrawable(R.drawable.background_blocks_12dp_disabled));
        }



        //set buttons when exam submitted by student
        btnClickFunctionSettingsTeacher(holder, position);


    }

    private void btnClickFunctionSettingsStudents(vh holder, int position) {

        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)) {
            holder.teacherSection.setVisibility(View.GONE);
        } else {
            holder.studentSection.setVisibility(View.GONE);
        }

        //btn click functions according to Text
        holder.btnStartExamGoLive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (holder.btnStartExamGoLive.getText().toString().equalsIgnoreCase(LocalConstants.Exam_Constants.btnText_GoLive)) {

                }else if(holder.btnStartExamGoLive.getText().toString().equalsIgnoreCase(LocalConstants.Exam_Constants.btnText_JoinExam)){
                    if (examModelList.get(position).getExamType().equalsIgnoreCase(LocalConstants.Exam_TYPE.ORAL)) {
                        ((TestFragment) fragment).startOralTest(examModelList.get(position));
                    }
                }
                //Start Exam
                else if (holder.btnStartExamGoLive.getText().toString().equalsIgnoreCase(LocalConstants.Exam_Constants.btnText_StartExam)) {
                    //Oral Test
                    if (examModelList.get(position).getExamType().equalsIgnoreCase(LocalConstants.Exam_TYPE.ORAL)) {
                        ((TestFragment) fragment).startOralTest(examModelList.get(position));
                    }
                    else {
                        //Mcq / Subjective
                        tinyDB.putString(LocalConstants.Exam_Constants.ExamId, examModelList.get(position).getExamId());
                        tinyDB.putString(LocalConstants.Exam_Constants.STUDENT_NAME, tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.NAME));
                        tinyDB.putString(LocalConstants.Exam_Constants.ExamClass, examModelList.get(position).getClassName());
                        tinyDB.putString(LocalConstants.Exam_Constants.ExamSubject, examModelList.get(position).getExamTitle());
                        tinyDB.putString(LocalConstants.Exam_Constants.ExamTime, examModelList.get(position).getStartTime());
                        tinyDB.putString(LocalConstants.Exam_Constants.EndDate, examModelList.get(position).getEndTime());
                        tinyDB.putString(LocalConstants.Exam_Constants.ExamMaxMarks, examModelList.get(position).getMaxMarks());
                        tinyDB.putString(LocalConstants.Exam_Constants.ExamTotalQ, examModelList.get(position).getTotalQuestions());
                        tinyDB.putString(LocalConstants.Exam_Constants.ExamTotalQAtt, examModelList.get(position).getToAttempt());
                        tinyDB.putString(LocalConstants.Exam_Constants.ExamPerCorrect, examModelList.get(position).getMarksPerCorrect());
                        tinyDB.putString(LocalConstants.Exam_Constants.ExamNegativePerIncorrect, examModelList.get(position).getNegativePerIncorrect());
                        Intent intent = new Intent(mContext, ExamQActivity.class);
                        mContext.startActivity(intent);
                    }
                    //Toast.makeText(mContext, "test click", Toast.LENGTH_SHORT).show();
                }
            }
        });

       holder.btnMakeLive.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               ((TestFragment) fragment).makeExamLive(examModelList.get(position));
           }
       });

        holder.btnAddQuestions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tinyDB.putString(LocalConstants.Exam_Constants.ExamId, examModelList.get(position).getExamId());

                //tinyDB.putString(LocalConstants.Exam_Constants.STUDENT_NAME);
                //tinyDB.putString(LocalConstants.Exam_Constants.ExamClass);
                //tinyDB.putString(LocalConstants.Exam_Constants.ExamSubject);
                //tinyDB.putString(LocalConstants.Exam_Constants.ExamTime);
                //tinyDB.putString(LocalConstants.Exam_Constants.ExamMaxMarks);
                //tinyDB.putString(LocalConstants.Exam_Constants.ExamTotalQ);
                //tinyDB.putString(LocalConstants.Exam_Constants.ExamTotalQAtt);

                //Intent intent = new Intent(mContext, ExamQActivity.class);
                //mContext.startActivity(intent);
                ((TestFragment) fragment).callAddQuestionFragment();
            }
        });

        holder.btnResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tinyDB.putString(LocalConstants.Exam_Constants.ExamId, examModelList.get(position).getExamId());
                Intent intent = new Intent(mContext, ResultActivity.class);
                mContext.startActivity(intent);
            }
        });

        holder.btnAnsKey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tinyDB.putString(LocalConstants.Exam_Constants.ExamId, examModelList.get(position).getExamId());
                tinyDB.putString(LocalConstants.Exam_Constants.STUDENT_NAME, tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.NAME));
                tinyDB.putString(LocalConstants.Exam_Constants.ExamClass, examModelList.get(position).getClassName());
                tinyDB.putString(LocalConstants.Exam_Constants.ExamSubject, examModelList.get(position).getExamTitle());
                tinyDB.putString(LocalConstants.Exam_Constants.ExamTime, examModelList.get(position).getStartTime());
                tinyDB.putString(LocalConstants.Exam_Constants.EndDate, examModelList.get(position).getEndTime());
                tinyDB.putString(LocalConstants.Exam_Constants.ExamMaxMarks, examModelList.get(position).getMaxMarks());
                tinyDB.putString(LocalConstants.Exam_Constants.ExamTotalQ, examModelList.get(position).getTotalQuestions());
                tinyDB.putString(LocalConstants.Exam_Constants.ExamTotalQAtt, examModelList.get(position).getToAttempt());
                tinyDB.putString(LocalConstants.Exam_Constants.ExamPerCorrect, examModelList.get(position).getMarksPerCorrect());
                tinyDB.putString(LocalConstants.Exam_Constants.ExamNegativePerIncorrect, examModelList.get(position).getNegativePerIncorrect());
                tinyDB.putBoolean(LocalConstants.Exam_Constants.Show_Ans_Keys,true);
                Intent intent = new Intent(mContext, ExamQActivity.class);
                mContext.startActivity(intent);
            }
        });
    }


    private void btnClickFunctionSettingsTeacher(vh holder, int position) {

        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)) {
            holder.teacherSection.setVisibility(View.GONE);
        } else {
            holder.studentSection.setVisibility(View.GONE);
        }

        //btn click functions according to Text
        holder.btnMakeLive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (holder.btnMakeLive.getText().toString().equalsIgnoreCase(LocalConstants.Exam_Constants.btnText_GoLive)) {

                } else if(holder.btnMakeLive.getText().toString().equalsIgnoreCase(LocalConstants.Exam_Constants.btnText_MakeLive)){
                    ((TestFragment) fragment).makeExamLive(examModelList.get(position));
                }
                //Start Exam
                else if (holder.btnMakeLive.getText().toString().equalsIgnoreCase(LocalConstants.Exam_Constants.btnText_StartExam)) {
                    //Oral Test
                    if (examModelList.get(position).getExamType().equalsIgnoreCase(LocalConstants.Exam_TYPE.ORAL)) {
                        ((TestFragment) fragment).startOralTestAlert(examModelList.get(position));
                    }

                    else {
                        //Mcq / Subjective
                        tinyDB.putString(LocalConstants.Exam_Constants.ExamId, examModelList.get(position).getExamId());
                        tinyDB.putString(LocalConstants.Exam_Constants.STUDENT_NAME, tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.NAME));
                        tinyDB.putString(LocalConstants.Exam_Constants.ExamClass, examModelList.get(position).getClassName());
                        tinyDB.putString(LocalConstants.Exam_Constants.ExamSubject, examModelList.get(position).getExamTitle());
                        tinyDB.putString(LocalConstants.Exam_Constants.ExamTime, examModelList.get(position).getStartTime());
                        tinyDB.putString(LocalConstants.Exam_Constants.EndDate, examModelList.get(position).getEndTime());
                        tinyDB.putString(LocalConstants.Exam_Constants.ExamMaxMarks, examModelList.get(position).getMaxMarks());
                        tinyDB.putString(LocalConstants.Exam_Constants.ExamTotalQ, examModelList.get(position).getTotalQuestions());
                        tinyDB.putString(LocalConstants.Exam_Constants.ExamTotalQAtt, examModelList.get(position).getToAttempt());
                        tinyDB.putString(LocalConstants.Exam_Constants.ExamPerCorrect, examModelList.get(position).getMarksPerCorrect());
                        tinyDB.putString(LocalConstants.Exam_Constants.ExamNegativePerIncorrect, examModelList.get(position).getNegativePerIncorrect());
                        Intent intent = new Intent(mContext, ExamQActivity.class);
                        mContext.startActivity(intent);
                    }
                    //Toast.makeText(mContext, "test click", Toast.LENGTH_SHORT).show();
                }
            }
        });

        /*holder.btnMakeLive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((TestFragment) fragment).makeExamLive(examModelList.get(position));
            }
        });*/

        holder.btnAddQuestions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tinyDB.putString(LocalConstants.Exam_Constants.ExamId, examModelList.get(position).getExamId());

                //tinyDB.putString(LocalConstants.Exam_Constants.STUDENT_NAME);
                //tinyDB.putString(LocalConstants.Exam_Constants.ExamClass);
                //tinyDB.putString(LocalConstants.Exam_Constants.ExamSubject);
                //tinyDB.putString(LocalConstants.Exam_Constants.ExamTime);
                //tinyDB.putString(LocalConstants.Exam_Constants.ExamMaxMarks);
                //tinyDB.putString(LocalConstants.Exam_Constants.ExamTotalQ);
                //tinyDB.putString(LocalConstants.Exam_Constants.ExamTotalQAtt);

                //Intent intent = new Intent(mContext, ExamQActivity.class);
                //mContext.startActivity(intent);
                ((TestFragment) fragment).callAddQuestionFragment();
            }
        });

        holder.btnResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tinyDB.putString(LocalConstants.Exam_Constants.ExamId, examModelList.get(position).getExamId());
                Intent intent = new Intent(mContext, ResultActivity.class);
                mContext.startActivity(intent);
            }
        });

        holder.btnAnsKey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tinyDB.putString(LocalConstants.Exam_Constants.ExamId, examModelList.get(position).getExamId());
                tinyDB.putString(LocalConstants.Exam_Constants.STUDENT_NAME, tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.NAME));
                tinyDB.putString(LocalConstants.Exam_Constants.ExamClass, examModelList.get(position).getClassName());
                tinyDB.putString(LocalConstants.Exam_Constants.ExamSubject, examModelList.get(position).getExamTitle());
                tinyDB.putString(LocalConstants.Exam_Constants.ExamTime, examModelList.get(position).getStartTime());
                tinyDB.putString(LocalConstants.Exam_Constants.EndDate, examModelList.get(position).getEndTime());
                tinyDB.putString(LocalConstants.Exam_Constants.ExamMaxMarks, examModelList.get(position).getMaxMarks());
                tinyDB.putString(LocalConstants.Exam_Constants.ExamTotalQ, examModelList.get(position).getTotalQuestions());
                tinyDB.putString(LocalConstants.Exam_Constants.ExamTotalQAtt, examModelList.get(position).getToAttempt());
                tinyDB.putString(LocalConstants.Exam_Constants.ExamPerCorrect, examModelList.get(position).getMarksPerCorrect());
                tinyDB.putString(LocalConstants.Exam_Constants.ExamNegativePerIncorrect, examModelList.get(position).getNegativePerIncorrect());
                tinyDB.putBoolean(LocalConstants.Exam_Constants.Show_Ans_Keys,true);
                Intent intent = new Intent(mContext, ExamQActivity.class);
                mContext.startActivity(intent);
            }
        });
    }

    public void updateList(List<ExamModel> list) {
        examModelList = list;
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {

        return examModelList == null ? 0 : examModelList.size();
    }

    class vh extends RecyclerView.ViewHolder {
        TextView txtStatus, submittedStudents;//, btnStartExam;
        TextView btnAddQuestions, btnStartExamGoLive,btnResult,btnMakeLive,btnAnsKey;
        TextView txtTitle, txtTime, txtMaxMarks, txtDuration;

        LinearLayout teacherSection,studentSection;
        TextView txtBtnViewSubmission;//, txtBtnAddEditSolution;

        ImageView imgBtnEdit;

        public vh(@NonNull View itemView) {
            super(itemView);

            txtTitle = itemView.findViewById(R.id.txtTitle);
            submittedStudents = itemView.findViewById(R.id.submittedStudents);
            this.txtStatus = itemView.findViewById(R.id.txtStatus);
            this.btnAddQuestions = itemView.findViewById(R.id.btnAddQuestions);
            this.btnStartExamGoLive = itemView.findViewById(R.id.btnStartExamGoLive);
            //this.btnStartExam = itemView.findViewById(R.id.btnStartExam);
            this.imgBtnEdit = itemView.findViewById(R.id.imgBtnEdit);
            this.txtTime = itemView.findViewById(R.id.txtTime);
            this.txtBtnViewSubmission = itemView.findViewById(R.id.txtBtnViewSubmission);
            this.btnResult = itemView.findViewById(R.id.btnResult);
            this.btnMakeLive= itemView.findViewById(R.id.btnMakeLive);
            this.btnAnsKey= itemView.findViewById(R.id.btnAnsKey);
            //this.txtBtnAddEditSolution = itemView.findViewById(R.id.txtBtnAddEditSolution);

            this.teacherSection = itemView.findViewById(R.id.teacherSection);
            this.studentSection= itemView.findViewById(R.id.studentSection);

            this.txtMaxMarks = itemView.findViewById(R.id.txtMaxMarks);
            this.txtDuration = itemView.findViewById(R.id.txtDuration);

            //set buttons visible false
            // this.txtBtnViewSubmission.setVisibility(View.GONE);
            // this.imgBtnEdit.setVisibility(View.GONE);
            // this.btnAddQuestions.setVisibility(View.GONE);
            // this.btnStartExamGoLive.setVisibility(View.GONE);
            // this.txtBtnAddEditSolution.setVisibility(View.GONE);

            this.txtBtnViewSubmission.setEnabled(false);
            this.imgBtnEdit.setEnabled(false);
            this.btnAddQuestions.setEnabled(false);
            this.btnStartExamGoLive.setEnabled(false);
            this.btnResult.setVisibility(View.GONE);
            this.btnAnsKey.setVisibility(View.GONE);
            //this.txtBtnAddEditSolution.setEnabled(false);

            this.btnMakeLive.setText(LocalConstants.Exam_Constants.btnText_MakeLive);

        }
    }
}
