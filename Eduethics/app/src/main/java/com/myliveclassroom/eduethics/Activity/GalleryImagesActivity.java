package com.myliveclassroom.eduethics.Activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;


import com.myliveclassroom.eduethics.Adapter.GalleryImgAdapter;
import com.myliveclassroom.eduethics.Adapter.SlidingAdapter;
import com.myliveclassroom.eduethics.Models.ApiResponseModel;
import com.myliveclassroom.eduethics.Models.GalleryModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.classes.TinyDB;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;

public class GalleryImagesActivity extends BaseActivity {

    @BindView(R.id.txtPageTitle)
    TextView txtPageTitle;

    @BindView(R.id.imgBack)
    ImageView imgBack;

    @BindView(R.id.imgbtnAdd)
    ImageView imgbtnAdd;

    @BindView(R.id.btnAdd)
    TextView btnAdd;

    RecyclerView recyclerGallery;
    List<GalleryModel> galleryModelList;
    GalleryImgAdapter galleryAdapter;

    LinearLayout layoutCircularAdd;
    TextView txtBtnCancel, txtBtnSubmit, txtAttachment;
    EditText editTextHead, editTextDetail, editTextDate;
    ImageView imgAttachFile;

    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    RelativeLayout sliderView;
    Boolean isSliderOpen=false;
    String []imageArray;

    private static final int REQUEST_STORAGE_PERMISSION = 100;
    private static final int REQUEST_PICK_PHOTO = 101;

    //create a single thread pool to our image compression class.
    private ExecutorService mExecutorService = Executors.newFixedThreadPool(1);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.MyThemeNoBar);
        setContentView(R.layout.activity_gallery_images);

        mContext = this;
        tinyDB = new TinyDB(this);
        ButterKnife.bind(this);
        init();

        sliderView = findViewById(R.id.sliderView);

        indicator = (CircleIndicator)
                findViewById(R.id.indicator);
        indicator.setVisibility(View.GONE);
        slider();
    }

    private void Back() {
        if(!isSliderOpen) {
            super.onBackPressed();
        }
        recyclerGallery.setVisibility(View.VISIBLE);
        sliderView.setVisibility(View.GONE);
        isSliderOpen=false;
    }

    @Override
    public void onBackPressed()
    {
        if(!isSliderOpen) {
            super.onBackPressed();
        }
        recyclerGallery.setVisibility(View.VISIBLE);
        sliderView.setVisibility(View.GONE);
        isSliderOpen=false;
        indicator.setVisibility(View.GONE);
    }

    private void init() {

        imgbtnAdd.setVisibility(View.INVISIBLE);
        imgBack.setVisibility(View.VISIBLE);
        btnAdd.setVisibility(View.GONE);
        btnAdd.setText("");
        txtPageTitle.setText("Gallery");

       /* if (tinyDB.getString("IsAdmin").equalsIgnoreCase("Y")) {
            imgbtnAdd.setVisibility(View.VISIBLE);
            btnAdd.setVisibility(View.VISIBLE);
            imgbtnAdd.setImageDrawable(mContext.getDrawable(R.drawable.ic_delete));
            btnAdd.setText("Delete");
        }*/

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Back();
            }
        });

        initRecyclerView();

    }

    private void initRecyclerView() {
        galleryModelList = new ArrayList<>();
        recyclerGallery = findViewById(R.id.recyclerGallery);
        recyclerGallery.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(),3);
        recyclerGallery.setLayoutManager(layoutManager);
        listGallery();
    }

    private void listGallery() {
        imageArray=getIntent().getExtras().getString("Images").split(",");

        galleryAdapter = new GalleryImgAdapter(mContext, imageArray);
        recyclerGallery.setAdapter(galleryAdapter);
    }

    private CircleIndicator indicator;

    public void startSlider(int pageIndex) {
        //slider();
        isSliderOpen=true;

        currentPage = pageIndex;
        mPager.setCurrentItem(currentPage, true);
        recyclerGallery.setVisibility(View.GONE);
        sliderView.setVisibility(View.VISIBLE);
        indicator.setVisibility(View.VISIBLE);
    }

    private void slider() {

        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(new SlidingAdapter(mContext, imageArray));



        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;

        NUM_PAGES = imageArray.length;

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;
            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });
    }

    String imageList="";
    String arrayTemp[];
    public void deleteImg(int position){
        imageList="";
        arrayTemp=new String[imageArray.length-1];
        for (int i=0; i < imageArray.length; i++) {
            if(i!=position){
                imageList =imageList+ imageArray[i]+",";
            }
        }
        if(imageArray.length==1){
            //imageList=imageArray[0];
            imageArray=new String[0];
            imageList="";
        }


        if(imageList.contains(",")){
            imageArray=imageList.split(",");
        }

        GalleryModel galleryModel = new GalleryModel();
        galleryModel.setGalleryId(tinyDB.getString("GalleryId"));
        galleryModel.setImageName(imageList);

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<ApiResponseModel> call = apiService.GalleryImageDelete(galleryModel);
        call.enqueue(new Callback<ApiResponseModel>() {
            @Override
            public void onResponse(Call<ApiResponseModel> call, Response<ApiResponseModel> response) {
                try {
                    ApiResponseModel apiResponseModel=response.body();
                    Toast.makeText(GalleryImagesActivity.this, apiResponseModel.getMessage(), Toast.LENGTH_SHORT).show();
                    galleryAdapter = new GalleryImgAdapter(mContext, imageArray);
                    recyclerGallery.setAdapter(galleryAdapter);
                } catch (Exception ex) {
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ApiResponseModel> call, Throwable t) {
                Log.e(mTAG, "error =" + t.toString());
            }
        });
    }
}
