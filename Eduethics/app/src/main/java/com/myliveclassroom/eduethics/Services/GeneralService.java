package com.myliveclassroom.eduethics.Services;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.job.JobService;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.provider.AlarmClock;
import android.provider.CalendarContract;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.JobIntentService;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.myliveclassroom.eduethics.Activity.MainActivity;
import com.myliveclassroom.eduethics.Models.TimeTableModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.TinyDB;

import org.devio.rn.splashscreen.SplashScreen;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.PROFILE;
import static com.myliveclassroom.eduethics.Utils.LocalConstants.MONTHS_3Char;

public class GeneralService extends JobIntentService {

    private final String TAG = "BackgroundServiceClass";
    public Context context = this;
    public Context mContext;
    public Handler handler = null;
    public static Runnable runnable = null;
    private String CHANNEL_ID = "EDU_ETHICS_NOTIFICATION_1001001";
    static final int JOB_ID = 1000;

    private final int LOCATION_INTERVAL = 1000 * 2;
    private final int LOCATION_DISTANCE = 1;

    public static final String RECEIVER = "receiver";
    public static final int SHOW_RESULT = 123;

    MainActivity mainActivity;
    TinyDB tinyDB;

    public GeneralService() {
        mainActivity = MainActivity.instance;
    }

    public static void enqueueWork(Context mcontext, Intent intent) {
        //enqueueWork(context, GeneralService.class, JOB_ID, work);


        //Intent intent = new Intent(context, JobService.class);
        //intent.putExtra(RECEIVER, workerResultReceiver);
        //intent.setAction(ACTION_DOWNLOAD);
        enqueueWork(mcontext, GeneralService.class, JOB_ID, intent);
        Log.i("htis", "enque done");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        Log.i(TAG, "onCreate");
        tinyDB = new TinyDB(GeneralService.this);
        mContext = GeneralService.this;
        try {
            startForeground(12345678, getNotification());
            triggerAlarm();
        } catch (Exception ex) {
            Log.i("htis", ex.getMessage());
        }
    }

    @Override
    public void onDestroy() {

    }

    @Override
    protected void onHandleWork(@NonNull @NotNull Intent intent) {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    private String getDateTime(String format) {
        DateFormat dateFormat = new SimpleDateFormat(format);//"yyyyMMddHHmmssSS");
        Date date = new Date();
        return dateFormat.format(date);
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {

        Log.e(TAG, "onTaskRemoved()");
        sendBroadcast(new Intent("YouWillNeverKillMe"));

        Intent i = new Intent(context, GeneralService.class);
        context.startService(i);
        super.onTaskRemoved(rootIntent);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Log.e(TAG, "onLowMemory()");
    }

    private NotificationManager notificationManager;
    Notification.Builder builder = null;

    private Notification getNotification() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = null;
            channel = new NotificationChannel(CHANNEL_ID, "My Channel", NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
            builder = new Notification.Builder(getApplicationContext(), CHANNEL_ID)
                    .setAutoCancel(false)
                    .setSmallIcon(R.drawable.ic_notification)
                    .setVibrate(new long[]{1000, 1000})
                    .setSubText("Do not close this app!")
                    .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                    .setContentTitle("Edu Ethics - MyLiveClassRoom");

        } else {
            String strTitle = "HTIS Employee Self Care";
            String strText = "Do not close this app";

            builder = new Notification.Builder(this)
                    .setSmallIcon(R.drawable.ic_notification)
                    .setContentText("Do not close this app")
                    .setContentTitle("Edu Ethics - MyLiveClassRoom")
                    .setAutoCancel(false);
            notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            notificationManager.notify(111, builder.build());

        }
        return builder.build();
    }

    private void triggerAlarm() {
        Log.e(mTAG, "triggerAlarm");
        handler = new Handler();
        handler.post(runnableCode);
    }


    private Runnable runnableCode = new Runnable() {
        @Override
        public void run() {
            // Do something here on the main thread
            Log.d(mTAG + "Handlers", "triggerAlarml");
            // Repeat this the same runnable code block again another 2 seconds
            // 'this' is referencing the Runnable object

            getNextPeriod();
            handler.postDelayed(this, 1000 * 60*5);
        }
    };

    List<TimeTableModel> timeTableModelList;
    int mCalDay, mCalMonth, mCalYear;
    String mDate, mTime;

    private void getNextPeriod() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        mDate = formatter.format(new Date());
        mTime = mDate.split(" ")[1];
        TimeTableModel timeTableModel = new TimeTableModel();

        timeTableModel.setUserId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER)) {
            timeTableModel.setTeacherId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
        }
        timeTableModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        timeTableModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));
        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)) {
            timeTableModel.setClassId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID));
            timeTableModel.setSubjectId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SUBJECT_ID));
        }

        timeTableModel.setProfileType(tinyDB.getString(PROFILE));
        timeTableModel.setInstituteType(tinyDB.getString(LocalConstants.INSTITUTE_TYPE.TYPE));

        timeTableModel.setScheduleDate(mDate);
        timeTableModel.setTimeFrom(mTime.split(":")[0] + ":" + mTime.split(":")[1]);

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = apiService.getTimeTableDayScheduleNextPeriod(timeTableModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.e(mTAG, "userModelList" + response.body().toString());
                try {
                    List<TimeTableModel> timeTableModelFilterList = new ArrayList<>();
                    timeTableModelList = new ArrayList<>();

                    if (response.body() != null) {
                        Type type = new TypeToken<List<TimeTableModel>>() {
                        }.getType();
                        timeTableModelList = new Gson().fromJson(response.body().getAsJsonArray("Response"), type);
                        if (timeTableModelList.size() > 0) {
                            // tinyDB.putBoolean(LocalConstants.ALARM_CONSTANTS.IS_ALARM_SET, true);
                            // tinyDB.putString(LocalConstants.ALARM_CONSTANTS.ALARM_TIME, timeTableModelList.get(0).getScheduleDate() + " " + timeTableModelList.get(0).getTimeFrom());
                            // mDate = timeTableModelList.get(0).getScheduleDate();
                            // mTime = timeTableModelList.get(0).getTimeFrom();

                            for (TimeTableModel tableModel : timeTableModelList) {
                                //mainActivity.getViewValues(tableModel);
                                //setAlarm();
                                sentCallToActivityMethod(tableModel);
                            }
                        }
                    }
                } catch (Exception ex) {
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
            }
        });
    }

    private void sentCallToActivityMethod(TimeTableModel timeTableModel) {
        Intent intent = new Intent("my-event");
        // add data
        Gson gson = new Gson();
        String myJson = gson.toJson(timeTableModel);
        intent.putExtra("MyGsonObj", myJson);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void setAlarm() {

        int day = Integer.parseInt(mDate.split("-")[0]);
        int month;
        for (int i = 0; i < MONTHS_3Char.length; i++) {
            if (MONTHS_3Char[i].equalsIgnoreCase(mDate.split("-")[1])) {
                month = i;
            }
        }
        int year = Integer.parseInt(mDate.split("-")[2]);
        int hour = Integer.parseInt(mTime.split(":")[0]);
        int minute = Integer.parseInt(mTime.split(":")[1]);

        Calendar calSet = Calendar.getInstance();
        calSet.set(Calendar.HOUR_OF_DAY, hour);
        calSet.set(Calendar.MINUTE, minute);
        calSet.set(Calendar.SECOND, 0);
        calSet.set(Calendar.MILLISECOND, 0);

        /*Intent intent = new Intent(getBaseContext(), AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                getBaseContext(), 1, intent, 0);
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, calSet.getTimeInMillis(),
                pendingIntent);*/
    }
}
