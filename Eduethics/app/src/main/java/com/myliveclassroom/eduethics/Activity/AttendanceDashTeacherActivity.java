package com.myliveclassroom.eduethics.Activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.FragmentTransaction;

import com.myliveclassroom.eduethics.Adapter.TimetableAdapterWebApp;
import com.myliveclassroom.eduethics.Models.TimeTableModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.CommonFunctions;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
;import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;

public class AttendanceDashTeacherActivity extends BaseActivity {

    List<TimeTableModel> timeTableModelList;
    TimetableAdapterWebApp timetableAdapter;
    Spinner spinnerTimeTable;

    @BindView(R.id.txtPageTitle)
    TextView txtPageTitle;

    @BindView(R.id.imgBack)
    ImageView imgBack;

    @BindView(R.id.imgbtnAdd)
    ImageView imgbtnAdd;

    @BindView(R.id.btnAdd)
    TextView btnAdd;

    @BindView(R.id.txtPresentCount)
    TextView txtPresentCount;

    @BindView(R.id.txtAbsentCount)
    TextView txtAbsentCount;

    @BindView(R.id.txtLeaveCount)
    TextView txtLeaveCount;

    @BindView(R.id.txtHolidayCount)
    TextView txtHolidayCount;

    @BindView(R.id.txtBtnMarkAttendance)
    TextView txtBtnMarkAttendance;

    @BindView(R.id.txtAttendanceStatusHead)
    TextView txtAttendanceStatusHead;

    private String mSelectedValue;
    private String mClassId, mClassName,mPeriodID;
    private Date mSelectedDate;
    private String mDate;
    private ProgressBar progressBar;

    private CaldroidFragment caldroidFragment;
    private CaldroidFragment dialogCaldroidFragment;
    /*List<CalendarModel> calendarList;*/
    SimpleDateFormat formatter, formatter1;
    List<String> disableDates;
    List<TimeTableModel> attendanceList;
    List<String> timeTableHeads;


    String mDayId;

    ColorDrawable green;
    ColorDrawable red;
    ColorDrawable blue;
    ColorDrawable grey;
    ColorDrawable yellow;
    ColorDrawable brown;
    ColorDrawable black3;
    ColorDrawable white;

    @SuppressLint("WrongConstant")
    private void setCustomResourceForDates() {

        if (caldroidFragment != null) {
            if (disableDates != null) {
                //this is for sundays
                for (int j = 0; j < disableDates.size(); j++) {
                    Log.e("tag", "disable" + disableDates.get(j));
                    caldroidFragment.setBackgroundDrawableForDate(grey, CommonFunctions.getDateFormated(disableDates.get(j)));
                    caldroidFragment.setTextColorForDate(R.color.black3, CommonFunctions.getDateFormated(disableDates.get(j)));
                }
            }
            int presentCount = 0;
            int absentCount = 0;
            int leaveCount = 0;
            int holidayCount = 0;

            for (int i = 0; i < attendanceList.size(); i++) {
                String curDate2 = attendanceList.get(i).getAttDate();
                if (attendanceList.get(i).getAttTag().equalsIgnoreCase("P")) {
                    String curDate = attendanceList.get(i).getAttDate();
                    caldroidFragment.setBackgroundDrawableForDate(green, CommonFunctions.getDateFormated(curDate));
                    caldroidFragment.setTextColorForDate(R.color.black3, CommonFunctions.getDateFormated(curDate));
                    presentCount = presentCount + 1;
                } else if (attendanceList.get(i).getAttTag().equalsIgnoreCase("A")) {
                    String curDate = attendanceList.get(i).getAttDate();
                    caldroidFragment.setBackgroundDrawableForDate(red, CommonFunctions.getDateFormated(curDate));
                    caldroidFragment.setTextColorForDate(R.color.black3, CommonFunctions.getDateFormated(curDate));
                    absentCount = absentCount + 1;
                } else if (attendanceList.get(i).getAttTag().equalsIgnoreCase("H")) {
                    String curDate = attendanceList.get(i).getAttDate();
                    caldroidFragment.setBackgroundDrawableForDate(brown, CommonFunctions.getDateFormated(curDate));
                    caldroidFragment.setTextColorForDate(R.color.white, CommonFunctions.getDateFormated(curDate));
                    holidayCount = holidayCount + 1;
                } else if (attendanceList.get(i).getAttTag().equalsIgnoreCase("L")) {
                    String curDate = attendanceList.get(i).getAttDate();
                    caldroidFragment.setBackgroundDrawableForDate(blue, CommonFunctions.getDateFormated(curDate));
                    caldroidFragment.setTextColorForDate(R.color.black3, CommonFunctions.getDateFormated(curDate));
                    leaveCount = leaveCount + 1;
                }
            }

            txtPresentCount.setText("" + presentCount);
            txtAbsentCount.setText("" + absentCount);
            txtLeaveCount.setText("" + leaveCount);
            txtHolidayCount.setText("" + holidayCount);

            caldroidFragment.refreshView();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.MyThemeNoBar);
        setContentView(R.layout.activity_attendance_dash);

        _BaseActivity(this);
        ButterKnife.bind(this);
        init();

        setAdapterCalendarList();
    }

    private void init() {
        formatter = new SimpleDateFormat("dd MMM yyyy");
        formatter1 = new SimpleDateFormat("dd-MMM-yyyy");
        mSelectedDate = new Date();
        mDate = formatter1.format(mSelectedDate);

        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        white = new ColorDrawable(mContext.getResources().getColor(R.color.white));
        green = new ColorDrawable(mContext.getResources().getColor(R.color.green_att));
        red = new ColorDrawable(mContext.getResources().getColor(R.color.red_att));
        blue = new ColorDrawable(mContext.getResources().getColor(R.color.blue_att));
        grey = new ColorDrawable(mContext.getResources().getColor(R.color.grey_att));
        yellow = new ColorDrawable(mContext.getResources().getColor(R.color.yellow_att));
        brown = new ColorDrawable(mContext.getResources().getColor(R.color.brown_att));
        black3 = new ColorDrawable(mContext.getResources().getColor(R.color.black3_att));

        spinnerTimeTable = findViewById(R.id.spinnerTimeTable);
        imgbtnAdd.setVisibility(View.GONE);
        imgBack.setVisibility(View.VISIBLE);
        btnAdd.setVisibility(View.VISIBLE);
        btnAdd.setText("Add");
        btnAdd.setText("Attendance Report");

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tinyDB.putString(LocalConstants.URL.REPORT_TYPE,LocalConstants.URL.REPORT_ATTENDANCE_TEACHER);
                Intent intent=new Intent(mContext,ReportActivity.class);
                startActivity(intent);
            }
        });


        txtPageTitle.setText(R.string.pagetitle_attendance);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Back();
            }
        });

        caldroidFragment = new CaldroidFragment();

        Bundle args = new Bundle();
        Calendar cal = Calendar.getInstance();
        args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
        args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
        args.putBoolean(CaldroidFragment.ENABLE_SWIPE, true);
        args.putBoolean(CaldroidFragment.SIX_WEEKS_IN_CALENDAR, true);
        caldroidFragment.setArguments(args);

        // Set todays day
        Calendar c = Calendar.getInstance();
        mDayId = c.get(Calendar.DATE) + "";


        getTeacherTimeTable();
    }

    //@OnClick(R.id.imgBack)
    private void Back() {
        super.onBackPressed();
    }


    private void setAdapterCalendarList() {
        // Attach to the activity
        disableDates = new ArrayList<>();
        attendanceList = new ArrayList<>();
        disableDates.add("02-feb-2020");
        disableDates.add("09-feb-2020");
        disableDates.add("16-feb-2020");
        disableDates.add("23-feb-2020");
        //setCustomResourceForDates();

        FragmentTransaction t = getSupportFragmentManager().beginTransaction();
        t.replace(R.id.calendar1, caldroidFragment);
        t.commitAllowingStateLoss();


        // Setup listener
        final CaldroidListener listener = new CaldroidListener() {

            @Override
            public void onSelectDate(Date date, View view) {
                // attendanceList(date);
                Toast.makeText(getApplicationContext(), formatter.format(date),
                        Toast.LENGTH_SHORT).show();


                caldroidFragment.setBackgroundDrawableForDate(white, mSelectedDate);
                caldroidFragment.setTextColorForDate(R.color.black3, mSelectedDate);

                caldroidFragment.setBackgroundDrawableForDate(blue, date);
                caldroidFragment.setTextColorForDate(R.color.white, date);
                caldroidFragment.refreshView();
                mSelectedDate = date;
                mDate = formatter1.format(date);
                txtAttendanceStatusHead.setText("Attendance Status : " + mDate);

                Calendar c = Calendar.getInstance();
                c.setTime(mSelectedDate); // yourdate is an object of type Date
                mDayId = c.get(Calendar.DATE) + "";

                getTeacherTimeTable();
                attendanceSummary();
            }

            @SuppressLint("WrongConstant")
            @Override
            public void onChangeMonth(int month, int year) {
                //attendanceList(month,year);
            }


            @Override
            public void onLongClickDate(Date date, View view) {
                Toast.makeText(getApplicationContext(),
                        "Long click " + formatter.format(date),
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCaldroidViewCreated() {

                Button leftButton = caldroidFragment.getLeftArrowButton();
                Button rightButton = caldroidFragment.getRightArrowButton();
                TextView textView = caldroidFragment.getMonthTitleTextView();
                // Do customization here
            }
        };
        // Setup Caldroid
        caldroidFragment.setCaldroidListener(listener);
    }

    String mSpinnerSelectedText;

    private void getTeacherTimeTable() {
        progressBar.setVisibility(View.VISIBLE);
        TimeTableModel listModel = new TimeTableModel();

        listModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));
        listModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        listModel.setTeacherId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
        listModel.setScheduleDate(mDate + "");

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = apiService.getTeacherTimeTable(listModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                progressBar.setVisibility(View.GONE);
                try {
                    Type type = new TypeToken<List<TimeTableModel>>() {
                    }.getType();

                    timeTableModelList = new Gson().fromJson(response.body().getAsJsonArray("Response"), type);
                    timetableAdapter = new TimetableAdapterWebApp(mContext, timeTableModelList);
                    timeTableHeads = new ArrayList<>();

                    for (TimeTableModel timeTableModel : timeTableModelList) {
                        timeTableHeads.add(timeTableModel.getClassName() + " - " + CommonFunctions.capitalize(timeTableModel.getSubjectName()) + "  -  " + timeTableModel.getTimeFrom() + "-" + timeTableModel.getTimeTo());
                    }

                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(mContext,
                            android.R.layout.simple_spinner_item, timeTableHeads);
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerTimeTable.setAdapter(dataAdapter);

                    spinnerTimeTable.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            mSpinnerSelectedText = String.valueOf(spinnerTimeTable.getSelectedItem());
                            mClassId = timeTableModelList.get(i).getClassId();
                            mPeriodID=timeTableModelList.get(i).getTimeTableId();
                            mClassName = timeTableModelList.get(i).getClassName();
                            TextView tv = (TextView) spinnerTimeTable.getSelectedView();
                            tv.setTypeface(Typeface.DEFAULT_BOLD); //to make text bold
                            //tv.setTypeface(Typeface.DEFAULT); //to make text normal
                            attendanceSummary();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (Exception ex) {
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(mTAG, "error =" + t.toString());
            }
        });
    }


    private void attendanceSummary() {
        if (mClassId == null || mDate == null) return;
        txtAttendanceStatusHead.setText("Attendance Status : " + mDate);
        progressBar.setVisibility(View.VISIBLE);
        attendanceList = new ArrayList<>();
        TimeTableModel listModel = new TimeTableModel();
        listModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));
        listModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        listModel.setClassId(mClassId);
        listModel.setTimeTableId(mPeriodID);

        //listModel.setAttDate("1-"+monthArray[month-1]+"-"+year);
        listModel.setAttDate(mDate);
        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<List<TimeTableModel>> call = apiService.AttendanceSummary(listModel);
        call.enqueue(new Callback<List<TimeTableModel>>() {
            @Override
            public void onResponse(Call<List<TimeTableModel>> call, Response<List<TimeTableModel>> response) {
                //int statusCode = response.code();
                progressBar.setVisibility(View.GONE);
                try {
                    Log.e(mTAG, "error =" + response.toString());
                    attendanceList = response.body();
                    setCustomResourceForDates();

                } catch (Exception ex) {
                    Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<List<TimeTableModel>> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @OnClick(R.id.txtBtnMarkAttendance)
    public void markAttendance() {
        if (mClassId == null || mDate == null) {
            Toast.makeText(mContext, "Please select date", Toast.LENGTH_SHORT).show();
            return;
        }
        Intent intent = new Intent(AttendanceDashTeacherActivity.this, AttendanceMarkActivity.class);
        intent.putExtra("ClassId", mClassId);
        intent.putExtra("ClassName", mClassName);
        intent.putExtra("Date", mDate);
        intent.putExtra("PeriodId",mPeriodID);

        tinyDB.putString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID, mClassId);
        tinyDB.putString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_NAME, mClassName);
        tinyDB.putString("Date", mDate);

        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        attendanceSummary();
    }

    @Override
    protected void onDestroy() {
        try {
            super.onDestroy();
        } catch (Exception ex) {

        }
    }
}
