package com.myliveclassroom.eduethics.Adapter;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.myliveclassroom.eduethics.Activity.HomeWorkActivity;
import com.myliveclassroom.eduethics.Models.HomeWorkModel;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.classes.CommonFunctions;
import com.myliveclassroom.eduethics.classes.TinyDB;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeWorkAdapter extends RecyclerView.Adapter<HomeWorkAdapter.ViewHolder> {

    List<HomeWorkModel> homeWorkModelList;
    Context mContext;
    List<String> listAttTags = new ArrayList<String>();
    TinyDB tinyDB;
    Date dateToday;
    String dateTodayStr;
    Date workDate;
    SimpleDateFormat dateformat = new SimpleDateFormat("dd-MMM-yyyy");

    List<String> homeWorkHeadList = new ArrayList<String>();
    CommonFunctions comfun = new CommonFunctions();

    public HomeWorkAdapter(Context context, List<HomeWorkModel> list) {
        homeWorkModelList = list;
        mContext = context;
        tinyDB = new TinyDB(mContext);

        Calendar startDate = Calendar.getInstance();
        dateTodayStr = dateformat.format(startDate.getTime());

        String[] heads = tinyDB.getString("HomeworkHead").split(",");
        for (String s : heads) {
            homeWorkHeadList.add(s);
        }
        /*homeWorkHeadList.add("Learn");
        homeWorkHeadList.add("Dictation");
        homeWorkHeadList.add("YouTube Video link");*/
        //notificationCenter=new NotificationCenter(mContext);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_work_adp_layout, parent, false);
        return new ViewHolder(view);
    }


    String doneattachments;
    String doneattachmentArray[] = {};

    String attachments;
    String attachmentArray[] = {};
    private long mLastClickTime = 0;
    String mVideoLink;
    String[] mVideoLinkArray;
    TextView txtBtnDownload;
    TextView txtBtnDownload2;
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.txtSubjectHead.setText(CommonFunctions.capitalize(homeWorkModelList.get(position).getSubjectName()));
        holder.txtHead.setText(CommonFunctions.capitalize(homeWorkModelList.get(position).getWorkHead()));
        holder.txtDesc.setText(homeWorkModelList.get(position).getWorkDetail());
        holder.imgBtnAssign.setVisibility(View.GONE);
        attachments = homeWorkModelList.get(position).getAttachment();
        doneattachments = homeWorkModelList.get(position).getDoneAttachment();

        mVideoLink = homeWorkModelList.get(position).getWorkDetail();
        if (mVideoLink.toLowerCase().contains("youtu") && mVideoLink.toLowerCase().contains("/")) {
            mVideoLinkArray = mVideoLink.split("/");
        }
        attachmentArray = new String[]{};
        doneattachmentArray = new String[]{};
        if (attachments != null && !attachments.isEmpty()) {
            attachmentArray = attachments.split(",");
        }
        if (doneattachments != null && !doneattachments.isEmpty()) {
            doneattachmentArray = doneattachments.split(",");
        }
        holder.imgAttIcon.setVisibility(View.GONE);
        holder.txtAttachCount.setVisibility(View.GONE);
        holder.imgDownload.setVisibility(View.GONE);
        holder.txtAttachCount.setText(attachments != null ? " x " + attachmentArray.length : "");
        int sr = 0;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (attachmentArray.length > 0) {
            //******************Attachment List*********************


            final View rowHeadLayout = inflater.inflate(R.layout.file_table_row_head, null);
            TextView txtRowHead = rowHeadLayout.findViewById(R.id.txtRowHead);
            txtRowHead.setText("Work Assigned by Teacher");
            TableRow trAttachment = (TableRow) rowHeadLayout;
            holder.tableAttachments.addView(trAttachment);

            for (String str : attachmentArray) {
                sr = sr + 1;
                final View rowLayout = inflater.inflate(R.layout.file_table_row, null);
                TextView txtSr = rowLayout.findViewById(R.id.txtSr);
                txtSr.setText(sr + ".");
                TextView txtFileName = rowLayout.findViewById(R.id.txtFileName);
                txtFileName.setText(str);

                txtBtnDownload = rowLayout.findViewById(R.id.txtBtnDownload);
                if(sr<attachmentArray.length){
                    txtBtnDownload.setVisibility(View.GONE);
                }
                trAttachment = (TableRow) rowLayout;
                holder.tableAttachments.addView(trAttachment);
            }
        }
        if (doneattachmentArray.length > 0) {
            sr = 0;
            final View rowHeadLayout2 = inflater.inflate(R.layout.file_table_row_head, null);
            TextView txtRowHead2 = rowHeadLayout2.findViewById(R.id.txtRowHead);
            txtRowHead2.setText("Work submitted by Student");
            TableRow trAttachment = (TableRow) rowHeadLayout2;
            holder.tableAttachments.addView(trAttachment);

            for (String str : doneattachmentArray) {
                sr = sr + 1;
                final View rowLayout = inflater.inflate(R.layout.file_table_row, null);
                TextView txtSr = rowLayout.findViewById(R.id.txtSr);
                txtSr.setText(sr + ".");
                TextView txtFileName = rowLayout.findViewById(R.id.txtFileName);
                txtFileName.setText(str);
                txtBtnDownload2 = rowLayout.findViewById(R.id.txtBtnDownload);
                trAttachment = (TableRow) rowLayout;
                if(sr<attachmentArray.length){
                    txtBtnDownload2.setVisibility(View.GONE);
                }
                holder.tableAttachments.addView(trAttachment);
            }
            //******************Attachment List End*********************
        }

        holder.imgBtnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mContext instanceof HomeWorkActivity) {
                    ((HomeWorkActivity) mContext).deleteEntry(homeWorkModelList.get(position));
                }
            }
        });

        if (homeWorkModelList.get(position).getWorkHead().toLowerCase().contains("youtube")) {
            holder.imgWatchVideo.setVisibility(View.VISIBLE);

            holder.imgWatchVideo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    watchYoutubeVideo(mContext, mVideoLinkArray[mVideoLinkArray.length - 1]);
                }
            });
        }
        if (homeWorkModelList.get(position).getWorkDoneId().equalsIgnoreCase("0")) {
            holder.txtBtnUploadHomeWork.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mContext instanceof HomeWorkActivity) {
                        ((HomeWorkActivity) mContext).openHomeWorkUploadDialog(homeWorkModelList.get(position));
                    }
                }
            });
        } else {
            holder.txtBtnUploadHomeWork.setVisibility(View.GONE);
            /*holder.txtBtnUploadHomeWork.setText("Work already uploaded");
            holder.txtBtnUploadHomeWork.setBackgroundColor(mContext.getResources().getColor(R.color.white));
            holder.txtBtnUploadHomeWork.setTextColor(mContext.getResources().getColor(R.color.black3));*/
        }

        holder.txtPercentage.setText("");
        if (txtBtnDownload != null) {
            txtBtnDownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                        return;
                    }
                    mLastClickTime = SystemClock.elapsedRealtime();
                    if (mContext instanceof HomeWorkActivity) {

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    // Your implementation goes here
                                    ((HomeWorkActivity) mContext).downloadFiles(attachmentArray, holder.txtPercentage);
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            }
                        }).start();
                    }
                }
            });
        }
        if (txtBtnDownload2 != null) {
            txtBtnDownload2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                        return;
                    }
                    mLastClickTime = SystemClock.elapsedRealtime();
                    if (mContext instanceof HomeWorkActivity) {

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    // Your implementation goes here
                                    ((HomeWorkActivity) mContext).downloadFiles(doneattachmentArray, holder.txtPercentage);
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            }
                        }).start();
                    }
                }
            });
        }
    }

    public static void watchYoutubeVideo(Context context, String id) {
        Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + id));
        Intent webIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://www.youtube.com/watch?v=" + id));
        try {
            context.startActivity(appIntent);
        } catch (ActivityNotFoundException ex) {
            context.startActivity(webIntent);
        }
    }

    @Override
    public int getItemCount() {
        return homeWorkModelList == null ? 0 : homeWorkModelList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtSubjectHead)
        TextView txtSubjectHead;

        @BindView(R.id.txtHead)
        TextView txtHead;

        @BindView(R.id.txtDesc)
        TextView txtDesc;

        @BindView(R.id.txtBtnAssignHomeWork)
        TextView txtBtnAssignHomeWork;

        @BindView(R.id.layoutAssignWork)
        LinearLayout layoutAssignWork;

        @BindView(R.id.imgBtnAssign)
        ImageView imgBtnAssign;

        @BindView(R.id.imgBtnDelete)
        ImageView imgBtnDelete;

        @BindView(R.id.imgAttIcon)
        ImageView imgAttIcon;

        @BindView(R.id.txtAttachCount)
        TextView txtAttachCount;

        @BindView(R.id.imgDownload)
        ImageView imgDownload;

        @BindView(R.id.imgWatchVideo)
        ImageView imgWatchVideo;

        @BindView(R.id.txtPercentage)
        TextView txtPercentage;

        @BindView(R.id.tableAttachments)
        TableLayout tableAttachments;

        @BindView(R.id.txtBtnUploadHomeWork)
        TextView txtBtnUploadHomeWork;


        public ViewHolder(@NonNull View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}

