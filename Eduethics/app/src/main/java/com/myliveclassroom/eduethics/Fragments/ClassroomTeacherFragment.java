package com.myliveclassroom.eduethics.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.transition.TransitionInflater;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.myliveclassroom.eduethics.Activity.MainActivity;
import com.myliveclassroom.eduethics.Adapter.ClassroomTeachersAdapter2;
import com.myliveclassroom.eduethics.Adapter.ScheduleAdapterTeacher;
import com.myliveclassroom.eduethics.Fragments.SubFragments.ViewTimeTable;
import com.myliveclassroom.eduethics.Fragments.TeacherFragments.CreateClassroomFragment;
import com.myliveclassroom.eduethics.Models.TimeTableModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.ExtraFunctions;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.TinyDB;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;

public class ClassroomTeacherFragment extends Fragment {

    Context mContext;
    TinyDB tinyDB;
    RecyclerView classroomRecyclerView, scheduleRecycler;
    List<Map<String, Object>> classRoomsData;
    ClassroomTeachersAdapter2 classroomTeachersAdapter;
    List<TimeTableModel> classSubjectModelList;
    List<TimeTableModel> timeTableModelList=new ArrayList<>();

    public ClassroomTeacherFragment(Context context) {
        this.mContext = context;
        tinyDB=new TinyDB(mContext);
        MainActivity.TO_WHICH_FRAG = LocalConstants.TO_FRAG.TO_CLASSROOM;
    }

    View root;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_classroom_teacher, container, false);
        MainActivity.actionBarTitle.setText("Eduethics (Teacher)");


        init();

        //classroomRecyclerView = root.findViewById(R.id.recyclerViewClassroom);
        //classroomRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        //scheduleRecycler = root.findViewById(R.id.scheduleRecycler);
        //scheduleRecycler.setLayoutManager(new LinearLayoutManager(getContext()));


        ((TextView) root.findViewById(R.id.date)).setText(ExtraFunctions.getReadableDateInString(Calendar.getInstance().get(Calendar.DAY_OF_MONTH), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.YEAR)));


        //TODO
        root.findViewById(R.id.viewTimeTable).setVisibility(View.GONE);
        root.findViewById(R.id.viewTimeTable).setOnClickListener(v -> viewTimeTable());


        /*FirebaseFirestore.getInstance()
                .collection("teacher")
                .document(GlobalVariables.uid)
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (getActivity() == null) return;
                        if (!documentSnapshot.getString("isSite").equals("no")) {
                            ((TextView) root.findViewById(R.id.t)).setText("View Website");
                            root.findViewById(R.id.website).setOnClickListener(v -> getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragmentFrame, new WebsiteView(documentSnapshot.getString("isSite"))).commit());
                        } else {
                            root.findViewById(R.id.website).setOnClickListener(v -> getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragmentFrame, new WebSitePage()).commit());
                        }
                        classRoomsData = (List<Map<String, Object>>) documentSnapshot.get("classroom");
                        if (classRoomsData != null) {
                            ids = new ArrayList<>();
                            for (Map<String, Object> obj : classRoomsData) {
                                ids.add(obj.get("id").toString());
                            }
                            schedule(ids);
                            classroomRecyclerView.setAdapter(new ClassroomTeachersAdapter(ClassroomTeacherFragment.this, classRoomsData));
                        }

                    }
                });
        */
        root.findViewById(R.id.createClassroom).setOnClickListener(v -> getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragmentFrame, new CreateClassroomFragment(mContext, -1)).commit());

        return root;
    }

    private void init(){
        classroomRecyclerView = root.findViewById(R.id.recyclerViewClassroom);
        classroomRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        classSubjectModelList=new ArrayList<>();
        classroomTeachersAdapter=new ClassroomTeachersAdapter2(mContext,classSubjectModelList,ClassroomTeacherFragment.this);
        classroomRecyclerView.setAdapter(classroomTeachersAdapter);
        classListWithSubjects();

        //timeTableAll();
    }

    List<String> ids = new ArrayList<>();

    private void schedule(List<String> ids) {
        List<Map<String, String>> dataForSend = new ArrayList<>();
        for (String id : ids) {
            FirebaseFirestore.getInstance()
                    .collection("classroom")
                    .document(id)
                    .get()
                    .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {

                            List<Map<String, Object>> table = (List) documentSnapshot.get("timetable");
                            String dayToday = LocalConstants.DAYS[Calendar.getInstance().get(Calendar.DAY_OF_WEEK) - 1];
                            for (Map<String, Object> obj : table) {
                                if (obj.get("day").toString().equals(dayToday))
                                    if ((boolean) obj.get("isSet")) {
                                        Map<String, String> temp1 = new HashMap<>();
                                        temp1.put("time", obj.get("fromTime").toString());
                                        temp1.put("class", documentSnapshot.getString("className"));
                                        temp1.put("subject", documentSnapshot.getString("subject"));
                                        temp1.put("id", id);
                                        dataForSend.add(temp1);
                                    }
                            }
                            if (ids.indexOf(id) == ids.size() - 1) {
                                if (dataForSend.size() != 0) {
                                    TransitionManager.beginDelayedTransition(root.findViewById(R.id.MM));
                                    root.findViewById(R.id.noSch).setVisibility(View.GONE);
                                    scheduleRecycler.setVisibility(View.VISIBLE);
                                    scheduleRecycler.setAdapter(new ScheduleAdapterTeacher(dataForSend, ClassroomTeacherFragment.this));
                                }

                            }
                        }
                    });
        }
    }

    private void viewTimeTable() {
        getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(
                R.anim.slide_in,  // enter
                R.anim.fade_out,  // exit
                R.anim.fade_in,   // popEnter
                R.anim.slide_out  // popExit
        ).replace(R.id.fragmentFrame, new ViewTimeTable(timeTableModelList,mContext)).commit();
    }

    public void click(int position) {
        getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(
                R.anim.slide_in,  // enter
                R.anim.fade_out,  // exit
                R.anim.fade_in,   // popEnter
                R.anim.slide_out  // popExit
        ).replace(R.id.fragmentFrame, new HomeClassroomFragment(getContext(), classSubjectModelList.get(position).getClassId(), 0)).commit();
    }


    public void joinClass(int position) {
        getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(
                R.anim.slide_in,  // enter
                R.anim.fade_out,  // exit
                R.anim.fade_in,   // popEnter
                R.anim.slide_out  // popExit
        ).replace(R.id.fragmentFrame, new HomeClassroomFragment(mContext, ids.get(position), LocalConstants.TYPE.TEACHER)).commit();
    }

    @Override
    public void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TransitionInflater inflater = TransitionInflater.from(requireContext());
        setExitTransition(inflater.inflateTransition(R.transition.fade));
    }

    //WEBAPP
    private void classListWithSubjects(){

        TimeTableModel timeTableModel = new TimeTableModel();

        timeTableModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        timeTableModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));
        timeTableModel.setUserId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = apiService.classListWithSubjects(timeTableModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                try {
                    if(response.body()==null){
                        Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    Type type = new TypeToken<List<TimeTableModel>>() {
                    }.getType();
                    classSubjectModelList = new Gson().fromJson(response.body().getAsJsonArray("Response"), type);
                    //Log.e(mTAG, "userModelList" + classSubjectModelList.toString());

                    classroomTeachersAdapter.updateList(classSubjectModelList);

                } catch (Exception ex) {
                    Toast.makeText(mContext, LocalConstants.APP_MESSAGES.ERROR1, Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, LocalConstants.APP_MESSAGES.ERROR1, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void timeTableAll(){
        TimeTableModel timeTableModel = new TimeTableModel();

        timeTableModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        timeTableModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));
        timeTableModel.setUserId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = apiService.getTimeTableAll(timeTableModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                try {
                    if(response.body()==null){
                        Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    Type type = new TypeToken<List<TimeTableModel>>() {
                    }.getType();
                    timeTableModelList = new Gson().fromJson(response.body().getAsJsonArray("Response"), type);
                    //Log.e(mTAG, "userModelList" + classSubjectModelList.toString());

                } catch (Exception ex) {
                    Toast.makeText(mContext, LocalConstants.APP_MESSAGES.ERROR1, Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, LocalConstants.APP_MESSAGES.ERROR1, Toast.LENGTH_SHORT).show();
            }
        });
    }
}