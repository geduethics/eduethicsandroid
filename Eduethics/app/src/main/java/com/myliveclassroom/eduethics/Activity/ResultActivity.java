package com.myliveclassroom.eduethics.Activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.myliveclassroom.eduethics.Adapter.ExamAdapter;
import com.myliveclassroom.eduethics.Models.ApiResponseModel;
import com.myliveclassroom.eduethics.Models.ExamModel;
import com.myliveclassroom.eduethics.Models.UserModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.API_BASE_URL;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.APP_PACKAGE_NAME;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.PROFILE;

public class ResultActivity extends BaseActivity {

    @BindView(R.id.txtPageTitle)
    TextView txtPageTitle;

    @BindView(R.id.imgBack)
    ImageView imgBack;

    @BindView(R.id.imgbtnAdd)
    ImageView imgbtnAdd;

    @BindView(R.id.btnAdd)
    TextView btnAdd;

    @BindView(R.id.recyclerExam)
    RecyclerView recyclerExam;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    ExamAdapter examAdapter;
    List<ExamModel> examModelList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.MyThemeNoBar);
        setContentView(R.layout.activity_result);
        _BaseActivity(this);
        ButterKnife.bind(this);
        init();
    }

    private void Back() {
        super.onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    private void init() {
        progressBar.setVisibility(View.GONE);
        imgbtnAdd.setVisibility(View.INVISIBLE);
        imgBack.setVisibility(View.VISIBLE);
        btnAdd.setVisibility(View.GONE);
        btnAdd.setText("");

        txtPageTitle.setText("Results");
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Back();
            }
        });
        initRecyclerView();
    }

    private void initRecyclerView() {
        recyclerExam.setLayoutManager(new LinearLayoutManager(mContext));
        //countDownStart();
    }

    private void listExams() {
        progressBar.setVisibility(View.VISIBLE);
        ExamModel examModel = new ExamModel();
        if(tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)
                || tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.PARENT)) {
            examModel.setStudentId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
        }else if(tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER)){
            examModel.setCreatedBy(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
        }

        examModel.setProfileType(tinyDB.getString(tinyDB.getString(PROFILE)));
        examModel.setExamId(tinyDB.getString(LocalConstants.Exam_Constants.ExamId));

        examModelList = new ArrayList<>();

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = apiService.ResultSummary(examModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                try {
                    progressBar.setVisibility(View.GONE);
                    if (response.body() == null) {
                        Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    Type type = new TypeToken<List<ExamModel>>() {
                    }.getType();
                    examModelList = new Gson().fromJson(response.body().getAsJsonArray("Response"), type);
                    examAdapter = new ExamAdapter(mContext, examModelList);
                    recyclerExam.setAdapter(examAdapter);
                } catch (Exception ex) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                progressBar.setVisibility(View.GONE);
                Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private Handler handler = new Handler();
    private Runnable runnable;

    private void countDownStart() {
        runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    handler.postDelayed(this, 10000);
                    listExams();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        handler.postDelayed(runnable, 0);
    }

    public void downloadCertificate(ExamModel examModel) {
//Certificate
        progressBar.setVisibility(View.VISIBLE);
        examModel.setStudentName(tinyDB.getString("Name"));
        examModel.setStudentId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
        examModelList = new ArrayList<>();

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<ApiResponseModel> call = apiService.Certificate(examModel);
        call.enqueue(new Callback<ApiResponseModel>() {
            @Override
            public void onResponse(Call<ApiResponseModel> call, Response<ApiResponseModel> response) {
                try {
                    progressBar.setVisibility(View.GONE);
                    if (response.body() != null) {
                        new DownloadFileFromURL().execute(examModel.getCertificate());
                    }
                } catch (Exception ex) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ApiResponseModel> call, Throwable t) {
                // Log error here since request failed
                progressBar.setVisibility(View.GONE);
                Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private class DownloadFileFromURL extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //uploadNotification(notificationId,"Download starting","0% downloaded");
            //progressBar.setProgress(0);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                }
            });


        }

        /**
         * Downloading file in background thread
         */
        String mFileName="";
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {


                Random r = new Random();
                int i1 = r.nextInt(80 - 65) + 65;
                downloadedFileListArray=new String[1];
                downloadedFileListArray[0]= "e-Sikhya_Certificate"+i1+".pdf";
                mFileName="e-Sikhya_Certificate"+i1+".pdf";
                URL url = new URL(API_BASE_URL + "uploads/certificates/" + f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // this will be useful so that you can show a tipical 0-100% progress bar
                int lenghtOfFile = conection.getContentLength();
                Log.e(mTAG, "lenghtOfFile =" + lenghtOfFile);
                // download the file
                InputStream input = new BufferedInputStream(url.openStream(), 8192);



                // Output stream
                OutputStream output = new FileOutputStream(Environment.getExternalStorageDirectory().toString() + "/" + Environment.DIRECTORY_DOWNLOADS.toString() + "/"+mFileName);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }
            return null;
        }

        /**
         * Updating progress bar
         */
        @Override
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            Log.e(mTAG, "progress=" + progress[0]);
            //notifyUploadProgress(notificationId,"Downloading 1 of "+mTotalFiles,progress[0]+"%",100,Integer.valueOf(progress[0]));
            //progressBar.setProgress(Integer.valueOf(progress[0]));
            //progressBar.setVisibility(View.VISIBLE);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                }
            });

        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String str) {

            openDialog(null);
        }
    }


    AlertDialog alertDialog;
    String downloadedFileListArray[];
    public void openDialog(String[] arr) {
        alertDialog = null;
        if (downloadedFileListArray == null) {
            downloadedFileListArray = arr;
        }
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        dialogBuilder.setTitle("Open file");
        ListView list = new ListView(mContext);
        list.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, downloadedFileListArray));
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View view, int pos, long id) {
                final String path = Environment.getExternalStorageDirectory().toString() + "/" + Environment.DIRECTORY_DOWNLOADS.toString() + "/" + downloadedFileListArray[pos];
                File file = new File(path);
                //Uri photoURI = FileProvider.getUriForFile(mContext, getApplicationContext().getPackageName() + ".provider", file);
                Uri photoURI = FileProvider.getUriForFile(mContext, APP_PACKAGE_NAME+".provider", file);
                String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension
                        (MimeTypeMap.getFileExtensionFromUrl(path));
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setType(mimeType);
                intent.setDataAndType(photoURI, mimeType);
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    // handle no application here....
                    Toast.makeText(mContext, "Could not open this file", Toast.LENGTH_SHORT).show();
                }
            }
        });
        dialogBuilder.setView(list);

        alertDialog = dialogBuilder.create();
        alertDialog.show();

    }

    @Override
    protected void onStop() {
        super.onStop();
        handler.removeCallbacks(runnable);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runnable);
    }

    @Override
    protected void onResume() {
        super.onResume();
        listExams();
    }
}
