package com.myliveclassroom.eduethics.Fragments.HomeFragemtns;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;

import com.myliveclassroom.eduethics.Activity.MainActivity;
import com.myliveclassroom.eduethics.Activity.VideoConfrenceActivity;
import com.myliveclassroom.eduethics.Fragments.SubFragments.AttendanceStudent;
import com.myliveclassroom.eduethics.Fragments.SubFragments.AttendenceTeacher;
import com.myliveclassroom.eduethics.Fragments.SubFragments.FeeDetailStudent;
import com.myliveclassroom.eduethics.Fragments.TeacherFragments.AddFeeFragment;
import com.myliveclassroom.eduethics.Fragments.TeacherFragments.TeachersFeeStudentView;
import com.myliveclassroom.eduethics.Fragments.TeacherFragments.TimeTableCreateFragment;
import com.myliveclassroom.eduethics.Models.TimeTableModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.AppShareMessage;
import com.myliveclassroom.eduethics.Utils.ExtraFunctions;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.TinyDB;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;


import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.PROFILE;


public class SummaryFragment extends Fragment {

    List<TimeTableModel> classDetailModel = new ArrayList<>();

    Button btnGoLive;
    TinyDB tinyDB;
    Context mContext;
    TextView whichSub, classRoomIdTV, feeText, aboutClassStu, aboutInsStu;
    Context context;

    TableLayout timeTable_Table;
    EditText aboutClass, aboutInt;

    List<TimeTableModel> timeTableModelList = new ArrayList<>();

    ConstraintLayout constraintLayoutIns;
    ConstraintLayout constraintLayoutClass;
    ConstraintLayout constraintLayoutTimeTable;

    ConstraintLayout MAIN_SUMMARY;
    String mRoomName = "EduEthicsLiveClassRoom";

    EditText txtEditAboutInstructor, txtEditAboutClassroom;

    public SummaryFragment(Context context) {
        this.context = context;
        this.mContext = context;
        tinyDB = new TinyDB(mContext);
    }

    View root;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_summary_teacher, container, false);

        btnGoLive = root.findViewById(R.id.goLive);
        txtEditAboutClassroom = root.findViewById(R.id.aboutClassroomText);
        txtEditAboutInstructor = root.findViewById(R.id.aboutInstructorText);

        root.findViewById(R.id.inviteStudentUP).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2 = new Intent();
                intent2.setAction(Intent.ACTION_SEND);
                intent2.setType("text/plain");
                intent2.putExtra(Intent.EXTRA_TEXT, AppShareMessage.StudentInvite(mContext));
                startActivity(Intent.createChooser(intent2, "Share via"));
            }
        });


        //TODO
        root.findViewById(R.id.viewAtendenceBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)) {
                    getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(
                            R.anim.slide_in,  // enter
                            R.anim.fade_out,  // exit
                            R.anim.fade_in,   // popEnter
                            R.anim.slide_out  // popExit
                    ).replace(R.id.fragmentFrame, new AttendanceStudent()).commit();

                } else {
                    getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(
                            R.anim.slide_in,  // enter
                            R.anim.fade_out,  // exit
                            R.anim.fade_in,   // popEnter
                            R.anim.slide_out  // popExit
                    ).replace(R.id.fragmentFrame, new AttendenceTeacher()).commit();
                }
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_NEVER);
        }

        MainActivity.TO_WHICH_FRAG = LocalConstants.TO_FRAG.TO_DASHBOARD;
        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER)) {
            teacher();
        } else if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT))
            student();

        forAll();
        getClassDetail();

        return root;
    }

    private void getClassDetail() {
        TimeTableModel timeTableModel = new TimeTableModel();
        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER)) {
            timeTableModel.setUserId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
        }

        timeTableModel.setClassId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID));

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = apiService.TutorClassList(timeTableModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                try {
                    if (response.body() != null) {

                        Type type = new TypeToken<List<TimeTableModel>>() {
                        }.getType();
                        classDetailModel = new Gson().fromJson(response.body().getAsJsonArray("Response"), type);
                        for (TimeTableModel timeTableModel1 : classDetailModel) {

                            txtEditAboutClassroom.setText(timeTableModel1.getAboutClass());
                            txtEditAboutInstructor.setText(timeTableModel1.getAboutTutor());
                            ((TextView)  root.findViewById(R.id.aboutClassroomTextStudent)).setText(timeTableModel1.getAboutClass());
                            ((TextView)  root.findViewById(R.id.aboutInstructorTextStudent)).setText(timeTableModel1.getAboutClass());

                            if (timeTableModel1.getCourseFee().equalsIgnoreCase("0.00")) {
                                feeText.setText(timeTableModel1.getMonthlyFee() + " / month");
                            } else if (timeTableModel1.getMonthlyFee().equalsIgnoreCase("0.00")) {
                                feeText.setText(timeTableModel1.getCourseFee() + " / course");
                            }
                        }
                    }

                } catch (Exception ex) {

                    Toast.makeText(mContext, LocalConstants.APP_MESSAGES.ERROR1, Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, LocalConstants.APP_MESSAGES.ERROR1, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateAbout(String about) {
        boolean allDone = true;

        //After all validations
        //Save data
        TimeTableModel timeTableModel = new TimeTableModel();
        timeTableModel.setClassId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID));
        if (about.equalsIgnoreCase("Tutor")) {
            timeTableModel.setAboutClass("");
            timeTableModel.setAboutTutor(txtEditAboutInstructor.getText().toString());
        } else {
            timeTableModel.setAboutClass(txtEditAboutClassroom.getText().toString());
            timeTableModel.setAboutTutor("");
        }


        root.findViewById(R.id.progress).setVisibility(View.VISIBLE);

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = apiService.ClassAboutUpdate(timeTableModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                root.findViewById(R.id.progress).setVisibility(View.GONE);
                try {
                    if (response.body() != null) {
                        if (new Gson().fromJson(response.body().get("Status"), String.class).toString().equalsIgnoreCase("Success")) {
                            // finish();
                        } else {
                            Toast.makeText(mContext, LocalConstants.APP_MESSAGES.ERROR1, Toast.LENGTH_SHORT).show();
                        }
                        Log.e(mTAG, response.toString());
                    }

                } catch (Exception ex) {
                    Log.e(mTAG, "error =" + ex.getMessage());
                    Toast.makeText(mContext, LocalConstants.APP_MESSAGES.ERROR1, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, LocalConstants.APP_MESSAGES.ERROR1, Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void forAll() {

        root.findViewById(R.id.temp20).setOnClickListener(v -> showAboutClass());
        root.findViewById(R.id.temp19).setOnClickListener(v -> showAboutIns());
        root.findViewById(R.id.temp18).setOnClickListener(v -> showTimeTable());
        root.findViewById(R.id.temp1).setOnClickListener(v -> showFeeCons());
        root.findViewById(R.id.viewFee).setOnClickListener(v -> viewFee());


        whichSub = root.findViewById(R.id.whichSub);
        classRoomIdTV = root.findViewById(R.id.classRoomId);
        constraintLayoutIns = root.findViewById(R.id.aboutInstructorConstraint);
        constraintLayoutClass = root.findViewById(R.id.aboutClassroomConstraint);
        constraintLayoutTimeTable = root.findViewById(R.id.timeTableConstraint);
        MAIN_SUMMARY = root.findViewById(R.id.MAIN_SUMMARY);
        feeText = root.findViewById(R.id.feeText);

        //((Button) root.findViewById(R.id.viewFee)).setText(HomeClassroomFragment.model.getFee().equals("0") ? "Add Fee" : "View");
        //feeText.setText(HomeClassroomFragment.model.getFee().equals("0") ? "Fee not Added" : getString(R.string.Rs) + HomeClassroomFragment.model.getFee());

        whichSub.setText(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_NAME) + " | " + tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SUBJECT_NAME));
        // classRoomIdTV.setText("Classroom Id : " + HomeClassroomFragment.model.getId());
        timeTable_Table = root.findViewById(R.id.tableTimetable);


        //List<Map<String, Object>> timeTable = HomeClassroomFragment.model.getTimetable();
        getDays();
        showAboutIns();
        showAboutClass();
        showTimeTable();
        showFeeCons();
    }

    private void getDays() {

        TimeTableModel timeTableModel = new TimeTableModel();

        timeTableModel.setUserId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
        //timeTableModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        //timeTableModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));
        timeTableModel.setClassId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID));
        //timeTableModel.setSubjectId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SUBJECT_ID));
        timeTableModel.setProfileType(tinyDB.getString(PROFILE));
        //timeTableModel.setTeacherId("0");
        timeTableModel.setInstituteType(tinyDB.getString(LocalConstants.INSTITUTE_TYPE.TYPE));

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = apiService.getTimeTableDays(timeTableModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.e(mTAG, "userModelList" + response.body().toString());
                try {
                    timeTableModelList = new ArrayList<>();
                    if (response.body() != null) {
                        Type type = new TypeToken<List<TimeTableModel>>() {
                        }.getType();
                        timeTableModelList = new Gson().fromJson(response.body().getAsJsonArray("Response"), type);

                        TableRow tableRow = new TableRow(context);
                        tableRow.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                        tableRow.addView(getTV("Day", getResources().getColor(R.color.customPrimary), 18));
                        tableRow.addView(getTV("From", getResources().getColor(R.color.customPrimary), 18));
                        tableRow.addView(getTV("To", getResources().getColor(R.color.customPrimary), 18));
                        timeTable_Table.addView(tableRow);

                        for (TimeTableModel map : timeTableModelList) {
                            if ((Boolean) map.getIsTimeSet()) {
                                TableRow tableRowT = new TableRow(context);
                                tableRowT.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                                tableRowT.addView(getTV(map.getDayName(), Color.BLACK, 14));
                                tableRowT.addView(getTV(ExtraFunctions.getReadableTime(Integer.parseInt(map.getTimeFrom().split(":")[0]), Integer.parseInt(map.getTimeFrom().split(":")[1])), ResourcesCompat.getColor(getResources(), R.color.darkgrey, null), 14));
                                tableRowT.addView(getTV(ExtraFunctions.getReadableTime(Integer.parseInt(map.getTimeTo().split(":")[0]), Integer.parseInt(map.getTimeTo().split(":")[1])), ResourcesCompat.getColor(getResources(), R.color.darkgrey, null), 14));
                                timeTable_Table.addView(tableRowT);
                            }
                        }
                    }
                } catch (Exception ex) {
                    Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void student() {
        root.findViewById(R.id.inviteStudentUP).setVisibility(View.GONE);
        root.findViewById(R.id.viewTimeTable).setVisibility(View.GONE);
        root.findViewById(R.id.saveAboutInst).setVisibility(View.GONE);
        root.findViewById(R.id.temp22).setVisibility(View.GONE);
        root.findViewById(R.id.aboutInstructorText).setVisibility(View.GONE);
        root.findViewById(R.id.temp21).setVisibility(View.GONE);
        root.findViewById(R.id.saveAboutClass).setVisibility(View.GONE);
        root.findViewById(R.id.aboutClassroomText).setVisibility(View.GONE);

        root.findViewById(R.id.viewFee).setVisibility(((Button) root.findViewById(R.id.viewFee)).getText().toString().equals("Add Fee") ? View.GONE : View.VISIBLE);

        root.findViewById(R.id.aboutClassroomTextStudent).setVisibility(View.VISIBLE);
        root.findViewById(R.id.aboutInstructorTextStudent).setVisibility(View.VISIBLE);

        aboutInsStu = root.findViewById(R.id.aboutInstructorTextStudent);
        aboutClassStu = root.findViewById(R.id.aboutClassroomTextStudent);

        ((TextView) root.findViewById(R.id.goLive)).setText("Join class");
        btnGoLive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkAndPermission()) {
                    tinyDB.putString(LocalConstants.Jitsi_CONSTANTS.PROFILE, LocalConstants.Jitsi_CONSTANTS.JOIN_AS_STUDENT);
                    root.findViewById(R.id.progress).setVisibility(View.VISIBLE);
                    String id = ((int) (Math.random() * 100000000)) + "";
                    startActivity(new Intent(getContext(), VideoConfrenceActivity.class).putExtra("roomId", mRoomName + tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID)).putExtra("classId", tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID)));
                }
            }
        });
    }

    private boolean checkAndPermission() {
        boolean isAllSet = true;
        for (String permission : LocalConstants.PERMISSIONS) {
            if (ContextCompat.checkSelfPermission(getContext(), permission)
                    == PackageManager.PERMISSION_DENIED) {
                isAllSet = false;
            }
        }
        if (!isAllSet) {
            ActivityCompat.requestPermissions(getActivity(), LocalConstants.PERMISSIONS, LocalConstants.REQ_CODE.ALL);
        }
        return isAllSet;
    }

    private void teacher() {
        aboutClass = root.findViewById(R.id.aboutClassroomText);
        aboutInt = root.findViewById(R.id.aboutInstructorText);

        // aboutInt.setText(HomeClassroomFragment.model.getAboutInstructor());
        //aboutClass.setText(HomeClassroomFragment.model.getAboutClass());

       //((Button) root.findViewById(R.id.viewFee)).setText("Add Fee");

        root.findViewById(R.id.saveAboutClass).setOnClickListener(v -> saveAboutClass());
        root.findViewById(R.id.saveAboutInst).setOnClickListener(v -> saveAboutInst());
        root.findViewById(R.id.viewTimeTable).setOnClickListener(v -> getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(
                R.anim.slide_in,  // enter
                R.anim.fade_out,  // exit
                R.anim.fade_in,   // popEnter
                R.anim.slide_out  // popExit
        ).replace(R.id.fragmentFrame, new TimeTableCreateFragment(context)).commit());

        btnGoLive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkAndPermission()) {
                    tinyDB.putString(LocalConstants.Jitsi_CONSTANTS.PROFILE, LocalConstants.Jitsi_CONSTANTS.JOIN_AS_TUTOR);
                    root.findViewById(R.id.progress).setVisibility(View.VISIBLE);
                    String id = ((int) (Math.random() * 100000000)) + "";
                    startActivity(new Intent(getContext(), VideoConfrenceActivity.class).putExtra("roomId", mRoomName + tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID)).putExtra("classId", tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID)));
                }
            }
        });
    }


    private void viewFee() {

        if (((Button) root.findViewById(R.id.viewFee)).getText().toString().equals("Add Fee")) {
            getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(
                    R.anim.slide_in,  // enter
                    R.anim.fade_out,  // exit
                    R.anim.fade_in,   // popEnter
                    R.anim.slide_out  // popExit
            ).replace(R.id.fragmentFrame, new AddFeeFragment(context)).commit();
        } else if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)
                || tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.PARENT))
            getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(
                    R.anim.slide_in,  // enter
                    R.anim.fade_out,  // exit
                    R.anim.fade_in,   // popEnter
                    R.anim.slide_out  // popExit
            ).replace(R.id.fragmentFrame, new FeeDetailStudent(context)).commit();
        else
            getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(
                    R.anim.slide_in,  // enter
                    R.anim.fade_out,  // exit
                    R.anim.fade_in,   // popEnter
                    R.anim.slide_out  // popExit
            ).replace(R.id.fragmentFrame, new TeachersFeeStudentView(mContext)).commit();
    }

    private void saveAboutInst() {
        Map<String, Object> map = new HashMap<>();
        map.put("aboutInstructor", ((EditText) root.findViewById(R.id.aboutInstructorText)).getText().toString());
        updateAbout("Tutor");

    }

    private void saveAboutClass() {
        Map<String, Object> map = new HashMap<>();
        map.put("aboutClass", ((EditText) root.findViewById(R.id.aboutClassroomText)).getText().toString());
        updateAbout("Class");
    }

    private TextView getTV(String s, int color, float size) {
        TextView textView = new TextView(context);
        textView.setText(s);
        TableRow.LayoutParams param = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        param.weight = 1;
        param.setMargins(4, 2, 4, 2);
        textView.setLayoutParams(param);
        textView.setBackgroundColor(Color.TRANSPARENT);
        textView.setTextSize(size);
        textView.setTextColor(color);
        textView.setPadding(8, 6, 8, 6);
        textView.setGravity(Gravity.CENTER);
        return textView;
    }


    private void showAboutIns() {
        TransitionManager.beginDelayedTransition(MAIN_SUMMARY);
        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER)) {
            aboutInt.setVisibility(aboutInt.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
            root.findViewById(R.id.temp22).setVisibility(aboutInt.getVisibility());
            root.findViewById(R.id.saveAboutInst).setVisibility(aboutInt.getVisibility());
        } else if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT))
            aboutInsStu.setVisibility(aboutInsStu.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
    }

    private void showAboutClass() {
        TransitionManager.beginDelayedTransition(MAIN_SUMMARY);
        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER)) {
            aboutClass.setVisibility(aboutClass.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
            root.findViewById(R.id.temp21).setVisibility(aboutClass.getVisibility());
            root.findViewById(R.id.saveAboutClass).setVisibility(aboutClass.getVisibility());
        } else if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)) {
            aboutClassStu.setVisibility(aboutClassStu.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
        }
    }

    private void showTimeTable() {
        TransitionManager.beginDelayedTransition(MAIN_SUMMARY);
        timeTable_Table.setVisibility(timeTable_Table.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER))
            root.findViewById(R.id.viewTimeTable).setVisibility(timeTable_Table.getVisibility());
    }


    public void showFeeCons() {
        TransitionManager.beginDelayedTransition(MAIN_SUMMARY);
        feeText.setVisibility(feeText.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
        root.findViewById(R.id.viewFee).setVisibility(feeText.getVisibility());
    }

    @Override
    public void onResume() {
        super.onResume();
        root.findViewById(R.id.progress).setVisibility(View.GONE);
        if (tinyDB.getString("FROMACTIVITY").equalsIgnoreCase("VIDEO")) {
        }
    }

}