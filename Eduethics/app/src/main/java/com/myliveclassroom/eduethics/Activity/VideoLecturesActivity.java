package com.myliveclassroom.eduethics.Activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.myliveclassroom.eduethics.Adapter.VideoLectureAdapter;
import com.myliveclassroom.eduethics.Models.ApiResponseModel;
import com.myliveclassroom.eduethics.Models.ExamModel;
import com.myliveclassroom.eduethics.Models.TimeTableModel;
import com.myliveclassroom.eduethics.Models.UserModel;
import com.myliveclassroom.eduethics.Models.VideoLectureModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.TinyDB;
import com.google.android.material.textfield.TextInputEditText;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;

public class VideoLecturesActivity extends BaseActivity {

    @BindView(R.id.txtPageTitle)
    TextView txtPageTitle;

    @BindView(R.id.imgBack)
    ImageView imgBack;

    @BindView(R.id.imgbtnAdd)
    ImageView imgbtnAdd;

    @BindView(R.id.btnAdd)
    TextView btnAdd;

    @BindView(R.id.recyclerVideo)
    RecyclerView recyclerVideo;

    @BindView(R.id.layoutVideoLectureAdd)
    LinearLayout layoutVideoLectureAdd;

    @BindView(R.id.txtBtnCancel)
    TextView txtBtnCancel;

    @BindView(R.id.txtBtnSubmit)
    TextView txtBtnSubmit;

    @BindView(R.id.txtBtnSearch)
    TextView txtBtnSearch;

    @BindView(R.id.txtNoResult)
    TextView txtNoResult;



    @BindView(R.id.spinnerClass)
    Spinner spinnerClass;

    @BindView(R.id.spinnerSubject)
    Spinner spinnerSubject;

    @BindView(R.id.spinnerClassList)
    Spinner spinnerClassList;

    @BindView(R.id.spinnerSubjectList)
    Spinner spinnerSubjectList;

    @BindView(R.id.txtEditVideoLink)
    TextInputEditText txtEditVideoLink;

    @BindView(R.id.txtEditVideoTitle)
    TextInputEditText txtEditVideoTitle;

    @BindView(R.id.txtEditTeacherName)
    TextInputEditText txtEditTeacherName;

    List<VideoLectureModel> videoLectureModelList;
    VideoLectureAdapter videoLectureAdapter;

    boolean isAddLayoutOpen = false;
    ProgressBar progressBar;

    List<TimeTableModel> classList;
    List<TimeTableModel> subjectList;
    String mClassId="0",mSubjectId="0";

    //String mGalleryId = "0";

    //create a single thread pool to our image compression class.
    private ExecutorService mExecutorService = Executors.newFixedThreadPool(1);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_lectures);

        mContext = this;
        tinyDB = new TinyDB(this);
        ButterKnife.bind(this);
        init();
    }

    private void Back() {
        if (!isAddLayoutOpen) {
            super.onBackPressed();
        }
        recyclerVideo.setVisibility(View.VISIBLE);
        layoutVideoLectureAdd.setVisibility(View.GONE);
        isAddLayoutOpen = false;
    }

    @Override
    public void onBackPressed() {
        if (!isAddLayoutOpen) {
            super.onBackPressed();
        }
        recyclerVideo.setVisibility(View.VISIBLE);
        layoutVideoLectureAdd.setVisibility(View.GONE);
        isAddLayoutOpen = false;

    }


    private void init() {
        classList=new ArrayList<>();
        subjectList=new ArrayList<>();

        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        txtBtnCancel = findViewById(R.id.txtBtnCancel);
        txtBtnSubmit = findViewById(R.id.txtBtnSubmit);


        imgbtnAdd.setVisibility(View.INVISIBLE);
        imgBack.setVisibility(View.VISIBLE);
        btnAdd.setVisibility(View.GONE);
        btnAdd.setText("");

        if (tinyDB.getString("IsAdmin").equalsIgnoreCase("Y")) {
            imgbtnAdd.setVisibility(View.VISIBLE);
            btnAdd.setVisibility(View.VISIBLE);
            btnAdd.setText("Add");
        }
        layoutVideoLectureAdd.setVisibility(View.GONE);


        txtPageTitle.setText(R.string.VIDEO_LECTURES);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Back();
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                layoutVideoLectureAdd.setVisibility(View.VISIBLE);
                recyclerVideo.setVisibility(View.GONE);
                isAddLayoutOpen = true;
                resetForm();
            }
        });

        imgbtnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layoutVideoLectureAdd.setVisibility(View.VISIBLE);
                recyclerVideo.setVisibility(View.GONE);
                isAddLayoutOpen = true;
                resetForm();
            }
        });

        txtBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutVideoLectureAdd.setVisibility(View.GONE);
                recyclerVideo.setVisibility(View.VISIBLE);
                isAddLayoutOpen = false;
                resetForm();
            }
        });

        txtBtnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VideoLectureSave();
            }
        });
        txtBtnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listVideos();
            }
        });

        listClasses();
        listSubjects();
        initRecyclerView();

    }

    private void initRecyclerView() {
        videoLectureModelList = new ArrayList<>();

        recyclerVideo.setLayoutManager(new LinearLayoutManager(mContext));
        //listVideos();
    }

    private void listVideos() {
        showProgress();
        VideoLectureModel videoLectureModel = new VideoLectureModel();
        videoLectureModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        videoLectureModel.setClassId(mClassId);
        videoLectureModel.setSubjectId(mSubjectId);
        txtBtnSearch.setEnabled(false);
        txtBtnSearch.setText("Please wait");
        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<List<VideoLectureModel>> call = apiService.VideoLectureList(videoLectureModel);
        call.enqueue(new Callback<List<VideoLectureModel>>() {
            @Override
            public void onResponse(Call<List<VideoLectureModel>> call, Response<List<VideoLectureModel>> response) {
                txtBtnSearch.setEnabled(true);
                txtBtnSearch.setText("Search");
                hideProgress();
                try {
                    if (response.body()!=null){
                        if(response.body().size()==0){
                            txtNoResult.setVisibility(View.VISIBLE);
                        }
                        else{
                            txtNoResult.setVisibility(View.GONE);
                        }
                    }
                    videoLectureModelList = response.body();
                    videoLectureAdapter = new VideoLectureAdapter(mContext, videoLectureModelList);
                    recyclerVideo.setAdapter(videoLectureAdapter);

                } catch (Exception ex) {
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<List<VideoLectureModel>> call, Throwable t) {
                Log.e(mTAG, "error =" + t.toString());
                hideProgress();
                txtBtnSearch.setEnabled(true);
                txtBtnSearch.setText("Search");
            }
        });
    }

    private void VideoLectureSave() {

        VideoLectureModel videoLectureModel=new VideoLectureModel();
        videoLectureModel.setClassId(mClassId);
        videoLectureModel.setSubjectId(mSubjectId);
        videoLectureModel.setVideoLink(txtEditVideoLink.getText().toString());
        videoLectureModel.setVideoTitle(txtEditVideoTitle.getText().toString());
        videoLectureModel.setTeacherName(txtEditTeacherName.getText().toString());

        disableSubmitBtn();
        showProgress();

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<ApiResponseModel> call = apiService.VideoLectureSave(videoLectureModel);
        call.enqueue(new Callback<ApiResponseModel>() {
            @Override
            public void onResponse(Call<ApiResponseModel> call, Response<ApiResponseModel> response) {
                hideProgress();
                enableSubmitBtn();
                try {
                    ApiResponseModel responseModel = response.body();
                    if(responseModel.getMessage().equalsIgnoreCase("Success")){
                        myToast(mContext,"Record saved successfully",Toast.LENGTH_SHORT);
                        resetForm();
                    }

                } catch (Exception ex) {
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ApiResponseModel> call, Throwable t) {
                Log.e(mTAG, "error =" + t.toString());
                hideProgress();
                enableSubmitBtn();
            }
        });
    }

    private void listClasses() {

        showProgress();
        TimeTableModel timeTableModel=new TimeTableModel();
        timeTableModel.setSchoolId(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID);
        timeTableModel.setSessionId(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID);

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = apiService.classList(timeTableModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                try {
                    hideProgress();
                    if (response.body() == null) {
                        Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (response.body() != null) {
                        Type type = new TypeToken<List<TimeTableModel>>() {
                        }.getType();
                        classList = new Gson().fromJson(response.body().getAsJsonArray("Response"), type);

                        List<String> spinnerArray=new ArrayList<>();
                        spinnerArray.add("--All Class--");
                        for(TimeTableModel str : classList){
                            spinnerArray.add(str.getClassName());
                        }

                        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, spinnerArray);
                        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down vieww
                        spinnerClass.setAdapter(spinnerArrayAdapter);
                        spinnerClassList.setAdapter(spinnerArrayAdapter);
                        spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int myPosition, long myID) {
                                if(myPosition>0) {
                                    mClassId = classList.get(myPosition - 1).getClassId();
                                    listSubjects();
                                }else
                                {
                                    mClassId="0";
                                    listSubjects();
                                }
                                //Log.i("renderSpinner -> ", "onItemSelected: " + myPosition + "/" + myID);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parentView) {
                                // your code here
                            }
                        });
                        spinnerClassList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int myPosition, long myID) {
                                if(myPosition>0) {
                                    mClassId = classList.get(myPosition - 1).getClassId();
                                    listSubjects();
                                }else
                                {
                                    mClassId="0";
                                }
                                //Log.i("renderSpinner -> ", "onItemSelected: " + myPosition + "/" + myID);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parentView) {
                                // your code here
                            }
                        });
                    }
                } catch (Exception ex) {
                    Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                hideProgress();
                Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void listSubjects() {
        UserModel userModel=new UserModel();
        userModel.setClassId(mClassId);

        showProgress();
        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<List<TimeTableModel>> call = apiService.sujectList(userModel);
        call.enqueue(new Callback<List<TimeTableModel>>() {
            @Override
            public void onResponse(Call<List<TimeTableModel>> call, Response<List<TimeTableModel>> response) {
                try {
                    hideProgress();
                    if (response.body() != null) {
                        subjectList = response.body();
                        List<String> spinnerArray=new ArrayList<>();
                        spinnerArray.add("--All Subject--");
                        for(TimeTableModel str : subjectList){
                            spinnerArray.add(str.getSubjectName());
                        }

                        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, spinnerArray);
                        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down vieww
                        spinnerSubject.setAdapter(spinnerArrayAdapter);
                        spinnerSubjectList.setAdapter(spinnerArrayAdapter);
                        spinnerSubject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                            @Override
                            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int myPosition, long myID) {
                                if(myPosition>0) {
                                    mSubjectId = subjectList.get(myPosition - 1).getSubjectId();
                                }
                                else
                                {
                                    mSubjectId="0";
                                }
                                Log.i("renderSpinner -> ", "onItemSelected: " + myPosition + "/" + myID);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parentView) {
                                // your code here
                            }
                        });

                        spinnerSubjectList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                            @Override
                            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int myPosition, long myID) {
                                if(myPosition>0) {
                                    mSubjectId = subjectList.get(myPosition - 1).getSubjectId();
                                }else
                                {
                                    mSubjectId="0";
                                }
                                Log.i("renderSpinner -> ", "onItemSelected: " + myPosition + "/" + myID);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parentView) {
                                // your code here
                            }
                        });
                    }
                } catch (Exception ex) {
                    Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<List<TimeTableModel>> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                hideProgress();
                Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    int clickCount = 0;

    private void enableSubmitBtn() {
        clickCount = 0;
        txtBtnSubmit.setEnabled(true);
        txtBtnSubmit.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
        txtBtnSubmit.setText("Submit");
        recyclerVideo.setVisibility(View.VISIBLE);
        isAddLayoutOpen = false;
    }

    private void resetForm(){
        txtEditTeacherName.setText("");
        txtEditVideoLink.setText("");
        txtEditVideoTitle.setText("");
    }

    private void disableSubmitBtn() {
        clickCount = 1;
        txtBtnSubmit.setEnabled(false);
        txtBtnSubmit.setText("Please wait");
        txtBtnSubmit.setBackgroundColor(mContext.getResources().getColor(R.color.grey_att));
    }

    @Override
    public void onResume() {
        super.onResume();
        listVideos();
    }

    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

}
