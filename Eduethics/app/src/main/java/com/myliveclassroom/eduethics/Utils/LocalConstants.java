package com.myliveclassroom.eduethics.Utils;


import static android.Manifest.permission;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.API_BASE_URL;

public interface LocalConstants {


    String NOTIFICATION_ID = "com.newprop.classroom";
    String NOTIFICATION_NAME = "Classroom Notification";
    String STORAGE_REF = "gs://pc-api-6009912515183436118-930.appspot.com";

    String[] PERMISSIONS = {
            permission.MODIFY_AUDIO_SETTINGS,
            //permission.CALL_PHONE,
            permission.RECORD_AUDIO,
            permission.BLUETOOTH,
            permission.CAMERA,
            permission.READ_EXTERNAL_STORAGE,
            permission.READ_CALENDAR,
            permission.WRITE_CALENDAR
    };

    interface TYPE {
        int TEACHER = 1;
        int STUDENT = -1;
        int SCHOOL = 2;
    }

    interface PERMISSION_CODE {
        int SELECT_PICTURE = 100;
        int PICKFILE_RESULT_CODE = 101;
        int CAMERA_REQUEST = 102;
        int IMAGE_PROFILE = 103;
        int IMAGE_INS = 104;
    }

    interface REQ_CODE {
        int CAMERA_REQUEST = 200;
        int ALL = 201;
    }

    interface FROM {
        int FROM_CREATE_ASSIGNMENT = 0;
        int FROM_CREATE_TEST = 1;
        int FROM_SUMBIT_TEST = 2;
        int FROM_ADD_SOLUTION = 4;
        int FROM_TEST_FRAG = 5;
        int FROM_ASSIGNMENT_FRAG = 6;
        int FROM_EVALUATE_FRAG = 7;
        int FROM_SUMBIT_TEST_ATT = 8;
        int FROM_QUESTION_ADD_FRAGMENT =9;
        int FROM_QUESTION_LIST_FRAGMENT =8;
        int FROM_EXAM_CREATE_FRAGMENT=FROM_QUESTION_ADD_FRAGMENT;
        int FROM_FEE_STUDENT_LIST=11;
        int FROM_HOMEWORK_ACTVITY=15;
        int FROM_HOMEWORK_ACTVITY_STUDENT=16;
        int FROM_TeachersFeeStudentView=151;
    }

    interface TO_FRAG {
        int TO_DASHBOARD = 100;
        int TO_HOME_CLASSROOM = 0;
        int TO_CLASSROOM = 1;
        int TO_TEST_FRAG = 4;
        int TO_ASSIGNMENT_FRAG = 3;
        int TO_BACK_ENROLL = 5;
        int TO_FEEDBACK_FORM = 6;
        int TO_FEES_SUMMARY = 17;
        int EXIT = -1;
        int TO_TeachersFeeStudentView=151;
    }

    interface FRAGMENT_TABS {
        int  SUMMARY = 0;
        int  STUDENTS = 1;
        int CHAT = 2;
        int ASSIGNMENTS =3;
        int TEST = 4;
        int NOTICE_BOARD = 5;
        int STUDY_MATERIAL = 6 ;
    }

    String[] MONTHS_3Char = {
            "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    };

    String[] MONTHS = {
            "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
    };

    String[] DAYS = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

    interface INSTITUTE_TYPE {
        String TYPE = "INSTITUTE_TYPE";
        String SCHOOL = "SCHOOL";
        String TUTION = "TUTION";
        String COLLEGE = "COLLEGE";
    }

    interface PROFILE_TYPE {
        String STUDENT = "Student";
        String STUDENT_SCHOOL = "SchoolStudent";
        String PARENT = "Parent";
        String TEACHER = "Teacher";
        String TEACHER_SCHOOL = "SchoolTeacher";
        String OWNER = "Owner";
        String SCHOOL = "School";

        //TODO
        //Need to change profile name of TEACHER_SCHOOL on later stage
        //TEACHER_SCHOOL="SchoolTeacher"
    }

    interface TINYDB_LOGIN_KEYS {
        String SESSION_ID = "SessionID";
        String STUDENT_ID = "StudentID";
        String TEACHER_ID="TeacherID";
        String USER_ID = "UserID";
        String SCHOOL_ID = "SchoolID";
        String CLASS_ID = "ClassID";
        String CLASS_NAME = "ClassNAME";
        String SUBJECT_NAME = "SubjectNAME";
        String SECTION_ID = "SectionId";
        String SUBJECT_ID = "SubjectId";
        String SECTION_NAME = "SectionName";
        String NAME = "Name";
        String PROLFILE_TYPE = "PROFILE_TYPE";
    }

    interface TINYDB_KEYS {
        String TEMP_DATE = "mTEMPDATE";
        String TEMP_SCREEN_NAME = "mTEMPSCREENNAME";
    }

    interface API_STATUS {
        String STATUS = "Status";
        String SUCCESS = "success";
        String FAILED = "fail";
        String MESSAGE = "fail";
    }

    interface APP_MESSAGES {
        String ERROR1 = "Something went wrong";
    }

    interface Jitsi_CONSTANTS {
        String PROFILE = "JITSI_PROFILE";
        String JOIN_AS_STUDENT = "Student";
        String JOIN_AS_TUTOR = "Tutor";
    }

    interface NOTIFICATION {
        String TITLE = "Edu Ethics";
    }

    interface Exam_Constants {
        String Exam_IsStarted = "Exam_IsExamStarted";
        String ExamId = "Exam_EXAMID";
        String EndDate = "Exam_EndDate";
        String STUDENT_NAME = "Exam_StudentName";
        String ExamClass = "Exam_ExamClass";
        String ExamSubject = "Exam_ExamSubject";
        String ExamTime = "Exam_ExamTime";
        String ExamMaxMarks = "Exam_ExamMaxMarks";
        String ExamTotalQ = "Exam_ExamTotalQ";
        String ExamTotalQAtt = "Exam_ExamTotalQAtt";

        String ExamPerCorrect = "Exam_ExamPerCorrect";
        String ExamNegativePerIncorrect = "Exam_ExamNegativePerIncorrect";

        //Button GO Live Texts
        String btnText_MakeLive="Set Live";
        String btnText_GoLive="Go Live";
        String btnText_StartExam="Start Exam";
        String btnText_JoinExam="Join";

        //Exam Status
        String Status_Submitted="Submitted";

        //Exam Ans Keys
        String Show_Ans_Keys="ShowAnswers";
    }

    interface Exam_TYPE {
        String ORAL = "Oral";
        String MCQ = "Mcq";
        String SUBJECTIVE="Subjective";
    }

    interface Video_TYPE{
        String VideoClass="VideoClass";
        String VideoExam="Exam";
    }

    interface FILE_TYPE {
        String PNG = "PNG";
        String JPG = "JPG";
        String PDF="PDF";
    }

    interface PAYMENT_BTN_TXT {
        String PAY = "Pay";
        String PAY_NOW = "Pay Now";
        String VIEW_DETAIL = "View Detail";
        String VIEW="View";
    }

    interface PAYMENT_TXN_STATUS{
       String INITIATED="Initiated";
       String SUCCESS="Success";
       String FAIL="Fail";
    }


    interface ALARM_CONSTANTS {
        String IS_ALARM_SET = "IS_ALARM_SET";
        String ALARM_TIME = "ALARM_TIME";
    }

    interface HOME_WORK_STATUS {
        String UPLOADED = "Uploaded";
        String CORRECT_IT = "Correct it";
        String Excellent = "Excellent";
        String GOOD="Good";
    }

    interface FEE_VIEWS {
        int  ADMIN_SCREEN_1 = 0;
        int  ADMIN_SCREEN_2 = 1;
        int ADMIN_SCREEN_3 = 2;
        int ADMIN_SCREEN_4 =3;
        int TEACHER_SCREEN_1 =44;
        int OTHER_SCREEN =55;
        String FEE_SCREEN="FEE_SCREEN";
    }

    interface URL {
        String  REPORT_TYPE = "mReportURL";
        String REPORT_ATTENDANCE_TEACHER="mReportATTENDANCE_TEACHER";
        String REPORT_ATTENDANCE_STUDENT="mReportATTENDANCE_STUDENT";
        String REPORT_EXAM="mReportEXAM";
        String REPORT_FEE="mReportFEE";
        String  URL_REPORTS_Attendance_Teacher=API_BASE_URL+"Report/AttendanceReport";
        String  URL_REPORTS_Attendance_Student=API_BASE_URL+"Report/AttendanceReportStudentDateWise";
    }
}
