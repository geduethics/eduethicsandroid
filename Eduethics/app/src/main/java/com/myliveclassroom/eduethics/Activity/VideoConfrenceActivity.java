package com.myliveclassroom.eduethics.Activity;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.myliveclassroom.eduethics.Models.ApiResponseModel;
import com.myliveclassroom.eduethics.Models.ExamModel;
import com.myliveclassroom.eduethics.Models.TimeTableModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.TinyDB;
import com.facebook.react.modules.core.PermissionListener;

import org.jitsi.meet.sdk.BroadcastEvent;
import org.jitsi.meet.sdk.JitsiMeetActivityDelegate;
import org.jitsi.meet.sdk.JitsiMeetActivityInterface;
import org.jitsi.meet.sdk.JitsiMeetConferenceOptions;
import org.jitsi.meet.sdk.JitsiMeetFragment;
import org.jitsi.meet.sdk.JitsiMeetUserInfo;
import org.jitsi.meet.sdk.JitsiMeetView;

import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.PROFILE;

public class VideoConfrenceActivity extends FragmentActivity
        implements JitsiMeetActivityInterface {

    JitsiMeetFragment fragment;
    JitsiMeetView meetview;
    private static final String ACTION_JITSI_MEET_CONFERENCE = "org.jitsi.meet.CONFERENCE";
    private static final String JITSI_MEET_CONFERENCE_OPTIONS = "JitsiMeetConferenceOptions";
    TinyDB tinyDB;
    Context mContext;
    String mRoomId;
    String mClassType = "";

    public VideoConfrenceActivity() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_confrence);
        MainActivity.TO_WHICH_FRAG = LocalConstants.TO_FRAG.TO_FEEDBACK_FORM;
        tinyDB = new TinyDB(this);
        mContext = this;
        mRoomId = getIntent().getStringExtra("roomId");
        if (getIntent().hasExtra("mClassType")) {
            mClassType = getIntent().getStringExtra("mClassType");
        }
        registerForBroadcastMessages();

        if (extraInitialize())
            initialize();

        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)) {
            autoAtendance();
        }
        startCall();
    }

    protected boolean extraInitialize() {
        return true;
    }

    protected void initialize() {
        // Join the room specified by the URL the app was launched with.
        // Joining without the room option displays the welcome page.
        join(getConferenceOptions(getIntent()));
    }

    public void join(JitsiMeetConferenceOptions options) {
        getJitsiView().join(options);
    }

    protected JitsiMeetView getJitsiView() {
        fragment
                = (JitsiMeetFragment) getSupportFragmentManager().findFragmentById(org.jitsi.meet.sdk.R.id.jitsiFragment);
        meetview = fragment.getJitsiView();
        return meetview;
    }

    private @Nullable
    JitsiMeetConferenceOptions getConferenceOptions(Intent intent) {
        String action = intent.getAction();
        Log.e("intent---", intent + "----" + intent.getData());
        if (Intent.ACTION_VIEW.equals(action)) {
            Uri uri = intent.getData();
            if (uri != null) {
                return new JitsiMeetConferenceOptions.Builder().setRoom(uri.toString()).build();
            }
        } else if (ACTION_JITSI_MEET_CONFERENCE.equals(action)) {
            return intent.getParcelableExtra(JITSI_MEET_CONFERENCE_OPTIONS);
        }

        return null;
    }

    private void startCall() {
        try {
            JitsiMeetUserInfo info = new JitsiMeetUserInfo();
            //info.setDisplayName(tinyDB.getString(LocalConstants.Jitsi_CONSTANTS.PROFILE)+" | "+tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.NAME)+" "+"###96");
            if (mClassType.equalsIgnoreCase(LocalConstants.Video_TYPE.VideoExam)) {
                info.setDisplayName(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.NAME) + " " + "###96");
            } else {
                info.setDisplayName(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.NAME));
            }
            //info.setDisplayName("NAVi###76");
            JitsiMeetConferenceOptions options
                    = new JitsiMeetConferenceOptions.Builder()
                    // .setSubject(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_NAME) + " - " + tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SUBJECT_NAME))

                    .setWelcomePageEnabled(false)
                    .setServerURL(new URL("https://meet.jit.si/"))
                    .setFeatureFlag("call-integration.enabled", false)
                    .setFeatureFlag("resolution", 360)
                    //.setFeatureFlag("userType.type", tinyDB.getString(LocalConstants.Jitsi_CONSTANTS.PROFILE))// For teacher type is tutor / while for other this type is Student
                    .setFeatureFlag("userType.type", tinyDB.getString(LocalConstants.Jitsi_CONSTANTS.PROFILE))
                    .setFeatureFlag("roomName.show", tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_NAME) + " - " + tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SUBJECT_NAME))
                    .setFeatureFlag("classType.type", mClassType)
                    // .setFeatureFlag("userType.type", "Student" )
                    .setRoom(mRoomId)
                    .setAudioMuted(true)
                    .setVideoMuted(true)
                    .setAudioOnly(false)
                    .setUserInfo(info)

                    .build();

            if (fragment != null && meetview != null)
                meetview.join(options);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        // Here we are trying to handle the following corner case: an application using the SDK
        // is using this Activity for displaying meetings, but there is another "main" Activity
        // with other content. If this Activity is "swiped out" from the recent list we will get
        // Activity#onDestroy() called without warning. At this point we can try to leave the
        // current meeting, but when our view is detached from React the JS <-> Native bridge won't
        // be operational so the external API won't be able to notify the native side that the
        // conference terminated. Thus, try our best to clean up.
        leave();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);

        super.onDestroy();
    }

    @Override
    public void finish() {
        leave();
        if (!mClassType.equalsIgnoreCase(LocalConstants.Video_TYPE.VideoExam)) {
            if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER) && tinyDB.getString(LocalConstants.INSTITUTE_TYPE.TYPE).equalsIgnoreCase(LocalConstants.INSTITUTE_TYPE.SCHOOL)) {
                Intent intent = new Intent(VideoConfrenceActivity.this, OnlineFeedbackActivity.class);
                startActivity(intent);
            }
        }
        super.finish();
    }

    public void leave() {
        if (fragment != null && meetview != null)
            meetview.leave();
    }

    private void onBroadcastReceived(Intent intent) {

        if (intent != null) {
            BroadcastEvent event = new BroadcastEvent(intent);

            switch (event.getType()) {
                case CONFERENCE_JOINED:
                    onConferenceJoined(event.getData());
                    break;
                case CONFERENCE_WILL_JOIN:
                    onConferenceWillJoin(event.getData());
                    break;
                case CONFERENCE_TERMINATED:
                    onConferenceTerminated(event.getData());
                    break;
                case RESULT_CHANGED:
                    Log.e("event====", "event====" + event.getData());
                    break;
            }
        }
    }

    private void onConferenceWillJoin(HashMap<String, Object> data) {
        Log.e("onConferenceWillJoin", "onConferenceWillJoin joined: " + data);
    }

    private void onConferenceTerminated(HashMap<String, Object> data) {
        Log.e("onConferenceJoined", "onConferenceJoined joined: " + data);
        finish();
    }

    private void onConferenceJoined(HashMap<String, Object> data) {
        Log.e("onConferenceJoined", "onConferenceJoined joined: " + data);
    }

    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            onBroadcastReceived(intent);
        }
    };

    private void registerForBroadcastMessages() {
        IntentFilter intentFilter = new IntentFilter();

        for (BroadcastEvent.Type type : BroadcastEvent.Type.values()) {
            intentFilter.addAction(type.getAction());
        }

        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        JitsiMeetActivityDelegate.onActivityResult(this, requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        JitsiMeetActivityDelegate.onBackPressed();
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        JitsiMeetConferenceOptions options;

        if ((options = getConferenceOptions(intent)) != null) {
            join(options);
            return;
        }

        JitsiMeetActivityDelegate.onNewIntent(intent);
    }

    @Override
    protected void onUserLeaveHint() {
        getJitsiView().enterPictureInPicture();
    }

    @Override
    public void requestPermissions(String[] permissions, int requestCode, PermissionListener listener) {
        JitsiMeetActivityDelegate.requestPermissions(this, permissions, requestCode, listener);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        JitsiMeetActivityDelegate.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    private final int interval = 25000; //1000= 1 Second
    private Handler handler = new Handler();

    private void autoAtendance() {
        Runnable runnable = new Runnable() {
            public void run() {
              // Toast.makeText(VideoConfrenceActivity.this, "Attendance marked", Toast.LENGTH_SHORT).show();
                markAttendance();
            }
        };
        handler.postAtTime(runnable, System.currentTimeMillis() + interval);
        handler.postDelayed(runnable, interval);
    }

    private void markAttendance() {
        List<TimeTableModel> timeTableModelList=new ArrayList<>();
        TimeTableModel timeTableModel = new TimeTableModel();
        timeTableModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        timeTableModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));

        timeTableModel.setStudentId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
        timeTableModel.setClassId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID));
        timeTableModel.setSubjectId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SUBJECT_ID));

        timeTableModel.setAttDate(tinyDB.getString("AttDate").trim());
        timeTableModel.setUserId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID).trim());
        timeTableModel.setAttTag("P");

        timeTableModelList.add(timeTableModel);
        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = apiService.attendanceSubmit(timeTableModelList);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                try {
                    if (response.body() != null) {
                         if(new Gson().fromJson(response.body().get("Success"), String.class).equalsIgnoreCase("true")){
                            Log.e(mTAG, "Attendance marked successfully");
                             Toast.makeText(VideoConfrenceActivity.this, "Attendance marked", Toast.LENGTH_SHORT).show();
                        }else{
                            Log.e(mTAG, new Gson().fromJson(response.body().get("Message"), String.class));
                             Toast.makeText(VideoConfrenceActivity.this,  new Gson().fromJson(response.body().get("Message"), String.class), Toast.LENGTH_SHORT).show();
                        }
                        //ApiResponseModel apiResponseModel = response.body();
                        //Toast.makeText(VideoConfrenceActivity.this, response.body().getMessage()==null?"Done":response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        //finish();
                        //Log.e(mTAG, apiResponseModel.getMessage());
                    }

                } catch (Exception ex) {
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(VideoConfrenceActivity.this, LocalConstants.APP_MESSAGES.ERROR1, Toast.LENGTH_SHORT).show();
            }
        });
    }
}