package com.myliveclassroom.eduethics.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.myliveclassroom.eduethics.Fragments.HomeFragemtns.StudentFragment;
import com.myliveclassroom.eduethics.Models.TimeTableModel;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.TinyDB;

import java.util.List;

import static com.myliveclassroom.eduethics.Utils.HomeConstants.PROFILE;

public class StudentsAdapter extends RecyclerView.Adapter<StudentsAdapter.vh> {

    Fragment fragment;
    List<TimeTableModel> studentModelList;
    int of;
    Context mContext;
    TinyDB tinyDB;

    public StudentsAdapter(Fragment fragment, Context context, List<TimeTableModel> studentModelList, int of) {
        this.fragment = fragment;
        this.studentModelList = studentModelList;
        this.of = of;
        mContext = context;
        tinyDB = new TinyDB(mContext);
    }

    @NonNull
    @Override
    public vh onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new vh(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_student_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull vh holder, int position) {

        holder.name.setText(studentModelList.get(position).getStudentName());

        /*Map<String, Object> single = timeTableModelList.get(position);
        holder.name.setText(single.get("name").toString());
        if (of==1 && !GlobalVariables.isStudent){
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((StudentFragment)fragment).openFee(position);
                }
            });
        }

        if (single.get("image").toString().equals("-")) {
            holder.initialName.setText(single.get("name").toString().substring(0, 1));
        } else {
            Glide.with(fragment.getContext()).load(Uri.parse(single.get("image").toString())).centerCrop().circleCrop().into(holder.image);
            holder.initialName.setVisibility(View.GONE);
        }
        */
        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER)) {
            if (of == 1) {
                holder.call.setOnClickListener(v ->
                                ((StudentFragment)fragment).callPhone(studentModelList.get(position).getMobile())
                        /*fragment.startActivity(
                                new Intent(Intent.ACTION_CALL)
                                        .setData(Uri.parse("tel:" + studentModelList.get(position).getMobile())))*/
                );
                holder.delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((StudentFragment) fragment).deleteStudent(position);
                    }
                });
            } else {
                holder.call.setImageDrawable(ResourcesCompat.getDrawable(fragment.getResources(), R.drawable.ic_check, null));
                holder.call.setOnClickListener(v -> ((StudentFragment) fragment).action(studentModelList.get(position), 1));
                holder.delete.setOnClickListener(v -> ((StudentFragment) fragment).action(studentModelList.get(position), -1));
            }

        } else {
            holder.delete.setVisibility(View.INVISIBLE);
            holder.call.setVisibility(View.INVISIBLE);
            holder.number.setVisibility(View.GONE);
        }

        if (fragment != null) {
            holder.relativeRadio.setVisibility(View.GONE);
        }
    }



    public void updateList(List<TimeTableModel> list) {
        studentModelList = list;
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return studentModelList == null ? 0 : studentModelList.size();
    }

    class vh extends RecyclerView.ViewHolder {

        ImageView image, delete, call;
        TextView number, name, initialName;
        RelativeLayout relativeRadio;

        public vh(@NonNull View itemView) {
            super(itemView);
            this.image = itemView.findViewById(R.id.image);
            this.number = itemView.findViewById(R.id.number);
            this.call = itemView.findViewById(R.id.call);
            this.delete = itemView.findViewById(R.id.delete);
            this.name = itemView.findViewById(R.id.name);
            this.initialName = itemView.findViewById(R.id.nameInitial);
            this.relativeRadio = itemView.findViewById(R.id.relativeRadio);
        }
    }

}
