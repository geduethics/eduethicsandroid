package com.myliveclassroom.eduethics.Utils;

import com.dropbox.core.v2.teamlog.SfTeamInviteDetails;

public class GlobalVariables {

    public static String uid;
    public static String name;
    public static Boolean isStudent=false;

    public static  final String MAIN_FOLDER="EduEthics";
    public static  final String mTAG="EduEthics";

    public static  final String API_BASE_URL="http://eduethicsapi.webapptechnology.com/";//http://api.feepay.online.webapptechnology.com/";
    public static  final String SignalR_BASE_URL="http://eduethicssignalR.webapptechnology.com/chathub";
    public static final String API_BASE_URL_REPORTS=API_BASE_URL+"Report";


    public static final String ATTACHMENT_ARRAY="AttachmentArray";

    public static final String CAMERA_METHOD = "Camera";
    public static final String GALLERY_METHOD = "Gallery";
    public static  final int PICK_PDF_REQUEST_CODE=1001;
    public static  final int PICK_CAMERA_REQUEST_CODE=1002;
    public static  final int PICK_GALLERY_REQUEST_CODE=1003;

    public static final String dayArray_PB[]={"ਸੋਮਵਾਰ","ਮੰਗਲਵਾਰ","ਬੁੱਧਵਾਰ", "ਵੀਰਵਾਰ", "ਸ਼ੁੱਕਰਵਾਰ", "ਸ਼ਨੀਵਾਰ", "ਐਤਵਾਰ"};
    public static final String dayArray[]={"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};

    public static final String monthArray_PB[]={"jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec"};
    public static final String monthArray[]={"jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec"};

    public static final String mcqAnsSrArray[]={"a","b","c","d","e","f","g","h","i","g","h","i"};

    public static final String LOCALE_PB="pa";
    public static final String LOCALE_EN="en";
    public static final String LOCALE="Locale";

    public static final String TINY_CONSTANT_AppVersion="AppVersion";
    public static final String TINY_CONSTANT_HomeworkHead="HomeworkHead";
    public static final String TINY_CONSTANT_HOMEWORK_DONE_STATUS="HomeworkDoneStatus";
    public static final String TINY_CONSTANT_HomeworkNotification="Homework";
    public static final String TINY_CONSTANT_NotificationURL="NotificationURL";
    public static final String TINY_CONSTANT_MainURL="MainURL";


    public static final String CIRCULAR_CIRCULAR="Circular";
    public static final String CIRCULAR_DAILY_DOSE="DailyDose";
    public static final String CIRCULAR_WORD_OF_THE_DAY="WordOfTheDay";
    public static final String CIRCULAR_UDAAN="Udaan";

    public static final String APP_PACKAGE_NAME="com.myliveclassroom.eduethics";

    //Test account paytm
    public static final String PAYTM_MERCHANT_ID = "vsNtTt55476366177202"; //YOUR TEST MERCHANT ID
    public static final String url_paytm_callback = "https://securegw-stage.paytm.in/theia/paytmCallback";


    //Live account paytm
    //public static final String PAYTM_MERCHANT_ID = "nplQNi13791089817732"; //Live MERCHANT ID
    //public static final String url_paytm_callback = "https://securegw-stage.paytm.in/theia/paytmCallback";
   }
