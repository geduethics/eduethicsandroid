package com.myliveclassroom.eduethics.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.myliveclassroom.eduethics.Models.UserModel;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.classes.CommonFunctions;
import com.myliveclassroom.eduethics.classes.TinyDB;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.API_BASE_URL;


public class StudentListHomeAdapter  extends RecyclerView.Adapter<StudentListHomeAdapter.MyViewHolder> {

    TinyDB tinyDB;
    Context mContext;
    List<UserModel> userModelList;

    public StudentListHomeAdapter(Context context,List<UserModel> userModelList1){
        mContext=context;
        userModelList=userModelList1;
        tinyDB=new TinyDB(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.student_list_home_page, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.txtStudentName.setText(CommonFunctions.capitalize(userModelList.get(position).getName()));
        holder.txtClass.setText("Class : "+CommonFunctions.capitalize(userModelList.get(position).getClassName()) );
        holder.txtRollNo.setText("Roll No. : "+userModelList.get(position).getRollNo());

        if(userModelList.get(position).getProfilePic()!=null && !userModelList.get(position).getProfilePic().equalsIgnoreCase(""))
        {
            Glide.with(mContext)
                    .asBitmap()
                    .load(API_BASE_URL + "ProfilePic/" + userModelList.get(position).getProfilePic())
                    .into(holder.imgStudentPhoto);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* if (mContext instanceof MainActivity) {
                    ((MainActivity) mContext).changeProfile(userModelList.get(position));
                }*/
            }
        });
    }

    @Override
    public int getItemCount() {
        return (userModelList==null?0:userModelList.size());
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        CircleImageView imgStudentPhoto;
        TextView txtStudentName;
        TextView txtRollNo;
        TextView txtClass;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            txtStudentName =itemView.findViewById(R.id.txtStudentName);
            txtRollNo = itemView.findViewById(R.id.txtRollNo);
            txtClass=itemView.findViewById(R.id.txtClass);
            imgStudentPhoto = itemView.findViewById(R.id.imgStudentPhoto);
        }
    }
}
