package com.myliveclassroom.eduethics.Fragments.HomeFragemtns;

import android.content.Context;
import android.os.Bundle;
import android.transition.TransitionInflater;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.myliveclassroom.eduethics.Activity.MainActivity;
import com.myliveclassroom.eduethics.Adapter.AssignmentAdapter;
import com.myliveclassroom.eduethics.Fragments.Exam.StartSubmission;
import com.myliveclassroom.eduethics.Fragments.Exam.ViewSubmissionTest;
import com.myliveclassroom.eduethics.Fragments.Exam.CreateAssignmentFragment;
import com.myliveclassroom.eduethics.Models.HomeWorkModel;
import com.myliveclassroom.eduethics.Models.TimeTableModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.TinyDB;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.PROFILE;


public class AssignmentFragment extends Fragment {

    RecyclerView recyclerAssignment;
    Context context,mContext;

    public TextView submittedText, pendingText;
    TinyDB tinyDB;
    List<HomeWorkModel> homeWorkModelList=new ArrayList<>();
    AssignmentAdapter assignmentAdapter;
    LinearLayout emptyLayout;

    public AssignmentFragment(Context context) {
        this.context = context;
        this.mContext=context;
        MainActivity.TO_WHICH_FRAG = LocalConstants.TO_FRAG.TO_HOME_CLASSROOM;
        tinyDB=new TinyDB(mContext);
    }

    View root;

    @Override
    public void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TransitionInflater inflater = TransitionInflater.from(requireContext());
        setEnterTransition(inflater.inflateTransition(R.transition.slide_right));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_assignment, container, false);

        emptyLayout=root.findViewById(R.id.empty);
        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)) {
            ((TextView) root.findViewById(R.id.tit)).setText("Assignments scheculed by teacher \nappear here");
            root.findViewById(R.id.createAssignment).setVisibility(View.GONE);
        } else {
            teacher();
        }

        pendingText = root.findViewById(R.id.pendingText);
        submittedText = root.findViewById(R.id.submittedText);

        initRecycler();

        root.findViewById(R.id.createAssignment).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO
                getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(
                        R.anim.slide_in,  // enter
                        R.anim.fade_out,  // exit
                        R.anim.fade_in,   // popEnter
                        R.anim.slide_out  // popExit
                ).replace(R.id.fragmentFrame, new CreateAssignmentFragment(mContext)).commit();
            }
        });

        return root;
    }

    private void initRecycler(){
        recyclerAssignment = root.findViewById(R.id.recyclerAssignment);
        recyclerAssignment.setLayoutManager(new LinearLayoutManager(context));
        assignmentAdapter=new AssignmentAdapter(AssignmentFragment.this,mContext,homeWorkModelList);
        recyclerAssignment.setAdapter(assignmentAdapter);
        getHomeWork();
    }

    private void getHomeWork(){

        TimeTableModel timeTableModel=new TimeTableModel();

        timeTableModel.setProfileType(tinyDB.getString(PROFILE));
        timeTableModel.setClassId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID));
        timeTableModel.setUserId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = apiService.homeWorkList(timeTableModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                try {
                    if (response.body() != null) {

                        Type type = new TypeToken<List<HomeWorkModel>>() {
                        }.getType();
                        homeWorkModelList = new Gson().fromJson(response.body().getAsJsonArray("Response"), type);
                        if (homeWorkModelList.size() == 0) {
                            emptyLayout.setVisibility(View.VISIBLE);
                            ((TextView) root.findViewById(R.id.emptyMessage)).setText("No Assignment Found!");
                        }else{
                            emptyLayout.setVisibility(View.GONE);
                        }
                        assignmentAdapter.updateList(homeWorkModelList);
                    }

                } catch (Exception ex) {
                    Toast.makeText(mContext, LocalConstants.APP_MESSAGES.ERROR1, Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed

                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, LocalConstants.APP_MESSAGES.ERROR1, Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void teacher() {
        root.findViewById(R.id.pendingText).setVisibility(View.GONE);
        root.findViewById(R.id.submittedText).setVisibility(View.GONE);
        root.findViewById(R.id.createAssignment).setOnClickListener(v -> getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(
                R.anim.slide_in,  // enter
                R.anim.fade_out,  // exit
                R.anim.fade_in,   // popEnter
                R.anim.slide_out  // popExit
        ).replace(R.id.fragmentFrame, new CreateAssignmentFragment(mContext)).commit());
    }

    public void view(HomeWorkModel homeWorkModel) {
      getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(
                R.anim.slide_in,  // enter
                R.anim.fade_out,  // exit
                R.anim.fade_in,   // popEnter
                R.anim.slide_out  // popExit
        ).replace(R.id.fragmentFrame, new ViewSubmissionTest(mContext,homeWorkModel, LocalConstants.FROM.FROM_ASSIGNMENT_FRAG)).commit();

    }


    public void edit(  HomeWorkModel homeWorkModel) {
       /* getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(
                R.anim.slide_in,  // enter
                R.anim.fade_out,  // exit
                R.anim.fade_in,   // popEnter
                R.anim.slide_out  // popExit
        ).replace(R.id.fragmentFrame, new CreateAssignment(HomeClassroomFragment.model.getAssignments().get(position), position)).commit();*/
    }

    public void open(HomeWorkModel homeWorkModel, boolean isDead) {
        getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(
                R.anim.slide_in,  // enter
                R.anim.fade_out,  // exit
                R.anim.fade_in,   // popEnter
                R.anim.slide_out  // popExit
        ).replace(R.id.fragmentFrame, new StartSubmission(mContext,homeWorkModel, LocalConstants.FROM.FROM_ASSIGNMENT_FRAG, isDead)).commit();
    }

    public void downloadImageAndShow(  HomeWorkModel homeWorkModel) {

    }

    public void solution(  HomeWorkModel homeWorkModel) {
   /*     getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(
                R.anim.slide_in,  // enter
                R.anim.fade_out,  // exit
                R.anim.fade_in,   // popEnter
                R.anim.slide_out  // popExit
        ).replace(R.id.fragmentFrame, new AddSolution("assignments", HomeClassroomFragment.model.getAssignments().get(position), true)).commit();
*/
    }


}