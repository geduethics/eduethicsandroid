package com.myliveclassroom.eduethics.Fragments.Exam;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.myliveclassroom.eduethics.Activity.MainActivity;
import com.myliveclassroom.eduethics.Fragments.HomeClassroomFragment;
import com.myliveclassroom.eduethics.Fragments.WebAppFrag.ExamQuestionFragment;
import com.myliveclassroom.eduethics.Models.ExamModel;
import com.myliveclassroom.eduethics.Models.QuestionModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.TinyDB;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;
import static com.myliveclassroom.eduethics.Utils.LocalConstants.FROM.FROM_QUESTION_LIST_FRAGMENT;


public class ExamMcqCreateFragment extends Fragment {
    Context mContext;
    View root;
    TinyDB tinyDB;

    EditText txtQuestion, txtQ1, txtQ2, txtQ3, txtQ4;
    CheckBox chk1, chk2, chk3, chk4;
    Button saveCreate;
    Boolean isChk1 = false, isChk2 = false, isChk3 = false, isChk4 = false;


    public ExamMcqCreateFragment(Context context) {

        mContext = context;
        tinyDB = new TinyDB(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_exam_mcq_create, container, false);

        MainActivity.TO_WHICH_FRAG = FROM_QUESTION_LIST_FRAGMENT;

        init();

        return root;
    }

    private void init() {
        txtQuestion = root.findViewById(R.id.Question);
        txtQ1 = root.findViewById(R.id.Q1);
        txtQ2 = root.findViewById(R.id.Q2);
        txtQ3 = root.findViewById(R.id.Q3);
        txtQ4 = root.findViewById(R.id.Q4);
        chk1 = root.findViewById(R.id.chk1);
        chk2 = root.findViewById(R.id.chk2);
        chk3 = root.findViewById(R.id.chk3);
        chk4 = root.findViewById(R.id.chk4);

        saveCreate = root.findViewById(R.id.saveCreateAssignment);
        saveCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                examMcqSave();
            }
        });


        chk1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    isChk1 = true;
                } else {
                    isChk1 = false;
                }
            }
        });

        chk2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (((CheckBox) v).isChecked()) {
                    isChk2 = true;
                } else {
                    isChk2 = false;
                }

            }
        });

        chk3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (((CheckBox) v).isChecked()) {
                    isChk3 = true;
                } else {
                    isChk3 = false;
                }

            }
        });

        chk4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    isChk4 = true;
                } else {
                    isChk4 = false;
                }
            }
        });
    }

    QuestionModel questionModel = new QuestionModel();
    List<QuestionModel> questionModelList = new ArrayList<>(5);

    private void examMcqSave() {
        questionModelList = new ArrayList<>(5);
        questionModel.setChkChoice("Y");
        questionModel.setCreatedBy(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
        questionModel.setQuestion(txtQuestion.getText().toString());
        questionModel.setExamId(tinyDB.getString(LocalConstants.Exam_Constants.ExamId));
        questionModelList.add(0, questionModel);

        //Ans1
        questionModel = new QuestionModel();
        questionModel.setAns(txtQ1.getText().toString());

        if (isChk1) {
            questionModel.setIsAns("Y");
        } else {
            questionModel.setIsAns("N");
        }
        questionModelList.add(1, questionModel);

        //Ans2
        questionModel = new QuestionModel();
        questionModel.setAns(txtQ2.getText().toString());

        if (isChk2) {
            questionModel.setIsAns("Y");
        } else {
            questionModel.setIsAns("N");
        }
        questionModelList.add(2, questionModel);

        //Ans3
        questionModel = new QuestionModel();
        questionModel.setAns(txtQ3.getText().toString());

        if (isChk3) {
            questionModel.setIsAns("Y");
        } else {
            questionModel.setIsAns("N");
        }
        questionModelList.add(3, questionModel);

        //Ans4
        questionModel = new QuestionModel();
        questionModel.setAns(txtQ4.getText().toString());

        if (isChk4) {
            questionModel.setIsAns("Y");
        } else {
            questionModel.setIsAns("N");
        }
        questionModelList.add(4, questionModel);


        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = apiService.ExamMcqCreate(questionModelList);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.e(mTAG, "userModelList" + response.body().toString());
                try {
                    if (response.body() == null) {
                        Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    String status = new Gson().fromJson(response.body().get("Status"), String.class);
                    String mes = new Gson().fromJson(response.body().get("Message"), String.class);

                    if (status.equalsIgnoreCase("Success")) {
                        Toast.makeText(mContext, "Done", Toast.LENGTH_SHORT).show();
                        getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(
                                R.anim.slide_in,  // enter
                                R.anim.fade_out,  // exit
                                R.anim.fade_in,   // popEnter
                                R.anim.slide_out  // popExit
                        ).replace(R.id.fragmentFrame, new ExamQuestionFragment(mContext)).commit();
                    } else {
                        Toast.makeText(mContext, mes, Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception ex) {
                    Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void onBackPressed() {
        Log.e(mTAG, "this is test");
        getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(
                R.anim.slide_in,  // enter
                R.anim.fade_out,  // exit
                R.anim.fade_in,   // popEnter
                R.anim.slide_out  // popExit
        ).replace(R.id.fragmentFrame, new HomeClassroomFragment(mContext, tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID), LocalConstants.TYPE.TEACHER)).commit();

    }
}