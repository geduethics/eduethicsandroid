package com.myliveclassroom.eduethics.Activity;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.myliveclassroom.eduethics.Fragments.ClassroomStudentFragment;
import com.myliveclassroom.eduethics.Fragments.ClassroomTeacherFragment;
import com.myliveclassroom.eduethics.Fragments.ContactUsFragment;
import com.myliveclassroom.eduethics.Fragments.DashboardFragment;
import com.myliveclassroom.eduethics.Fragments.HomeClassroomFragment;
import com.myliveclassroom.eduethics.Fragments.HowToUseFragment;
import com.myliveclassroom.eduethics.Fragments.ProfileFragment;
import com.myliveclassroom.eduethics.Fragments.SubFragments.NotificationFrag;
import com.myliveclassroom.eduethics.Fragments.TeacherFragments.OnlineStudentFeedbackFragment;
import com.myliveclassroom.eduethics.Fragments.TeacherFragments.TeachersFeeStudentView;
import com.myliveclassroom.eduethics.Fragments.WebAppFrag.ExamQuestionFragment;
import com.myliveclassroom.eduethics.Fragments.WebAppFrag.FeeSchoolFragment;
import com.myliveclassroom.eduethics.Fragments.WebLogin;
import com.myliveclassroom.eduethics.Models.Alarm.Event;
import com.myliveclassroom.eduethics.Models.Alarm.Notification;
import com.myliveclassroom.eduethics.Models.ApiResponseModel;
import com.myliveclassroom.eduethics.Models.TimeTableModel;
import com.myliveclassroom.eduethics.Models.UserModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Services.GeneralService;
import com.myliveclassroom.eduethics.Services.alarm.ServiceAutoLauncher;
import com.myliveclassroom.eduethics.Utils.GlobalVariables;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.Utils.UtilsAlarm;
import com.myliveclassroom.eduethics.classes.TinyDB;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.microsoft.signalr.HubConnection;
import com.microsoft.signalr.HubConnectionBuilder;
import com.microsoft.signalr.HubConnectionState;
import com.myliveclassroom.eduethics.database.DBHelper;
import com.myliveclassroom.eduethics.database.DBTables;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.READ_CALENDAR;
import static android.Manifest.permission.WRITE_CALENDAR;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.SignalR_BASE_URL;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.TINY_CONSTANT_HOMEWORK_DONE_STATUS;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.TINY_CONSTANT_HomeworkHead;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.PROFILE;
import static com.myliveclassroom.eduethics.Utils.LocalConstants.FROM.FROM_QUESTION_ADD_FRAGMENT;
import static com.myliveclassroom.eduethics.Utils.LocalConstants.FROM.FROM_QUESTION_LIST_FRAGMENT;
import static com.myliveclassroom.eduethics.Utils.LocalConstants.FROM.FROM_TeachersFeeStudentView;
import static com.myliveclassroom.eduethics.Utils.LocalConstants.TO_FRAG.EXIT;
import static com.myliveclassroom.eduethics.Utils.LocalConstants.TO_FRAG.TO_ASSIGNMENT_FRAG;
import static com.myliveclassroom.eduethics.Utils.LocalConstants.TO_FRAG.TO_BACK_ENROLL;
import static com.myliveclassroom.eduethics.Utils.LocalConstants.TO_FRAG.TO_CLASSROOM;
import static com.myliveclassroom.eduethics.Utils.LocalConstants.TO_FRAG.TO_DASHBOARD;
import static com.myliveclassroom.eduethics.Utils.LocalConstants.TO_FRAG.TO_FEEDBACK_FORM;
import static com.myliveclassroom.eduethics.Utils.LocalConstants.TO_FRAG.TO_FEES_SUMMARY;
import static com.myliveclassroom.eduethics.Utils.LocalConstants.TO_FRAG.TO_HOME_CLASSROOM;
import static com.myliveclassroom.eduethics.Utils.LocalConstants.TO_FRAG.TO_TEST_FRAG;


public class MainActivity extends AppCompatActivity {

    @SuppressLint("StaticFieldLeak")
    public static TextView actionBarTitle;
    @SuppressLint("StaticFieldLeak")
    public static ImageView toolbarImageFilter;
    public static int TO_WHICH_FRAG = -1;
    @SuppressLint("StaticFieldLeak")
    public static FirebaseFirestore db = FirebaseFirestore.getInstance();

    ActionBarDrawerToggle actionBarDrawerToggle;
    DrawerLayout drawawerMain;
    NavigationView navigationMainPage;

    Context mContext;
    TinyDB tinyDB;
    int TorS;
    public static MainActivity instance;
    Fragment fragment;
    private int alarmYear, alarmMonth, alarmDay, alarmHour, alarmMinute;

    public static Fragment frag = null;
    int mCalDay = 28;
    int mCalMonth = 9, mCalYear;
    int mCalMinStart = 20;
    int mCalHourStart = 13;
    int mCalMinEnd = 45;
    int mCalHourEnd = 14;

    private int notColor;
    //private DBHelper dbHelper;
    private List<Notification> notifications;
    private Event event;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        instance = this;
        mContext = this;
        tinyDB = new TinyDB(this);

        getmHubConnection();

        Thread.setDefaultUncaughtExceptionHandler((paramThread, paramThrowable) -> {
            new Thread() {
                @Override
                public void run() {
                    Looper.prepare();
                    Snackbar.make(findViewById(R.id.DrawawerMain), "Network Issue", 4000).show();
                    Looper.loop();
                }
            }.start();
            try {
                Thread.sleep(4000); // Let the Toast display before app will get shutdown
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.exit(0);
        });

        TorS = tinyDB.getString(PROFILE).toLowerCase().equals("teacher") ? LocalConstants.TYPE.TEACHER : LocalConstants.TYPE.STUDENT;

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setCustomView(R.layout.custom_action_bar);
        } else if (getActionBar() != null) {
//            getActionBar().setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP);
            getActionBar().setDisplayShowCustomEnabled(true);
            getActionBar().setCustomView(R.layout.custom_action_bar);
        }

        //Scan for Weblogin
        if (tinyDB != null) {
            if (tinyDB.getBoolean("IsExam")) {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragmentFrame, new WebLogin(mContext)).commit();
            }
        }

        if (getSupportActionBar() != null) {

            toolbarImageFilter = getSupportActionBar().getCustomView().findViewById(R.id.toolbarImage);
            actionBarTitle = getSupportActionBar().getCustomView().findViewById(R.id.actionBarTitle);
            getSupportActionBar().getCustomView().findViewById(R.id.notiToolbar).setOnClickListener(v -> getSupportFragmentManager().beginTransaction().replace(R.id.fragmentFrame, new NotificationFrag()).commit());

            getSupportActionBar().getCustomView().findViewById(R.id.webLoginToolbar).setOnClickListener(v -> getSupportFragmentManager().beginTransaction().replace(R.id.fragmentFrame, new WebLogin()).commit());

            getSupportActionBar().getCustomView().findViewById(R.id.toggleStudentAndParent).setVisibility(View.GONE);

        } else {
            if (getActionBar() != null) {
                toolbarImageFilter = getActionBar().getCustomView().findViewById(R.id.toolbarImage);
                actionBarTitle = getActionBar().getCustomView().findViewById(R.id.actionBarTitle);
                getActionBar().getCustomView().findViewById(R.id.toggleStudentAndParent).setVisibility(View.GONE);
            }
        }

        if (actionBarTitle != null) {
            if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER) ||
                    tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER_SCHOOL)
            ) {
                actionBarTitle.setText("Teacher");
            } else {
                if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT) ||
                        tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT_SCHOOL)
                ) {
                    actionBarTitle.setText("Student");
                }
            }
        }

        drawawerMain = findViewById(R.id.DrawawerMain);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawawerMain, R.string.open, R.string.close);
        drawawerMain.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.getDrawerArrowDrawable().setColor(ResourcesCompat.getColor(getResources(), R.color.black, null));
        actionBarDrawerToggle.syncState();

        actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
        if (getSupportActionBar() != null) {
            this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getColor(R.color.BackGround)));
        }


        navigationMainPage = findViewById(R.id.navigationMainPage);
        navigationMainPage.setNavigationItemSelectedListener(item -> {
            setThisIdFragment(item.getItemId());
            return true;
        });

        navigationMainPage.setCheckedItem(R.id.nav_dashboard);
        setThisIdFragment(R.id.nav_dashboard);

        firebaseRegistrationCode();
        initViews();
        //readCalendar();
        /*if (!checkPermission()) {
            requestPermissions();
        }*/
        //initVariables();
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("my-event"));

    }

    private void initViews() {
        event = new Event();
        notifications = new ArrayList<>();
        //dbHelper = new DBHelper(this);

        initVariables();
        notifications.add(new Notification(getString(R.string._10_minutes_before)));

    }

    private void initVariables() {
        Calendar mCal = Calendar.getInstance();
        mCal.setTimeZone(TimeZone.getDefault());
        alarmHour = mCal.get(Calendar.HOUR_OF_DAY);
        alarmMinute = mCal.get(Calendar.MINUTE);

        try {
            mCal.setTime(UtilsAlarm.eventDateFormat.parse("09-Oct-2021"));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        alarmYear = mCal.get(Calendar.YEAR);
        alarmMonth = mCal.get(Calendar.MONTH);
        alarmDay = mCal.get(Calendar.DAY_OF_MONTH);
    }

    private void init() {
        //if Login person is Teacher /*(or Owner)*/ and Institute type not registered
        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER)) {// || tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.OWNER)) {
            if (tinyDB.getString(LocalConstants.INSTITUTE_TYPE.TYPE).equalsIgnoreCase("")) {

            }
        }
    }

    private void setThisIdFragment(int itemId) {
        Fragment fragment = null;
        boolean icCheckedItem = true;
        switch (itemId) {

            case R.id.nav_dashboard:
               /* if (TorS == LocalConstants.TYPE.STUDENT) {
                    fragment = new ClassroomStudentFragment(this);
                } else {
                    fragment = new ClassroomTeacherFragment(this);
                } */

                if (tinyDB.getString(LocalConstants.INSTITUTE_TYPE.TYPE).equalsIgnoreCase(LocalConstants.INSTITUTE_TYPE.TUTION)
                        || tinyDB.getString(LocalConstants.INSTITUTE_TYPE.TYPE).equalsIgnoreCase("")) {
                    if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER)) {
                        fragment = new ClassroomTeacherFragment(this);
                    } else if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)) {
                        fragment = new ClassroomStudentFragment(this);
                    } else if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER_SCHOOL)) {
                        tinyDB.putString(PROFILE, LocalConstants.PROFILE_TYPE.TEACHER);
                        fragment = new DashboardFragment(this, LocalConstants.PROFILE_TYPE.TEACHER);
                    } else if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT_SCHOOL)) {
                        tinyDB.putString(PROFILE, LocalConstants.PROFILE_TYPE.STUDENT);
                        fragment = new DashboardFragment(this, LocalConstants.PROFILE_TYPE.STUDENT);
                    }
                } else if (tinyDB.getString(LocalConstants.INSTITUTE_TYPE.TYPE).equalsIgnoreCase(LocalConstants.INSTITUTE_TYPE.SCHOOL)) {
                    if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.SCHOOL)) {
                        fragment = new DashboardFragment(this, LocalConstants.PROFILE_TYPE.SCHOOL);
                    } else if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER)
                            || tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER_SCHOOL)) {
                        tinyDB.putString(PROFILE, LocalConstants.PROFILE_TYPE.TEACHER);
                        fragment = new DashboardFragment(this, LocalConstants.PROFILE_TYPE.TEACHER);
                    } else if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT_SCHOOL)
                            || tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)) {
                        tinyDB.putString(PROFILE, LocalConstants.PROFILE_TYPE.STUDENT);
                        fragment = new DashboardFragment(this, LocalConstants.PROFILE_TYPE.STUDENT);
                    }
                } else {
                    if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER)) {
                        fragment = new DashboardFragment(this, LocalConstants.PROFILE_TYPE.TEACHER);
                    } else if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)) {
                        fragment = new DashboardFragment(this, LocalConstants.PROFILE_TYPE.STUDENT);
                    } else if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.PARENT)) {
                        fragment = new DashboardFragment(this, LocalConstants.PROFILE_TYPE.PARENT);
                    } else if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.OWNER)) {
                        fragment = new DashboardFragment(this, LocalConstants.PROFILE_TYPE.OWNER);
                    }
                }
                break;
            case R.id.nav_profile:
                fragment = new ProfileFragment(this);
                break;
            case R.id.nav_contactUs:
                fragment = new ContactUsFragment(this);
                break;
            case R.id.nav_language:
                Toast.makeText(this, "Working", Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_privacy:
                //findViewById(R.id.progress).setVisibility(View.VISIBLE);
                /*db.collection("utilData").document("appData").get().addOnSuccessListener(documentSnapshot -> {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(documentSnapshot.getString("privacypage"))));
                    findViewById(R.id.progress).setVisibility(View.GONE);
                });
                icCheckedItem = false;*/
                break;
            case R.id.nav_useClassroom:
                fragment = new HowToUseFragment();
                break;
            case R.id.nav_rate:
                icCheckedItem = false;
                break;
            case R.id.nav_share:
                icCheckedItem = false;
                break;
            case R.id.nav_logout:
                logout();
                icCheckedItem = false;
                break;
            default:
                break;
        }

        if (fragment != null)
            getSupportFragmentManager().beginTransaction().setCustomAnimations(
                    R.anim.slide_in,  // enter
                    R.anim.fade_out,  // exit
                    R.anim.fade_in,   // popEnter
                    R.anim.slide_out  // popExit
            ).replace(R.id.fragmentFrame, fragment).commit();

        drawawerMain.closeDrawer(GravityCompat.START);

        if (icCheckedItem)
            navigationMainPage.setCheckedItem(itemId);

    }

    private void logout() {
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("USER_PREF",
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        GlobalVariables.isStudent = null;
        GlobalVariables.name = "";
        GlobalVariables.uid = "";
        editor.putString("type", "none");
        editor.clear();
        editor.apply();
        FirebaseAuth.getInstance().signOut();
        startActivity(new Intent(this, SpalashActivity.class));
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (actionBarDrawerToggle.onOptionsItemSelected(item)) return true;
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        /*if (tinyDB.getString(LocalConstants.INSTITUTE_TYPE.TYPE).equalsIgnoreCase(LocalConstants.INSTITUTE_TYPE.TUTION)) {
            TO_WHICH_FRAG = TO_CLASSROOM;
        } */
        switch (TO_WHICH_FRAG) {
            case TO_DASHBOARD:
                if (tinyDB.getString(LocalConstants.INSTITUTE_TYPE.TYPE).equalsIgnoreCase(LocalConstants.INSTITUTE_TYPE.TUTION)) {
                    if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER))
                        getSupportFragmentManager().beginTransaction().setCustomAnimations(
                                R.anim.slide_in,  // enter
                                R.anim.fade_out,  // exit
                                R.anim.fade_in,   // popEnter
                                R.anim.slide_out  // popExit
                        ).replace(R.id.fragmentFrame, new ClassroomTeacherFragment(this)).commit();
                    else
                        getSupportFragmentManager().beginTransaction().setCustomAnimations(
                                R.anim.slide_in,  // enter
                                R.anim.fade_out,  // exit
                                R.anim.fade_in,   // popEnter
                                R.anim.slide_out  // popExit
                        ).replace(R.id.fragmentFrame, new ClassroomStudentFragment(this)).commit();
                    navigationMainPage.setCheckedItem(R.id.nav_dashboard);
                } else if (tinyDB.getString(LocalConstants.INSTITUTE_TYPE.TYPE).equalsIgnoreCase(LocalConstants.INSTITUTE_TYPE.SCHOOL)) {
                    getSupportFragmentManager().beginTransaction().setCustomAnimations(
                            R.anim.slide_in,  // enter
                            R.anim.fade_out,  // exit
                            R.anim.fade_in,   // popEnter
                            R.anim.slide_out  // popExit
                    ).replace(R.id.fragmentFrame, new DashboardFragment(this, tinyDB.getString(PROFILE))).commit();
                }
                break;
            case TO_ASSIGNMENT_FRAG:
                getSupportFragmentManager().beginTransaction().setCustomAnimations(
                        R.anim.slide_in,  // enter
                        R.anim.fade_out,  // exit
                        R.anim.fade_in,   // popEnter
                        R.anim.slide_out  // popExit
                ).replace(R.id.fragmentFrame, new HomeClassroomFragment(this, tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID), TO_ASSIGNMENT_FRAG)).commit();
                break;
            case TO_TEST_FRAG:
                HomeClassroomFragment.toWhichFrag = TO_WHICH_FRAG;
                getSupportFragmentManager().beginTransaction().setCustomAnimations(
                        R.anim.slide_in,  // enter
                        R.anim.fade_out,  // exit
                        R.anim.fade_in,   // popEnter
                        R.anim.slide_out  // popExit
                ).replace(R.id.fragmentFrame, new HomeClassroomFragment(this, HomeClassroomFragment.classId, HomeClassroomFragment.from)).commit();
                break;
            case TO_CLASSROOM:
                if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER))
                    getSupportFragmentManager().beginTransaction().setCustomAnimations(
                            R.anim.slide_in,  // enter
                            R.anim.fade_out,  // exit
                            R.anim.fade_in,   // popEnter
                            R.anim.slide_out  // popExit
                    ).replace(R.id.fragmentFrame, new ClassroomTeacherFragment(this)).commit();
                else
                    getSupportFragmentManager().beginTransaction().setCustomAnimations(
                            R.anim.slide_in,  // enter
                            R.anim.fade_out,  // exit
                            R.anim.fade_in,   // popEnter
                            R.anim.slide_out  // popExit
                    ).replace(R.id.fragmentFrame, new ClassroomStudentFragment(this)).commit();
                navigationMainPage.setCheckedItem(R.id.nav_dashboard);
                break;
            case TO_HOME_CLASSROOM:
                HomeClassroomFragment.toWhichFrag = TO_WHICH_FRAG;
                getSupportFragmentManager().beginTransaction().setCustomAnimations(
                        R.anim.slide_in,  // enter
                        R.anim.fade_out,  // exit
                        R.anim.fade_in,   // popEnter
                        R.anim.slide_out  // popExit
                ).replace(R.id.fragmentFrame, new HomeClassroomFragment(this, HomeClassroomFragment.classId, HomeClassroomFragment.from)).commit();
                break;
            case TO_FEEDBACK_FORM:
                if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER))
                    getSupportFragmentManager().beginTransaction().setCustomAnimations(
                            R.anim.slide_in,  // enter
                            R.anim.fade_out,  // exit
                            R.anim.fade_in,   // popEnter
                            R.anim.slide_out  // popExit
                    ).replace(R.id.fragmentFrame, new OnlineStudentFeedbackFragment(this)).commit();
            case TO_BACK_ENROLL:
                ((ClassroomStudentFragment) frag).backEnroll();
                break;
            case EXIT:
            case FROM_QUESTION_ADD_FRAGMENT:
                getSupportFragmentManager().beginTransaction().setCustomAnimations(
                        R.anim.slide_in,  // enter
                        R.anim.fade_out,  // exit
                        R.anim.fade_in,   // popEnter
                        R.anim.slide_out  // popExit
                ).replace(R.id.fragmentFrame, new HomeClassroomFragment(this, tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID), LocalConstants.FRAGMENT_TABS.TEST)).commit();
                break;

            case FROM_QUESTION_LIST_FRAGMENT:
                getSupportFragmentManager().beginTransaction().setCustomAnimations(
                        R.anim.slide_in,  // enter
                        R.anim.fade_out,  // exit
                        R.anim.fade_in,   // popEnter
                        R.anim.slide_out  // popExit
                ).replace(R.id.fragmentFrame, new ExamQuestionFragment(mContext)).commit();
                break;
            case TO_FEES_SUMMARY:
                getSupportFragmentManager().beginTransaction().setCustomAnimations(
                        R.anim.slide_in,  // enter
                        R.anim.fade_out,  // exit
                        R.anim.fade_in,   // popEnter
                        R.anim.slide_out  // popExit
                ).replace(R.id.fragmentFrame, new FeeSchoolFragment(mContext, LocalConstants.FEE_VIEWS.ADMIN_SCREEN_1)).commit();
                break;

            case FROM_TeachersFeeStudentView:
                getSupportFragmentManager().beginTransaction().setCustomAnimations(
                        R.anim.slide_in,  // enter
                        R.anim.fade_out,  // exit
                        R.anim.fade_in,   // popEnter
                        R.anim.slide_out  // popExit
                ).replace(R.id.fragmentFrame, new TeachersFeeStudentView(mContext)).commit();
                break;
            default:
                super.onBackPressed();
        }
    }

    HubConnection mHubConnection;
    String mConnectionId;

    public void getmHubConnection() {

        if (mHubConnection != null) {
            mHubConnection.setKeepAliveInterval(2000);
        }


        if (mHubConnection == null) {
            mHubConnection = HubConnectionBuilder.create(SignalR_BASE_URL)
                    .build();
            mHubConnection.start().blockingAwait();
        }
        if (mHubConnection.getConnectionState() == HubConnectionState.DISCONNECTED) {
            mHubConnection = HubConnectionBuilder.create(SignalR_BASE_URL)
                    .build();
            mHubConnection.start().blockingAwait();

        }
        recieveMsg();
    }

    private void recieveMsg() {
        try {
            mHubConnection.on("ReceiveMessage", (user, message) ->
            {
                mConnectionId = message;
                Log.e(mTAG, "SignalR | " + message);

            }, String.class, String.class);
        } catch (Exception ex) {
            Log.e(mTAG, ex.getMessage());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //Toast.makeText(this, "Get something", Toast.LENGTH_SHORT).show();
        if(tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.OWNER)){
            return;
        }
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (scanningResult != null) {
            if (scanningResult.getContents() != null) {

                mConnectionId = UUID.randomUUID().toString();
                getmHubConnection();
                //mHubConnection.start().blockingAwait();
                String studentId, classId;
                studentId = tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID);
                classId = tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID);

                mHubConnection.send("LiveClass", studentId, classId, "Mobile",tinyDB.getString("mClassType"));

                /*WebView webView = new WebView(this);
                webView.getSettings().setJavaScriptEnabled(true);
                webView.getSettings().setDomStorageEnabled(true);
                db.collection("utilData").document("appData").get().addOnSuccessListener(documentSnapshot -> {
                    webView.loadUrl(documentSnapshot.getString("webloginsite") + scanningResult.getContents().toString().trim() + "/" + GlobalVariables.uid + "/" + GlobalVariables.isStudent);
                    Snackbar.make(findViewById(R.id.fragmentFrame), "Wait", 1000).show();
                });*/
            }
        } else {
            //Snackbar.make(findViewById(R.id.fragmentFrame), "Failed", 1000).show();
        }
    }

    public static Map<Integer, String> ids = new HashMap<>();

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (ids.get(v.getId()).split(";")[0].equals("cls")) {
            menu.add(0, v.getId(), 1, "Edit");
        }
        menu.add(0, v.getId(), 0, "Delete");
    }

    /*@Override
    public boolean onContextItemSelected(MenuItem item) {
        try {
            StorageReference refStorage = FirebaseStorage.getInstance().getReferenceFromUrl(LocalConstants.STORAGE_REF);
            String data = ids.get(item.getItemId()).split(";")[1];
            switch (ids.get(item.getItemId()).split(";")[0]) {
                case "notice":
                    for (Map<String, Object> objectMap : HomeClassroomFragment.model.getNotice()) {
                        if (objectMap.get("attachment").toString().equals(data)) {
                            HomeClassroomFragment.model.getNotice().remove(objectMap);
                            break;
                        }
                    }
                    db.collection("classroom").document(HomeClassroomFragment.classId).update("notice", HomeClassroomFragment.model.getNotice()).addOnSuccessListener(unused -> doneTask(0));
                    break;
                case "assignment":
                    for (Map<String, Object> objectMap : HomeClassroomFragment.model.getAssignments())
                        if (objectMap.get("resultId").toString().equals(data)) {
                            HomeClassroomFragment.model.getAssignments().remove(objectMap);
                            break;
                        }
                    db.collection("classroom").document(HomeClassroomFragment.classId).update("assignments", HomeClassroomFragment.model.getTest()).addOnSuccessListener(unused -> db.collection("classroom").document(HomeClassroomFragment.classId).collection("assignmentSub").document(data).delete().addOnSuccessListener(unused1 -> doneTask(1)));
                    break;
                case "test":
                    for (Map<String, Object> objectMap : HomeClassroomFragment.model.getTest())
                        if (objectMap.get("resultId").toString().equals(data)) {
                            HomeClassroomFragment.model.getTest().remove(objectMap);
                            break;
                        }
                    db.collection("classroom").document(HomeClassroomFragment.classId).update("test", HomeClassroomFragment.model.getTest()).addOnSuccessListener(unused -> db.collection("classroom").document(HomeClassroomFragment.classId).collection("testSub").document(data).delete().addOnSuccessListener(unused12 -> doneTask(2)));
                    break;
                case "sm":
                    db.collection("classroom").document(HomeClassroomFragment.classId).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            List<Map<String, Object>> dataList = (List) documentSnapshot.get("studyMaterial");
                            for (Map<String, Object> dataSingle : dataList) {
                                if (dataSingle.get("name").toString().equals(data)) {
                                    dataList.remove(dataSingle);
                                    break;
                                }
                            }
                            db.collection("classroom").document(HomeClassroomFragment.classId).update("studyMaterial", dataList).addOnSuccessListener(unused -> doneTask(3));
                            if (data.contains(".") || data.contains("/"))
                                refStorage.child(data).delete();
                        }
                    });
                    break;
                case "cls":
                    String classId = ids.get(item.getItemId()).split(";")[1];
                    if (item.getTitle().equals("Delete")) {
                        db.collection("classroom")
                                .document(classId).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                List<Map<String, Object>> currentStudents = (List) documentSnapshot.get("currentStudents");
                                List<Map<String, Object>> pendingStudents = (List) documentSnapshot.get("pendingStudents");
                                for (Map<String, Object> objectr : currentStudents) {
                                    db.collection("students").document(objectr.get("id").toString()).get().addOnSuccessListener(documentSnapshot1 -> {
                                        List<String> classes = (List) documentSnapshot1.get("classes");
                                        if (classes.contains(classId)) {
                                            classes.remove(classId);
                                            db.collection("students").document(objectr.get("id").toString()).update("classes", classes);
                                        }
                                    });
                                }
                                for (Map<String, Object> objectr : pendingStudents) {
                                    db.collection("students").document(objectr.get("id").toString()).get().addOnSuccessListener(documentSnapshot1 -> {
                                        List<String> classes = (List) documentSnapshot1.get("classes");
                                        if (classes.contains(classId + "_p")) {
                                            classes.remove(classId + "_p");
                                            db.collection("students").document(objectr.get("id").toString()).update("classes", classes);
                                        }
                                    });
                                }
                                db.collection("classroom").document(classId).delete().addOnSuccessListener(unused -> getSupportFragmentManager().beginTransaction().replace(R.id.fragmentFrame, new ClassroomTeacherFragment(MainActivity.this)).commit());
                            }
                        });
                        db.collection("attendence").document(classId).delete();
                        db.collection("fees").document(classId).delete();
                        db.collection("fees").document(classId).update(classId, FieldValue.delete());
                        db.collection("teacher").document(GlobalVariables.uid).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                List<Map<String, Object>> data = (List) documentSnapshot.get("classroom");
                                for (Map<String, Object> datum : data) {
                                    if (datum.get("id").toString().equals(classId)) {
                                        data.remove(datum);
                                        break;
                                    }
                                }
                                db.collection("teacher").document(GlobalVariables.uid).update("classroom", data);
                            }
                        });
                    } else if (item.getTitle().equals("Edit")) {
                        CreateClassroomFragment fragment = new CreateClassroomFragment(this, 314);
                        fragment.classId = classId;
                        fragment.classGet = ids.get(item.getItemId()).split(";")[2];
                        fragment.subjectGet = ids.get(item.getItemId()).split(";")[3];

                        getSupportFragmentManager().beginTransaction().replace(R.id.fragmentFrame, fragment).commit();
                    }
                    break;
                default:
                    break;
            }

        } catch (Exception e) {
            Toast.makeText(this, "Try Again!", Toast.LENGTH_SHORT).show();
        }

        return true;
    }*/


    private void doneTask(int i) {
        switch (i) {
            case 0:
                HomeClassroomFragment.toWhichFrag = 5;
                break;
            case 1:
                HomeClassroomFragment.toWhichFrag = 3;
                break;
            case 2:
                HomeClassroomFragment.toWhichFrag = 4;
                break;
            case 3:
                HomeClassroomFragment.toWhichFrag = 6;
                break;
            default:
                break;
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.fragmentFrame, new HomeClassroomFragment(this, HomeClassroomFragment.classId, HomeClassroomFragment.from)).commit();
    }

    private void firebaseRegistrationCode() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(mTAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        tinyDB.putString("FcmToken", token);
                        // Log and toast
                        String msg = getString(R.string.msg_token_fmt, token);
                        Log.e(mTAG, msg);
                        updateFcmToken(token);
                    }
                });
    }

    private void updateFcmToken(String fcmToken) {
        String userId = tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID);

        UserModel user = new UserModel();
        user.setUserId(userId);
        user.setFcmToken(fcmToken);


        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call call = apiService.FcmTokenUpdate(user);
        call.enqueue(new Callback<ApiResponseModel>() {
            @Override
            public void onResponse(Call<ApiResponseModel> call, Response<ApiResponseModel> response) {
                int statusCode = response.code();
                try {
                    Log.e(mTAG, "done");
                    Log.e(mTAG, response.body().getMessage());
                } catch (Exception ex) {
                    Log.e(mTAG, ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ApiResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
            }
        });
    }

    private void getHomeworkHeads() {
        ApiInterface apiService = ApiClient.getClient(mContext).create(ApiInterface.class);
        Call<List<ApiResponseModel>> call = apiService.getHomeworkHead();
        call.enqueue(new Callback<List<ApiResponseModel>>() {
            @Override
            public void onResponse(Call<List<ApiResponseModel>> call, Response<List<ApiResponseModel>> response) {
                //Toast.makeText()
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        tinyDB.putString(TINY_CONSTANT_HomeworkHead, response.body().get(0).getMessage());
                        tinyDB.putString(TINY_CONSTANT_HOMEWORK_DONE_STATUS, response.body().get(0).getStatus());
                    } else {
                        Log.e(mTAG, "Returned empty response");//Toast.makeText(getContext(),"Nothing returned",Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<ApiResponseModel>> call, Throwable t) {
                Log.e(mTAG, "Returned empty response" + t.getMessage());//Toast.makeText(getContext(),"Nothing returned",Toast.LENGTH_LONG).show();

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getHomeworkHeads();

        // Register mMessageReceiver to receive messages.
    }

    // handler for received Intents for the "my-event" event
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Extract data included in the Intent
            Gson gson = new Gson();
            TimeTableModel ob = gson.fromJson(intent.getStringExtra("MyGsonObj"), TimeTableModel.class);
            if (ob != null) {
                getViewValues(ob);
            }
        }
    };

   /* public void createNotification() {
        Intent myIntent = new Intent(getApplicationContext(), GeneralService.class);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        PendingIntent pendingIntent = PendingIntent.getService(this, 0, myIntent, 0);
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.AM_PM, Calendar.AM);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), 1000 * 60 * 60 * 24, pendingIntent);
    }

    private static final int PERMISSION_REQUEST_CODE = 200;*/


 /*   private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_CALENDAR);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), READ_CALENDAR);

        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
    }


    private void requestPermissions() {
        String[] perms = {"android.permission.FINE_LOCATION", "android.permission.CAMERA"};

        ActivityCompat.requestPermissions(this, new String[]{WRITE_CALENDAR, READ_CALENDAR}, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(permsRequestCode, permissions, grantResults);
        switch (permsRequestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    boolean permission_writecal = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean permission_readcal = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    if (permission_readcal && permission_writecal) {
                        Toast.makeText(mContext, "Permission granted", Toast.LENGTH_SHORT).show();

                        // TODO
                        //addEventToCalendar
                        *//*addEventToCalendar("This is test titie", "test desc",
                                "location",
                                2021, mCalMonth, mCalDay, 2021,
                                mCalMonth, mCalDay, mCalHourStart, mCalMinStart, mCalHourEnd, mCalMinEnd,
                                false, "EventFrequency");*//*

                    } else {
                        requestPermissions();
                    }
                }
                break;
        }
    }*/


   /* boolean permissions = false;
    final int callbackId = 42;*/

    /*private void addEventToCalendar(String EventTitle, String EventDescription, String EventLocation, int EventStartDateYear, int EventStartDateMonth, int EventStartDateDay, int EventEndDateYear, int EventEndDateMonth, int EventEndDateDay, int BeginHour, int BeginMin, int EndHour, int EndMin, Boolean AllDay, String EventFrequency,int TimeTableId) {

        Toast.makeText(this, "Adding Event To Your Calendar...", Toast.LENGTH_SHORT).show();
        ContentValues event = new ContentValues();
        Calendar startcalendar = Calendar.getInstance();
        Calendar endcalendar = Calendar.getInstance();
        startcalendar.set(EventStartDateYear, EventStartDateMonth, EventStartDateDay, BeginHour, BeginMin);
        endcalendar.set(EventStartDateYear, EventEndDateMonth, EventEndDateDay, EndHour, EndMin);
        event.put(CalendarContract.Events.CALENDAR_ID, TimeTableId);
        event.put(CalendarContract.Events.TITLE, EventTitle);
        event.put(CalendarContract.Events.DESCRIPTION, EventDescription);
        event.put(CalendarContract.Events.EVENT_LOCATION, EventLocation);
        event.put(CalendarContract.Events.DTSTART, startcalendar.getTimeInMillis());
        event.put(CalendarContract.Events.DTEND, endcalendar.getTimeInMillis());
        event.put(CalendarContract.Events.ALL_DAY, AllDay);
        event.put(CalendarContract.Events.HAS_ALARM, true);
        // event.put(CalendarContract.Events.RRULE, "WEEKLY");
        event.put(CalendarContract.Events.EVENT_TIMEZONE, "GMT+05:30");

        try {
            Uri url = getContentResolver().insert(CalendarContract.Events.CONTENT_URI, event);

            long eventId = Long.parseLong(url.getLastPathSegment());

            ContentValues reminder = new ContentValues();
            reminder.put(CalendarContract.Reminders.EVENT_ID, eventId);
            reminder.put(CalendarContract.Reminders.MINUTES, 10);
            reminder.put(CalendarContract.Reminders.METHOD, CalendarContract.Reminders.METHOD_ALERT);
            getContentResolver().insert(CalendarContract.Reminders.CONTENT_URI, reminder);

            //Toast.makeText(this, "Event Added To Your Calendar!", Toast.LENGTH_SHORT).show();
        } catch (Exception ex) {
            Log.e(mTAG, ex.getMessage());
        }
    }
*/

    /*private void initViews() {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        tinyDB.putString("frequency", "Repeat One-Time");
        notificationModelList.add(new AlarmNotificationModel(tinyDB.getString("reminder")));
    }

    private void setAlarms() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(alarmYear, alarmMonth, alarmDay);

        calendar.set(Calendar.HOUR_OF_DAY, alarmHour);
        calendar.set(Calendar.MINUTE, alarmMinute);
        calendar.set(Calendar.SECOND, 0);

        for (AlarmNotificationModel  notification : notificationModelList) {
            Calendar aCal = (Calendar) calendar.clone();
            String notificationPreference = notification.getTime();

            if (notificationPreference.equals("10 minutes before")) {
                aCal.add(Calendar.MINUTE, -10);
            } else if (notificationPreference.equals("1 hour before")) {
                aCal.add(Calendar.HOUR_OF_DAY, -1);
            } else if (notificationPreference.equals("1 day before")) {
                aCal.add(Calendar.DAY_OF_MONTH, -1);
            } else {
                Log.i(mTAG, "setAlarms: ");
            }

            setAlarm(notification, aCal.getTimeInMillis());
        }
    }

    private List<AlarmEventModel> alarmEventModelList=new ArrayList<>();
    private List<AlarmNotificationModel> notificationModelList=new ArrayList<>();

    private void setAlarm(AlarmNotificationModel notification, long triggerAtMillis) {

        *//*info.setText("\n\n***\n"
                + "Alarm is set@ " + target.getTime() + "\n"
                + "***\n");*//*
     *//*Intent intent = new Intent(getBaseContext(), AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getBaseContext(), 1001, intent, 0);
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, target.getTimeInMillis(), pendingIntent);*//*

        for (AlarmEventModel event : alarmEventModelList)
        {
            Intent intent = new Intent(getApplicationContext(), ServiceAutoLauncher.class);
            intent.putExtra("eventTitle", event.getTitle());
            intent.putExtra("eventNote", event.getNote());
            intent.putExtra("eventColor", event.getColor());
            intent.putExtra("eventTimeStamp", event.getDate() + ", " + event.getTime());
            intent.putExtra("interval", getInterval());
            intent.putExtra("notificationId", notification.getChannelId());
            intent.putExtra("soundName", tinyDB.getString("ringtone"));

            Log.d(mTAG, "setAlarm: " + notification.getId());

            PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), notification.getId(), intent, 0);
            AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, triggerAtMillis, pendingIntent);
        }
    }

    private String getInterval() {
        String interval = getString(R.string.one_time);
        String repeatingPeriod = "daily";
        if (repeatingPeriod.equals(getString(R.string.daily))) {
            interval = getString(R.string.daily);
        } else if (repeatingPeriod.equals(getString(R.string.weekly))) {
            interval = getString(R.string.weekly);
        } else if (repeatingPeriod.equals(getString(R.string.monthly))) {
            interval = getString(R.string.monthly);
        } else if (repeatingPeriod.equals(getString(R.string.yearly))) {
            interval = getString(R.string.yearly);
        }
        return interval;
    }*/

    /*private void deleteEvent(int eventId)
    {
        Uri CALENDAR_URI = Uri.parse("content://com.android.calendar/events");
        Uri uri = ContentUris.withAppendedId(CALENDAR_URI, eventId);
        getContentResolver().delete(uri, null, null);
    }

    private  void readCalendar(){
        Cursor cursor=getContentResolver().query(Uri.parse("content://com.android.calendar/calendars"), new String[]{"_id", "displayname"}, null, null, null);

        cursor.moveToFirst();
        // Get calendars name
        String calendarNames[] = new String[cursor.getCount()];
        // Get calendars id
        int[] calendarId = new int[cursor.getCount()];
        for (int i = 0; i < calendarNames.length; i++)
        {
            calendarId[i] = cursor.getInt(0);
            calendarNames[i] = cursor.getString(1);
            cursor.moveToNext();
        }
        cursor.close();
    }*/


   /* private void addEventInfo(TimeTableModel timeTableModel){
        AlarmEventModel event1=new AlarmEventModel();
        event1.setTitle(timeTableModel.getClassName()+" - "+timeTableModel.getSubjectName());
        event1.setAllDay(false);
        event1.setDate(timeTableModel.getScheduleDate());
        event1.setMonth(timeTableModel.getMonth());
        event1.setYear(timeTableModel.getYear());
        event1.setTime(timeTableModel.getTimeFrom());
        event1.setDuration(timeTableModel.getDuration());
        event1.setNotify(true);
        event1.setRecurring(false);
        event1.setRecurringPeriod("");
        event1.setNote(timeTableModel.getClassName());
        *//*if (notColor == 0) {
            notColor = getResources().getInteger(R.color.red);
            event.setColor(notColor);
        } else {
            event.setColor(notColor);
        }*//*
        event1.setLocation("Attend from Home / Anywhere");
        event1.setPhoneNumber("");
        event1.setMail("");
        alarmEventModelList.add(event1);
    }*/


    private void setAlarms() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(alarmYear, alarmMonth, alarmDay);

        calendar.set(Calendar.HOUR_OF_DAY, alarmHour);
        calendar.set(Calendar.MINUTE, alarmMinute);
        calendar.set(Calendar.SECOND, 0);

        for (Notification notification : notifications) {
            Calendar aCal = (Calendar) calendar.clone();
            String notificationPreference = notification.getTime();

            if (notificationPreference.equals(getString(R.string._10_minutes_before))) {
                aCal.add(Calendar.MINUTE, -10);
            } else if (notificationPreference.equals(getString(R.string._1_hour_before))) {
                aCal.add(Calendar.HOUR_OF_DAY, -1);
            } else if (notificationPreference.equals(getString(R.string._1_day_before))) {
                aCal.add(Calendar.DAY_OF_MONTH, -1);
            } else {
                Log.i(mTAG, "setAlarms: ");
            }

            setAlarm(notification, aCal.getTimeInMillis());
        }
    }

    private String getString(String key) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        return sharedPreferences.getString(key, "Indigo");
    }

    private void setAlarm(Notification notification, long triggerAtMillis) {
        Intent intent = new Intent(getApplicationContext(), ServiceAutoLauncher.class);
        intent.putExtra("eventTitle", event.getTitle());
        intent.putExtra("eventNote", event.getNote());
        intent.putExtra("eventColor", event.getColor());
        intent.putExtra("eventTimeStamp", event.getDate() + ", " + event.getTime());
        intent.putExtra("interval", getInterval());
        intent.putExtra("notificationId", notification.getChannelId());
        intent.putExtra("soundName", getString("ringtone"));

        Log.d(mTAG, "setAlarm: " + notification.getId());

        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), notification.getId(), intent, 0);
        AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, triggerAtMillis, pendingIntent);
    }

    private String getInterval() {
        String interval = getString(R.string.one_time);
        String repeatingPeriod = "Repeat One-Time";
        if (repeatingPeriod.equals(getString(R.string.daily))) {
            interval = getString(R.string.daily);
        } else if (repeatingPeriod.equals(getString(R.string.weekly))) {
            interval = getString(R.string.weekly);
        } else if (repeatingPeriod.equals(getString(R.string.monthly))) {
            interval = getString(R.string.monthly);
        } else if (repeatingPeriod.equals(getString(R.string.yearly))) {
            interval = getString(R.string.yearly);
        }
        return interval;
    }

    public void getViewValues(TimeTableModel timeTableModel) {
        event = new Event();
        Date aDate = null;
        try {
            aDate = UtilsAlarm.eventDateFormat.parse(timeTableModel.getScheduleDate());
        } catch (ParseException e) {
            e.printStackTrace();
            Log.e(mTAG, "An error has occurred while parsing the date string");
        }
        event.setTitle(timeTableModel.getClassName() + " - " + timeTableModel.getSubjectName());
        event.setAllDay(false);
        event.setDate(UtilsAlarm.eventDateFormat.format(aDate));
        event.setMonth(UtilsAlarm.monthFormat.format(aDate));
        event.setYear(UtilsAlarm.yearFormat.format(aDate));
        event.setTime(timeTableModel.getTimeFrom());
        event.setDuration("15");
        event.setNotify(!notifications.isEmpty());
        event.setRecurring(false);
        event.setRecurringPeriod("1");
        event.setNote("Your class for subject " + timeTableModel.getSubjectName() + " is about to start at " + timeTableModel.getTimeFrom());
        if (notColor == 0) {
            notColor = getResources().getInteger(R.color.red);
            event.setColor(notColor);
        } else {
            event.setColor(notColor);
        }
        event.setLocation("");
        event.setPhoneNumber("");
        event.setMail("");
        setAlarms();
    }

    private boolean confirmInputs() {
        if (!validateEventTitle()) {
            return false;
        }

        if (!validateNotifications()) {
            Toast.makeText(mContext, "Notifiactin not set", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean validateEventTitle() {
        String eventTitleString = "This is test notificaiton";
        if (eventTitleString.isEmpty()) {
            ;
            return false;
        } else {
            return true;
        }
    }

    private boolean validateNotifications() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(alarmYear, alarmMonth, alarmDay);

        calendar.set(Calendar.HOUR_OF_DAY, alarmHour);
        calendar.set(Calendar.MINUTE, alarmMinute);
        calendar.set(Calendar.SECOND, 0);

        for (Notification notification : notifications) {
            Calendar aCal = (Calendar) calendar.clone();
            String notificationPreference = notification.getTime();

            if (notificationPreference.equals(getString(R.string._10_minutes_before))) {
                aCal.add(Calendar.MINUTE, -10);
            } else if (notificationPreference.equals(getString(R.string._1_hour_before))) {
                aCal.add(Calendar.HOUR_OF_DAY, -1);
            } else if (notificationPreference.equals(getString(R.string._1_day_before))) {
                aCal.add(Calendar.DAY_OF_MONTH, -1);
            } else {
                Log.i(mTAG, "setAlarms: ");
            }

            if (aCal.before(Calendar.getInstance())) {
                return false;
            }
        }
        return true;
    }

  /*  private ArrayList<Notification> readNotifications(int eventId) {
            ArrayList<Notification> notifications = new ArrayList<>();
            SQLiteDatabase sqLiteDatabase = dbHelper.getReadableDatabase();
            Cursor cursor = dbHelper.readEventNotifications(sqLiteDatabase, eventId);
            while (cursor.moveToNext()) {
            Notification notification = new Notification();
            notification.setId(cursor.getInt(cursor.getColumnIndex(DBTables.NOTIFICATION_ID)));
            notification.setEventId(cursor.getInt(cursor.getColumnIndex(DBTables.NOTIFICATION_EVENT_ID)));
            notification.setTime(cursor.getString(cursor.getColumnIndex(DBTables.NOTIFICATION_TIME)));
            notification.setChannelId(cursor.getInt(cursor.getColumnIndex(DBTables.NOTIFICATION_CHANNEL_ID)));
            notifications.add(notification);
        }

        return notifications;
    }*/

    /*private int getEventId(String eventTitle, String eventDate, String eventTime) {
        SQLiteDatabase sqLiteDatabase = dbHelper.getReadableDatabase();
        Event event1 = dbHelper.readEventByTimestamp(sqLiteDatabase, eventTitle, eventDate, eventTime);
        sqLiteDatabase.close();
        return event1.getId();
    }*/
}