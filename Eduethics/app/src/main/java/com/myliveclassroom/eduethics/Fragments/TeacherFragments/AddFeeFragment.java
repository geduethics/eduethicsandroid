package com.myliveclassroom.eduethics.Fragments.TeacherFragments;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.myliveclassroom.eduethics.Activity.MainActivity;
import com.myliveclassroom.eduethics.Models.FeeModelTutor;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.myliveclassroom.eduethics.classes.TinyDB;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;


public class AddFeeFragment extends Fragment {


    Context mContext;
    View root;
    //TextView fromDate, fromMonth, fromYear, toDate, toMonth, toYear;
    TextInputLayout feeLayout;
    TextInputEditText feeText;
    TextView dateReminder1, dateReminder2;
    Calendar calendar = Calendar.getInstance();
    View selectedMonthView = null;
    int selectedMonthFrom = calendar.get(Calendar.MONTH) + 1;
    int selectedMonthTo = calendar.get(Calendar.MONTH) + 2;
    int selectedYearTo = Calendar.getInstance().get(Calendar.YEAR);
    String selectedDate = "1";
    TextView[] dates = null;
    TinyDB tinyDB;
    String mCourseFee = "0";
    String mMonthlyFee = "0";
    Boolean isRdoMonthSelected = true;


    RadioGroup radioGroup;
    RadioButton rdoMonth, rdoCourse;
    TextView fromDate,toDate,toMonth,toYear,fromMonth,fromYear;


    public AddFeeFragment(Context context) {
        this.mContext = context;
        tinyDB = new TinyDB(mContext);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_add_fee, container, false);
        MainActivity.TO_WHICH_FRAG = LocalConstants.TO_FRAG.TO_HOME_CLASSROOM;

        fromDate = root.findViewById(R.id.fromDate);
        fromMonth=root.findViewById(R.id.fromMonth);
        fromYear=root.findViewById(R.id.fromYear);

        toDate = root.findViewById(R.id.toDate);
        toMonth = root.findViewById(R.id.toMonth);
        toYear = root.findViewById(R.id.toYear);

        dateReminder1 = root.findViewById(R.id.dateRem1);
        dateReminder2 = root.findViewById(R.id.dateRem2);

        fromDate.setOnClickListener(v -> selectDate());
        toDate.setOnClickListener(v -> selectDate());
        fromMonth.setOnClickListener(v -> selectMonth(1, calendar.get(Calendar.MONTH)));
        toMonth.setOnClickListener(v -> selectMonth(2, selectedMonthFrom));
        fromYear.setOnClickListener(v -> selectYear(1));
        toYear.setOnClickListener(v -> selectYear(2));
        fromMonth.setText(LocalConstants.MONTHS[selectedMonthFrom].substring(0, 3));
        toMonth.setText(LocalConstants.MONTHS[selectedMonthTo]);

        feeLayout = root.findViewById(R.id.tempf3);
        feeText = root.findViewById(R.id.feeText);

        rdoMonth = root.findViewById(R.id.monthlyFee);
        rdoCourse = root.findViewById(R.id.courseFee);
        radioGroup = root.findViewById(R.id.feeTypeRadioGroup);

        RadioGroup feeTypeRadioGroup = root.findViewById(R.id.feeTypeRadioGroup);

        root.findViewById(R.id.tt3).setVisibility(View.VISIBLE);
        dateReminder2.setVisibility(View.VISIBLE);
        dateReminder1.setVisibility(View.VISIBLE);
        feeLayout.setHint("Classroom Fee (per month)");
        mMonthlyFee = feeText.getText().toString();
        mCourseFee = "0";

        feeTypeRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                View radioButton = feeTypeRadioGroup.findViewById(checkedId);
                int index = feeTypeRadioGroup.indexOfChild(radioButton);
                switch (radioButton.getId()) {
                    case R.id.monthlyFee:
                        root.findViewById(R.id.tt3).setVisibility(View.VISIBLE);
                        dateReminder2.setVisibility(View.VISIBLE);
                        dateReminder1.setVisibility(View.VISIBLE);
                        feeLayout.setHint("Classroom Fee (per month)");
                        isRdoMonthSelected = true;
                        break;
                    case R.id.courseFee:
                        isRdoMonthSelected = false;
                        root.findViewById(R.id.tt3).setVisibility(View.GONE);
                        dateReminder2.setVisibility(View.GONE);
                        dateReminder1.setVisibility(View.GONE);
                        feeLayout.setHint("Course Fee");
                        break;
                }
            }
        });

        //rdoMonth.setChecked(false);

        root.findViewById(R.id.createFee).setOnClickListener(v -> createFee());

        return root;
    }

    private void createFee() {
        boolean allDone = true;

        if (isRdoMonthSelected) {
            mCourseFee = "0";
            mMonthlyFee = feeText.getText().toString();
        } else {
            mMonthlyFee = "0";
            mCourseFee = feeText.getText().toString();
        }
        root.findViewById(R.id.progress).setVisibility(View.VISIBLE);

        //After all validations
        //Save data
        FeeModelTutor feeModelTutor = new FeeModelTutor();
        feeModelTutor.setClassId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID));

        //NextDueDate-> In case of monthly set startdate+one month

        feeModelTutor.setStartDate("1-sep-2021");
        feeModelTutor.setReminderDate1(dateReminder1.getText().toString());
        feeModelTutor.setReminderDate1(dateReminder2.getText().toString());
        feeModelTutor.setMonthlyFee(mMonthlyFee);
        feeModelTutor.setCourseFee(mCourseFee);

        root.findViewById(R.id.progress).setVisibility(View.VISIBLE);

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = apiService.TutorFee_SaveClassFee(feeModelTutor);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                root.findViewById(R.id.progress).setVisibility(View.GONE);
                try {
                    if (response.body() != null) {
                        if (new Gson().fromJson(response.body().get("Status"), String.class).toString().equalsIgnoreCase("Success")) {
                            // finish();
                        } else {
                            Toast.makeText(mContext, LocalConstants.APP_MESSAGES.ERROR1, Toast.LENGTH_SHORT).show();
                        }
                        Log.e(mTAG, response.toString());
                    }

                } catch (Exception ex) {
                    Log.e(mTAG, "error =" + ex.getMessage());
                    Toast.makeText(mContext, LocalConstants.APP_MESSAGES.ERROR1, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, LocalConstants.APP_MESSAGES.ERROR1, Toast.LENGTH_SHORT).show();
            }
        });

    }


    private void selectMonth(int type, int min) {
        Dialog dialog;
        dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.dialog_select_date);

        dialog.findViewById(R.id.dateTable).setVisibility(View.GONE);

        ListView listView = dialog.findViewById(R.id.monthsList);
        listView.setVisibility(View.VISIBLE);

        List<String> lst = new ArrayList<>();
        if (type == 1) {
            lst.add(LocalConstants.MONTHS[min]);
            lst.add(LocalConstants.MONTHS[min + 1]);

        } else if (Integer.parseInt(toYear.getText().toString()) == calendar.get(Calendar.YEAR) + 1) {
            lst.addAll(Arrays.asList(LocalConstants.MONTHS).subList(0, 11));
        } else
            lst.addAll(Arrays.asList(LocalConstants.MONTHS).subList(selectedMonthFrom + 1, selectedMonthFrom + 1 > 10 ? LocalConstants.MONTHS.length - 1 : 12));


        listView.setAdapter(new ArrayAdapter<>(mContext, android.R.layout.simple_list_item_1, lst));

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position,
                                    long arg3) {
                if (selectedMonthView != null)
                    selectedMonthView.setBackground(null);
                v.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.selected_month, null));
                selectedMonthView = v;
                if (type == 1) {
                    selectedMonthFrom = position + min;
                } else
                    selectedMonthTo = position + (Integer.parseInt(toYear.getText().toString()) == calendar.get(Calendar.YEAR) + 1 ? 0 : selectedMonthFrom + 1);

            }
        });

        dialog.findViewById(R.id.select).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type == 2) {
                    toMonth.setText(LocalConstants.MONTHS[selectedMonthTo].substring(0, 3));
                } else {
                    fromMonth.setText(LocalConstants.MONTHS[selectedMonthFrom].substring(0, 3));
                    if (selectedMonthTo <= selectedMonthFrom && Integer.parseInt(toYear.getText().toString()) == calendar.get(Calendar.YEAR)) {
                        selectedMonthTo = selectedMonthFrom + 1;
                        toMonth.setText(LocalConstants.MONTHS[selectedMonthTo].substring(0, 3));
                    }
                }
                nextRem();
                dialog.dismiss();
            }
        });
        dialog.findViewById(R.id.cancel).setOnClickListener(v -> dialog.dismiss());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void nextRem() {
        dateReminder1.setText(selectedDate + " " + LocalConstants.MONTHS[selectedMonthFrom + 1] + " " + fromYear.getText().toString());
        dateReminder2.setText(selectedDate + " " + LocalConstants.MONTHS[selectedMonthFrom + 2] + " " + fromYear.getText().toString());
    }

    private void selectYear(int type) {
        Dialog dialog;
        dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.dialog_select_date);

        dialog.findViewById(R.id.dateTable).setVisibility(View.GONE);

        ListView listView = dialog.findViewById(R.id.monthsList);
        listView.setVisibility(View.VISIBLE);

        List<String> lst = new ArrayList<>();
        lst.add(calendar.get(Calendar.YEAR) + "");
        if (type == 2)
            lst.add(calendar.get(Calendar.YEAR) + 1 + "");

        listView.setAdapter(new ArrayAdapter<>(mContext, android.R.layout.simple_list_item_1, lst));

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position,
                                    long arg3) {
                if (selectedMonthView != null)
                    selectedMonthView.setBackground(null);
                v.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.selected_month, null));
                selectedMonthView = v;
                selectedYearTo = position;
            }
        });

        dialog.findViewById(R.id.select).setOnClickListener(v -> {
            if (type == 2) {
                if (selectedYearTo == 0 && selectedMonthTo < calendar.get(Calendar.MONTH)) {
                    Toast.makeText(mContext, "YES", Toast.LENGTH_SHORT).show();
                    toMonth.setText(LocalConstants.MONTHS[selectedMonthFrom + 1]);
                } else {
                    Toast.makeText(mContext, selectedMonthTo + " " + calendar.get(Calendar.MONTH), Toast.LENGTH_SHORT).show();

                }
                toYear.setText((calendar.get(Calendar.YEAR) + selectedYearTo) + "");
            } else fromYear.setText((calendar.get(Calendar.YEAR)) + "");
            nextRem();
            dialog.dismiss();
        });
        dialog.findViewById(R.id.cancel).setOnClickListener(v -> dialog.dismiss());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void selectDate() {
        dates = new TextView[28];
        Dialog dialog;
        dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.dialog_select_date);

        TableLayout tableLayout = dialog.findViewById(R.id.dateTable);

        int c = 1;
        for (int k = 1; k <= 4; k++) {
            TableRow tableRowHead = new TableRow(mContext);
            tableRowHead.setLayoutParams(new TableLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            for (int i = 0; i < 7; i++) {
                TextView textView = new TextView(mContext);
                TableRow.LayoutParams params = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.weight = 1;
                params.setMargins(8, 8, 8, 8);
                textView.setLayoutParams(params);
                textView.setText((c++) + "");
                textView.setTextColor(getResources().getColor(R.color.darkgrey));
                textView.setBackgroundColor(Color.TRANSPARENT);
                textView.setTextSize(18);
                textView.setOnClickListener(v -> {
                    for (TextView date : dates)
                        date.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.circler_unselected_date, null));
                    v.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.circle_selected_date, null));
                    selectedDate = ((TextView) v).getText().toString();
                });
                if (selectedDate.equals(textView.getText().toString()))
                    textView.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.circle_selected_date, null));
                else
                    textView.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.circler_unselected_date, null));
                textView.setPadding(6, 6, 6, 6);
                textView.setGravity(Gravity.CENTER);
                dates[c - 2] = textView;
                tableRowHead.addView(textView);
            }
            tableLayout.addView(tableRowHead);
        }


        dialog.findViewById(R.id.select).setOnClickListener(v -> {
            fromDate.setText(selectedDate);
            toDate.setText(selectedDate);
            dates = null;
            nextRem();
            dialog.dismiss();

        });
        dialog.findViewById(R.id.cancel).setOnClickListener(v -> {
            dialog.dismiss();
            dates = null;
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

}