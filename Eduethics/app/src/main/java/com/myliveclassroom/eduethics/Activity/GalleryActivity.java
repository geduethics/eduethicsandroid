package com.myliveclassroom.eduethics.Activity;

import android.content.ClipData;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.FileUtils;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.myliveclassroom.eduethics.Adapter.GalleryAdapter;
import com.myliveclassroom.eduethics.Models.ApiResponseModel;
import com.myliveclassroom.eduethics.Models.GalleryModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.TinyDB;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.ATTACHMENT_ARRAY;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.PICK_GALLERY_REQUEST_CODE;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;


public class GalleryActivity extends BaseActivity {

    @BindView(R.id.txtPageTitle)
    TextView txtPageTitle;

    @BindView(R.id.imgBack)
    ImageView imgBack;

    @BindView(R.id.imgbtnAdd)
    ImageView imgbtnAdd;

    @BindView(R.id.btnAdd)
    TextView btnAdd;

    RecyclerView recyclerGallery;
    List<GalleryModel> galleryModelList;
    GalleryAdapter galleryAdapter;

    LinearLayout layoutCircularAdd;
    TextView txtBtnCancel, txtBtnSubmit, txtAttachment;
    EditText editTextHead, editTextDetail, editTextDate;
    ImageView imgAttachFile;

    private static final int REQUEST_STORAGE_PERMISSION = 100;
    private static final int REQUEST_PICK_PHOTO = 101;

    boolean isAddLayoutOpen = false;
    ProgressBar progressBar;

    //create a single thread pool to our image compression class.
    private ExecutorService mExecutorService = Executors.newFixedThreadPool(1);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.MyThemeNoBar);
        setContentView(R.layout.activity_gallery);

        mContext = this;
        tinyDB = new TinyDB(this);
        ButterKnife.bind(this);
        init();
    }

    private void Back() {
        if (!isAddLayoutOpen) {
            super.onBackPressed();
        }
        recyclerGallery.setVisibility(View.VISIBLE);
        layoutCircularAdd.setVisibility(View.GONE);
        isAddLayoutOpen = false;
    }

    @Override
    public void onBackPressed() {
        if (!isAddLayoutOpen) {
            super.onBackPressed();
        }
        recyclerGallery.setVisibility(View.VISIBLE);
        layoutCircularAdd.setVisibility(View.GONE);
        isAddLayoutOpen = false;

    }


    private void init() {
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        layoutCircularAdd = findViewById(R.id.layoutCircularAdd);
        txtBtnCancel = findViewById(R.id.txtBtnCancel);
        txtBtnSubmit = findViewById(R.id.txtBtnSubmit);
        txtAttachment = findViewById(R.id.txtAttachment);
        mtxtAttachment = txtAttachment;

        imgbtnAdd.setVisibility(View.INVISIBLE);
        imgBack.setVisibility(View.VISIBLE);
        btnAdd.setVisibility(View.GONE);
        btnAdd.setText("");

        editTextHead = findViewById(R.id.editTextHead);
        editTextDetail = findViewById(R.id.editTextDetail);
        editTextDate = findViewById(R.id.editTextDate);

        if (tinyDB.getString("IsAdmin").equalsIgnoreCase("Y")) {
            imgbtnAdd.setVisibility(View.VISIBLE);
            btnAdd.setVisibility(View.VISIBLE);
            btnAdd.setText("Add");
        }
        layoutCircularAdd.setVisibility(View.GONE);


        txtPageTitle.setText("Gallery");
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Back();
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                layoutCircularAdd.setVisibility(View.VISIBLE);
                recyclerGallery.setVisibility(View.GONE);
                mGalleryId = "0";
                editTextHead.setText("");
                isAddLayoutOpen = true;
            }
        });

        imgbtnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layoutCircularAdd.setVisibility(View.VISIBLE);
                recyclerGallery.setVisibility(View.GONE);
                mGalleryId = "0";
                editTextHead.setText("");
                isAddLayoutOpen = true;
            }
        });

        txtBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutCircularAdd.setVisibility(View.GONE);
                recyclerGallery.setVisibility(View.VISIBLE);
                mGalleryId = "0";
                editTextHead.setText("");
                isAddLayoutOpen = false;
            }
        });

        txtBtnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GallerySave();
            }
        });

        imgAttachFile = findViewById(R.id.imgAttachFile);
        imgAttachFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(GalleryActivity.this);*/
                imagesListArray = new ArrayList<>();
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_GALLERY_REQUEST_CODE);
            }
        });

        initRecyclerView();
    }

    private void initRecyclerView() {
        galleryModelList = new ArrayList<>();
        recyclerGallery = findViewById(R.id.recyclerGallery);
        recyclerGallery.setLayoutManager(new LinearLayoutManager(mContext));
        //listGallery();
    }

    private void listGallery() {
        showProgress();
        GalleryModel galleryModel = new GalleryModel();
        galleryModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        galleryModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<List<GalleryModel>> call = apiService.GalleryList(galleryModel);
        call.enqueue(new Callback<List<GalleryModel>>() {
            @Override
            public void onResponse(Call<List<GalleryModel>> call, Response<List<GalleryModel>> response) {
                hideProgress();
                try {
                    galleryModelList = response.body();
                    galleryAdapter = new GalleryAdapter(mContext, galleryModelList);
                    recyclerGallery.setAdapter(galleryAdapter);

                } catch (Exception ex) {
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<List<GalleryModel>> call, Throwable t) {
                Log.e(mTAG, "error =" + t.toString());
                hideProgress();
            }
        });
    }

    private void GallerySave() {

        disableSubmitBtn();
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("GalleryId", mGalleryId);
            jsonObject.put("CreatedBy", tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
            jsonObject.put("SchoolId", tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
            jsonObject.put("SessionId", tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));
            jsonObject.put("GalleryTitle", editTextHead.getText().toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }


        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);


        String ClaimImages = tinyDB.getString(ATTACHMENT_ARRAY);
        //String[] claimImagesArry = ClaimImages.split(",");
        MultipartBody.Part[] body = new MultipartBody.Part[imagesListArray.size()];

        int j = 0;
        for (int i = 0; i < imagesListArray.size(); i++) {
            String path = imagesListArray.get(i).replace("file:", "");
            if (!path.equalsIgnoreCase("")) {
                File file1 = new File(path);
                RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file1);
                try {
                    body[j] = MultipartBody.Part.createFormData("uploaded_file_" + String.valueOf(j), file1.getName(), requestFile);
                    j++;
                } catch (Exception ex) {
                    Log.e(mTAG, "" + ex.getMessage());
                }
            }
        }

        RequestBody modelParm = RequestBody.create(MediaType.parse("text/plain"), jsonObject.toString());

        Call<Object> call = apiService.GallerySave(body, modelParm);

        // Call<ApiResponse> call = apiService.PostSave(Desc,CreatedBy);
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                //int statusCode = response.code();
                enableSubmitBtn();
                try {
                    Log.e("res", response.body().toString());
                    tinyDB.putString(ATTACHMENT_ARRAY, "");
                    txtAttachment.setText("No attachment");
                    layoutCircularAdd.setVisibility(View.GONE);
                    Toast.makeText(mContext, "Done!", Toast.LENGTH_SHORT).show();
                    listGallery();
                } catch (Exception ex) {
                    Log.e(mTAG, "error =" + ex.getMessage());
                    Toast.makeText(mContext, "Something went wrong2!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                // myProgressDialog.hideProgress();
                Toast.makeText(mContext, "Something went wrong1!", Toast.LENGTH_SHORT).show();
                enableSubmitBtn();
            }
        });
    }

    TextView mtxtAttachment;
    List<String> imagesListArray = new ArrayList<>();
    String imageEncoded;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        //Bitmap photo = comfun.getBitMapFromPath(mContext, mCurrentPhotoPath);

        try {
            // When an Image is picked
            if (requestCode == PICK_GALLERY_REQUEST_CODE && resultCode == RESULT_OK
                    && null != data) {
                // Get the Image from data


                if (data.getClipData() != null) {
                    ClipData mClipData = data.getClipData();
                    ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
                    for (int i = 0; i < mClipData.getItemCount(); i++) {

                        ClipData.Item item = mClipData.getItemAt(i);
                        Uri uri = item.getUri();
                        String path = saveURITemp(uri);
                    }
                    Log.v("LOG_TAG", "Selected Images" + mArrayUri.size());
                } else {
                    if (data.getData() != null) {
                        Uri mImageUri = data.getData();
                        String path = saveURITemp(mImageUri);
                    }
                }
            } else {
                Toast.makeText(this, "You haven't picked Image",
                        Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();
        }

        /*if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();


                File finalFile = new File(resultUri.getPath());

                String fileAttachmentArray = tinyDB.getString(ATTACHMENT_ARRAY);
                fileAttachmentArray = (fileAttachmentArray.equalsIgnoreCase("") ? "" : fileAttachmentArray + ",") + finalFile.getAbsolutePath();
                tinyDB.putString(ATTACHMENT_ARRAY, fileAttachmentArray);
                 mtxtAttachment.setText(fileAttachmentArray.split(",").length + " attachments");
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }*/
        mtxtAttachment.setText(imagesListArray.size() + " attachments");
        super.onActivityResult(requestCode, resultCode, data);
    }

    private String saveURITemp(Uri uri) {
        Uri receivedUri = uri;
        String path = "";
        if (receivedUri != null) {
            try {
                InputStream is = getContentResolver().openInputStream(receivedUri);
                Bitmap image = null;
                try {
                    image = comfun.handleSamplingAndRotationBitmap(this, receivedUri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                File f = comfun.saveBitmap(GalleryActivity.this, image, receivedUri);
                path = f.getAbsolutePath();
                path = comfun.compressImage(mContext, path,"");
                imagesListArray.add(path);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return path;
    }


    int clickCount = 0;

    private void enableSubmitBtn() {
        clickCount = 0;
        txtBtnSubmit.setEnabled(true);
        txtBtnSubmit.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
        txtBtnSubmit.setText("Submit");
        recyclerGallery.setVisibility(View.VISIBLE);
        mGalleryId = "0";
        editTextHead.setText("");
        isAddLayoutOpen = false;
    }

    private void disableSubmitBtn() {
        clickCount = 1;
        txtBtnSubmit.setEnabled(false);
        txtBtnSubmit.setText("Please wait");
        txtBtnSubmit.setBackgroundColor(mContext.getResources().getColor(R.color.grey_att));
    }

    String mGalleryId = "0";

    public void showAddMoreLayout(GalleryModel galleryModel) {
        layoutCircularAdd.setVisibility(View.VISIBLE);
        recyclerGallery.setVisibility(View.GONE);
        mGalleryId = galleryModel.getGalleryId();
        editTextHead.setText(galleryModel.getGalleryTitle());
        isAddLayoutOpen = true;
    }

    @Override
    public void onResume() {
        super.onResume();
        listGallery();
    }

    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    private boolean doubleBackToExitPressedOnce = false;

    public void deleteGallery(GalleryModel galleryModel) {
        if (doubleBackToExitPressedOnce) {
            showProgress();
            ApiInterface apiService =
                    ApiClient.getClient(mContext).create(ApiInterface.class);

            Call<ApiResponseModel> call = apiService.GalleryDelete(galleryModel);
            call.enqueue(new Callback<ApiResponseModel>() {
                @Override
                public void onResponse(Call<ApiResponseModel> call, Response<ApiResponseModel> response) {
                    hideProgress();
                    try {
                        ApiResponseModel apiResponseModel = response.body();
                        Toast.makeText(GalleryActivity.this, apiResponseModel.getMessage(), Toast.LENGTH_SHORT).show();
                        listGallery();
                    } catch (Exception ex) {
                        Log.e(mTAG, "error =" + ex.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<ApiResponseModel> call, Throwable t) {
                    hideProgress();
                    Log.e(mTAG, "error =" + t.toString());
                }
            });
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        myToast(mContext, "double click to delete", Toast.LENGTH_SHORT);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
}
