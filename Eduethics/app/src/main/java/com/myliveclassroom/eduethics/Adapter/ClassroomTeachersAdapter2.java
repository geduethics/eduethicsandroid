package com.myliveclassroom.eduethics.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.myliveclassroom.eduethics.Activity.MainActivity;
import com.myliveclassroom.eduethics.Activity.TimeTableActivity;
import com.myliveclassroom.eduethics.Activity.TimeTableCreateActivity;
import com.myliveclassroom.eduethics.Activity.TimeTableTActivity;
import com.myliveclassroom.eduethics.Fragments.ClassroomTeacherFragment;
import com.myliveclassroom.eduethics.Fragments.DynamicFragment;
import com.myliveclassroom.eduethics.Fragments.WebAppFrag.CreateTimeTableSchoolFragment;
import com.myliveclassroom.eduethics.Fragments.WebAppFrag.TimeTableTFragment;
import com.myliveclassroom.eduethics.Models.TimeTableModel;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.ExtraFunctions;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.TinyDB;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.List;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;

public class ClassroomTeachersAdapter2 extends RecyclerView.Adapter<ClassroomTeachersAdapter2.vh> {

    Fragment fragment;

    List<TimeTableModel> classSubjectModelList;
    Context mContext;
    TinyDB tinyDB;

    public ClassroomTeachersAdapter2(Context context, List<TimeTableModel> timeTableModels, Fragment fragment) {

        this.fragment = fragment;
        MainActivity.ids = null;
        MainActivity.ids = new HashMap<>();
        mContext = context;
        classSubjectModelList = timeTableModels;
        tinyDB = new TinyDB(mContext);
    }

    @NonNull
    @Override
    public vh onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (mContext instanceof TimeTableTActivity || mContext instanceof TimeTableActivity) {
            return new vh(LayoutInflater.from(mContext).inflate(R.layout.single_schecule_layout, parent, false));
        } else {
            return new vh(LayoutInflater.from(mContext).inflate(R.layout.classroom_teacher_single_item, parent, false));
        }
    }

    boolean showLiveBtn = true;

    @Override
    public void onBindViewHolder(@NonNull @NotNull ClassroomTeachersAdapter2.vh holder, int position) {
        holder.subject.setText(classSubjectModelList.get(position).getSubjectName());

        if (tinyDB.getString(LocalConstants.INSTITUTE_TYPE.TYPE).equalsIgnoreCase(LocalConstants.INSTITUTE_TYPE.TUTION)) {
            holder.classTV.setText(classSubjectModelList.get(position).getClassName());
        } else {
            holder.classTV.setText(classSubjectModelList.get(position).getClassName()  );
        }
        if (holder.txtTimes != null) {
            String t1 = classSubjectModelList.get(position).
                    getTimeFrom().length() > 5 ? classSubjectModelList.get(position).getTimeFrom().substring(0, 5)
                    : classSubjectModelList.get(position).getTimeFrom();
            String t2 = classSubjectModelList.get(position).
                    getTimeTo().length() > 5 ? classSubjectModelList.get(position).getTimeTo().substring(0, 5)
                    : classSubjectModelList.get(position).getTimeTo();
            holder.txtTimes.setText(t1 + " - " + t2);
        }

        if (classSubjectModelList.get(position).getDayId() != null) {
            //Compare date
            int dayNumber = Integer.parseInt(classSubjectModelList.get(position).getDayId());
            int hh1 = Integer.parseInt(classSubjectModelList.get(position).getTimeFrom().split(":")[0]);
            int min1 = Integer.parseInt(classSubjectModelList.get(position).getTimeFrom().split(":")[1]);

            int hh2 = Integer.parseInt(classSubjectModelList.get(position).getTimeTo().split(":")[0]);
            int min2 = Integer.parseInt(classSubjectModelList.get(position).getTimeTo().split(":")[1]);
            showLiveBtn = ExtraFunctions.isThisDateTimeGreater(dayNumber, hh1, min1, hh2, min2, 2);
        }
        //if x1 =false(small) then this is less than current date time
        //and if x2 is false(small) then no live class
        //if x1 is false(greater) and x2 is true then live class


        //
        if (holder.goLiveNow != null) {
            if (fragment != null && fragment instanceof TimeTableTFragment) {
                if (showLiveBtn) {
                    holder.goLiveNow.setText("Go Live");
                    holder.goLiveNow.setVisibility(View.VISIBLE);
                } else {
                    holder.goLiveNow.setVisibility(View.GONE);
                }

                holder.goLiveNow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        tinyDB.putString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_NAME, classSubjectModelList.get(position).getClassName());
                        tinyDB.putString(LocalConstants.TINYDB_LOGIN_KEYS.SUBJECT_NAME, classSubjectModelList.get(position).getSubjectName());
                        tinyDB.putString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID, classSubjectModelList.get(position).getClassId());
                        tinyDB.putString(LocalConstants.TINYDB_LOGIN_KEYS.SUBJECT_ID, classSubjectModelList.get(position).getSubjectId());
                        ((TimeTableTFragment) fragment).goLive();
                    }
                });
            } else if (fragment != null && fragment instanceof CreateTimeTableSchoolFragment) {
                holder.goLiveNow.setText("Create Timetable");
                holder.goLiveNow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       /* tinyDB = new TinyDB(fragment.getActivity());
                        tinyDB.putString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID, classSubjectModelList.get(position).getClassId());
                        tinyDB.putString(LocalConstants.TINYDB_LOGIN_KEYS.SUBJECT_ID, classSubjectModelList.get(position).getSubjectId());
                        */

                        ((TimeTableTActivity) mContext).showTimeFragment();
                        Toast.makeText(mContext, "this is create timetable", Toast.LENGTH_SHORT).show();
                    }
                });
            } else if (fragment != null && fragment instanceof DynamicFragment) {
                if (showLiveBtn) {
                    holder.goLiveNow.setText("Join Class");
                    holder.goLiveNow.setVisibility(View.VISIBLE);
                } else {
                    holder.goLiveNow.setVisibility(View.GONE);
                }

                holder.goLiveNow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        tinyDB.putString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_NAME, classSubjectModelList.get(position).getClassName());
                        tinyDB.putString(LocalConstants.TINYDB_LOGIN_KEYS.SUBJECT_NAME, classSubjectModelList.get(position).getSubjectName());
                        tinyDB.putString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID, classSubjectModelList.get(position).getClassId());
                        tinyDB.putString(LocalConstants.TINYDB_LOGIN_KEYS.SUBJECT_ID, classSubjectModelList.get(position).getSubjectId());
                        ((DynamicFragment) fragment).joinClass();
                    }
                });
            }
        }

        int id = View.generateViewId();

        MainActivity.ids.put(id, "cls;" + classSubjectModelList.get(position).
                getClassId() + ";" + classSubjectModelList.get(position).
                getClassName() + ";" + classSubjectModelList.get(position).
                getStudentName());

        holder.itemView.setId(id);

        if (fragment != null) {
            fragment.getActivity().registerForContextMenu(holder.itemView);
        }
        //holder.itemView.setOnClickListener(v -> ((ClassroomTeacherFragment) fragment).click(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tinyDB.putString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID, classSubjectModelList.get(position).getClassId() == null ? "" : classSubjectModelList.get(position).getClassId());
                tinyDB.putString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_NAME, classSubjectModelList.get(position).getClassName() == null ? "" : classSubjectModelList.get(position).getClassName());
                tinyDB.putString(LocalConstants.TINYDB_LOGIN_KEYS.SUBJECT_NAME, classSubjectModelList.get(position).getSubjectName() == null ? "" : classSubjectModelList.get(position).getSubjectName());
                tinyDB.putString(LocalConstants.TINYDB_LOGIN_KEYS.SUBJECT_ID, classSubjectModelList.get(position).getSubjectId() == null ? "0" : classSubjectModelList.get(position).getSubjectId());

                if (fragment != null && fragment instanceof ClassroomTeacherFragment) {
                    ((ClassroomTeacherFragment) fragment).click(position);
                } else if (fragment != null && fragment instanceof TimeTableTFragment) {
                    Toast.makeText(mContext, "this is TimeTableTFragment", Toast.LENGTH_SHORT).show();
                } else if (mContext instanceof TimeTableCreateActivity) {
                    Toast.makeText(mContext, "Test msg", Toast.LENGTH_SHORT).show();
                }
                Log.e(mTAG, classSubjectModelList.get(position).getClassId());
            }
        });

    }

    public void updateList(List<TimeTableModel> list) {
        classSubjectModelList = list;
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return classSubjectModelList == null ? 0 : classSubjectModelList.size();
    }

    static class vh extends RecyclerView.ViewHolder {
        TextView subject, classTV, totalStudents, goLiveNow, txtTimes;


        public vh(@NonNull View itemView) {
            super(itemView);
            this.classTV = itemView.findViewById(R.id.classSingleItem);
            this.subject = itemView.findViewById(R.id.subjectSingleItem);
            this.totalStudents = itemView.findViewById(R.id.totalStudents);
            this.goLiveNow = itemView.findViewById(R.id.goLiveNow);
            this.txtTimes = itemView.findViewById(R.id.txtTimes);
        }
    }
}



