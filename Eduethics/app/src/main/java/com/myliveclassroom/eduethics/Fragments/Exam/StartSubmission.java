package com.myliveclassroom.eduethics.Fragments.Exam;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonObject;
import com.myliveclassroom.eduethics.Activity.MainActivity;
import com.myliveclassroom.eduethics.Adapter.UploadImagesAdapter;
import com.myliveclassroom.eduethics.Models.HomeWorkModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.ExtraFunctions;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.myliveclassroom.eduethics.classes.CommonFunctions;
import com.myliveclassroom.eduethics.classes.TinyDB;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.APP_PACKAGE_NAME;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.MAIN_FOLDER;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.PICK_CAMERA_REQUEST_CODE;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.PICK_GALLERY_REQUEST_CODE;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.PICK_PDF_REQUEST_CODE;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;
import static com.myliveclassroom.eduethics.Utils.LocalConstants.FROM.FROM_ASSIGNMENT_FRAG;
import static com.myliveclassroom.eduethics.Utils.LocalConstants.FROM.FROM_EVALUATE_FRAG;
import static com.myliveclassroom.eduethics.Utils.LocalConstants.FROM.FROM_TEST_FRAG;
import static com.myliveclassroom.eduethics.classes.CommonFunctions.createImageFileNew;
import static com.myliveclassroom.eduethics.classes.CommonFunctions.getBytes;
import static com.myliveclassroom.eduethics.classes.CommonFunctions.getImageUri;
import static com.myliveclassroom.eduethics.classes.CommonFunctions.getPath;
import static com.myliveclassroom.eduethics.classes.CommonFunctions.saveFile;
import static com.myliveclassroom.eduethics.classes.CommonFunctions.writeToFile;

public class StartSubmission extends Fragment {

    HomeWorkModel homeWorkModel;
    Dialog dialog;
    Context mContext;
    Button BtnSubmit;

    List<String> imagesListArray = new ArrayList<>();
    List<String> imagesListArrayDone = new ArrayList<>();
    CommonFunctions comfun = new CommonFunctions();

    UploadImagesAdapter imagesadapter = new UploadImagesAdapter(imagesListArray, this, LocalConstants.FROM.FROM_SUMBIT_TEST_ATT, null);
    UploadImagesAdapter imagesadapterAlready;

    RecyclerView uploadsRecyclerview, solutionRecyclerView;

    int from;

    List<String> attachmentsDataFix = new ArrayList<>();

    public StartSubmission(Context context, HomeWorkModel homeWorkModel, int from, boolean isDead) {
        mContext = context;
        this.homeWorkModel = homeWorkModel;
        this.from = from;
        this.isDead = isDead;
        imagesadapterAlready = new UploadImagesAdapter(attachmentsDataFix, this, FROM_EVALUATE_FRAG, null);
        tinyDB = new TinyDB(mContext);
    }

    FirebaseStorage storage = FirebaseStorage.getInstance();
    StorageReference storageRef = storage.getReferenceFromUrl(LocalConstants.STORAGE_REF);    //change the url according to your firebase app
    String collection = "";
    String documentName = "";
    TextView score, feedback;
    View root;
    String mCurrentPhotoPath = "";

    boolean isDead;
    View progressView;
    TinyDB tinyDB;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_start_submission, container, false);

        imagesadapter.setProgress(root.findViewById(R.id.progress));
        imagesadapterAlready.setProgress(root.findViewById(R.id.progress));

        MainActivity.TO_WHICH_FRAG = from == FROM_ASSIGNMENT_FRAG ? LocalConstants.TO_FRAG.TO_ASSIGNMENT_FRAG : LocalConstants.TO_FRAG.TO_TEST_FRAG;


        ((TextView) root.findViewById(R.id.topic)).setText(homeWorkModel.getWorkHead());
        ((TextView) root.findViewById(R.id.maxMarks)).setText(homeWorkModel.getMaxMarks());
        ((TextView) root.findViewById(R.id.deadline)).setText(homeWorkModel.getWorkDate().trim() + " " + ExtraFunctions.getReadableTime(Integer.parseInt(homeWorkModel.getTime().split(":")[0]), Integer.parseInt(homeWorkModel.getTime().split(":")[1])));
        ((TextView) root.findViewById(R.id.des)).setText(homeWorkModel.getWorkDetail());

        progressView = root.findViewById(R.id.progress);
        RecyclerView recyclerviewAttachments = root.findViewById(R.id.recyclerviewAttachments);
        recyclerviewAttachments.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerviewAttachments.setAdapter(imagesadapterAlready);
        String[] attachments = homeWorkModel.getAttachment().split(",");
        for (String s : attachments) {
            attachmentsDataFix.add(s);
        }
        if (attachments.length == 0) {
            recyclerviewAttachments.setVisibility(View.GONE);
            root.findViewById(R.id.temp4).setVisibility(View.GONE);
        }
        imagesadapterAlready.notifyDataSetChanged();
        BtnSubmit = root.findViewById(R.id.submit);
        solutionRecyclerView = root.findViewById(R.id.solutionRecyclerView);
        score = root.findViewById(R.id.score);
        feedback = root.findViewById(R.id.feedback);

        uploadsRecyclerview = root.findViewById(R.id.uploadsRecyclerview);
        uploadsRecyclerview.setLayoutManager(new LinearLayoutManager(getContext()));
        uploadsRecyclerview.setAdapter(imagesadapter);

        if (from == FROM_TEST_FRAG) {
            collection = "testSub";
            documentName = "test";
        } else if (from == FROM_ASSIGNMENT_FRAG) {
            collection = "assignmentSub";
            documentName = "assignments";
        }

        if (!isDead) {
            root.findViewById(R.id.addAttachmentBtn).setOnClickListener(v -> addAtachment());
        } else {
            root.findViewById(R.id.addAttachmentBtn).setVisibility(View.INVISIBLE);
            root.findViewById(R.id.temp0).setVisibility(View.GONE);
            root.findViewById(R.id.addAttachmentBtn).setVisibility(View.GONE);
            root.findViewById(R.id.uploadsRecyclerview).setVisibility(View.GONE);
            BtnSubmit.setVisibility(View.GONE);
            showSolutions();
        }


        //BtnSubmit.setOnClickListener(v -> submit());
        BtnSubmit.setOnClickListener(v -> uploadHomeWork());

        return root;
    }

    private void showSolutions() {

        root.findViewById(R.id.solitionConstraint).setVisibility(View.VISIBLE);
        String[] doneAttachments = homeWorkModel.getDoneAttachment().split(",");
        for (String s : doneAttachments) {
            imagesListArrayDone.add(s);
        }

        if (imagesListArrayDone.size() == 0) {
            root.findViewById(R.id.empty).setVisibility(View.VISIBLE);
            ((TextView) root.findViewById(R.id.emptyMessage)).setText("No Solution has been added by teacher");

            solutionRecyclerView.setVisibility(View.GONE);
            root.findViewById(R.id.temp6).setVisibility(View.GONE);
        } else {
            solutionRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            solutionRecyclerView.setAdapter(imagesadapterAlready);
            solutionRecyclerView.setAdapter(new UploadImagesAdapter(imagesListArrayDone, StartSubmission.this, FROM_TEST_FRAG, root.findViewById(R.id.progress)));
            root.findViewById(R.id.submit).setVisibility(View.GONE);
        }

        /*if (data2 != null) {
            if (data2.get("feedback") != null) {
                feedback.setText(data2.get("feedback").toString());
                score.setText(data2.get("score").toString());
                root.findViewById(R.id.addAttachmentBtn).setVisibility(View.INVISIBLE);
            } else {
                feedback.setVisibility(View.GONE);
                score.setVisibility(View.GONE);
            }
        } else {
            root.findViewById(R.id.temp8).setVisibility(View.GONE);
            root.findViewById(R.id.score).setVisibility(View.GONE);
            root.findViewById(R.id.feedback).setVisibility(View.GONE);
            root.findViewById(R.id.temp7).setVisibility(View.GONE);
        }*/

    }

    private void addAtachment() {
        dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.upload_dialog);
        dialog.findViewById(R.id.camera).setOnClickListener(v -> {
            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_DENIED)
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, LocalConstants.REQ_CODE.CAMERA_REQUEST);
            else {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                //startActivityForResult(cameraIntent, LocalConstants.PERMISSION_CODE.CAMERA_REQUEST);
                /***************************************
                 *      Custom file name for Camera file
                 ***************************************/
                File photoFile = null;
                try {
                    photoFile = createImageFileNew(LocalConstants.FILE_TYPE.JPG);
                    mCurrentPhotoPath = photoFile.getAbsolutePath();
                } catch (IOException ex) {
                    // Error occurred while creating the File
                    Log.i(mTAG, "IOException");
                }
                // Continue only if the File was successfully created
                if (photoFile != null) {
                    Uri imageUri = FileProvider.getUriForFile(
                            mContext,
                            APP_PACKAGE_NAME + ".provider",
                            photoFile);
                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                    startActivityForResult(cameraIntent, PICK_CAMERA_REQUEST_CODE);
                }
                dialog.dismiss();
            }
        });

        dialog.findViewById(R.id.pdf).setOnClickListener(v -> {
            Intent chooseFile;
            Intent intent;
            chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
            chooseFile.setType("application/pdf");
            intent = Intent.createChooser(chooseFile, "Choose a file");
            startActivityForResult(intent, PICK_PDF_REQUEST_CODE);
            dialog.dismiss();
        });

        dialog.findViewById(R.id.image).setOnClickListener(v -> {
            Intent i = new Intent();
            i.setType("image/*");
            i.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(i, "Select Picture"), PICK_GALLERY_REQUEST_CODE);
            dialog.dismiss();
        });

        dialog.findViewById(R.id.upload).setOnClickListener(v -> dialog.dismiss());
        dialog.findViewById(R.id.cancel).setOnClickListener(v -> dialog.dismiss());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    /*private void submit() {
        progressView.setVisibility(View.VISIBLE);

        for (String s : attachmentsOnEdit)
            if (!attachments.contains(s)) {
                forDelete.add(s);
                urls.remove(s);
            }

        if (forDelete.size() != 0) for (int i = 0; i < forDelete.size(); i++) {
            StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(forDelete.get(i));
            int finalI = i;
            storageReference.delete().addOnSuccessListener(aVoid -> {
                if (finalI == forDelete.size() - 1) subFunctionUpload();
            }).addOnFailureListener(exception -> {
                if (finalI == forDelete.size() - 1) subFunctionUpload();
            });
        }
        else subFunctionUpload();

    } */

    /*private void subFunctionUpload() {
        String child = "";
        if (from == FROM_ASSIGNMENT_FRAG) {
            child = "assignmentData";
        } else if (from == FROM_TEST_FRAG) {
            child = "testData";
        }
        if (forAdd.size() == 0) {
            uploadToClassroom();
            return;
        }
        for (int i = 0; i < attachments.size(); i++) {
            Bitmap bitmap = null;
            Uri selectedDataPath = null;
            if (forAdd.contains(attachments.get(i))) {
                if (attachments.get(i).startsWith("314")) bitmap = (Bitmap) attachmentsData.get(i);
                else selectedDataPath = (Uri) attachmentsData.get(i);

                if (selectedDataPath == null) {
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    byte[] data = baos.toByteArray();

                    final String randomKey = UUID.randomUUID().toString();

                    UploadTask uploadTask = storageRef.child(child + "/" + HomeClassroomFragment.classId + "/sub/" + randomKey + ".jpg").putBytes(data);
                    uploadTask.addOnFailureListener(exception -> Toast.makeText(getContext(), "failed", Toast.LENGTH_SHORT).show()).addOnSuccessListener(taskSnapshot -> {
                        urls.add(taskSnapshot.getMetadata().getPath());
                        uploadImageCounter++;
                        checkandpost();
                    });
                } else {
                    final String randomKey = UUID.randomUUID().toString();
                    String extension;
                    if (selectedDataPath.getPath().contains(".")) {
                        extension = selectedDataPath.getPath().substring(selectedDataPath.getPath().lastIndexOf("."));
                    } else {
                        extension = ".pdf";
                    }

                    StorageReference riversRef = storageRef.child((child + "/" + HomeClassroomFragment.classId + "/sub/" + randomKey + extension));


                    riversRef.putFile(selectedDataPath)
                            .addOnSuccessListener(taskSnapshot -> {
                                progressView.setVisibility(View.GONE);
                                String name = riversRef.getDownloadUrl().toString();
                                urls.add(taskSnapshot.getMetadata().getPath());
                                uploadImageCounter++;
                                checkandpost();
                            }).addOnProgressListener(snapshot -> {
                    }).addOnFailureListener(e -> Toast.makeText(getContext(), "Unable to post your Image !", Toast.LENGTH_SHORT).show());

                }
            }

        }

    }*/

    int uploadImageCounter = 0;

    /*private void checkandpost() {
        if (uploadImageCounter == forAdd.size()) {
            uploadToClassroom();
        }
    }

    private void uploadToClassroom() {
        Map<String, Object> mapF = new HashMap<>();
        mapF.put("feedback", "-");
        mapF.put("isSeen", false);
        mapF.put("name", "name1");
        mapF.put("score", "-/" + homeWorkModel.getMaxMarks());
        mapF.put("attachment", urls);

        Map<String, Object> map = new HashMap<>();
        map.put(GlobalVariables.uid, mapF);
    }*/


    private void uploadHomeWork() {
        disableSubmitBtn();
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("CreatedBy", tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
            jsonObject.put("SchoolId", tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
            jsonObject.put("SessionId", tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));
            jsonObject.put("HomeworkId", homeWorkModel.getHomeworkId());
            jsonObject.put("StudentId", tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));

            jsonObject.put("ClassId", homeWorkModel.getClassId());
            jsonObject.put("SubjectId", homeWorkModel.getSubjectId());
            jsonObject.put("SubjectName", homeWorkModel.getSubjectName());
            jsonObject.put("FcmToken", tinyDB.getString("FcmToken"));

        } catch (JSONException e) {
            e.printStackTrace();
        }


        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);


        MultipartBody.Part[] body = new MultipartBody.Part[imagesListArray.size()];

        for (int i = 0; i < imagesListArray.size(); i++) {
            File file = null;
            if (imagesListArray.get(i).toLowerCase().endsWith(".pdf")) {
                file = new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_DOCUMENTS + "/" + MAIN_FOLDER + "/" + "Pdf") + "/" + imagesListArray.get(i));
            } else {
                file = new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES + "/" + MAIN_FOLDER + "/" + "Images") + "/" + imagesListArray.get(i));
            }


            RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
            try {
                body[i] = MultipartBody.Part.createFormData("uploaded_file_" + i, file.getName(), requestFile);
            } catch (Exception ex) {
                Log.e(mTAG, "" + ex.getMessage());
            }
        }

        RequestBody modelParm = RequestBody.create(MediaType.parse("text/plain"), jsonObject.toString());

        Call<JsonObject> call = apiService.HomeWorkDoneSubmitWithFile(body, modelParm);

        // Call<ApiResponse> call = apiService.PostSave(Desc,CreatedBy);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                //int statusCode = response.code();
                enableSubmitBtn();
                try {
                    Log.e("res", response.body().toString());

                } catch (Exception ex) {
                    Log.e(mTAG, "error =" + ex.getMessage());
                    Toast.makeText(mContext, "Something went wrong2!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                // myProgressDialog.hideProgress();
                Toast.makeText(mContext, "Something went wrong1!", Toast.LENGTH_SHORT).show();
                enableSubmitBtn();
            }
        });
    }

    private void showProgress() {
        progressView.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        progressView.setVisibility(View.GONE);
    }

    private void enableSubmitBtn() {
        hideProgress();
        BtnSubmit.setEnabled(true);
        BtnSubmit.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
        BtnSubmit.setText("Submit");
    }

    private void disableSubmitBtn() {
        showProgress();
        BtnSubmit.setEnabled(false);
        BtnSubmit.setText("Please wait");
        BtnSubmit.setBackgroundColor(mContext.getResources().getColor(R.color.grey_att));
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data == null) return;
        switch (requestCode) {
           /* case LocalConstants.PERMISSION_CODE.PICKFILE_RESULT_CODE:
                attachmentsData.add(data.getData());
                attachments.add(data.getData().getPath());
                forAdd.add(data.getData().getPath());
                imagesadapter.notifyItemInserted(attachments.size() - 1);
                ((TextView) dialog.findViewById(R.id.pdf)).setBackground(getActivity().getDrawable(R.drawable.second_btn));
                break;*/
            case PICK_PDF_REQUEST_CODE:
                if (data == null) return;
                File pdfFile = null;
                try {
                    pdfFile = createImageFileNew(LocalConstants.FILE_TYPE.PDF);
                } catch (IOException ex) {
                    // Error occurred while creating the File
                    Log.i(mTAG, "IOException");
                }

                Uri uri = data.getData();
                byte[] inputData = null;
                InputStream iStream = null;
                try {
                    iStream = mContext.getContentResolver().openInputStream(uri);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                try {
                    inputData = getBytes(iStream);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    writeToFile(inputData, pdfFile.getAbsolutePath());
                } catch (IOException e) {
                    e.printStackTrace();
                }

                imagesListArray.add(pdfFile.getName());
                imagesadapter.notifyItemInserted(imagesListArray.size() - 1);
                break;
           /* case LocalConstants.PERMISSION_CODE.SELECT_PICTURE:
                Uri selectedImageUri = data.getData();
                if (null != selectedImageUri) {
                    ((TextView) dialog.findViewById(R.id.image)).setBackground(getActivity().getDrawable(R.drawable.second_btn));
                    attachmentsData.add(selectedImageUri);
                    attachments.add(selectedImageUri.getPath());
                    forAdd.add(selectedImageUri.getPath());
                    imagesadapter.notifyItemInserted(attachments.size() - 1);
                }
                break;
            case LocalConstants.PERMISSION_CODE.CAMERA_REQUEST:
                ((TextView) dialog.findViewById(R.id.camera)).setBackground(getActivity().getDrawable(R.drawable.second_btn));
                attachmentsData.add((Bitmap) data.getExtras().get("data"));
                attachments.add("314" + UUID.randomUUID().toString());
                forAdd.add("314" + UUID.randomUUID().toString());
                imagesadapter.notifyItemInserted(attachments.size() - 1);
                break;
        }*/
            case PICK_GALLERY_REQUEST_CODE:
                if (data == null) return;
                //*************OK**************
                if (requestCode == PICK_GALLERY_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
                    if (data == null) {
                        return;
                    }
                    Uri selectedImageUri = data.getData();
                    if (null != selectedImageUri) {
                        Uri imageUri = data.getData();
                        Bitmap bitmap = null;
                        try {
                            bitmap = MediaStore.Images.Media.getBitmap(mContext.getContentResolver(), imageUri);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        Uri u = getImageUri(mContext, bitmap);
                        File finalFile = null;
                        File file = new File(getPath(mContext, u));

                        file = saveFile(file, LocalConstants.FILE_TYPE.JPG);

                        imagesListArray.add(file.getName());
                        imagesadapter.notifyItemInserted(imagesListArray.size() - 1);
                    }
                }

                break;
            case PICK_CAMERA_REQUEST_CODE:
                try {
                    Bitmap mImageBitmap = null;
                    Uri tempUri = Uri.parse(mCurrentPhotoPath);
                    File finalFile = new File(tempUri.getPath());
                    mCurrentPhotoPath = comfun.compressImage(mContext, finalFile.getAbsolutePath(), MAIN_FOLDER + "/Images/");
                    imagesListArray.add(finalFile.getName());
                    imagesadapter.notifyItemInserted(imagesListArray.size() - 1);
                } catch (Exception ex) {
                    Log.e(mTAG, ex.getMessage());
                }
                break;
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == LocalConstants.REQ_CODE.CAMERA_REQUEST) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "camera permission granted", Toast.LENGTH_LONG).show();
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, LocalConstants.PERMISSION_CODE.CAMERA_REQUEST);
            } else {
                Toast.makeText(getActivity(), "camera permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void deleteImage(int position) {
        imagesListArray.remove(position);
        imagesadapter.notifyItemRemoved(position);
    }
}
