package com.myliveclassroom.eduethics.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.myliveclassroom.eduethics.Activity.MainActivity;
import com.myliveclassroom.eduethics.Fragments.HomeFragemtns.AssignmentFragment;
import com.myliveclassroom.eduethics.Models.HomeWorkModel;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.TinyDB;

import java.util.HashMap;
import java.util.List;

import static com.myliveclassroom.eduethics.Utils.HomeConstants.PROFILE;

public class AssignmentAdapter extends RecyclerView.Adapter<AssignmentAdapter.vh> {

    List<HomeWorkModel> homeWorkModelList;
    Fragment fragment;
    int submitted = 0;
    Context mContext;
    TinyDB tinyDB;

    public AssignmentAdapter(Fragment fragment, Context context, List<HomeWorkModel> homeWorkModels) {
        this.fragment = fragment;
        //this.assignments = HomeClassroomFragment.model.getAssignments();
        MainActivity.ids = null;
        MainActivity.ids = new HashMap<>();
        homeWorkModelList = homeWorkModels;
        mContext = context;
        tinyDB = new TinyDB(mContext);
    }

    @NonNull
    @Override
    public vh onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new vh(LayoutInflater.from(fragment.getContext()).inflate(R.layout.assignment_single_item, parent, false));
    }

    Boolean isDead=false;
    @Override
    public void onBindViewHolder(@NonNull vh holder, int position) {

        if((homeWorkModelList.get(position).getIsDone()==null?0:Integer.parseInt(homeWorkModelList.get(position).getIsDone()))>0) isDead=true;

        if(tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)){
            holder.teacherSection.setVisibility(View.GONE);
            holder.itemView.setOnClickListener(v -> ((AssignmentFragment) fragment).open(homeWorkModelList.get(position), isDead));
        }

        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER)) {
            int id = View.generateViewId();
            //MainActivity.ids.put(id, "assignment;"+data.get("resultId").toString());
            //holder.itemView.setId(id);
            //fragment.getActivity().registerForContextMenu(holder.itemView);

            holder.submittedStudents.setText(homeWorkModelList.get(position).getIsDone() + "/" + homeWorkModelList.size() + " Submitted");
            holder.status.setVisibility(View.GONE);
            //holder.itemView.findViewById(R.id.edit).setOnClickListener(v -> ((AssignmentFragment) fragment).edit(homeWorkModelList.get(position)));
            holder.itemView.findViewById(R.id.viewSubmission).setOnClickListener(v -> ((AssignmentFragment) fragment).view(homeWorkModelList.get(position)));
            holder.itemView.findViewById(R.id.solution).setOnClickListener(v -> ((AssignmentFragment) fragment).solution(homeWorkModelList.get(position)));
        }

        holder.title.setText(homeWorkModelList.get(position).getWorkHead());

        //TODO
        // //Temprory commented for student screen
        //holder.time.setText(ExtraFunctions.getReadableTime(Integer.parseInt(homeWorkModelList.get(position).getTime().split(":")[0]), Integer.parseInt(homeWorkModelList.get(position).getTime().split(":")[1])));

        holder.maxNo.setText("Max Marks : " + homeWorkModelList.get(position).getMaxMarks()==null?"":homeWorkModelList.get(position).getMaxMarks());
        holder.date.setText(homeWorkModelList.get(position).getWorkDate()==null?"":homeWorkModelList.get(position).getWorkDate());
        holder.time.setText(homeWorkModelList.get(position).getTime()==null?"":homeWorkModelList.get(position).getTime());
        holder.status.setText(homeWorkModelList.get(position).getStatus()==null?"":homeWorkModelList.get(position).getStatus());
        holder.status.setVisibility(View.GONE);
    }

    private int getSubmited(){
        int x=0;
        for (HomeWorkModel h:homeWorkModelList){
            if(Integer.parseInt(h.getIsDone())>0){
                x=x+1;
            }
        }
        return x;
    }

    public void updateList(List<HomeWorkModel> list) {
        homeWorkModelList = list;
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return homeWorkModelList.size();
    }

    class vh extends RecyclerView.ViewHolder {
        TextView title, date, time, status, maxNo, submittedStudents;
        ConstraintLayout teacherSection;
        public vh(@NonNull View itemView) {
            super(itemView);
            this.submittedStudents = itemView.findViewById(R.id.submittedStudents);
            this.date = itemView.findViewById(R.id.date);
            this.time = itemView.findViewById(R.id.time);
            this.status = itemView.findViewById(R.id.status);
            this.maxNo = itemView.findViewById(R.id.maxMarks);
            this.title = itemView.findViewById(R.id.title);
            this.teacherSection=itemView.findViewById(R.id.teacherSection);

        }
    }
}
