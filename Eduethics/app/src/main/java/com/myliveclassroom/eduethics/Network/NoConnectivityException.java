package com.myliveclassroom.eduethics.Network;

import java.io.IOException;

class NoConnectivityException extends IOException {

    @Override
    public String getMessage() {
        return "No Internet Connection";
        // You can send any message whatever you want from here.
    }
}