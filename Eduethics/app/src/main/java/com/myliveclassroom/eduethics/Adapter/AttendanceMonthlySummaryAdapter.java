package com.myliveclassroom.eduethics.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.myliveclassroom.eduethics.Models.TimeTableModel;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.classes.CommonFunctions;
import com.myliveclassroom.eduethics.classes.TinyDB;

import java.util.ArrayList;
import java.util.List;


public class AttendanceMonthlySummaryAdapter extends RecyclerView.Adapter<AttendanceMonthlySummaryAdapter.ViewHolder> {

    List<TimeTableModel> timeTableModelList;
    Context mContext;
    List<String> listAttTags = new ArrayList<String>();
    TinyDB tinyDB;

    public AttendanceMonthlySummaryAdapter(Context context, List<TimeTableModel> list) {
        timeTableModelList = list;
        mContext = context;
        tinyDB=new TinyDB(mContext);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_monthly_summary_attendance, parent, false);
        return new ViewHolder(view);
    }

    boolean isHeaderIncluded=false;
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.txtMonthName.setText(tinyDB.getString("mMonth")+"-"+tinyDB.getString("mYear")); //Student name is subject name
        holder.txtPresent.setText(timeTableModelList.get(position).getPresentPer().replace(".00","") +"%");
        holder.txtAbsent.setText(
                Float.parseFloat(timeTableModelList.get(position).getPresentPer())==0?"100%":
                timeTableModelList.get(position).getAbsentPer().replace(".00","")+"%");
        holder.txtLeave.setText(timeTableModelList.get(position).getLeavePer().replace(".00","")+"%");

        holder.txtMonthName.setTypeface(null, Typeface.NORMAL);
        holder.txtPresent.setTypeface(null, Typeface.NORMAL);
        holder.txtAbsent.setTypeface(null, Typeface.NORMAL);
        holder.txtLeave.setTypeface(null, Typeface.NORMAL);
    }

    @Override
    public int getItemCount() {
        return (timeTableModelList==null?0: timeTableModelList.size());
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void updateList(List<TimeTableModel> listAll) {
        timeTableModelList = listAll;
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        TextView txtMonthName;
        TextView txtPresent;
        TextView txtAbsent,txtLeave;
        public ViewHolder(@NonNull View view) {
            super(view);

            txtMonthName=view.findViewById(R.id.txtMonthName);
            txtPresent=view.findViewById(R.id.txtPresent);
            txtAbsent=view.findViewById(R.id.txtAbsent);
            txtLeave=view.findViewById(R.id.txtLeave);


        }
    }
}

