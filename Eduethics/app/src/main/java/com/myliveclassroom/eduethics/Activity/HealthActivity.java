package com.myliveclassroom.eduethics.Activity;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.myliveclassroom.eduethics.Adapter.StudentFeedbackAdapter;
import com.myliveclassroom.eduethics.Adapter.StudentHealthFeedbackAdapter;
import com.myliveclassroom.eduethics.Models.ExamModel;
import com.myliveclassroom.eduethics.Models.TimeTableModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;

public class HealthActivity extends BaseActivity {

    @BindView(R.id.txtPageTitle)
    TextView txtPageTitle;

    @BindView(R.id.imgBack)
    ImageView imgBack;

    @BindView(R.id.imgbtnAdd)
    ImageView imgbtnAdd;

    @BindView(R.id.btnAdd)
    TextView btnAdd;


    @BindView(R.id.txtDate)
    TextView txtDate;

    @BindView(R.id.btnsubmitHealthRecord)
    Button btnsubmitHealthRecord;



    public List<TimeTableModel> studentList;

    private String mClassId;
    private String mSelectedDate;

    StudentHealthFeedbackAdapter attendanceAdapter;

    ProgressBar progressBar;
    RecyclerView recyclerViewAttendance;

    List<TimeTableModel> classList;

    @BindView(R.id.spinnerClass)
    Spinner spinnerClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.MyThemeNoBar);
        setContentView(R.layout.activity_health);
        ButterKnife.bind(this);
        _BaseActivity(this);
        init();
    }

    private void init() {
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        mClassId = tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID);
        mSelectedDate = tinyDB.getString("Date");
        txtDate.setText(tinyDB.getString("Date"));
        tinyDB.putString("AttDate", tinyDB.getString("Date"));


        imgbtnAdd.setVisibility(View.GONE);
        imgBack.setVisibility(View.VISIBLE);
        btnAdd.setVisibility(View.GONE);
        btnAdd.setText("");
        imgbtnAdd.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_tick3));
        txtPageTitle.setText("Health Record");
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Back();
            }
        });

        imgbtnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(mContext, "test from frag", Toast.LENGTH_SHORT).show();
                Log.e(mTAG, "" + studentList.size());
            }
        });
        initRecyclerView();

        btnsubmitHealthRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitHealthRecords();
            }
        });
    }

    //@OnClick(R.id.imgBack)
    private void Back() {
        super.onBackPressed();
    }

    private void initRecyclerView() {
        studentList = new ArrayList<>();
        recyclerViewAttendance = findViewById(R.id.recyclerViewAttendance);
        recyclerViewAttendance.setLayoutManager(new LinearLayoutManager(mContext));
        attendanceAdapter = new StudentHealthFeedbackAdapter(mContext, studentList);
        recyclerViewAttendance.setAdapter(attendanceAdapter);

        listClasses();
        getStudentList();
    }

    private void listClasses() {
        TimeTableModel timeTableModel = new TimeTableModel();
        timeTableModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        timeTableModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = apiService.classList(timeTableModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                try {
                    if (response.body() == null) {
                        Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (response.body() != null) {
                        Type type = new TypeToken<List<TimeTableModel>>() {
                        }.getType();
                        classList = new Gson().fromJson(response.body().getAsJsonArray("Response"), type);

                        List<String> spinnerArray = new ArrayList<>();
                        spinnerArray.add("--All Class--");
                        for (TimeTableModel str : classList) {
                            spinnerArray.add(str.getClassName());
                        }

                        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, spinnerArray);
                        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down vieww
                        spinnerClass.setAdapter(spinnerArrayAdapter);
                        spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int myPosition, long myID) {
                                if (myPosition > 0) {
                                    mClassId = classList.get(myPosition - 1).getClassId();
                                    getStudentList();
                                } else {
                                    mClassId = "0";
                                }
                                //Log.i("renderSpinner -> ", "onItemSelected: " + myPosition + "/" + myID);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parentView) {
                                // your code here
                            }
                        });

                    }
                } catch (Exception ex) {
                    Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getStudentList() {
        progressBar.setVisibility(View.VISIBLE);
        TimeTableModel listModel = new TimeTableModel();

        listModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        listModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));
        //listModel.setStudentId("0");
        listModel.setClassId(mClassId);
        listModel.setUserId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
        listModel.setAttDate(tinyDB.getString("AttDate"));

        Log.e(mTAG, tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_NAME) + mClassId);

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        //Call<List<TimeTableModel>> call = apiService.getClassStudent(listModel);
        Call<JsonObject> call = apiService.StudentListSchool(listModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (response.body() == null) {
                        Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    Type type = new TypeToken<List<TimeTableModel>>() {
                    }.getType();
                    studentList = new Gson().fromJson(response.body().getAsJsonArray("Response"), type);
                    /*attendanceAdapter.updateList(studentList);*/
                    attendanceAdapter = new StudentHealthFeedbackAdapter(mContext, studentList );
                    recyclerViewAttendance.setAdapter(attendanceAdapter);

                } catch (Exception ex) {
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(mTAG, "error =" + t.toString());
            }
        });
    }

    private void submitHealthRecords(){
        progressBar.setVisibility(View.VISIBLE);

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = apiService.classHealthSubmit(studentList);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                progressBar.setVisibility(View.GONE);
                try {
                    if(response.body()!=null) {
                        if(new Gson().fromJson(response.body().get("Status"),String.class).toString().equalsIgnoreCase("Success")){
                            Toast.makeText(mContext, "Record submitted successfully", Toast.LENGTH_SHORT).show();
                            finish();
                        } else{
                            Toast.makeText(mContext, LocalConstants.APP_MESSAGES.ERROR1, Toast.LENGTH_SHORT).show();
                        }
                        Log.e(mTAG, response.toString());
                    }

                } catch (Exception ex) {
                    Log.e(mTAG, "error =" + ex.getMessage());
                    Toast.makeText(mContext, LocalConstants.APP_MESSAGES.ERROR1, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, LocalConstants.APP_MESSAGES.ERROR1, Toast.LENGTH_SHORT).show();
            }
        });
    }
}