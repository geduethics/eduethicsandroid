package com.myliveclassroom.eduethics.Activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.myliveclassroom.eduethics.Fragments.TeacherFragments.TimeTableScheduleCreateFragment;
import com.myliveclassroom.eduethics.Fragments.TeacherFragments.TimeTableScheduleViewFragment;
import com.myliveclassroom.eduethics.Fragments.WebAppFrag.TimeTableTFragment;
import com.myliveclassroom.eduethics.Models.TimeTableModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.HomeConstants;
import com.google.android.material.tabs.TabLayout;
import com.myliveclassroom.eduethics.Utils.LocalConstants;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.LOCALE;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.LOCALE_PB;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.dayArray;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.dayArray_PB;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.PROFILE;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.SCHEDULE_TEACHER;


public class TimeTableTActivity extends BaseActivity {

    @BindView(R.id.txtPageTitle)
    TextView txtPageTitle;

    @BindView(R.id.imgBack)
    ImageView imgBack;

    @BindView(R.id.imgbtnAdd)
    ImageView imgbtnAdd;

    @BindView(R.id.btnAdd)
    TextView btnAdd;

    @BindView(R.id.layoutTabs)
    LinearLayout layoutTabs;

    @BindView(R.id.layoutFragment)
    LinearLayout layoutFragment;


    TimeTableModel timeTableModel;
    int noOfTabs = 0;
    Fragment fragment;
    private ViewPagerAdapterInner viewPagerAdapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.MyThemeNoBar);
        setContentView(R.layout.activity_time_table_t);
        ButterKnife.bind(this);
        _BaseActivity(this);
        getNoDays();
        init();
        tinyDB.putString("Screen", HomeConstants.TIMETABLE);
    }

    private void init() {
        imgbtnAdd.setVisibility(View.VISIBLE);
        imgBack.setVisibility(View.VISIBLE);
        btnAdd.setVisibility(View.VISIBLE);
        btnAdd.setText("Create");
        //imgbtnAdd.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_tick3));

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Back();
            }
        });

        imgbtnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFragment();
            }
        });
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFragment();
            }
        });

        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)
                || tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.PARENT)
        ) {
            imgbtnAdd.setVisibility(View.GONE);
            btnAdd.setVisibility(View.GONE);
        }
        if (tinyDB.getString(LocalConstants.TINYDB_KEYS.TEMP_SCREEN_NAME).equalsIgnoreCase(SCHEDULE_TEACHER)) {
            txtPageTitle.setText("Schedule");
            imgbtnAdd.setVisibility(View.GONE);
            btnAdd.setVisibility(View.GONE);
        } else {
            txtPageTitle.setText("Time Table");
        }
        showSheduleViewFragment();
    }

    private void showSheduleViewFragment() {
        layoutTabs.setVisibility(View.GONE);
        layoutFragment.setVisibility(View.VISIBLE);
        getSupportFragmentManager().beginTransaction().setCustomAnimations(
                R.anim.slide_in,  // enter
                R.anim.fade_out,  // exit
                R.anim.fade_in,   // popEnter
                R.anim.slide_out  // popExit
        ).replace(R.id.fragmentFrame, new TimeTableScheduleViewFragment(mContext)).commit();
    }

    private void showFragment() {
       /* layoutTabs.setVisibility(View.GONE);
        layoutFragment.setVisibility(View.VISIBLE);
                *//*Intent intent=new Intent(TimeTableTActivity.this,TimeTableCreateActivity.class);
                startActivity(intent);*//*
        getSupportFragmentManager().beginTransaction().setCustomAnimations(
                R.anim.slide_in,  // enter
                R.anim.fade_out,  // exit
                R.anim.fade_in,   // popEnter
                R.anim.slide_out  // popExit
        ).replace(R.id.fragmentFrame, new CreateTimeTableSchoolFragment(mContext)).commit();*/

        layoutTabs.setVisibility(View.GONE);
        layoutFragment.setVisibility(View.VISIBLE);
        getSupportFragmentManager().beginTransaction().setCustomAnimations(
                R.anim.slide_in,  // enter
                R.anim.fade_out,  // exit
                R.anim.fade_in,   // popEnter
                R.anim.slide_out  // popExit
        ).replace(R.id.fragmentFrame, new TimeTableScheduleCreateFragment(mContext)).commit();
        //).replace(R.id.fragmentFrame, new TimeTableCreateFragment(mContext)).commit();
    }

    public void showTimeFragment() {
        layoutTabs.setVisibility(View.GONE);
        layoutFragment.setVisibility(View.VISIBLE);
        getSupportFragmentManager().beginTransaction().setCustomAnimations(
                R.anim.slide_in,  // enter
                R.anim.fade_out,  // exit
                R.anim.fade_in,   // popEnter
                R.anim.slide_out  // popExit
        ).replace(R.id.fragmentFrame, new TimeTableScheduleCreateFragment(mContext)).commit();
        //).replace(R.id.fragmentFrame, new TimeTableCreateFragment(mContext)).commit();
    }

    //@OnClick(R.id.imgBack)
    private void Back() {
        super.onBackPressed();
    }

    private void getNoDays() {
        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<TimeTableModel> call = apiService.getNoOfDays();
        call.enqueue(new Callback<TimeTableModel>() {
            @Override
            public void onResponse(Call<TimeTableModel> call, Response<TimeTableModel> response) {
                //int statusCode = response.code();
                try {
                    timeTableModel = response.body();

                    if (timeTableModel != null) {
                        noOfTabs = Integer.valueOf(timeTableModel.getNoOfDays());
                    }
                    fragment = null;
                    viewPagerAdapter = new ViewPagerAdapterInner(getSupportFragmentManager(), noOfTabs);
                    viewPager = findViewById(R.id.viewpager);
                    viewPager.setOffscreenPageLimit(1);
                    tinyDB.putString("DayId", "1");
                    if (fragment == null) {
                        viewPager.setAdapter(viewPagerAdapter);
                    }
                    tabLayout = findViewById(R.id.tabs);
                    tabLayout.setupWithViewPager(viewPager);
                    tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                        @Override
                        public void onTabSelected(TabLayout.Tab tab) {
                            int i = tab.getPosition();

                            String tabId = String.valueOf(i + 1);
                            tinyDB.putString("DayId", tabId);
                            Log.e(mTAG, "onTabSelected=" + tab.getText() + tabId);
                        }

                        @Override
                        public void onTabUnselected(TabLayout.Tab tab) {

                        }

                        @Override
                        public void onTabReselected(TabLayout.Tab tab) {

                        }
                    });

                    // isAllTabLoaded = true;

                } catch (Exception ex) {
                    Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<TimeTableModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public class ViewPagerAdapterInner extends FragmentStatePagerAdapter {
        private int noOfItems;

        public ViewPagerAdapterInner(FragmentManager fm, int noOfItems) {
            super(fm);
            this.noOfItems = noOfItems;
        }

        @Override
        public Fragment getItem(int position) {
            fragment = TimeTableTFragment.newInstance(position);
            return fragment;
        }

        @Override
        public int getCount() {
            return noOfItems;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (tinyDB.getString(LOCALE).equalsIgnoreCase(LOCALE_PB)) {
                return dayArray_PB[position];
            } else {
                return dayArray[position];
            }
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

    }

}
