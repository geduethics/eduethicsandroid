package com.myliveclassroom.eduethics.Fragments.HomeFragemtns;

import android.Manifest;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.transition.TransitionInflater;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import com.bumptech.glide.Glide;
import com.myliveclassroom.eduethics.Activity.MainActivity;
import com.myliveclassroom.eduethics.Adapter.FragmentAdapter;
import com.myliveclassroom.eduethics.Fragments.HomeClassroomFragment;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.ExtraFunctions;
import com.myliveclassroom.eduethics.Utils.GlobalVariables;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;


import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static android.content.ClipDescription.MIMETYPE_TEXT_PLAIN;


public class StudyMaterialFragment extends Fragment {

    Context context;

    TabLayout tabLayout;
    ViewPager2 pager2;
    FragmentAdapter adapter;


    StorageReference storageRef = FirebaseStorage.getInstance().getReferenceFromUrl(LocalConstants.STORAGE_REF);    //change the url according to your firebase app


    public StudyMaterialFragment(Context context) {
        this.context = context;
    }

    public View root;


    @Override
    public void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TransitionInflater inflater = TransitionInflater.from(requireContext());
        setEnterTransition(inflater.inflateTransition(R.transition.slide_right));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_study_material, container, false);

        /*if (GlobalVariables.isStudent)
            ((TextView) root.findViewById(R.id.tit)).setText("Study Material posted by the \nteacher can be found here");
*/
        //FirstTask();
        return root;
    }

    public void FirstTask() {
        selected = null;
        selectedBitmap = null;
        MainActivity.toolbarImageFilter.setOnClickListener(v -> Toast.makeText(context, "TOAST", Toast.LENGTH_SHORT).show());
        MainActivity.TO_WHICH_FRAG = LocalConstants.TO_FRAG.TO_CLASSROOM;
        pd = root.findViewById(R.id.progress);


        if (HomeClassroomFragment.from == LocalConstants.TYPE.STUDENT) {
            root.findViewById(R.id.submit).setVisibility(View.GONE);
        } else {
            root.findViewById(R.id.submit).setOnClickListener(v -> upload());
        }

        tabLayout = root.findViewById(R.id.tab_layout);
        pager2 = root.findViewById(R.id.pageViewerHomeClassroom);

        runTask();

    }


    Dialog dialog;

    private void upload() {

        dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.dialog_study_material_create);
        dialog.findViewById(R.id.youtubeVideo).setOnClickListener(v -> {
            dialog.findViewById(R.id.youtubelayout).setVisibility(View.VISIBLE);
            dialog.findViewById(R.id.imageordoclayout).setVisibility(View.GONE);
        });

        dialog.findViewById(R.id.pastelink).setOnClickListener(v -> {
            ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
            if ((clipboard.getPrimaryClipDescription().hasMimeType(MIMETYPE_TEXT_PLAIN)) && (clipboard.hasPrimaryClip())){
                ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);
                ((EditText)dialog.findViewById(R.id.ytLink)).setText(item.getText().toString());
            }
        });

        dialog.findViewById(R.id.imageOrDucument).setOnClickListener(v -> {
            dialog.findViewById(R.id.youtubelayout).setVisibility(View.GONE);
            dialog.findViewById(R.id.imageordoclayout).setVisibility(View.VISIBLE);
        });

        dialog.findViewById(R.id.camera).setOnClickListener(v -> {
            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_DENIED)
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, LocalConstants.REQ_CODE.CAMERA_REQUEST);
            else {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, LocalConstants.PERMISSION_CODE.CAMERA_REQUEST);
            }
        });

        dialog.findViewById(R.id.pdf).setOnClickListener(v -> {
            Intent chooseFile;
            Intent intent;
            chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
            chooseFile.setType("application/pdf");
            intent = Intent.createChooser(chooseFile, "Choose a file");
            startActivityForResult(intent, LocalConstants.PERMISSION_CODE.PICKFILE_RESULT_CODE);
        });

        dialog.findViewById(R.id.image).setOnClickListener(v -> {
            Intent i = new Intent();
            i.setType("image/*");
            i.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(i, "Select Picture"), LocalConstants.PERMISSION_CODE.SELECT_PICTURE);
        });


        dialog.findViewById(R.id.upload).setOnClickListener(v -> {
            uploadNow();

        });
        dialog.findViewById(R.id.cancel).setOnClickListener(v -> dialog.dismiss());

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }


    private void uploadNow() {
        pd.setVisibility(View.VISIBLE);
        boolean allDone = true;
        if (((EditText) dialog.findViewById(R.id.topic)).getText().toString().equals("")) {
            ((EditText) dialog.findViewById(R.id.topic)).setError("Required");
            allDone = false;
        }
        if (dialog.findViewById(R.id.youtubelayout).getVisibility() == View.VISIBLE) {
            if (((EditText) dialog.findViewById(R.id.ytLink)).getText().toString().equals("")) {
                ((EditText) dialog.findViewById(R.id.ytLink)).setError("Required");
                allDone = false;
            }
        }


        if (allDone) {
            if (dialog.findViewById(R.id.youtubelayout).getVisibility() == View.VISIBLE) {
                postToFirebase(((EditText) dialog.findViewById(R.id.ytLink)).getText().toString());
            } else {


                if (selectedBitmap != null) {
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    byte[] data = baos.toByteArray();

                    final String randomKey = UUID.randomUUID().toString();

                    UploadTask uploadTask = storageRef.child("studyMaterialData/" + HomeClassroomFragment.classId + "/" + randomKey + ".jpg").putBytes(data);
                    uploadTask.addOnFailureListener(exception -> {
                        if (getActivity() == null) return;
                        Toast.makeText(getContext(), "failed", Toast.LENGTH_SHORT).show();
                    }).addOnSuccessListener(taskSnapshot -> {

                        postToFirebase(taskSnapshot.getMetadata().getPath());
                    });
                } else {
                    final String randomKey = UUID.randomUUID().toString();
                    String extension;
                    if (selected.getPath().contains(".")) {
                        extension = selected.getPath().substring(selected.getPath().lastIndexOf("."));
                    } else {
                        extension = ".pdf";
                    }

                    StorageReference riversRef = storageRef.child(("studyMaterialData/" + HomeClassroomFragment.classId + "/" + randomKey + extension));

                    riversRef.putFile(selected)
                            .addOnSuccessListener(taskSnapshot -> {
                                postToFirebase(taskSnapshot.getMetadata().getPath());
                            })
                            .addOnFailureListener(e -> {
                                if (getActivity() == null) return;
                                Toast.makeText(getContext(), "Unable to post your Image !", Toast.LENGTH_SHORT).show();
                            });
                }
            }
        } else {
            pd.setVisibility(View.GONE);
        }
    }

    private void postToFirebase(String s) {
        FirebaseFirestore.getInstance().collection("classroom").document(HomeClassroomFragment.classId).get().addOnSuccessListener(documentSnapshot -> {
            List<Map<String, Object>> data = (List) documentSnapshot.get("studyMaterial");
            Map<String, Object> map = new HashMap<>();
            Calendar calendar = Calendar.getInstance();
            map.put("time", calendar.get(Calendar.HOUR) + ":" + calendar.get(Calendar.MINUTE) + ":" + calendar.get(Calendar.SECOND));
            map.put("topic", ((EditText) dialog.findViewById(R.id.topic)).getText().toString());
            map.put("description", ((EditText) dialog.findViewById(R.id.dec)).getText().toString());
            map.put("name", s);
            data.add(map);
            FirebaseFirestore.getInstance().collection("classroom").document(HomeClassroomFragment.classId).update("studyMaterial", data);
            pd.setVisibility(View.GONE);
            dialog.dismiss();
        });
    }

    View pd;


    List<Map<String, Object>> data2 = new ArrayList<>();

    Uri selected = null;
    Bitmap selectedBitmap = null;

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data == null) return;
        switch (requestCode) {
            case LocalConstants.PERMISSION_CODE.PICKFILE_RESULT_CODE:
                try {
                    selected = data.getData();
                    temp1(0);
                    selectedBitmap = null;
                } catch (Exception e) {
                    Toast.makeText(context, "Try Again!", Toast.LENGTH_SHORT).show();
                }
                break;
            case LocalConstants.PERMISSION_CODE.SELECT_PICTURE:
                try {
                    selected = data.getData();
                    temp1(1);
                    selectedBitmap = null;
                } catch (Exception e) {
                    Toast.makeText(context, "Try Again!", Toast.LENGTH_SHORT).show();
                }
                break;
            case LocalConstants.PERMISSION_CODE.CAMERA_REQUEST:
                try {
                    selectedBitmap = (Bitmap) data.getExtras().get("data");
                    temp1(2);
                } catch (Exception e) {
                    Toast.makeText(context, "Try Again!", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                Toast.makeText(context, "Network Error", Toast.LENGTH_SHORT).show();
                ;
        }

    }


    private void temp1(int i) {
        dialog.findViewById(R.id.image).setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.edit_text_selector, null));
        dialog.findViewById(R.id.pdf).setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.edit_text_selector, null));
        dialog.findViewById(R.id.camera).setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.edit_text_selector, null));
        if (i == 0) {
            dialog.findViewById(R.id.pdf).setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.second_btn, null));
        } else if (i == 1) {
            dialog.findViewById(R.id.image).setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.second_btn, null));
        } else if (i == 2) {
            dialog.findViewById(R.id.camera).setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.second_btn, null));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == LocalConstants.REQ_CODE.CAMERA_REQUEST) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "camera permission granted", Toast.LENGTH_LONG).show();
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, LocalConstants.PERMISSION_CODE.CAMERA_REQUEST);
            } else {
                Toast.makeText(getActivity(), "camera permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }


    public void runTask() {
        root.findViewById(R.id.progress).setVisibility(View.VISIBLE);
        FirebaseFirestore.getInstance()
                .collection("classroom")
                .document(HomeClassroomFragment.classId)
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        data2 = (List) documentSnapshot.get("studyMaterial");
                        if (getActivity() == null) return;
                        forward();
                    }
                });
    }


    private void forward() {

        if (getActivity() == null) {
            return;
        }

        List<Map<String, Object>> images = new ArrayList<>();
        List<Map<String, Object>> docs = new ArrayList<>();
        List<Map<String, Object>> links = new ArrayList<>();


        for (Map<String, Object> datum : data2) {
            if (datum.get("name").toString().endsWith("pdf")) {
                docs.add(datum);
            } else if (datum.get("name").toString().endsWith("jpeg") || datum.get("name").toString().endsWith("png") || datum.get("name").toString().endsWith("jpg")) {
                images.add(datum);
            } else {
                links.add(datum);
            }
        }


        FragmentManager fm = getActivity().getSupportFragmentManager();
        ArrayList<Fragment> fragments = new ArrayList<>();
        tabLayout.removeAllTabs();

        fragments.add(new DocsFragment(docs, this));
        fragments.add(new ImagesFragment(images, this));
        fragments.add(new LinksFragment(links, this));

        adapter = new FragmentAdapter(fm, getLifecycle(), fragments);
        pager2.setAdapter(adapter);

        tabLayout.addTab(tabLayout.newTab().setText("Docs"));
        tabLayout.addTab(tabLayout.newTab().setText("Images"));
        tabLayout.addTab(tabLayout.newTab().setText("Links"));

        tabLayout.setTabGravity(TabLayout.GRAVITY_CENTER);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);


        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                pager2.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        pager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                tabLayout.selectTab(tabLayout.getTabAt(position));

            }
        });


        pager2.setCurrentItem(0);
        root.findViewById(R.id.progress).setVisibility(View.GONE);
    }


    public static class LinksFragment extends Fragment {

        List<Map<String, Object>> data;
        Fragment fragment;

        public LinksFragment(List<Map<String, Object>> data, Fragment fragment) {
            this.data = data;
            this.fragment = fragment;
        }

        @Nullable
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View root = inflater.inflate(R.layout.recycler_view, container, false);
            RecyclerView RECYCLER_VIEW = root.findViewById(R.id.RECYCLER_VIEW);
            RECYCLER_VIEW.setLayoutManager(new LinearLayoutManager(getContext()));
            RECYCLER_VIEW.setAdapter(new adapter(data, "links", fragment));
            if (data.size() == 0) {
                root.findViewById(R.id.empty).setVisibility(View.VISIBLE);
                ((TextView) root.findViewById(R.id.emptyMessage)).setText("No Links Found!");
            }
            return root;
        }
    }

    public static class DocsFragment extends Fragment {

        List<Map<String, Object>> data;
        Fragment fragment;

        public DocsFragment(List<Map<String, Object>> data, Fragment fragment) {
            this.data = data;
            this.fragment = fragment;
        }

        @Nullable
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View root = inflater.inflate(R.layout.recycler_view, container, false);
            RecyclerView RECYCLER_VIEW = root.findViewById(R.id.RECYCLER_VIEW);
            RECYCLER_VIEW.setLayoutManager(new LinearLayoutManager(getContext()));
            RECYCLER_VIEW.setAdapter(new adapter(data, "docs", fragment));
            if (data.size() == 0) {
                root.findViewById(R.id.empty).setVisibility(View.VISIBLE);
                ((TextView) root.findViewById(R.id.emptyMessage)).setText("No Documents Found!");
            }
            return root;
        }

    }

    public static class ImagesFragment extends Fragment {

        List<Map<String, Object>> data;
        Fragment fragment;

        public ImagesFragment(List<Map<String, Object>> data, Fragment fragment) {
            this.data = data;
            this.fragment = fragment;
        }

        @Nullable
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View root = inflater.inflate(R.layout.recycler_view, container, false);
            RecyclerView RECYCLER_VIEW = root.findViewById(R.id.RECYCLER_VIEW);
            RECYCLER_VIEW.setLayoutManager(new LinearLayoutManager(getContext()));
            RECYCLER_VIEW.setAdapter(new adapter(data, "image", fragment));
            if (data.size() == 0) {
                root.findViewById(R.id.empty).setVisibility(View.VISIBLE);
                ((TextView) root.findViewById(R.id.emptyMessage)).setText("No Images Found!");
            }
            return root;
        }


    }

    public static class adapter extends RecyclerView.Adapter<adapter.vh> {

        List<Map<String, Object>> data;
        String type;
        Fragment context;

        public adapter(List<Map<String, Object>> data, String type, Fragment context) {
            this.type = type;
            this.data = data;
            this.context = context;
        }

        @NonNull
        @Override
        public vh onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new vh(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_study_material, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull vh holder, int position) {
            if (type.equals("image")) {
                Glide.with(context).load(R.drawable.ic_image).into((AppCompatImageView) holder.itemView.findViewById(R.id.imageType));
            } else if (type.equals("docs")) {
                Glide.with(context).load(R.drawable.ic_attachment_pin).into((AppCompatImageView) holder.itemView.findViewById(R.id.imageType));
            } else try {
                Picasso.get().load(Uri.parse("http://img.youtube.com/vi/" + data.get(position).get("name").toString() + "/0.jpg"))  .error(ResourcesCompat.getDrawable(context.getResources(), R.drawable.error_image, null)).into((RoundedImageView) holder.itemView.findViewById(R.id.youtubeImage));
                holder.itemView.findViewById(R.id.imageType).setVisibility(View.GONE);
                holder.itemView.findViewById(R.id.youtubeImage).setVisibility(View.VISIBLE);
            } catch (Exception e) {
                holder.itemView.findViewById(R.id.imageType).setVisibility(View.VISIBLE);
                holder.itemView.findViewById(R.id.youtubeImage).setVisibility(View.GONE);
            }

            try {
                ((AppCompatTextView) holder.itemView.findViewById(R.id.time)).setText(data.get(position).get("time").toString());
                ((AppCompatTextView) holder.itemView.findViewById(R.id.nameImage)).setText(data.get(position).get("topic").toString());
                ((TextView) holder.itemView.findViewById(R.id.dec)).setText(data.get(position).get("description").toString());
                if (data.get(position).get("description").toString().equals(""))
                    holder.itemView.findViewById(R.id.dec).setVisibility(View.GONE);
            } catch (Exception e) {
                ((AppCompatTextView) holder.itemView.findViewById(R.id.nameImage)).setText(data.get(position).get("name").toString());
                ((TextView) holder.itemView.findViewById(R.id.dec)).setVisibility(View.GONE);
            }
            if (!GlobalVariables.isStudent) {
                int id = View.generateViewId();
                MainActivity.ids.put(id, "sm;"+data.get(position).get("name").toString());
                holder.itemView.setId(id);
                context.getActivity().registerForContextMenu(holder.itemView);
            }
            holder.itemView.setOnClickListener(v -> {
                if (type.equals("links")) {
                    Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + data.get(position).get("name")));
                    Intent webIntent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://www.youtube.com/watch?v=" + data.get(position).get("name")));
                    try {
                        context.startActivity(appIntent);
                    } catch (ActivityNotFoundException ex) {
                        context.startActivity(webIntent);
                    }
                } else {
                    ExtraFunctions.downloadImageAndShow(null, data.get(position).get("name").toString(), context.getActivity());
                }
            });


        }


        @Override
        public int getItemCount() {
            return data.size();
        }

        class vh extends RecyclerView.ViewHolder {
            public vh(@NonNull View itemView) {
                super(itemView);
            }
        }
    }


}