package com.myliveclassroom.eduethics.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.myliveclassroom.eduethics.Models.FeeModel;
import com.myliveclassroom.eduethics.Models.PaymentMethodModel;
import com.myliveclassroom.eduethics.R;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class PaymentMethodAdapter extends RecyclerView.Adapter<PaymentMethodAdapter.ViewHolder> {
    List<PaymentMethodModel> paymentMethodModelList;
    Context mContext;
    Fragment fragment;

    public PaymentMethodAdapter(Fragment fragment, Context context, List<PaymentMethodModel> paymentMethodModels) {
        this.paymentMethodModelList = paymentMethodModels;
        mContext = context;
        this.fragment=fragment;
    }

    @Override
    public PaymentMethodAdapter.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.fee_payment_methods, parent, false));
    }

    @Override
    public void onBindViewHolder(@NotNull PaymentMethodAdapter.ViewHolder holder, int position) {
        holder.txtPaymentMethod.setText(paymentMethodModelList.get(position).getPaymentMethod());
        Glide.with(mContext)
                .asBitmap()
                .skipMemoryCache(true)
                .load(paymentMethodModelList.get(position).getImgPaymentIcon())
                .into(holder.imgPaymentIcon)
                ;
    }

    public void updateList(List<PaymentMethodModel> list) {
        paymentMethodModelList = list;
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return paymentMethodModelList == null ? 0 : paymentMethodModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgPaymentIcon;
        TextView txtPaymentMethod;

        public ViewHolder(@NotNull View itemView) {
            super(itemView);
            imgPaymentIcon = itemView.findViewById(R.id.imgPaymentIcon);
            txtPaymentMethod = itemView.findViewById(R.id.txtBtnPaymentText);
        }
    }


}
