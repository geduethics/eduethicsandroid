package com.myliveclassroom.eduethics.Fragments.WebAppFrag;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.myliveclassroom.eduethics.Activity.MainActivity;
import com.myliveclassroom.eduethics.Adapter.FeeSchoolAdapter;
import com.myliveclassroom.eduethics.Models.FeeModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.TinyDB;

import java.lang.reflect.Type;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.PROFILE;
import static com.myliveclassroom.eduethics.Utils.LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID;

public class FeeStudentListFragment extends Fragment {

    View root;
    List<FeeModel> feesModelList;
    RecyclerView recyclerViewFee;
    Context mContext;
    FeeSchoolAdapter feeSchoolAdapter;
    TinyDB tinyDB;
    FeeModel mFeeModel;

    public FeeStudentListFragment(Context context,FeeModel feeModel) {
        mContext=context;
        this.mFeeModel=feeModel;
        tinyDB=new TinyDB(mContext);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root= inflater.inflate(R.layout.fragment_fee_student_list, container, false);
        MainActivity.TO_WHICH_FRAG = LocalConstants.TO_FRAG.TO_FEES_SUMMARY;
        initRecyclerView();
        return root;
    }

    private void initRecyclerView() {
        recyclerViewFee = root.findViewById(R.id.recyclerViewFeeStudentList);
        recyclerViewFee.setLayoutManager(new LinearLayoutManager(mContext));
        feeSchoolAdapter = new FeeSchoolAdapter(FeeStudentListFragment.this, mContext, feesModelList, LocalConstants.FEE_VIEWS.OTHER_SCREEN);
        recyclerViewFee.setAdapter(feeSchoolAdapter);
        listStudents();
    }

    private void listStudents() {
        FeeModel feeModel = new FeeModel();
        feeModel.setSchoolId(tinyDB.getString(SCHOOL_ID));
        feeModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));
        feeModel.setIsPaid("N");
        feeModel.setClassId(mFeeModel.getClassId());

        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)) {
            feeModel.setStudentId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
            feeModel.setClassId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID));
        }

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = null;
        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.SCHOOL)) {
            call = apiService.feeSchoolSelectStudentWise(feeModel);
        } else if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER)) {
            call = apiService.feeSchoolSelectStudentWise(feeModel);
        } else if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)) {
            call = apiService.feeSchoolSelectStudentWise(feeModel);
        }

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                try {
                    if (response.body() == null) {
                        Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (new Gson().fromJson(response.body().get("Status"), String.class).equalsIgnoreCase("fail")) {
                        Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    Type type = new TypeToken<List<FeeModel>>() {
                    }.getType();

                    feesModelList = new Gson().fromJson(response.body().getAsJsonArray("Response"), type);
                    feeSchoolAdapter.updateList(feesModelList);
                }
                catch (Exception ex) {
                    Log.e(mTAG, "error =" + ex.toString());

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, "Something went wrong! Timeout", Toast.LENGTH_SHORT).show();
            }
        });
    }
}