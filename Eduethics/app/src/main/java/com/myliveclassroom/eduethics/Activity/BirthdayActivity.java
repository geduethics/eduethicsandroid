package com.myliveclassroom.eduethics.Activity;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;


import com.myliveclassroom.eduethics.Adapter.BirthdayAdapter;
import com.myliveclassroom.eduethics.Adapter.BirthdaySliderViewPagerAdapter;
import com.myliveclassroom.eduethics.Models.UserModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;


public class BirthdayActivity extends BaseActivity {

    @BindView(R.id.txtPageTitle)
    TextView txtPageTitle;

    @BindView(R.id.imgBack)
    ImageView imgBack;

    @BindView(R.id.imgbtnAdd)
    ImageView imgbtnAdd;

    @BindView(R.id.btnAdd)
    TextView btnAdd;

    RecyclerView recyclerBirthday;
    ProgressBar progressBar;
    BirthdayAdapter birthdayAdapter;

    List<UserModel> userModelList;

    @BindView(R.id.layoutHead)
    LinearLayout layoutHead;

    ViewPager viewPager;

    @BindView(R.id.frameBirthDay)
    FrameLayout frameBirthDay;
    List<UserModel> userModelBirthDayList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.MyThemeNoBar);
        setContentView(R.layout.activity_birthday);
        _BaseActivity(this);
        ButterKnife.bind(this);
        init();
    }

    private void Back() {
        super.onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void init() {
        recyclerBirthday = findViewById(R.id.recyclerBirthday);
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);

        imgbtnAdd.setVisibility(View.INVISIBLE);
        imgBack.setVisibility(View.VISIBLE);
        btnAdd.setVisibility(View.GONE);
        btnAdd.setText("");

        txtPageTitle.setText(getString(R.string.home_birthday));
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Back();
            }
        });
        viewPager = (ViewPager) findViewById(R.id.viewPager);

        initRecyclerView();
    }

    private void initRecyclerView() {
        userModelList = new ArrayList<>();
        recyclerBirthday = findViewById(R.id.recyclerBirthday);
        recyclerBirthday.setLayoutManager(new LinearLayoutManager(mContext));
        getBirthdayList();
    }

    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    private void getBirthdayList2() {
        showProgress();
        UserModel userModel = new UserModel();
        userModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        userModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<List<UserModel>> call = apiService.BirthdayList(userModel);
        call.enqueue(new Callback<List<UserModel>>() {
            @Override
            public void onResponse(Call<List<UserModel>> call, Response<List<UserModel>> response) {
                hideProgress();
                try {
                    userModelList = response.body();
                    birthdayAdapter = new BirthdayAdapter(mContext, userModelList);
                    recyclerBirthday.setAdapter(birthdayAdapter);

                } catch (Exception ex) {
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<List<UserModel>> call, Throwable t) {
                hideProgress();
                Log.e(mTAG, "error =" + t.toString());
            }
        });
    }

    BirthdaySliderViewPagerAdapter birthdaySliderViewPagerAdapter;
    String dateTodayStr,birthDateStr;
    Date dateToday,birthDate;
    SimpleDateFormat dateformat = new SimpleDateFormat("dd-MMM");
    private void getBirthdayList() {
        progressBar.setVisibility(View.VISIBLE);
        Calendar startDate = Calendar.getInstance();
        userModelBirthDayList=new ArrayList<>();
        userModelList = new ArrayList<>();
        UserModel userModel = new UserModel();
        userModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        userModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<List<UserModel>> call = apiService.BirthdayList(userModel);
        call.enqueue(new Callback<List<UserModel>>() {
            @Override
            public void onResponse(Call<List<UserModel>> call, Response<List<UserModel>> response) {
                progressBar.setVisibility(View.GONE);
                try {
                    userModelList = response.body();
                    List<UserModel> tempList=new ArrayList<>();
                    if(userModelList.size()>0){
                        for (UserModel userModel1:userModelList) {
                            dateTodayStr = dateformat.format(startDate.getTime());
                            dateTodayStr=dateTodayStr+"-2020";
                            dateToday = dateformat.parse(dateTodayStr);
                            birthDateStr=userModel1.getDOB()+"-2020";
                            birthDate = dateformat.parse(birthDateStr);
                            if (birthDate.equals(dateToday)) {
                                userModelBirthDayList.add(userModel1);
                            }
                            else{
                                tempList.add(userModel1);
                            }
                        }
                    }
                    if (userModelBirthDayList.size() > 0) {
                        birthdaySliderViewPagerAdapter = new BirthdaySliderViewPagerAdapter(mContext, userModelBirthDayList);
                        viewPager.setAdapter(birthdaySliderViewPagerAdapter);
                        viewPager.setVisibility(View.VISIBLE);
                        frameBirthDay.setVisibility(View.VISIBLE);
                        autoSlide();
                    } else {
                        viewPager.setVisibility(View.GONE);
                        frameBirthDay.setVisibility(View.GONE);
                    }
                    if(tempList.size()>0){
                        layoutHead.setVisibility(View.VISIBLE);
                    }
                    birthdayAdapter = new BirthdayAdapter(mContext, tempList);
                    recyclerBirthday.setAdapter(birthdayAdapter);
                } catch (Exception ex) {
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<List<UserModel>> call, Throwable t) {
                Log.e(mTAG, "error =" + t.toString());
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    int currentPage = 0;
    Timer timer;
    final long DELAY_MS = 500;//delay in milliseconds before task is to be executed
    final long PERIOD_MS = 3000; // time in milliseconds between successive task executions.

    private void autoSlide() {
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == userModelBirthDayList.size() - 1) {
                    currentPage = 0;
                }
                viewPager.setCurrentItem(currentPage++, true);
            }
        };

        timer = new Timer(); // This will create a new Thread
        timer.schedule(new TimerTask() { // task to be scheduled
            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);
    }
}
