package com.myliveclassroom.eduethics.Fragments.TeacherFragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.myliveclassroom.eduethics.Activity.MainActivity;
import com.myliveclassroom.eduethics.Activity.VideoConfrenceActivity;
import com.myliveclassroom.eduethics.Adapter.TimeTableScheduleAdapter;
import com.myliveclassroom.eduethics.Models.TimeTableModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.CommonFunctions;
import com.myliveclassroom.eduethics.classes.TinyDB;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.PROFILE;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.SCHEDULE_TEACHER;


public class TimeTableScheduleCreateFragment extends Fragment {

    Context context;
    TinyDB tinyDB;
    Context mContext;
    Spinner spinnerClass;
    Spinner spinnerSubject;
    Spinner spinnerTeacher;

    List<TimeTableModel> classList;
    List<TimeTableModel> subjectList;
    List<TimeTableModel> teacherList;

    String mClassId = "0", mSubjectId = "0", mTeacherId = "0";
    int mSpinnerPosition = 0;

    LinearLayout layoutSchoolOptions;
    RecyclerView recyclerViewTimeTable;

    List<Map<String, Object>> data = new ArrayList<>();
    List<TimeTableModel> timeTableModelList;
    List<TimeTableModel> timeTableModelFilterList = new ArrayList<>();

    View root;
    TimeTableScheduleAdapter dayAdapter;
    Button txtBtnSubmitTimetable;
    final Calendar myCalendar = Calendar.getInstance();
    EditText txtScheduleDate;
    boolean isEdit = false;
    ProgressBar progressBar;

    public TimeTableScheduleCreateFragment(Context context) {
        mContext = context;
        tinyDB = new TinyDB(mContext);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_time_table_schedule_view, container, false);
        MainActivity.TO_WHICH_FRAG = LocalConstants.TO_FRAG.TO_HOME_CLASSROOM;
        txtBtnSubmitTimetable = root.findViewById(R.id.txtBtnSubmitTimetable);
        if (tinyDB.getString(LocalConstants.INSTITUTE_TYPE.TYPE).equalsIgnoreCase(LocalConstants.INSTITUTE_TYPE.SCHOOL)) {
            root.findViewById(R.id.layoutSchoolOptions).setVisibility(View.VISIBLE);

            txtBtnSubmitTimetable.setVisibility(View.VISIBLE);
            txtBtnSubmitTimetable.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (TimeTableModel tableModel : timeTableModelFilterList) {
                        if (tableModel.getIsTimeSet()) {
                            setTimeTable(tableModel);
                        }
                    }
                }
            });
        } else {
            root.findViewById(R.id.layoutSchoolOptions).setVisibility(View.GONE);
        }

        init();
        return root;
    }


    private void init() {
        progressBar = root.findViewById(R.id.progressBar);
        txtScheduleDate = (EditText) root.findViewById(R.id.txtScheduleDate);
        if (!tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID).equalsIgnoreCase("")) {
            mClassId = tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID);
        }
        bindSpinner();
        StudentOptions();


        timeTableModelList = new ArrayList<>();
        recyclerViewTimeTable = root.findViewById(R.id.recyclerFeeStudent);
        recyclerViewTimeTable.setLayoutManager(new LinearLayoutManager(context));
        dayAdapter = new TimeTableScheduleAdapter(mContext, TimeTableScheduleCreateFragment.this);
        recyclerViewTimeTable.setAdapter(dayAdapter);

        setDate();
        updateLabel();

        if (tinyDB.getString(LocalConstants.INSTITUTE_TYPE.TYPE).equalsIgnoreCase(LocalConstants.INSTITUTE_TYPE.SCHOOL)) {
            getClassData();
        } else {
            //getDays();
        }
        //getDays();
    }

    private void StudentOptions() {
        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)
        ) {
            if (txtBtnSubmitTimetable != null)
                txtBtnSubmitTimetable.setVisibility(View.GONE);
            mClassId = tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID);
            if (spinnerClass != null)
                spinnerClass.setVisibility(View.GONE);
            //root.findViewById(R.id.frameDate).setVisibility(View.GONE);
            if (txtScheduleDate != null) {
                txtScheduleDate.setText(tinyDB.getString(LocalConstants.TINYDB_KEYS.TEMP_DATE));
                txtScheduleDate.setEnabled(false);
                root.findViewById(R.id.imgCal).setVisibility(View.GONE);
            }
        }
        if (tinyDB.getString(LocalConstants.TINYDB_KEYS.TEMP_SCREEN_NAME).equalsIgnoreCase(SCHEDULE_TEACHER)) {
            txtBtnSubmitTimetable.setVisibility(View.GONE);
        }
    }

    private void bindSpinner() {
        spinnerClass = root.findViewById(R.id.spinnerClass);
        spinnerSubject = root.findViewById(R.id.spinnerSubject);
        spinnerTeacher = root.findViewById(R.id.spinnerTeacher);

    }

    private void setDate() {

        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };

        txtScheduleDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                tinyDB.putString(LocalConstants.TINYDB_KEYS.TEMP_DATE, "");
                new DatePickerDialog(mContext, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    private void updateLabel() {
        String myFormat = "dd-MMM-yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        txtScheduleDate.setText(sdf.format(myCalendar.getTime()));

        if (!tinyDB.getString(LocalConstants.TINYDB_KEYS.TEMP_DATE).equalsIgnoreCase("")) {
            txtScheduleDate.setText(tinyDB.getString(LocalConstants.TINYDB_KEYS.TEMP_DATE));
        }
        getDaySchedule();
    }

    private void getClassData() {
        TimeTableModel timeTableModel = new TimeTableModel();
        timeTableModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        timeTableModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = apiService.classList(timeTableModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                try {
                    if (response.body() == null) {
                        Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (response.body() != null) {
                        Type type = new TypeToken<List<TimeTableModel>>() {
                        }.getType();
                        classList = new Gson().fromJson(response.body().getAsJsonArray("Response"), type);
                        List<String> spinnerArray = new ArrayList<>();
                        spinnerArray.add("--Select Class--");
                        /*for (TimeTableModel str : classList) {
                            spinnerArray.add(str.getClassName());
                        }*/
                        for (int i = 0; i < classList.size(); i++) {
                            spinnerArray.add(classList.get(i).getClassName());
                            if (mClassId.equalsIgnoreCase(classList.get(i).getClassId()) && mClassId != "0" && mClassId != "") {
                                mSpinnerPosition = i;
                            }
                        }

                        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, spinnerArray);
                        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down vieww
                        spinnerClass.setAdapter(spinnerArrayAdapter);

                        //SET last selected item
                        if (mClassId != "0" && mClassId != "") {
                            spinnerClass.setSelection(mSpinnerPosition + 1);
                        }
                        spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int myPosition, long myID) {
                                if (myPosition > 0) {
                                    mSpinnerPosition = myPosition;
                                    mClassId = classList.get(myPosition - 1).getClassId();
                                    //listSubjects();
                                    getDaySchedule();
                                } else {
                                    mSpinnerPosition = myPosition;
                                    mClassId = "0";
                                    //listSubjects();
                                }
                                //Log.i("renderSpinner -> ", "onItemSelected: " + myPosition + "/" + myID);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parentView) {
                                // your code here
                            }
                        });
                    }
                } catch (Exception ex) {
                    Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getDaySchedule() {

        if (txtScheduleDate.getText().toString().trim().equalsIgnoreCase("")) {
            Toast.makeText(mContext, "Please select Schedule Date", Toast.LENGTH_SHORT).show();
            return;
        }


        timeTableModelList = new ArrayList<>();
        TimeTableModel timeTableModel = new TimeTableModel();
        if (mClassId == null || mClassId.equalsIgnoreCase("0")) {
            return;
        }
        timeTableModel.setUserId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
        timeTableModel.setTeacherId(mTeacherId);
        timeTableModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        timeTableModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));
        timeTableModel.setClassId(mClassId);
        timeTableModel.setSubjectId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SUBJECT_ID));
        timeTableModel.setProfileType(tinyDB.getString(PROFILE));
        timeTableModel.setInstituteType(tinyDB.getString(LocalConstants.INSTITUTE_TYPE.TYPE));

        timeTableModel.setScheduleDate(txtScheduleDate.getText().toString());

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = apiService.getTimeTableDaySchedule(timeTableModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.e(mTAG, "userModelList" + response.body().toString());
                try {
                    timeTableModelFilterList = new ArrayList<>();
                    timeTableModelList = new ArrayList<>();

                    if (response.body() != null) {
                        Type type = new TypeToken<List<TimeTableModel>>() {
                        }.getType();
                        timeTableModelList = new Gson().fromJson(response.body().getAsJsonArray("Response"), type);

                        boolean add = true;
                        for (TimeTableModel timeTableModel1 : timeTableModelList) {
                            if (timeTableModelFilterList.size() > 0) {
                                for (TimeTableModel timeTableModel2 : timeTableModelFilterList) {
                                    if (timeTableModel1.getSubjectId().equalsIgnoreCase(timeTableModel2.getSubjectId())) {
                                        add = false;
                                        break;
                                    } else {
                                        add = true;
                                    }
                                }
                            }
                            if (add) {
                                if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)) {
                                    if(timeTableModel1.getIsTimeSet()){
                                    timeTableModelFilterList.add(timeTableModel1);
                                }}else{
                                    timeTableModelFilterList.add(timeTableModel1);
                                }

                            }
                        }

                        dayAdapter = new TimeTableScheduleAdapter(mContext, TimeTableScheduleCreateFragment.this);
                        recyclerViewTimeTable.setAdapter(dayAdapter);

                        dayAdapter.updateList(timeTableModelFilterList, timeTableModelList);
                        Log.e(mTAG, "userModelList" + timeTableModelList.toString());
                    }
                } catch (Exception ex) {
                    Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setTimeTable(TimeTableModel timeTableModel) {
        timeTableModel.setScheduleDate(txtScheduleDate.getText().toString());
        timeTableModel.setUserId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
        timeTableModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        timeTableModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));
        timeTableModel.setClassId(mClassId);
        //timeTableModel.setSubjectId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SUBJECT_ID));
        if (tinyDB.getString(LocalConstants.INSTITUTE_TYPE.TYPE).equalsIgnoreCase(LocalConstants.INSTITUTE_TYPE.SCHOOL)) {
            if (txtScheduleDate.getText().toString().trim().equalsIgnoreCase("")) {
                Toast.makeText(mContext, "Please enter Schedule Date", Toast.LENGTH_SHORT).show();
                return;
            }
        } else {
            timeTableModel.setTeacherId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
        }

        timeTableModel.setInstituteType(tinyDB.getString(LocalConstants.INSTITUTE_TYPE.TYPE));

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = null;
        if (tinyDB.getString(LocalConstants.INSTITUTE_TYPE.TYPE).equalsIgnoreCase(LocalConstants.INSTITUTE_TYPE.SCHOOL)) {
            call = apiService.setTimeTableDays(timeTableModel);
        } else {
            call = apiService.setTimeTableDays(timeTableModel);
        }

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Toast.makeText(mContext, "Done", Toast.LENGTH_SHORT).show();
                try {
                    getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(
                            R.anim.slide_in,  // enter
                            R.anim.fade_out,  // exit
                            R.anim.fade_in,   // popEnter
                            R.anim.slide_out  // popExit
                    ).replace(R.id.fragmentFrame, new TimeTableScheduleViewFragment(mContext)).commit();
                    Log.e(mTAG, "userModelList" + response.body().toString());
                }
                catch (Exception ex){
                    Log.e(mTAG, ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void datePicked(int position, int hourOfDayFrom, int minFrom, int hourOfDayTo, int minTo, boolean b) {
        timeTableModelList.get(position).setTimeFrom(hourOfDayFrom + ":" + minFrom);
        timeTableModelList.get(position).setTimeTo(hourOfDayTo + ":" + minTo);
        timeTableModelList.get(position).setIsTimeSet(b);
        isEdit = true;

        TimeTableModel timeTableModel = timeTableModelList.get(position);
        setTimeTable(timeTableModel);
        /*FirebaseFirestore.getInstance()
                .collection("classroom")
                .document(HomeClassroomFragment.classId)
                .update("timetable", data);*/
    }


    AlertDialog alertDialog;

    public void teacherSelect(int postition, List<TimeTableModel> timeTableModel, TextView txtViewTeacher, TextView txtTeacherId) {
        alertDialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);

        List<String> teacherList = new ArrayList<>();
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(mContext, android.R.layout.select_dialog_item);

        for (TimeTableModel timeTableModel1 : timeTableModel) {
            arrayAdapter.add(timeTableModel1.getTeacherName());
            teacherList.add(timeTableModel1.getTeacherId());
        }

        dialogBuilder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialogBuilder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strName = arrayAdapter.getItem(which);
                strName = CommonFunctions.capitalize(strName);
                if (strName.equalsIgnoreCase("")) {
                    return;
                }
                txtTeacherId.setText(teacherList.get(which));
                txtViewTeacher.setText(strName);
                timeTableModelFilterList.get(postition).setTeacherId(teacherList.get(which));
                timeTableModelFilterList.get(postition).setTeacherName(strName);
            }
        });

        alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    String mRoomName = "EduEthicsLiveClassRoom";

    public void goLive() {
        Toast.makeText(mContext, "Go Live", Toast.LENGTH_SHORT).show();
        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)) {
            tinyDB.putString(LocalConstants.Jitsi_CONSTANTS.PROFILE, LocalConstants.Jitsi_CONSTANTS.JOIN_AS_STUDENT);
        } else if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER)
                || tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER_SCHOOL)) {
            tinyDB.putString(LocalConstants.Jitsi_CONSTANTS.PROFILE, LocalConstants.Jitsi_CONSTANTS.JOIN_AS_TUTOR);
        }
        root.findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
        String id = ((int) (Math.random() * 100000000)) + "";
        startActivity(new Intent(getContext(), VideoConfrenceActivity.class).putExtra("roomId", mRoomName + tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID)).putExtra("classId", tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID)));

    }

    @Override
    public void onResume() {
        super.onResume();
        progressBar.setVisibility(View.GONE);
    }
}