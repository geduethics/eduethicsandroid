package com.myliveclassroom.eduethics.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.myliveclassroom.eduethics.Models.CircularModel;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.classes.TinyDB;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.TINY_CONSTANT_HomeworkNotification;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {
    TinyDB tinyDB;
    Context mContext;
    List<CircularModel> circularModelList;

    public NotificationAdapter(Context context, List<CircularModel> circularModels){
        circularModelList = circularModels;
        mContext = context;
        tinyDB=new TinyDB(mContext);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.circular_adapter, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.txtCircularDate.setText(circularModelList.get(position).getCircularDate());

        holder.txtAttachCountHead.setVisibility(View.GONE);
        holder.txtBtnDeleteCircular.setVisibility(View.GONE);
        holder.imgAttIconHead.setVisibility(View.GONE);

        if(circularModelList.get(position).getCircularType().contains(tinyDB.getString(TINY_CONSTANT_HomeworkNotification))){
            holder.imgIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_home_book_png));
            holder.txtCircularHead.setText(circularModelList.get(position).getCircularHead());
        }
    }

    @Override
    public int getItemCount() {
        return (circularModelList==null?0:circularModelList.size());
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtCircularHead)
        TextView txtCircularHead;

        @BindView(R.id.txtCircularDate)
        TextView txtCircularDate;

        @BindView(R.id.txtAttachCountHead)
        TextView txtAttachCountHead;

        @BindView(R.id.txtBtnDeleteCircular)
        TextView txtBtnDeleteCircular;

        @BindView(R.id.imgAttIconHead)
        ImageView imgAttIconHead;

        @BindView(R.id.imgIcon)
        ImageView imgIcon;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
