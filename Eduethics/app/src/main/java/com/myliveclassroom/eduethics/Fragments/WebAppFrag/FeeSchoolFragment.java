package com.myliveclassroom.eduethics.Fragments.WebAppFrag;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.myliveclassroom.eduethics.Activity.MainActivity;
import com.myliveclassroom.eduethics.Adapter.FeeSchoolAdapter;
import com.myliveclassroom.eduethics.AppEnvironment;
import com.myliveclassroom.eduethics.AppPreference;
import com.myliveclassroom.eduethics.Models.FeeModel;
import com.myliveclassroom.eduethics.Models.FeesModel;
import com.myliveclassroom.eduethics.Models.TimeTableModel;
import com.myliveclassroom.eduethics.MyApplication;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.TinyDB;
import com.payumoney.core.PayUmoneyConfig;
import com.payumoney.core.PayUmoneyConstants;
import com.payumoney.core.PayUmoneySdkInitializer;
import com.payumoney.sdkui.ui.utils.PayUmoneyFlowManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.APP_PACKAGE_NAME;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.PAYTM_MERCHANT_ID;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.PROFILE;
import static com.myliveclassroom.eduethics.Utils.LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID;

public class FeeSchoolFragment extends Fragment {

    View root;
    List<FeeModel> feesModelList;
    RecyclerView recyclerViewFee;
    Context mContext;
    FeeSchoolAdapter feeSchoolAdapter;
    TinyDB tinyDB;
    int mScreenID;

    public FeeSchoolFragment(Context context, int screen) {
        mContext = context;
        tinyDB = new TinyDB(context);
        mScreenID = 0;//screen;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_fee_school, container, false);
        MainActivity.TO_WHICH_FRAG = LocalConstants.TO_FRAG.TO_DASHBOARD;
        initRecyclerView();
        return root;
    }

    private void initRecyclerView() {
        recyclerViewFee = root.findViewById(R.id.recyclerViewFee);
        recyclerViewFee.setLayoutManager(new LinearLayoutManager(mContext));
        feeSchoolAdapter = new FeeSchoolAdapter(FeeSchoolFragment.this, mContext, feesModelList, mScreenID);
        recyclerViewFee.setAdapter(feeSchoolAdapter);
        listFee();
    }

    private void listFee() {
        FeeModel feeModel = new FeeModel();
        feeModel.setScreen(mScreenID + "");
        feeModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));

        feeModel.setSchoolId(tinyDB.getString(SCHOOL_ID));
        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)) {
            feeModel.setStudentId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
            feeModel.setClassId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID));
        }

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = null;
        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.SCHOOL)) {
            call = apiService.feeSchoolSelectByAdmin(feeModel);
        } else if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER)) {
            call = apiService.feeSchoolSelectByAdmin(feeModel);
        } else if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)) {
            call = apiService.feeSchoolSelectStudentWise(feeModel);
        }
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                try {
                    if (response.body() == null) {
                        Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (new Gson().fromJson(response.body().get("Status"), String.class).equalsIgnoreCase("fail")) {
                        Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    Type type = new TypeToken<List<FeeModel>>() {
                    }.getType();

                    feesModelList = new Gson().fromJson(response.body().getAsJsonArray("Response"), type);
                    feeSchoolAdapter.updateList(feesModelList);
                } catch (Exception ex) {

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(mTAG, "error =" + t.toString());
            }
        });
    }

    String isPaymentDone = "N";

    public void paynow_btnclick(FeeModel feeModel) {
        /*getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(
                R.anim.slide_in,  // enter
                R.anim.fade_out,  // exit
                R.anim.fade_in,   // popEnter
                R.anim.slide_out  // popExit
        ).replace(R.id.fragmentFrame, new FeePayFragment(mContext)).commit();*/

        String txnId = UUID.randomUUID().toString();
        feeModel.setTxnId(txnId);
        feeModel.setTxnStatus(LocalConstants.PAYMENT_TXN_STATUS.INITIATED);
        saveTxnDetail(feeModel);
        launchPayUMoneyFlow(feeModel);
    }

    public void viewFeeDetail_btnclick(FeeModel feeModel) {
        getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(
                R.anim.slide_in,  // enter
                R.anim.fade_out,  // exit
                R.anim.fade_in,   // popEnter
                R.anim.slide_out  // popExit
        ).replace(R.id.fragmentFrame, new FeeStudentListFragment(mContext, feeModel)).commit();
    }

    private void saveTxnDetail(FeeModel feeModel) {
        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call =  apiService.FeeSaveTxn(feeModel);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.e(mTAG, "userModelList" + response.body().toString());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private PayUmoneySdkInitializer.PaymentParam mPaymentParams;

    private AppPreference mAppPreference;

    private void launchPayUMoneyFlow(FeeModel feeModel) {

        PayUmoneyConfig payUmoneyConfig = PayUmoneyConfig.getInstance();

        //Use this to set your custom text on result screen button
        payUmoneyConfig.setDoneButtonText("Done");

        //Use this to set your custom title for the activity
        payUmoneyConfig.setPayUmoneyActivityTitle(getString(R.string.app_name));
        payUmoneyConfig.disableExitConfirmation(true);

        PayUmoneySdkInitializer.PaymentParam.Builder builder = new PayUmoneySdkInitializer.PaymentParam.Builder();

        double amount = 0;
        try {
            amount = 1;
        } catch (Exception e) {
            e.printStackTrace();
        }
        String txnId = feeModel.getTxnId(); //UUID.randomUUID().toString();  //System.currentTimeMillis() + "";
        //String txnId = "TXNID720431525261327973";
        String phone = feeModel.getMobile();
        String productName = "Fee Payment";
        String firstName = feeModel.getFatherName();
        String email = feeModel.getEmail();
        String udf1 = "";
        String udf2 = "";
        String udf3 = "";
        String udf4 = "";
        String udf5 = "";
        String udf6 = "";
        String udf7 = "";
        String udf8 = "";
        String udf9 = "";
        String udf10 = "";

        AppEnvironment appEnvironment = ((MyApplication) getActivity().getApplication()).getAppEnvironment();
        builder.setAmount(String.valueOf(amount))
                .setTxnId(txnId)
                .setPhone(phone)
                .setProductName(productName)
                .setFirstName(firstName)
                .setEmail(email)
                .setsUrl(appEnvironment.surl())
                .setfUrl(appEnvironment.furl())
                .setUdf1(udf1)
                .setUdf2(udf2)
                .setUdf3(udf3)
                .setUdf4(udf4)
                .setUdf5(udf5)
                .setUdf6(udf6)
                .setUdf7(udf7)
                .setUdf8(udf8)
                .setUdf9(udf9)
                .setUdf10(udf10)
                .setIsDebug(appEnvironment.debug())
                .setKey(appEnvironment.merchant_Key())
                .setMerchantId(appEnvironment.merchant_ID());

        try {
            mPaymentParams = builder.build();

            /*
             * Hash should always be generated from your server side.
             * */
            //    generateHashFromServer(mPaymentParams);

            /**//**
             * Do not use below code when going live
             * Below code is provided to generate hash from sdk.
             * It is recommended to generate hash from server side only.
             * */
            mPaymentParams = calculateServerSideHashAndInitiatePayment1(mPaymentParams);

            if (AppPreference.selectedTheme != -1) {
                PayUmoneyFlowManager.startPayUMoneyFlow(mPaymentParams, getActivity(), AppPreference.selectedTheme, mAppPreference.isOverrideResultScreen());
            } else {
                PayUmoneyFlowManager.startPayUMoneyFlow(mPaymentParams, getActivity(), R.style.AppTheme_default, false);
            }

        } catch (Exception e) {
            // some exception occurred
            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
            //payNowButton.setEnabled(true);
        }
    }

    /**
     * Thus function calculates the hash for transaction
     *
     * @param paymentParam payment params of transaction
     * @return payment params along with calculated merchant hash
     */
    private PayUmoneySdkInitializer.PaymentParam calculateServerSideHashAndInitiatePayment1(final PayUmoneySdkInitializer.PaymentParam paymentParam) {

        StringBuilder stringBuilder = new StringBuilder();
        HashMap<String, String> params = paymentParam.getParams();
        stringBuilder.append(params.get(PayUmoneyConstants.KEY) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.TXNID) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.AMOUNT) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.PRODUCT_INFO) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.FIRSTNAME) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.EMAIL) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.UDF1) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.UDF2) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.UDF3) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.UDF4) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.UDF5) + "||||||");

        AppEnvironment appEnvironment = ((MyApplication) getActivity().getApplication()).getAppEnvironment();
        stringBuilder.append(appEnvironment.salt());

        String hash = hashCal(stringBuilder.toString());
        paymentParam.setMerchantHash(hash);

        return paymentParam;
    }

    public static String hashCal(String str) {
        byte[] hashseq = str.getBytes();
        StringBuilder hexString = new StringBuilder();
        try {
            MessageDigest algorithm = MessageDigest.getInstance("SHA-512");
            algorithm.reset();
            algorithm.update(hashseq);
            byte messageDigest[] = algorithm.digest();
            for (byte aMessageDigest : messageDigest) {
                String hex = Integer.toHexString(0xFF & aMessageDigest);
                if (hex.length() == 1) {
                    hexString.append("0");
                }
                hexString.append(hex);
            }
        } catch (NoSuchAlgorithmException ignored) {
        }
        return hexString.toString();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (tinyDB.getString("Cancel").equalsIgnoreCase("N")) {
            if (tinyDB.getString("IsOrderDone").equalsIgnoreCase("Y")) {

                //TODO
               // order_submitToDB();
            } else {
                if (isPaymentDone.equalsIgnoreCase("N")) {

                }
            }
        }
        //viewBottomLayout
    }

    private class GetHashesFromServerTask extends AsyncTask<String, String, String> {
        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage("Please wait...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... postParams) {

            String merchantHash = "";
            try {
                //TODO Below url is just for testing purpose, merchant needs to replace this with their server side hash generation url
                URL url = new URL("https://payu.herokuapp.com/get_hash");

                String postParam = postParams[0];

                byte[] postParamsByte = postParam.getBytes("UTF-8");

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestProperty("Content-Length", String.valueOf(postParamsByte.length));
                conn.setDoOutput(true);
                conn.getOutputStream().write(postParamsByte);

                InputStream responseInputStream = conn.getInputStream();
                StringBuffer responseStringBuffer = new StringBuffer();
                byte[] byteContainer = new byte[1024];
                for (int i; (i = responseInputStream.read(byteContainer)) != -1; ) {
                    responseStringBuffer.append(new String(byteContainer, 0, i));
                }

                JSONObject response = new JSONObject(responseStringBuffer.toString());

                Iterator<String> payuHashIterator = response.keys();
                while (payuHashIterator.hasNext()) {
                    String key = payuHashIterator.next();
                    switch (key) {
                        /**
                         * This hash is mandatory and needs to be generated from merchant's server side
                         *
                         */
                        case "payment_hash":
                            merchantHash = response.getString(key);
                            break;
                        default:
                            break;
                    }
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return merchantHash;
        }

        @Override
        protected void onPostExecute(String merchantHash) {
            super.onPostExecute(merchantHash);

            progressDialog.dismiss();
            //payNowButton.setEnabled(true);

            if (merchantHash.isEmpty() || merchantHash.equals("")) {
                Toast.makeText(mContext, "Could not generate hash", Toast.LENGTH_SHORT).show();
            } else {
                mPaymentParams.setMerchantHash(merchantHash);

                if (AppPreference.selectedTheme != -1) {
                    PayUmoneyFlowManager.startPayUMoneyFlow(mPaymentParams, getActivity(), AppPreference.selectedTheme, mAppPreference.isOverrideResultScreen());
                } else {
                    PayUmoneyFlowManager.startPayUMoneyFlow(mPaymentParams, getActivity(), R.style.AppTheme_default, mAppPreference.isOverrideResultScreen());
                }
            }
        }


    }
}