package com.myliveclassroom.eduethics.Fragments.SubFragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.myliveclassroom.eduethics.Activity.MainActivity;
import com.myliveclassroom.eduethics.Adapter.FeeAdapter;
import com.myliveclassroom.eduethics.Fragments.HomeClassroomFragment;
import com.myliveclassroom.eduethics.Fragments.TeacherFragments.TeachersFeeStudentView;
import com.myliveclassroom.eduethics.Models.FeeModelTutor;
import com.myliveclassroom.eduethics.Models.TimeTableModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.myliveclassroom.eduethics.classes.TinyDB;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.PROFILE;

public class FeeDetailStudent extends Fragment {


    Context mContext;
    RecyclerView recyclerviewFeeDetails;
    TinyDB tinyDB;
    FeeModelTutor mFeeModelTutor;

    List<TimeTableModel> feeModelList = new ArrayList<>();
    List<FeeModelTutor> feeModelList2 = new ArrayList<>();
    View root;


    public FeeDetailStudent(Context context) {
        this.mContext = context;
        tinyDB = new TinyDB(mContext);
        mFeeModelTutor=null;
    }

    public FeeDetailStudent(Context context, FeeModelTutor feeModelTutor) {
        this.mContext = context;
        tinyDB = new TinyDB(mContext);
        mFeeModelTutor = feeModelTutor;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_fee_details, container, false);

        if(mFeeModelTutor!=null){
            MainActivity.TO_WHICH_FRAG = LocalConstants.TO_FRAG.TO_TeachersFeeStudentView;
        }else {
            MainActivity.TO_WHICH_FRAG = LocalConstants.TO_FRAG.TO_HOME_CLASSROOM;
        }

        recyclerviewFeeDetails = root.findViewById(R.id.recyclerviewFeeDetails);
        recyclerviewFeeDetails.setLayoutManager(new LinearLayoutManager(mContext));

        root.findViewById(R.id.collectFee).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(
                        R.anim.slide_in,  // enter
                        R.anim.fade_out,  // exit
                        R.anim.fade_in,   // popEnter
                        R.anim.slide_out  // popExit
                ).replace(R.id.fragmentFrame, new AddFeeEntry(mContext,mFeeModelTutor)).commit();
            }
        });

        if(tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)){
            root.findViewById(R.id.collectFee).setVisibility(View.GONE);
        }

       /* FirebaseFirestore.getInstance()
                .collection("fees")
                .document(HomeClassroomFragment.classId)
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        data = (List)documentSnapshot.get("students");
                        ((TextView)root.findViewById(R.id.fees)).setText(getString(R.string.Rs)+documentSnapshot.getString("feePerM")+"/Month");

                        Toast.makeText(mContext, ""+documentSnapshot.getString("startDate"), Toast.LENGTH_SHORT).show();
                        ((TextView)root.findViewById(R.id.startDateText)).setText(documentSnapshot.getString("startDate").equals("-")?"-":documentSnapshot.getString("startDate").split("-")[0]+" "+LocalConstants.MONTHS[(Integer.parseInt(documentSnapshot.getString("startDate").split("-")[1]))]+" "+documentSnapshot.getString("startDate").split("-")[2]);
                        ((TextView)root.findViewById(R.id.endDateText)).setText(documentSnapshot.getString("startDate").equals("-")?"-":
                                documentSnapshot.getString("startDate").split("-")[0]+" "+LocalConstants.MONTHS[(Integer.parseInt(documentSnapshot.getString("startDate").split("-")[1])+1)]+" "+documentSnapshot.getString("startDate").split("-")[2]);

                        //((TextView)root.findViewById(R.id.className)).setText(HomeClassroomFragment.model.getSubject());

                        recyclerviewFeeDetails.setAdapter(new FeeAdapter(data, FeeDetailsTeacher.this,((TextView)root.findViewById(R.id.totalRec)),((TextView)root.findViewById(R.id.totalDue)),((TextView)root.findViewById(R.id.totalAdv))));
                    }
                });
*/


        getFeeDetail();
        return root;
    }

    private void getFeeDetail() {
        //recyclerviewFeeDetails.setAdapter(new FeeAdapter(feeModelList2, FeeDetailsTeacher.this, ((TextView) root.findViewById(R.id.totalRec)), ((TextView) root.findViewById(R.id.totalDue)), ((TextView) root.findViewById(R.id.totalAdv))));

        TimeTableModel timeTableModel = new TimeTableModel();
        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)) {
            timeTableModel.setUserId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
        } else if (mFeeModelTutor != null) {
            timeTableModel.setUserId(mFeeModelTutor.getStudentId());
        }
        timeTableModel.setClassId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID));

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = apiService.TutorFeeListForStudent(timeTableModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                try {
                    if (response.body() != null) {

                        Type type = new TypeToken<List<FeeModelTutor>>() {
                        }.getType();
                        feeModelList2 = new Gson().fromJson(response.body().getAsJsonArray("Response"), type);
                        /*for (FeeModelTutor feeModelTutor : feeModelList2) {

                        }*/
                        if (feeModelList2.size() > 0) {
                            ((TextView) root.findViewById(R.id.className)).setText(feeModelList2.get(0).getClassName() + " " + feeModelList2.get(0).getSubjectName());
                            ((TextView) root.findViewById(R.id.startDateText)).setText(feeModelList2.get(0).getStartDate());
                            ((TextView) root.findViewById(R.id.endDateText)).setText(feeModelList2.get(0).getNextdueDate());
                            if (Float.parseFloat(feeModelList2.get(0).getCourseFee()) > 0) {
                                ((TextView) root.findViewById(R.id.fees)).setText(getString(R.string.Rs) + feeModelList2.get(0).getCourseFee() + "/course");
                            } else if (Float.parseFloat(feeModelList2.get(0).getMonthlyFee()) > 0) {
                                ((TextView) root.findViewById(R.id.fees)).setText(getString(R.string.Rs) + feeModelList2.get(0).getMonthlyFee() + "/month");
                            }
                        }
                        recyclerviewFeeDetails.setAdapter(new FeeAdapter(mContext, feeModelList2, FeeDetailStudent.this, ((TextView) root.findViewById(R.id.totalRec)), ((TextView) root.findViewById(R.id.totalDue)), ((TextView) root.findViewById(R.id.totalAdv))));
                    }
                } catch (Exception ex) {

                    Toast.makeText(mContext, LocalConstants.APP_MESSAGES.ERROR1, Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, LocalConstants.APP_MESSAGES.ERROR1, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void click(int position) {
        getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(
                R.anim.slide_in,  // enter
                R.anim.fade_out,  // exit
                R.anim.fade_in,   // popEnter
                R.anim.slide_out  // popExit
        ).replace(R.id.fragmentFrame, new TeachersFeeStudentView(mContext)).commit();
    }

}