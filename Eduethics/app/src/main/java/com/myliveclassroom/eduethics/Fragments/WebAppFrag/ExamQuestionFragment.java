package com.myliveclassroom.eduethics.Fragments.WebAppFrag;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.myliveclassroom.eduethics.Activity.MainActivity;
import com.myliveclassroom.eduethics.Adapter.QuestionAdapter;
import com.myliveclassroom.eduethics.Fragments.Exam.ExamMcqCreateFragment;
import com.myliveclassroom.eduethics.Models.QuestionModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.TinyDB;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.PROFILE;
import static com.myliveclassroom.eduethics.Utils.LocalConstants.FROM.FROM_QUESTION_ADD_FRAGMENT;


public class ExamQuestionFragment extends Fragment {

    TinyDB tinyDB;
   Context mContext;

    @BindView(R.id.recyclerQuestions)
    RecyclerView recyclerQuestions;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;


    @BindView(R.id.txtStudentName)
    TextView txtStudentName;

    @BindView(R.id.txtClass)
    TextView txtClass;

    @BindView(R.id.txtExamSubject)
    TextView txtExamSubject;

    @BindView(R.id.txtTotalTime)
    TextView txtTotalTime;

    @BindView(R.id.txtTotalMarks)
    TextView txtTotalMarks;

    @BindView(R.id.txtTotalQuestions)
    TextView txtTotalQuestions;

    @BindView(R.id.txtTotalQAttempt)
    TextView txtTotalQAttempt;

    @BindView(R.id.txtMarquee)
    TextView txtMarquee;

    @BindView(R.id.txtBtnSubmitExam)
    TextView txtBtnSubmitExam;

    @BindView(R.id.btnAddQuestions)
    Button btnAddQuestions;
    

    QuestionAdapter questionAdapter;
    List<QuestionModel> questionModelList;
    List<QuestionModel> questionList;
    List<String> qIds;

    String mEndDateStr = "";
    String mExamId = "";
    String mQuestionId = "";
    String mAnswer = "";

    String studentName;
    String mExamClass;
    String mSubject;
    String mExamTime;
    String mExamMaxMarks;
    String mExamTotalQ;
    String mExamTotalQAtt;
    View root;
    LinearLayout layoutStudentInfo;

    public ExamQuestionFragment(Context context ) {
        // Required empty public constructor
        mContext=context;
        tinyDB=new TinyDB(mContext);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root= inflater.inflate(R.layout.fragment_exam_question, container, false);
        MainActivity.TO_WHICH_FRAG = FROM_QUESTION_ADD_FRAGMENT;
        ButterKnife.bind(this,root);
        init();
        return  root;
    }

    private void init() {

        layoutStudentInfo=root.findViewById(R.id.layoutStudentInfo);
        layoutStudentInfo.setVisibility(View.GONE);
        mExamId = tinyDB.getString(LocalConstants.Exam_Constants.ExamId);
        mEndDateStr = tinyDB.getString(LocalConstants.Exam_Constants.EndDate);

        studentName = tinyDB.getString(LocalConstants.Exam_Constants.STUDENT_NAME);
        mExamClass = tinyDB.getString(LocalConstants.Exam_Constants.ExamClass);
        mSubject = tinyDB.getString(LocalConstants.Exam_Constants.ExamSubject);
        mExamTime = tinyDB.getString(LocalConstants.Exam_Constants.ExamTime);
        mExamMaxMarks = tinyDB.getString(LocalConstants.Exam_Constants.ExamMaxMarks);
        mExamTotalQ = tinyDB.getString(LocalConstants.Exam_Constants.ExamTotalQ);
        mExamTotalQAtt = tinyDB.getString(LocalConstants.Exam_Constants.ExamTotalQAtt);

        txtStudentName.setText(": " + studentName);
        txtClass.setText(": " + mExamClass);
        txtExamSubject.setText(": " + mSubject);
        txtTotalTime.setText(": " + mExamTime);
        txtTotalMarks.setText(": " + mExamMaxMarks);
        txtTotalQuestions.setText(": " + mExamTotalQ);
        txtTotalQAttempt.setText(": " + mExamTotalQAtt);

        progressBar.setVisibility(View.GONE);
        initRecyclerView();
        txtMarquee.setText("Exam timing : " + mExamTime + ", You will get alert message after time is over and you will be asked to submit your exam or you can submit your exam by pressing 'Submit' button");
        txtMarquee.setSelected(true);
    }
    
    
    @OnClick(R.id.btnAddQuestions)
    public void btnAddQuestions_click(View view){
        getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(
                R.anim.slide_in,  // enter
                R.anim.fade_out,  // exit
                R.anim.fade_in,   // popEnter
                R.anim.slide_out  // popExit
        ).replace(R.id.fragmentFrame, new ExamMcqCreateFragment(mContext)).commit();
    }

    private void initRecyclerView() {
        recyclerQuestions.setLayoutManager(new LinearLayoutManager(mContext));
        listQuestions();
    }

    private void listQuestions() {

        progressBar.setVisibility(View.VISIBLE);
        QuestionModel questionModel = new QuestionModel();
        questionModel.setExamId(mExamId);

        questionModel.setStudentId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
        questionModel.setClassName(mExamClass);
        questionModel.setSubjectName(mSubject);
        questionModel.setProfileType(tinyDB.getString(PROFILE));

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<List<QuestionModel>> call = apiService.QuestionList(questionModel);
        call.enqueue(new Callback<List<QuestionModel>>() {
            @Override
            public void onResponse(Call<List<QuestionModel>> call, Response<List<QuestionModel>> response) {
                try {
                    progressBar.setVisibility(View.GONE);
                    if (response.body() != null) {
                        questionModelList = response.body();
                        qIds = new ArrayList<>();
                        for (int j = 0; j < questionModelList.size(); j++) {
                            if (!qIds.contains(questionModelList.get(j).getQuestionId())) {
                                qIds.add(questionModelList.get(j).getQuestionId());
                            }
                        }
                        questionAdapter = new QuestionAdapter(mContext, qIds, questionModelList);
                        recyclerQuestions.setAdapter(questionAdapter);
                    }
                } catch (Exception ex) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<List<QuestionModel>> call, Throwable t) {
                // Log error here since request failed
                progressBar.setVisibility(View.GONE);
                Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}