package com.myliveclassroom.eduethics.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.myliveclassroom.eduethics.Activity.MainActivity;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.ExtraFunctions;
import com.myliveclassroom.eduethics.Utils.GlobalVariables;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NotieAdapter extends RecyclerView.Adapter<NotieAdapter.vh> {

    List<Map<String, Object>> data;
    Fragment context;

    public NotieAdapter(Fragment context) {
        //this.data = HomeClassroomFragment.model.getNotice();
        this.context = context;
        MainActivity.ids = null;
        MainActivity.ids = new HashMap<>();

    }

    @NonNull
    @Override
    public vh onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new vh(LayoutInflater.from(parent.getContext()).inflate(R.layout.notice_single_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull vh holder, int position) {

        if (!GlobalVariables.isStudent){
            int id = View.generateViewId();
            MainActivity.ids.put(id,"notice;"+ data.get(position).get("attachment").toString());
            holder.itemView.setId(id);
            context.getActivity().registerForContextMenu(holder.itemView);
        }

        holder.timeTV.setText(data.get(position).get("date").toString() + " (" + ExtraFunctions.getReadableTime(Integer.parseInt(data.get(position).get("time").toString().split(":")[0]), Integer.parseInt(data.get(position).get("time").toString().split(":")[1]))+")");
        holder.noticeTV.setText(data.get(position).get("notice").toString());

        if (data.get(position).get("attachment").toString().equals("-")) {
            holder.noticeImage.setVisibility(View.GONE);
        } else {
           /* FirebaseStorage storage = FirebaseStorage.getInstance();
            StorageReference storageRef = storage.getReferenceFromUrl(LocalConstants.STORAGE_REF);    //change the url according to your firebase app
            StorageReference pathReference = storageRef.child(data.get(position).get("attachment").toString());

            pathReference.getDownloadUrl().addOnSuccessListener(uri -> {

                if (data.get(position).get("attachment").toString().endsWith("png") || data.get(position).get("attachment").toString().endsWith("jpg") || data.get(position).get("attachment").toString().endsWith("jpeg")) {
                    holder.noticeImage.setPadding(0,0,0,0);
                    Glide.with(context).load(uri).centerCrop().into(holder.noticeImage);
                }

                holder.itemView.setOnClickListener(v -> {
                    if (data.get(position).get("attachment").toString().endsWith("pdf")) {
                        Intent intent = new Intent(context.getContext(), ImagePage.class);
                        intent.putExtra("urlImage", uri.toString()).putExtra("type", "pdf");
                        context.startActivity(intent);
                    } else if (data.get(position).get("attachment").toString().endsWith("png") || data.get(position).get("attachment").toString().endsWith("jpg") || data.get(position).get("attachment").toString().endsWith("jpeg")) {
                        Intent intent = new Intent(context.getContext(), ImagePage.class);
                        intent.putExtra("urlImage", uri.toString()).putExtra("type", "image");
                        context.startActivity(intent);
                    }
                });
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    Toast.makeText(context.getContext(), "Error While Getting Image", Toast.LENGTH_SHORT).show();
                }
            });*/
        }

    }

    @Override
    public int getItemCount() {
        return data==null?0:data.size();
    }

    class vh extends RecyclerView.ViewHolder {
        TextView timeTV, noticeTV;
        ImageView noticeImage;

        public vh(@NonNull View itemView) {
            super(itemView);
            this.noticeImage = itemView.findViewById(R.id.imageNotice);
            this.timeTV = itemView.findViewById(R.id.timeNoticeText);
            this.noticeTV = itemView.findViewById(R.id.noticeText);
        }

    }

}
