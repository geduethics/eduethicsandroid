package com.myliveclassroom.eduethics.Models;

public class CircularModel {
    private String CircularId;
    private String CircularHead;
    private String CircularDetail;
    private String CircularDate;
    private String CircularType; //All / Teacher
    private String ClassId;
    private String SubjectId;
    private String FileType;
    private String SchoolId;
    private String SessionId;
    private  String Attachment;

    public String getCircularId() {
        return CircularId;
    }

    public void setCircularId(String circularId) {
        CircularId = circularId;
    }

    public String getCircularDate() {
        return CircularDate;
    }

    public String getCircularDetail() {
        return CircularDetail;
    }

    public String getCircularHead() {
        return CircularHead;
    }

    public String getCircularType() {
        return CircularType;
    }

    public void setCircularDate(String circularDate) {
        CircularDate = circularDate;
    }

    public void setCircularDetail(String circularDetail) {
        CircularDetail = circularDetail;
    }

    public void setCircularHead(String circularHead) {
        CircularHead = circularHead;
    }

    public void setCircularType(String circularType) {
        CircularType = circularType;
    }

    public String getSessionId() {
        return SessionId;
    }

    public void setSessionId(String sessionId) {
        SessionId = sessionId;
    }

    public void setClassId(String classId) {
        ClassId = classId;
    }

    public void setSchoolId(String schoolId) {
        SchoolId = schoolId;
    }

    public String getClassId() {
        return ClassId;
    }

    public String getSchoolId() {
        return SchoolId;
    }

    public String getAttachment() {
        return Attachment;
    }

    public void setAttachment(String attachment) {
        Attachment = attachment;
    }

    public String getFileType() {
        return FileType;
    }

    public String getSubjectId() {
        return SubjectId;
    }

    public void setFileType(String fileType) {
        FileType = fileType;
    }

    public void setSubjectId(String subjectId) {
        SubjectId = subjectId;
    }
}
