package com.myliveclassroom.eduethics.Adapter;

import android.app.TimePickerDialog;
import android.content.Context;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.myliveclassroom.eduethics.Fragments.TeacherFragments.TimeTableScheduleCreateFragment;
import com.myliveclassroom.eduethics.Fragments.WebAppFrag.TimeTableTFragment;
import com.myliveclassroom.eduethics.Models.TimeTableModel;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.ExtraFunctions;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.CommonFunctions;
import com.myliveclassroom.eduethics.classes.TinyDB;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.annimon.stream.Stream;

import static com.myliveclassroom.eduethics.Utils.HomeConstants.PROFILE;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.SCHEDULE_TEACHER;

public class TimeTableScheduleAdapter extends RecyclerView.Adapter<TimeTableScheduleAdapter.ViewHolder> {

    TinyDB tinyDB;
    Fragment fragment;
    Context mContext;
    private List<TimeTableModel> timeTableModelListfilter2;
    private List<TimeTableModel> timeTableModelListAll2;
    Calendar dateTime = Calendar.getInstance();
    int[] t = new int[4];

    public TimeTableScheduleAdapter(Context context, Fragment fragment) {
        timeTableModelListfilter2 = null;
        timeTableModelListAll2 = null;
        mContext = context;
        this.fragment = fragment;
        tinyDB = new TinyDB(mContext);
    }

    @Override
    public TimeTableScheduleAdapter.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new TimeTableScheduleAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_timeable_schedule, parent, false));
    }

    boolean isSet = false;
    int lastFromHrs = 0, lastFromMin = 0, lastToHrs = 0, lastToMin = 0;
    boolean showLiveBtn = true;

    String mSubjectId = "";
    List<TimeTableModel> innerList = new ArrayList<>();

    @Override
    public void onBindViewHolder(@NonNull @NotNull TimeTableScheduleAdapter.ViewHolder holder, int position) {


        // bindSpinnerTeacher(holder, innerList);
        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)
                || tinyDB.getString(LocalConstants.TINYDB_KEYS.TEMP_SCREEN_NAME).equalsIgnoreCase(SCHEDULE_TEACHER)) {
            if (!(Boolean) timeTableModelListfilter2.get(position).getIsTimeSet() || !timeTableModelListfilter2.get(position).getTimeFrom().contains(":")
                    || !timeTableModelListfilter2.get(position).getTimeTo().contains(":")) {
                //holder.itemView.setVisibility(View.GONE);
            }
        }

        holder.txtTeacherName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)
                        || tinyDB.getString(LocalConstants.TINYDB_KEYS.TEMP_SCREEN_NAME).equalsIgnoreCase(SCHEDULE_TEACHER)) {
                    return;
                }
                innerList = Stream.of(timeTableModelListAll2).filter(arr -> arr.getSubjectId().equalsIgnoreCase(timeTableModelListfilter2.get(position).getSubjectId())).toList();

                ((TimeTableScheduleCreateFragment) fragment).teacherSelect(position, innerList, holder.txtTeacherName, holder.txtTeacherId);
            }
        });

        holder.imgTeacherEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)
                        || tinyDB.getString(LocalConstants.TINYDB_KEYS.TEMP_SCREEN_NAME).equalsIgnoreCase(SCHEDULE_TEACHER)) {
                    return;
                }
                innerList = Stream.of(timeTableModelListAll2).filter(arr -> arr.getSubjectId().equalsIgnoreCase(timeTableModelListfilter2.get(position).getSubjectId())).toList();

                ((TimeTableScheduleCreateFragment) fragment).teacherSelect(position, innerList, holder.txtTeacherName, holder.txtTeacherId);
            }
        });


        holder.dayName.setText(CommonFunctions.capitalize(timeTableModelListfilter2.get(position).getSubjectName()));
        isSet = false;

        t[0] = dateTime.get(Calendar.HOUR_OF_DAY);
        t[1] = dateTime.get(Calendar.MINUTE);
        t[2] = t[0];
        t[3] = t[1];


        if (!(Boolean) timeTableModelListfilter2.get(position).getIsTimeSet()) {
            holder.RelativeLayouttime.setVisibility(View.GONE);
            holder.daySwitch.setChecked(false);
            holder.fromTime.setText(ExtraFunctions.getReadableTime(t[0], t[1]));
            holder.toTime.setText(ExtraFunctions.getReadableTime(t[2], t[3]));
        } else if ((Boolean) timeTableModelListfilter2.get(position).getIsTimeSet()) {
            holder.daySwitch.setChecked(true);
            if (timeTableModelListfilter2.get(position).getTimeTo().contains(":")) {
                holder.toTime.setText(ExtraFunctions.getReadableTime(Integer.parseInt(timeTableModelListfilter2.get(position).getTimeTo().split(":")[0]), Integer.parseInt(timeTableModelListfilter2.get(position).getTimeTo().split(":")[1])));
                t[2] = Integer.parseInt(timeTableModelListfilter2.get(position).getTimeTo().split(":")[0]);
                t[3] = Integer.parseInt(timeTableModelListfilter2.get(position).getTimeTo().split(":")[1]);
            }
            if (timeTableModelListfilter2.get(position).getTimeFrom().contains(":")) {
                holder.fromTime.setText(ExtraFunctions.getReadableTime(Integer.parseInt(timeTableModelListfilter2.get(position).getTimeFrom().split(":")[0]), Integer.parseInt(timeTableModelListfilter2.get(position).getTimeFrom().split(":")[1])));
                t[0] = Integer.parseInt(timeTableModelListfilter2.get(position).getTimeFrom().split(":")[0]);
                t[1] = Integer.parseInt(timeTableModelListfilter2.get(position).getTimeFrom().split(":")[1]);
            }
            holder.txtTeacherId.setText(timeTableModelListfilter2.get(position).getTeacherId());
            holder.txtTeacherName.setText(CommonFunctions.capitalize(timeTableModelListfilter2.get(position).getTeacherName()));
        }

        holder.daySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                holder.RelativeLayouttime.setVisibility(isChecked ? View.VISIBLE : View.GONE);
                isSet = false;
                if (tinyDB.getString(LocalConstants.INSTITUTE_TYPE.TYPE).equalsIgnoreCase(LocalConstants.INSTITUTE_TYPE.TUTION)) {
                    ((TimeTableScheduleCreateFragment) fragment).datePicked(position, t[0], t[1], t[2], t[3], holder.daySwitch.isChecked());
                } else {
                    timeTableModelListfilter2.get(position).setIsTimeSet(holder.daySwitch.isChecked());
                    timeTableModelListfilter2.get(position).setTimeFrom(t[0] + ":" + t[1]);
                    timeTableModelListfilter2.get(position).setTimeFrom(t[2] + ":" + t[3]);
                }
                if (isChecked) {
                    holder.txtTeacherId.setText(timeTableModelListfilter2.get(position).getTeacherId());
                    holder.txtTeacherName.setText(CommonFunctions.capitalize(timeTableModelListfilter2.get(position).getTeacherName()));
                } else {
                    holder.txtTeacherId.setText("0");
                    holder.txtTeacherName.setText("Select Teacher");
                }
            }
        });

        holder.toTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)
                        || tinyDB.getString(LocalConstants.TINYDB_KEYS.TEMP_SCREEN_NAME).equalsIgnoreCase(SCHEDULE_TEACHER)) {
                    return;
                }
                TimePickerDialog timePickerDialog = new TimePickerDialog(fragment.getContext(), android.R.style.Theme_Holo_Light_Dialog_NoActionBar, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        holder.toTime.setText(ExtraFunctions.getReadableTime(hourOfDay, minute));
                        t[2] = hourOfDay;
                        t[3] = minute;
                        isSet = true;
                        lastToHrs = t[2];
                        lastToMin = t[3];
                        if (tinyDB.getString(LocalConstants.INSTITUTE_TYPE.TYPE).equalsIgnoreCase(LocalConstants.INSTITUTE_TYPE.TUTION)) {
                            ((TimeTableScheduleCreateFragment) fragment).datePicked(position, t[0], t[1], t[2], t[3], holder.daySwitch.isChecked());
                        } else {
                            timeTableModelListfilter2.get(position).setIsTimeSet(holder.daySwitch.isChecked());
                            timeTableModelListfilter2.get(position).setTimeTo(t[2] + ":" + t[3]);
                        }
                    }
                }, dateTime.get(Calendar.HOUR_OF_DAY), dateTime.get(Calendar.MINUTE), false);
                if (timeTableModelListfilter2.get(position).getTimeTo().contains(":")) {
                    lastFromHrs = Integer.parseInt(timeTableModelListfilter2.get(position).getTimeTo().split(":")[0]);
                    lastFromMin = Integer.parseInt(timeTableModelListfilter2.get(position).getTimeTo().split(":")[1]);
                    timePickerDialog.updateTime(lastFromHrs, lastFromMin);
                }
                timePickerDialog.show();
            }
        });

        holder.fromTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)
                        || tinyDB.getString(LocalConstants.TINYDB_KEYS.TEMP_SCREEN_NAME).equalsIgnoreCase(SCHEDULE_TEACHER)) {
                    return;
                }
                TimePickerDialog timePickerDialog = new TimePickerDialog(fragment.getContext(), android.R.style.Theme_Holo_Light_Dialog_NoActionBar, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        Log.e("Timerr", hourOfDay + "-" + minute);
                        holder.fromTime.setText(ExtraFunctions.getReadableTime(hourOfDay, minute));
                        t[0] = hourOfDay;
                        t[1] = minute;
                        isSet = true;
                        lastFromHrs = t[0];
                        lastFromMin = t[1];
                        if (tinyDB.getString(LocalConstants.INSTITUTE_TYPE.TYPE).equalsIgnoreCase(LocalConstants.INSTITUTE_TYPE.TUTION)) {
                            ((TimeTableScheduleCreateFragment) fragment).datePicked(position, t[0], t[1], t[2], t[3], holder.daySwitch.isChecked());
                        } else {
                            timeTableModelListfilter2.get(position).setIsTimeSet(holder.daySwitch.isChecked());
                            timeTableModelListfilter2.get(position).setTimeFrom(t[0] + ":" + t[1]);
                        }

                    }
                }, dateTime.get(Calendar.HOUR_OF_DAY), dateTime.get(Calendar.MINUTE), false);
                if (timeTableModelListfilter2.get(position).getTimeFrom().contains(":")) {
                    lastFromHrs = Integer.parseInt(timeTableModelListfilter2.get(position).getTimeFrom().split(":")[0]);
                    lastFromMin = Integer.parseInt(timeTableModelListfilter2.get(position).getTimeFrom().split(":")[1]);
                    timePickerDialog.updateTime(lastFromHrs, lastFromMin);
                }
                timePickerDialog.show();
            }
        });


        if ((Boolean) timeTableModelListfilter2.get(position).getIsTimeSet() || !timeTableModelListfilter2.get(position).getTimeFrom().contains(":")
                && timeTableModelListfilter2.get(position).getTimeTo().contains(":")) {

            if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)
                    || tinyDB.getString(LocalConstants.TINYDB_KEYS.TEMP_SCREEN_NAME).equalsIgnoreCase(SCHEDULE_TEACHER)) {
                //Compare date
                int hh1 = Integer.parseInt(timeTableModelListfilter2.get(position).getTimeFrom().split(":")[0]);
                int min1 = Integer.parseInt(timeTableModelListfilter2.get(position).getTimeFrom().split(":")[1]);

                int hh2 = Integer.parseInt(timeTableModelListfilter2.get(position).getTimeTo().split(":")[0]);
                int min2 = Integer.parseInt(timeTableModelListfilter2.get(position).getTimeTo().split(":")[1]);
                showLiveBtn = ExtraFunctions.isThisDateTimeGreater(Calendar.DAY_OF_WEEK, hh1, min1, hh2, min2, 4);
                //showLiveBtn = true;
                if (showLiveBtn) {
                    if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)) {
                        holder.txtBtnGoLiveNow.setText("Join Class");
                    } else if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER)
                            || tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER_SCHOOL)) {
                        holder.txtBtnGoLiveNow.setText("Go Live");
                    }

                    holder.txtBtnGoLiveNow.setVisibility(View.VISIBLE);
                    holder.txtBtnGoLiveNow.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            tinyDB.putString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_NAME, timeTableModelListfilter2.get(position).getClassName() == null ? "" : timeTableModelListfilter2.get(position).getClassName());
                            tinyDB.putString(LocalConstants.TINYDB_LOGIN_KEYS.SUBJECT_NAME, timeTableModelListfilter2.get(position).getSubjectName() == null ? "" : timeTableModelListfilter2.get(position).getSubjectName());
                            tinyDB.putString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID, timeTableModelListfilter2.get(position).getClassId());
                            tinyDB.putString(LocalConstants.TINYDB_LOGIN_KEYS.SUBJECT_ID, timeTableModelListfilter2.get(position).getSubjectId());

                                ((TimeTableScheduleCreateFragment) fragment).goLive();
                        }
                    });
                } else {
                    holder.txtBtnGoLiveNow.setVisibility(View.GONE);
                }
            }
        }
    }

    public void updateList(List<TimeTableModel> listFilterList, List<TimeTableModel> listAll) {
        timeTableModelListfilter2 = listFilterList;
        timeTableModelListAll2 = listAll;
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return timeTableModelListfilter2 == null ? 0 : timeTableModelListfilter2.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView dayName, fromTime, toTime, txtTeacherName, txtTeacherId, txtBtnGoLiveNow;
        SwitchCompat daySwitch;
        RelativeLayout RelativeLayouttime;
        ImageView imgTeacherEdit;
        //Spinner spinnerTeacher;

        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);

            this.imgTeacherEdit = itemView.findViewById(R.id.imgTeacherEdit);
            this.txtTeacherName = itemView.findViewById(R.id.txtTeacherName);
            this.txtTeacherId = itemView.findViewById(R.id.txtTeacherId);
            this.RelativeLayouttime = itemView.findViewById(R.id.time);
            this.dayName = itemView.findViewById(R.id.dayName);
            this.fromTime = itemView.findViewById(R.id.fromTime);
            this.toTime = itemView.findViewById(R.id.toTime);
            this.daySwitch = itemView.findViewById(R.id.daySwitch);
            this.txtTeacherId.setVisibility(View.GONE);
            //this.spinnerTeacher = itemView.findViewById(R.id.spinnerTeacher);
            txtBtnGoLiveNow = itemView.findViewById(R.id.txtBtnGoLiveNow);
            if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)
                    || tinyDB.getString(LocalConstants.TINYDB_KEYS.TEMP_SCREEN_NAME).equalsIgnoreCase(SCHEDULE_TEACHER)) {
                this.daySwitch.setVisibility(View.GONE);

            }
        }
    }
}
