package com.myliveclassroom.eduethics.Models;

public class VideoLectureModel {
    private String VideoId;
    private String ClassId;
    private String SubjectId;
    private String ClassName;
    private String SubjectName;
    private String SchoolId;
    private String VideoTitle;
    private String VideoLink;
    private String TeacherName;

    public void setClassName(String className) {
        ClassName = className;
    }

    public void setClassId(String classId) {
        ClassId = classId;
    }

    public String getClassName() {
        return ClassName;
    }

    public String getClassId() {
        return ClassId;
    }

    public String getSubjectId() {
        return SubjectId;
    }

    public String getVideoId() {
        return VideoId;
    }

    public void setSubjectId(String subjectId) {
        SubjectId = subjectId;
    }

    public void setVideoId(String videoId) {
        VideoId = videoId;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSchoolId(String schoolId) {
        SchoolId = schoolId;
    }

    public String getSchoolId() {
        return SchoolId;
    }

    public String getVideoLink() {
        return VideoLink;
    }

    public String getVideoTitle() {
        return VideoTitle;
    }

    public void setVideoLink(String videoLink) {
        VideoLink = videoLink;
    }

    public void setVideoTitle(String videoTitle) {
        VideoTitle = videoTitle;
    }

    public void setTeacherName(String teacherName) {
        TeacherName = teacherName;
    }

    public String getTeacherName() {
        return TeacherName;
    }
}

