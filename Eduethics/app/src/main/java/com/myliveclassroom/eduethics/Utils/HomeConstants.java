package com.myliveclassroom.eduethics.Utils;

public class HomeConstants {
    public static final String TEACHERS="Teachers";
    public static final String CIRUCLAR="Circular";
    public static final String PROFILE="Profile";
    public static final String ATTENDANCE="Attendance";
    public static final String FEE="Fee";
    public static final String RESULTS="Results";
    public static final String REPORTS="Reports";
    public static final String EXAMS="Exams";
    public static final String TIMETABLE="Time Table";
    public static final String SCHEDULE_STUDENT="Schedule";
    public static final String SCHEDULE_TEACHER=" Schedule ";
    public static final String ICARD="I Card";
    public static final String HOMEWORK="Home Work";
    public static final String EVENTS="Events & Holidays";
    public static final String HOLIDAYLIST="Holiday List";
    public static final String NOTIFICATIONS="Notifications";
    public static final String GALLERY="Gallery";
    public static final String LEAVE_APP="Leave Application";
    public static final String CIRCULARS="Circulars";
    public static final String MESSAGES="Message";
    public static final String BIRTHDAY="Birthday";
    public static final String VIDEO_LECTURES="Video Lectures";
    public static final String UDAAN="Udaan";
    public static final String DAILY_DOSE="Daily Dose";
    public static final String WORD_OF_THE_DAY="Word of the Day";
    public static final String SHARE_APP="Share this App";
    public static final String DIGITAL_LIBRARY="Digital Library";
    public static final String SYLLABUS="Syllabus";

    public static final String ATTENDANCE_TEACHER="Attendence";
    public static final String EXAM_TEACHER="Exam";
    public static final String TIMETABLE_TEACHER="Time Table Teacher";
    public static final String HEALTH_FEEBACK="Health Feedback";
    public static final String LEAVE_APP_TEACHER="Leave Applications";
    public static final String HOMEWORK_TEACHER="Homework_Teacher";

    public static final String EXAM_MANAGEMENT="Exam Management";
    public static final String EMPLOYEE_MANAGEMENT="Employee Management";

    public static final String GALLERYADMIN="Gallery";
    public static final String CIRCCULARADMIN="Circulars";

    public static final Integer MENU_INDEX_ADMIN=5;
    public static final Integer MENU_INDEX_PROFILE=2;


}