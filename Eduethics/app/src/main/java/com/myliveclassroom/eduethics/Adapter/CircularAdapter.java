package com.myliveclassroom.eduethics.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.myliveclassroom.eduethics.Activity.CircularsActivity;
import com.myliveclassroom.eduethics.Models.CircularModel;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.classes.TinyDB;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CircularAdapter extends RecyclerView.Adapter<CircularAdapter.ViewHolder> {
    TinyDB tinyDB;
    Context mContext;
    List<CircularModel> circularModelList;

    public CircularAdapter(Context context, List<CircularModel> circularModels){
        circularModelList = circularModels;
        mContext = context;
        tinyDB=new TinyDB(mContext);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.circular_adapter, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtCircularHead.setText(circularModelList.get(position).getCircularHead());
        holder.txtCircularDate.setText(circularModelList.get(position).getCircularDate());
        holder.txtAttachCountHead.setText("");
        holder.imgAttIconHead.setVisibility(View.GONE);
        if(circularModelList.get(position).getAttachment().trim().length()>0) {
            String[] aAttachCountHead = circularModelList.get(position).getAttachment().split(",");
            holder.txtAttachCountHead.setText("x" + aAttachCountHead.length);
            holder.imgAttIconHead.setVisibility(View.VISIBLE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mContext instanceof CircularsActivity) {
                    ((CircularsActivity) mContext).openCircular(circularModelList.get(position));
                }
               /* if (mContext instanceof UdaanActivity) {
                    ((UdaanActivity) mContext).openCircular(circularModelList.get(position));
                }*/
            }
        });

        if (tinyDB.getString("IsAdmin").equalsIgnoreCase("Y")) {
            holder.txtBtnDeleteCircular.setVisibility(View.VISIBLE);

            holder.txtBtnDeleteCircular.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mContext instanceof CircularsActivity) {
                        ((CircularsActivity) mContext).deleteCircular(circularModelList.get(position));
                    }
                }
            });
        }
        else{
            holder.txtBtnDeleteCircular.setVisibility(View.GONE);
        }

        /*if (mContext instanceof UdaanActivity || mContext instanceof WordOfTheDayActivity || mContext instanceof DailyDoseActivity) {
            holder.txtBtnDownload.setVisibility(View.VISIBLE);
        }*/
    }

    @Override
    public int getItemCount() {
        return (circularModelList==null?0:circularModelList.size());
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtCircularHead)
        TextView txtCircularHead;

        @BindView(R.id.txtCircularDate)
        TextView txtCircularDate;

        @BindView(R.id.txtAttachCountHead)
        TextView txtAttachCountHead;

        @BindView(R.id.txtBtnDeleteCircular)
        TextView txtBtnDeleteCircular;

        @BindView(R.id.txtBtnDownload)
        TextView txtBtnDownload;

        @BindView(R.id.imgAttIconHead)
        ImageView imgAttIconHead;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
