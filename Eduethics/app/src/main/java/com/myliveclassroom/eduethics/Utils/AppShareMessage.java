package com.myliveclassroom.eduethics.Utils;

import android.content.Context;

import com.myliveclassroom.eduethics.classes.TinyDB;

public class AppShareMessage {

    public static String StudentInvite(Context context){
        TinyDB tinyDB=new TinyDB(context);
        String student_invite="Your have been invited by " +tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.NAME)
                +" to join classroom " + tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_NAME)
                + " \n\nClassroom Id : " + tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID)
                + "\nSubject : " + tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SUBJECT_NAME);
        return  student_invite;
    }


}
