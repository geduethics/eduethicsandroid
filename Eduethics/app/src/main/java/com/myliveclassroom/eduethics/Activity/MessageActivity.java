package com.myliveclassroom.eduethics.Activity;


import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.myliveclassroom.eduethics.Adapter.MessageAdapter;
import com.myliveclassroom.eduethics.Models.MessageModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;


public class MessageActivity extends BaseActivity {

    @BindView(R.id.txtPageTitle)
    TextView txtPageTitle;

    @BindView(R.id.imgBack)
    ImageView imgBack;

    @BindView(R.id.imgbtnAdd)
    ImageView imgbtnAdd;

    @BindView(R.id.btnAdd)
    TextView btnAdd;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.recyclerMessage)
    RecyclerView recyclerMessage;

    List<MessageModel> messageModelList;
    MessageAdapter messageAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.MyThemeNoBar);
        setContentView(R.layout.activity_message);
        ButterKnife.bind(this);
        _BaseActivity(this);
        init();
    }

    private void init() {
        hideProgress();
        imgbtnAdd.setVisibility(View.INVISIBLE);
        imgBack.setVisibility(View.VISIBLE);
        btnAdd.setVisibility(View.GONE);
        btnAdd.setText("");
        imgbtnAdd.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_tick3));
        txtPageTitle.setText(R.string.home_msg);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Back();
            }
        });

        initRecyclerView();
    }

    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    //@OnClick(R.id.imgBack)
    private void Back() {
        super.onBackPressed();
    }

    private void initRecyclerView() {
        messageModelList = new ArrayList<>();
        recyclerMessage.setLayoutManager(new LinearLayoutManager(mContext));
        messageAdapter=new MessageAdapter(mContext,messageModelList);
        recyclerMessage.setAdapter(messageAdapter);
        listSubjects();

    }

    public void listSubjects() {
        MessageModel messageModel = new MessageModel();
        messageModel.setUserId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
        messageModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));
        messageModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));

        if(tinyDB.getString("UserType").contains("Parent")) {
            messageModel.setUserType("Student");
        }
        else {
            messageModel.setUserType("Teacher");
        }

        showProgress();
        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<List<MessageModel>> call = apiService.MessageList(messageModel);
        call.enqueue(new Callback<List<MessageModel>>() {
            @Override
            public void onResponse(Call<List<MessageModel>> call, Response<List<MessageModel>> response) {
                try {
                    hideProgress();
                    if (response.body() != null) {
                        messageModelList=response.body();
                        messageAdapter=new MessageAdapter(mContext,messageModelList);
                        recyclerMessage.setAdapter(messageAdapter);
                    }
                } catch (Exception ex) {
                    Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<List<MessageModel>> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                hideProgress();
            }
        });
    }

}
