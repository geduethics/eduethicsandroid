package com.myliveclassroom.eduethics.Models;

public class FeeModelTutor {
    private String StudentId;
    private String TeacherId;
    private String StudentName;
    private String ClassId;
    private String ClassName;
    private String SubjectName;
    private String CourseFee;
    private String MonthlyFee;
    private String StartDate;
    private String NextdueDate;
    private String ReminderDate1;
    private String ReminderDate2;


    private String AdvanceAmt;

    private String PaidAmt;
    private  String AmtHead;
    private String PaymentMode;
    private  String PaymentRemarks;

    private String PaymentDate;

    public void setPaymentDate(String paymentDate) {
        PaymentDate = paymentDate;
    }

    public String getPaymentDate() {
        return PaymentDate;
    }

    public void setClassId(String classId) {
        ClassId = classId;
    }

    public String getClassId() {
        return ClassId;
    }

    public String getAmtHead() {
        return AmtHead;
    }

    public String getFeeAmt() {
        return PaidAmt;
    }

    public void setPaidAmt(String paidAmt) {
        PaidAmt = paidAmt;
    }

    public String getPaidAmt() {
        return PaidAmt;
    }

    public String getPaymentMode() {
        return PaymentMode;
    }

    public void setAmtHead(String amtHead) {
        AmtHead = amtHead;
    }

    public String getPaymentRemarks() {
        return PaymentRemarks;
    }

    public void setFeeAmt(String feeAmt) {
        PaidAmt = feeAmt;
    }

    public void setPaymentMode(String paymentMode) {
        PaymentMode = paymentMode;
    }

    public void setPaymentRemarks(String paymentRemarks) {
        PaymentRemarks = paymentRemarks;
    }

    public String getAdvance() {
        return AdvanceAmt;
    }

    public void setAdvance(String advance) {
        AdvanceAmt = advance;
    }

    public void setStudentId(String studentId) {
        StudentId = studentId;
    }

    public String getStudentId() {
        return StudentId;
    }

    public String getCourseFee() {
        return CourseFee;
    }

    public String getMonthlyFee() {
        return MonthlyFee;
    }

    public String getTeacherId() {
        return TeacherId;
    }

    public String getNextdueDate() {
        return NextdueDate;
    }

    public void setCourseFee(String courseFee) {
        CourseFee = courseFee;
    }

    public String getReminderDate1() {
        return ReminderDate1;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setMonthlyFee(String monthlyFee) {
        MonthlyFee = monthlyFee;
    }

    public String getReminderDate2() {
        return ReminderDate2;
    }

    public void setNextdueDate(String nextdueDate) {
        NextdueDate = nextdueDate;
    }

    public void setReminderDate1(String reminderDate1) {
        ReminderDate1 = reminderDate1;
    }

    public void setTeacherId(String teacherId) {
        TeacherId = teacherId;
    }

    public void setReminderDate2(String reminderDate2) {
        ReminderDate2 = reminderDate2;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public void setStudentName(String studentName) {
        StudentName = studentName;
    }

    public void setClassName(String className) {
        ClassName = className;
    }

    public String getClassName() {
        return ClassName;
    }

    public String getStudentName() {
        return StudentName;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }
}
