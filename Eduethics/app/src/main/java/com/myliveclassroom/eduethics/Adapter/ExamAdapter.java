package com.myliveclassroom.eduethics.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.myliveclassroom.eduethics.Activity.ExamQActivity;
import com.myliveclassroom.eduethics.Activity.ResultActivity;
import com.myliveclassroom.eduethics.Models.ExamModel;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.classes.TinyDB;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ExamAdapter extends RecyclerView.Adapter<ExamAdapter.ViewHolder> {

    Context mContext;
    List<ExamModel> examModelList;
    TinyDB tinyDB;

    Date dateToday;
    String dateTodayStr;
    Date examDate;
    Date examStartTime;
    Date examEndTime;
    SimpleDateFormat dateformat = new SimpleDateFormat("dd-MMM-yyyy");
    SimpleDateFormat dateTimeformat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
    SimpleDateFormat timeformat = new SimpleDateFormat("hh:mm aa");

    public ExamAdapter(Context context, List<ExamModel> examList) {
        mContext = context;
        examModelList = examList;
        tinyDB = new TinyDB(mContext);

        Calendar startDate = Calendar.getInstance();
        dateTodayStr = dateTimeformat.format(startDate.getTime());
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.exam_summary_adapter, parent, false);
        return new ViewHolder(view);
    }

    boolean showStartBtn = false;

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtExamTitle.setText(examModelList.get(position).getExamTitle());
        holder.txtClass.setText(": " + examModelList.get(position).getClassName());
        holder.txtDate.setText(": " + examModelList.get(position).getStartDate());
        holder.txtStartTime.setText(": " + examModelList.get(position).getStartTimeStr() + " to " + examModelList.get(position).getEndTimeStr());
        holder.txtMaxMarks.setText(": " + examModelList.get(position).getMaxMarks());
        holder.txtQuestionToAttmpt.setText(": " + examModelList.get(position).getToAttempt());
        holder.txtMaxMarks.setText(": " + examModelList.get(position).getMaxMarks());
        holder.txtSubject.setText(": " + examModelList.get(position).getSubjectName());
        holder.txtExamSubmitDate.setText(": " + examModelList.get(position).getExamSubmitDate());
        holder.txtTotalQuestion.setText(": " + examModelList.get(position).getTotalQuestions());
        holder.txtMarksObtained.setText(": " + examModelList.get(position).getMarksObtained());


        try {
            dateToday = dateTimeformat.parse(dateTodayStr);
            examDate = dateformat.parse(examModelList.get(position).getStartTime());
            examStartTime = dateTimeformat.parse(examModelList.get(position).getStartTime());
            examEndTime = dateTimeformat.parse(examModelList.get(position).getEndTime());

            if (examStartTime.before(dateToday) && examEndTime.after(dateToday)) {
                showStartBtn = true;
            } else {
                showStartBtn = false;
            }
            if (examEndTime.before(dateToday)) {
                holder.txtMsg.setText("ਪ੍ਰੈਕਟਿਸ/ਡੈਮੋ ਟੈਸਟ ਦਾ ਟਾਈਮ ਖਤਮ ਹੋ ਚੁੱਕਾ ਹੈ।");
                holder.txtMsg.setVisibility(View.VISIBLE);
                holder.txtMsg.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
            }
            if (examStartTime.after(dateToday)) {
                holder.txtMsg.setText("Test will start on " + new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.getDefault()).format(examStartTime));
                holder.txtMsg.setVisibility(View.VISIBLE);
                holder.txtMsg.setTextColor(mContext.getResources().getColor(R.color.green2));
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        //ਪ੍ਰੈਕਟਿਸ/ਡੈਮੋ ਟੈਸਟ ਦਾ ਟਾਈਮ ਖਤਮ ਹੋ ਚੁੱਕਾ ਹੈ ।


        if (!showStartBtn) {
            holder.txtBtnStartExam.setVisibility(View.GONE);
        } else {
            holder.txtBtnStartExam.setVisibility(View.VISIBLE);
        }

        if (!examModelList.get(position).getExamIdDone().equalsIgnoreCase("0")) {
            holder.txtBtnStartExam.setVisibility(View.GONE);
            holder.txtMsg.setText("Exam submitted by you!");
            holder.txtMsg.setVisibility(View.VISIBLE);
            holder.txtMsg.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
        }
    //txtBtnStartExam
        if (mContext instanceof ResultActivity) {

            holder.txtMarksObtained.setText(": " + examModelList.get(position).getMarksObtained());
            holder.txtCorrectCount.setText(": " + examModelList.get(position).getCorrectCount());
            holder.txtAttempted.setText(": " + examModelList.get(position).getAttempted());
            holder.txtMsg.setVisibility(View.GONE);
            //((ResultActivity) mContext).clickAnsSubmit(holder.txtqid.getText().toString(),checkedId+"");
            holder.layourResult.setVisibility(View.VISIBLE);
            holder.txtBtnStartExam.setVisibility(View.GONE);
            holder.txtBtnExamResult.setVisibility(View.VISIBLE);
            if(examModelList.get(position).getCertificate().equalsIgnoreCase("")){
                holder.txtBtnExamResult.setVisibility(View.GONE);
            }
            holder.txtBtnExamResult.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((ResultActivity) mContext).downloadCertificate(examModelList.get(position));
                }
            });

        }
        holder.txtBtnStartExam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ExamQActivity.class);
                try {
                    examEndTime = dateTimeformat.parse(examModelList.get(position).getEndTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                tinyDB.putString("ExamId", examModelList.get(position).getExamId());
                tinyDB.putString("EndDate", new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.getDefault()).format(examEndTime));


                tinyDB.putString("ExamTitle", examModelList.get(position).getExamTitle());
                tinyDB.putString("ExamClass", examModelList.get(position).getClassName());
                tinyDB.putString("ExamSubject", examModelList.get(position).getSubjectName());
                tinyDB.putString("ExamStartDate", examModelList.get(position).getStartDate());
                tinyDB.putString("ExamTime", examModelList.get(position).getStartTimeStr() + " to " + examModelList.get(position).getEndTimeStr());
                tinyDB.putString("ExamMaxMarks", examModelList.get(position).getMaxMarks());
                tinyDB.putString("ExamTotalQ", examModelList.get(position).getToAttempt());
                tinyDB.putString("ExamTotalQAtt", examModelList.get(position).getToAttempt());

                Toast.makeText(mContext, new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.getDefault()).format(examEndTime), Toast.LENGTH_SHORT).show();
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return examModelList == null ? 0 : examModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtExamTitle)
        TextView txtExamTitle;

        @BindView(R.id.txtClass)
        TextView txtClass;

        @BindView(R.id.txtSubject)
        TextView txtSubject;

        @BindView(R.id.txtDate)
        TextView txtDate;

        @BindView(R.id.txtStartTime)
        TextView txtStartTime;

        @BindView(R.id.txtMaxMarks)
        TextView txtMaxMarks;

        @BindView(R.id.txtTotalQuestion)
        TextView txtTotalQuestion;

        @BindView(R.id.txtQuestionToAttmpt)
        TextView txtQuestionToAttmpt;

        @BindView(R.id.txtBtnStartExam)
        TextView txtBtnStartExam;

        @BindView(R.id.txtBtnExamResult)
        TextView txtBtnExamResult;

        @BindView(R.id.txtMsg)
        TextView txtMsg;

        @BindView(R.id.txtExamSubmitDate)
        TextView txtExamSubmitDate;

        @BindView(R.id.txtMarksObtained)
        TextView txtMarksObtained;

        @BindView(R.id.txtCorrectCount)
        TextView txtCorrectCount;

        @BindView(R.id.txtAttempted)
        TextView txtAttempted;

        @BindView(R.id.layourResult)
        LinearLayout layourResult;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
