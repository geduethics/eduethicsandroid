package com.myliveclassroom.eduethics.Adapter;

import android.app.TimePickerDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.myliveclassroom.eduethics.Fragments.TeacherFragments.TimeTableCreateFragment;
import com.myliveclassroom.eduethics.Models.TimeTableModel;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.ExtraFunctions;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.TinyDB;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class DayAdapter extends RecyclerView.Adapter<DayAdapter.viewHolder> {

    TinyDB tinyDB;
    Fragment fragment;
    Context mContext;
    List<TimeTableModel> timeTableModelList = new ArrayList<>();
    Calendar dateTime = Calendar.getInstance();
    int[] t = new int[4];


    public DayAdapter(Fragment fragment, Context context, List<TimeTableModel> timeTableModels) {
        timeTableModelList = timeTableModels;
        mContext = context;
        this.fragment = fragment;
        tinyDB = new TinyDB(mContext);
    }

    @NonNull
    @NotNull
    @Override
    public DayAdapter.viewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new DayAdapter.viewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_timeable, parent, false));
    }

    boolean isSet = false;
    int lastFromHrs = 0, lastFromMin = 0, lastToHrs = 0, lastToMin = 0;

    @Override
    public void onBindViewHolder(@NonNull @NotNull DayAdapter.viewHolder holder, int position) {
        holder.dayName.setText(timeTableModelList.get(position).getDayName());
        isSet = false;
      /*  t[0] = lastFromHrs > 0 ? lastFromHrs : dateTime.get(Calendar.HOUR_OF_DAY);
        t[1] = lastFromMin >= 0 ? lastFromMin : dateTime.get(Calendar.MINUTE);
        t[2] = lastToHrs > 0 ? lastToHrs : t[0];
        t[3] = lastToMin >= 0 ? lastToMin : t[1];*/

        t[0] = dateTime.get(Calendar.HOUR_OF_DAY);
        t[1] = dateTime.get(Calendar.MINUTE);
        t[2] = t[0];
        t[3] = t[1];


        if (!(Boolean) timeTableModelList.get(position).getIsTimeSet()) {
            holder.time.setVisibility(View.GONE);
            holder.daySwitch.setChecked(false);
            holder.fromTime.setText(ExtraFunctions.getReadableTime(t[0], t[1]));
            holder.toTime.setText(ExtraFunctions.getReadableTime(t[2], t[3]));
        } else if ((Boolean) timeTableModelList.get(position).getIsTimeSet()) {
            holder.daySwitch.setChecked(true);
            if (timeTableModelList.get(position).getTimeTo().contains(":")) {
                holder.toTime.setText(ExtraFunctions.getReadableTime(Integer.parseInt(timeTableModelList.get(position).getTimeTo().split(":")[0]), Integer.parseInt(timeTableModelList.get(position).getTimeTo().split(":")[1])));
            }
            if (timeTableModelList.get(position).getTimeFrom().contains(":")) {
                holder.fromTime.setText(ExtraFunctions.getReadableTime(Integer.parseInt(timeTableModelList.get(position).getTimeFrom().split(":")[0]), Integer.parseInt(timeTableModelList.get(position).getTimeFrom().split(":")[1])));
            }/*lastFromHrs = Integer.parseInt(timeTableModelList.get(position).getTimeFrom().split(":")[0]);
            lastFromMin = Integer.parseInt(timeTableModelList.get(position).getTimeFrom().split(":")[1]);
            lastToHrs = Integer.parseInt(timeTableModelList.get(position).getTimeTo().split(":")[0]);
            lastToMin = Integer.parseInt(timeTableModelList.get(position).getTimeTo().split(":")[1]);*/
        }

        /*if(!timeTableModelList.get(position).getIsTimeSet()) {

        }else{
            holder.toTime.setText(ExtraFunctions.getReadableTime(Integer.parseInt(timeTableModelList.get(position).getTimeTo().split(":")[0]), Integer.parseInt(timeTableModelList.get(position).getTimeTo().split(":")[1])));
            holder.fromTime.setText(ExtraFunctions.getReadableTime(Integer.parseInt(timeTableModelList.get(position).getTimeFrom().split(":")[0]), Integer.parseInt(timeTableModelList.get(position).getTimeFrom().split(":")[1])));
         }*/

        holder.daySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                holder.time.setVisibility(isChecked ? View.VISIBLE : View.GONE);
                isSet = false;
                if (tinyDB.getString(LocalConstants.INSTITUTE_TYPE.TYPE).equalsIgnoreCase(LocalConstants.INSTITUTE_TYPE.TUTION)) {
                    ((TimeTableCreateFragment) fragment).datePicked(position, t[0], t[1], t[2], t[3], holder.daySwitch.isChecked());
                } else {
                    timeTableModelList.get(position).setIsTimeSet(holder.daySwitch.isChecked());
                    timeTableModelList.get(position).setTimeFrom(t[0] + ":" + t[1]);
                    timeTableModelList.get(position).setTimeFrom(t[2] + ":" + t[3]);
                }
            }
        });


        holder.toTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new TimePickerDialog(fragment.getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        holder.toTime.setText(ExtraFunctions.getReadableTime(hourOfDay, minute));
                        t[2] = hourOfDay;
                        t[3] = minute;
                        isSet = true;
                        lastToHrs = t[2];
                        lastToMin = t[3];
                        if (tinyDB.getString(LocalConstants.INSTITUTE_TYPE.TYPE).equalsIgnoreCase(LocalConstants.INSTITUTE_TYPE.TUTION)) {
                            ((TimeTableCreateFragment) fragment).datePicked(position, t[0], t[1], t[2], t[3], holder.daySwitch.isChecked());
                        } else {
                            timeTableModelList.get(position).setIsTimeSet(holder.daySwitch.isChecked());
                            timeTableModelList.get(position).setTimeTo(t[2] + ":" + t[3]);
                        }
                    }
                }, dateTime.get(Calendar.HOUR_OF_DAY), dateTime.get(Calendar.MINUTE), false).show();
            }
        });

        holder.fromTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(fragment.getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        holder.fromTime.setText(ExtraFunctions.getReadableTime(hourOfDay, minute));
                        t[0] = hourOfDay;
                        t[1] = minute;
                        isSet = true;
                        lastFromHrs = t[0];
                        lastFromMin = t[1];
                        if (tinyDB.getString(LocalConstants.INSTITUTE_TYPE.TYPE).equalsIgnoreCase(LocalConstants.INSTITUTE_TYPE.TUTION)) {
                            ((TimeTableCreateFragment) fragment).datePicked(position, t[0], t[1], t[2], t[3], holder.daySwitch.isChecked());
                        } else {
                            timeTableModelList.get(position).setIsTimeSet(holder.daySwitch.isChecked());
                            timeTableModelList.get(position).setTimeFrom(t[0] + ":" + t[1]);
                        }
                    }
                }, dateTime.get(Calendar.HOUR_OF_DAY), dateTime.get(Calendar.MINUTE), false).show();

            }
        });
    }


    public void updateList(List<TimeTableModel> list) {
        timeTableModelList = list;
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return timeTableModelList == null ? 0 : timeTableModelList.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        TextView dayName, fromTime, toTime;
        SwitchCompat daySwitch;
        ConstraintLayout time;

        public viewHolder(@NonNull View itemView) {
            super(itemView);
            this.time = itemView.findViewById(R.id.time);
            this.dayName = itemView.findViewById(R.id.dayName);
            this.fromTime = itemView.findViewById(R.id.fromTime);
            this.toTime = itemView.findViewById(R.id.toTime);
            this.daySwitch = itemView.findViewById(R.id.daySwitch);
        }
    }
}
