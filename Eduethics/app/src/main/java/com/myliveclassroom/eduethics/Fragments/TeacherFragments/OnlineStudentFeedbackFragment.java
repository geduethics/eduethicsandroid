package com.myliveclassroom.eduethics.Fragments.TeacherFragments;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.myliveclassroom.eduethics.Adapter.StudentsAdapter;
import com.myliveclassroom.eduethics.Models.TimeTableModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.TinyDB;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;

public class OnlineStudentFeedbackFragment extends Fragment {

    Context mContext;
    View  root;
    TinyDB tinyDB;
    List<TimeTableModel> studentModelList,studentModelListPending,studentModelListEnrolled;
    RecyclerView recyclerViewPendingStudents, recyclerViewEnrolledStudents;
    StudentsAdapter studentsAdapterPending;
    StudentsAdapter studentsAdapterEnrolled;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public OnlineStudentFeedbackFragment(Context context) {
        // Required empty public constructor
        mContext=context;
        tinyDB=new TinyDB(mContext);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragmentrecyclerViewStudents
        root= inflater.inflate(R.layout.fragment_online_student_feedback, container, false);

        initRecycler();

        return  root;
    }

    private void initRecycler() {
        //Enrolled students recycler View
        studentModelList=new ArrayList<>();
        recyclerViewEnrolledStudents = root.findViewById(R.id.recyclerViewStudents);
        studentsAdapterEnrolled=new StudentsAdapter(OnlineStudentFeedbackFragment.this,mContext,studentModelList,1);
        recyclerViewEnrolledStudents.setAdapter(studentsAdapterEnrolled);
        listClassStudent();
    }

    private void listClassStudent(){
        TimeTableModel timeTableModel = new TimeTableModel();
        timeTableModel.setClassId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID));

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = apiService.listStudentsToTutor(timeTableModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                try {
                    if (response.body() == null) {
                        Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    Type type = new TypeToken<List<TimeTableModel>>() {
                    }.getType();

                    studentModelList = new Gson().fromJson(response.body().getAsJsonArray("Response"), type);

                    studentModelListPending=new ArrayList<>();
                    studentModelListEnrolled=new ArrayList<>();
                    for (TimeTableModel studentModel :studentModelList ){
                        if(studentModel.getIsStudentActive().equalsIgnoreCase("N")  ){
                            studentModelListPending.add(studentModel);
                        }else{
                            studentModelListEnrolled.add(studentModel);
                        }
                    }
                    studentsAdapterEnrolled.updateList(studentModelListEnrolled);

                } catch (Exception ex) {
                    Toast.makeText(mContext, LocalConstants.APP_MESSAGES.ERROR1, Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, LocalConstants.APP_MESSAGES.ERROR1, Toast.LENGTH_SHORT).show();
            }
        });
    }
}