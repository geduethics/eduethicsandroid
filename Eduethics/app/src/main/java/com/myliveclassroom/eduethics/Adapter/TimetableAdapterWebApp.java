package com.myliveclassroom.eduethics.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;


import com.myliveclassroom.eduethics.Models.TimeTableModel;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.HomeConstants;
import com.myliveclassroom.eduethics.classes.CommonFunctions;
import com.myliveclassroom.eduethics.classes.TinyDB;

import java.util.List;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;

public class TimetableAdapterWebApp extends RecyclerView.Adapter<TimetableAdapterWebApp.MyViewHolder> {

    List<TimeTableModel> timeTableModelList;
    Context mContext;
    TinyDB tinyDB;


    public TimetableAdapterWebApp(Context context, List<TimeTableModel> list) {
        timeTableModelList = list;
        mContext = context;
        tinyDB=new TinyDB(mContext);
    }

    @NonNull
    @Override
    public TimetableAdapterWebApp.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.time_table_adapter_view, parent, false);
        return new TimetableAdapterWebApp.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TimetableAdapterWebApp.MyViewHolder holder, int position) {
        holder.txtTeacher.setText(CommonFunctions.capitalize(timeTableModelList.get(position).getTeacherName()));
        try {
            holder.vColor.setBackgroundColor(Color.parseColor(timeTableModelList.get(position).getColorCode()));
            //holder.vDivider1.setBackgroundColor(Color.parseColor(timeTableModelList.get(position).getColorCode()));
        }
        catch (Exception ex)
        {
            Log.e(mTAG,"colorview: "+ex.getMessage());
        }
        if(timeTableModelList.get(position).getPeriodName().toLowerCase().equalsIgnoreCase("break")) {
            holder.txtSubject.setText(CommonFunctions.capitalize(timeTableModelList.get(position).getPeriodName()));
            holder.txtTimeslot.setText(timeTableModelList.get(position).getTimeFrom( ) + " to " +timeTableModelList.get(position).getTimeTo() );
            holder.txtPeriod.setVisibility(View.GONE);
            holder.txtClassRoom.setVisibility(View.GONE);
            holder.txtTeacher.setVisibility(View.GONE);
            holder.vDivider1.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            params.weight = 1.0f;
            params.gravity = Gravity.CENTER;
            holder.txtSubject.setLayoutParams(params);
            holder.txtSubject.setVisibility(View.VISIBLE);
            holder.txtSubject.setPadding(1,45,1,45);
            holder.txtTimeslot.setPadding(1,45,10,45);
            holder.txtSubject.setTypeface(null, Typeface.BOLD_ITALIC);
            holder.txtTimeslot.setTypeface(null, Typeface.BOLD_ITALIC);
        }
        else
        {
            holder.txtSubject.setText(CommonFunctions.capitalize(timeTableModelList.get(position).getSubjectName()));
            holder.txtTeacher.setText(CommonFunctions.capitalize(timeTableModelList.get(position).getTeacherName()));
            holder.txtTimeslot.setText(timeTableModelList.get(position).getTimeFrom( ) + " to " +timeTableModelList.get(position).getTimeTo() );
            holder.txtPeriod.setText(timeTableModelList.get(position).getPeriodName().length()>=2?timeTableModelList.get(position).getPeriodName().substring(0,2):timeTableModelList.get(position).getPeriodName());
            holder.txtSubject.setVisibility(View.VISIBLE);
            holder.txtTeacher.setVisibility(View.VISIBLE);
            holder.vDivider1.setVisibility(View.VISIBLE);
            holder.txtClassRoom.setText("Class: "+CommonFunctions.capitalize(timeTableModelList.get(position).getClassName()));
        }

        if(tinyDB.getString("Screen").equalsIgnoreCase(HomeConstants.HOMEWORK_TEACHER))
        {
            holder.layoutHomeWork.setVisibility(View.VISIBLE);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    /*Intent intent=new Intent(mContext, HomeWorkActivity.class);
                    tinyDB.putString("SubjectId",timeTableModelList.get(position).getSubjectId());
                    tinyDB.putString("ClassId",timeTableModelList.get(position).getClassId());
                    tinyDB.putString("SectionId",timeTableModelList.get(position).getSectionId());
                    tinyDB.putString("ClassName",timeTableModelList.get(position).getClassName());
                    tinyDB.putString("SectionName",timeTableModelList.get(position).getSectionName());
                    mContext.startActivity(intent);*/
                }
            });
        }
        else
        {
            holder.layoutHomeWork.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return (timeTableModelList==null?0:timeTableModelList.size());
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtTimeslot;
        TextView txtSubject;
        TextView txtClassRoom;
        TextView txtTeacher;
        TextView txtPeriod;
        View vDivider1;
        View vColor;
        LinearLayout layoutHomeWork;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtTimeslot=itemView.findViewById(R.id.txtTimeSlot);
            txtClassRoom=itemView.findViewById(R.id.txtClassRoom);
            txtSubject=itemView.findViewById(R.id.txtSubject);
            txtTeacher=itemView.findViewById(R.id.txtTeacher);
            txtPeriod=itemView.findViewById(R.id.txtPeriod);
            vDivider1=itemView.findViewById(R.id.vDivider1);
            vColor=itemView.findViewById(R.id.vColor);
            layoutHomeWork=itemView.findViewById(R.id.layoutHomeWork);
        }
    }
}
