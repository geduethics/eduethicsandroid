package com.myliveclassroom.eduethics.Fragments.TeacherFragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.myliveclassroom.eduethics.Activity.MainActivity;
import com.myliveclassroom.eduethics.Adapter.DayAdapter;
import com.myliveclassroom.eduethics.Models.TimeTableModel;
import com.myliveclassroom.eduethics.Models.UserModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.TinyDB;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;


import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.PROFILE;

public class TimeTableCreateFragment extends Fragment {

    Context context;
    TinyDB tinyDB;
    Context mContext;
    Spinner spinnerClass;
    Spinner spinnerSubject;
    Spinner spinnerTeacher;

    List<TimeTableModel> classList;
    List<TimeTableModel> subjectList;
    List<TimeTableModel> teacherList;

    String mClassId = "0", mSubjectId = "0", mTeacherId = "0" ;

    LinearLayout layoutSchoolOptions;
    RecyclerView recyclerViewTimeTable;

    List<Map<String, Object>> data = new ArrayList<>();
    List<TimeTableModel> timeTableModelList;
    View root;
    DayAdapter dayAdapter;
    TextView txtBtnSubmitTimetable;
    final Calendar myCalendar = Calendar.getInstance();
    EditText txtScheduleDate;

    public TimeTableCreateFragment(Context context) {
        this.context = context;
        mContext = context;
        tinyDB = new TinyDB(mContext);
    }

    boolean isEdit = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_time_tabke, container, false);

        MainActivity.TO_WHICH_FRAG = LocalConstants.TO_FRAG.TO_HOME_CLASSROOM;
        if (tinyDB.getString(LocalConstants.INSTITUTE_TYPE.TYPE).equalsIgnoreCase(LocalConstants.INSTITUTE_TYPE.SCHOOL)) {
            root.findViewById(R.id.layoutSchoolOptions).setVisibility(View.VISIBLE);
            txtBtnSubmitTimetable = root.findViewById(R.id.txtBtnSubmitTimetable);
            txtBtnSubmitTimetable.setVisibility(View.VISIBLE);
            txtBtnSubmitTimetable.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (TimeTableModel tableModel : timeTableModelList) {
                        if (tableModel.getIsTimeSet()) {
                            setTimeTable(tableModel);
                        }
                    }
                }
            });
        } else {
            root.findViewById(R.id.layoutSchoolOptions).setVisibility(View.GONE);
        }

        init();
        return root;
    }


    private void init() {
        bindSpinner();
        timeTableModelList = new ArrayList<>();
        recyclerViewTimeTable = root.findViewById(R.id.recyclerFeeStudent);
        recyclerViewTimeTable.setLayoutManager(new LinearLayoutManager(context));
        dayAdapter = new DayAdapter(TimeTableCreateFragment.this, mContext, timeTableModelList);
        recyclerViewTimeTable.setAdapter(dayAdapter);

        if (tinyDB.getString(LocalConstants.INSTITUTE_TYPE.TYPE).equalsIgnoreCase(LocalConstants.INSTITUTE_TYPE.SCHOOL)) {
            getClassData();
        } else {
            getDays();
        }

        //getDays();

    }

    private void bindSpinner() {
        spinnerClass = root.findViewById(R.id.spinnerClass);
        spinnerSubject = root.findViewById(R.id.spinnerSubject);
        spinnerTeacher = root.findViewById(R.id.spinnerTeacher);
        setDate();
    }

    private void setDate() {
        txtScheduleDate = (EditText) root.findViewById(R.id.txtScheduleDate);
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        txtScheduleDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(mContext, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    private void updateLabel() {
        String myFormat = "dd-MMM-yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        txtScheduleDate.setText(sdf.format(myCalendar.getTime()));
    }

    private void getClassData() {
        TimeTableModel timeTableModel = new TimeTableModel();
        timeTableModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        timeTableModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = apiService.classList(timeTableModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                try {
                    if (response.body() == null) {
                        Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (response.body() != null) {
                        Type type = new TypeToken<List<TimeTableModel>>() {
                        }.getType();
                        classList = new Gson().fromJson(response.body().getAsJsonArray("Response"), type);
                        List<String> spinnerArray = new ArrayList<>();
                        spinnerArray.add("--Select Class--");
                        for (TimeTableModel str : classList) {
                            spinnerArray.add(str.getClassName());
                        }

                        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, spinnerArray);
                        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down vieww
                        spinnerClass.setAdapter(spinnerArrayAdapter);
                        spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int myPosition, long myID) {
                                if (myPosition > 0) {
                                    mClassId = classList.get(myPosition - 1).getClassId();
                                    tinyDB.putString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID, mClassId);
                                    listSubjects();
                                } else {
                                    mClassId = "0";
                                    tinyDB.putString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID, mClassId);
                                    listSubjects();
                                }
                                //Log.i("renderSpinner -> ", "onItemSelected: " + myPosition + "/" + myID);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parentView) {
                                // your code here
                            }
                        });
                    }
                } catch (Exception ex) {
                    Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void listSubjects() {
        UserModel userModel = new UserModel();
        userModel.setClassId(mClassId);

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<List<TimeTableModel>> call = apiService.sujectList(userModel);
        call.enqueue(new Callback<List<TimeTableModel>>() {
            @Override
            public void onResponse(Call<List<TimeTableModel>> call, Response<List<TimeTableModel>> response) {
                try {
                    if (response.body() != null) {
                        subjectList = response.body();
                        List<String> spinnerArray = new ArrayList<>();
                        spinnerArray.add("--Select Subject--");
                        for (TimeTableModel str : subjectList) {
                            spinnerArray.add(str.getSubjectName());
                        }

                        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, spinnerArray);
                        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down vieww
                        spinnerSubject.setAdapter(spinnerArrayAdapter);
                        spinnerSubject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                            @Override
                            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int myPosition, long myID) {
                                if (myPosition > 0) {
                                    mSubjectId = subjectList.get(myPosition - 1).getSubjectId();
                                    tinyDB.putString(LocalConstants.TINYDB_LOGIN_KEYS.SUBJECT_ID, mSubjectId);
                                    getTeacherData();
                                } else {
                                    mSubjectId = "0";
                                    tinyDB.putString(LocalConstants.TINYDB_LOGIN_KEYS.SUBJECT_ID, mSubjectId);
                                }

                                Log.i("renderSpinner -> ", "onItemSelected: " + myPosition + "/" + myID);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parentView) {
                                // your code here
                            }
                        });
                    }
                } catch (Exception ex) {
                    Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<List<TimeTableModel>> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getTeacherData() {
        teacherList = new ArrayList<>();
        TimeTableModel timeTableModel = new TimeTableModel();
        timeTableModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        timeTableModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));
        timeTableModel.setSubjectId(mSubjectId);

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = apiService.getTeacherBySubject(timeTableModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                try {

                    if (response.body() != null) {
                        Type type = new TypeToken<List<TimeTableModel>>() {
                        }.getType();

                        teacherList = new Gson().fromJson(response.body().getAsJsonArray("Response"), type);

                        List<String> spinnerArray = new ArrayList<>();
                        spinnerArray.add("--Select Teacher--");
                        for (TimeTableModel str : teacherList) {
                            spinnerArray.add(str.getTeacherName());
                        }

                        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, spinnerArray);
                        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down vieww
                        spinnerTeacher.setAdapter(spinnerArrayAdapter);
                        spinnerTeacher.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                            @Override
                            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int myPosition, long myID) {
                                if (myPosition > 0) {
                                    mTeacherId = teacherList.get(myPosition - 1).getTeacherId();
                                    tinyDB.putString(LocalConstants.TINYDB_LOGIN_KEYS.TEACHER_ID, mTeacherId);
                                    getDays();
                                } else {
                                    mTeacherId = "0";
                                    tinyDB.putString(LocalConstants.TINYDB_LOGIN_KEYS.TEACHER_ID, mTeacherId);
                                }
                                Log.i("renderSpinner -> ", "onItemSelected: " + myPosition + "/" + myID);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parentView) {
                                // your code here
                            }
                        });

                    }
                } catch (Exception ex) {
                    Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getDays() {
        timeTableModelList = new ArrayList<>();
        TimeTableModel timeTableModel = new TimeTableModel();

        timeTableModel.setUserId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
        timeTableModel.setTeacherId(mTeacherId);
        timeTableModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        timeTableModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));
        timeTableModel.setClassId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID));
        timeTableModel.setSubjectId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SUBJECT_ID));
        timeTableModel.setProfileType(tinyDB.getString(PROFILE));
        timeTableModel.setInstituteType(tinyDB.getString(LocalConstants.INSTITUTE_TYPE.TYPE));

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = apiService.getTimeTableDays(timeTableModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.e(mTAG, "userModelList" + response.body().toString());
                try {
                    timeTableModelList = new ArrayList<>();
                    if (response.body() != null) {
                        Type type = new TypeToken<List<TimeTableModel>>() {
                        }.getType();
                        timeTableModelList = new Gson().fromJson(response.body().getAsJsonArray("Response"), type);
                        dayAdapter.updateList(timeTableModelList);
                        Log.e(mTAG, "userModelList" + timeTableModelList.toString());

                    }
                } catch (Exception ex) {
                    Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setTimeTable(TimeTableModel timeTableModel) {


        timeTableModel.setScheduleDate(txtScheduleDate.getText().toString());
        timeTableModel.setUserId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
        timeTableModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        timeTableModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));
        timeTableModel.setClassId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID));
        timeTableModel.setSubjectId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SUBJECT_ID));
        if (tinyDB.getString(LocalConstants.INSTITUTE_TYPE.TYPE).equalsIgnoreCase(LocalConstants.INSTITUTE_TYPE.SCHOOL)) {
            timeTableModel.setTeacherId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.TEACHER_ID));
            if (txtScheduleDate.getText().toString().trim().equalsIgnoreCase("")) {
                Toast.makeText(mContext, "Please enter Schedule Date", Toast.LENGTH_SHORT).show();
                return;
            }
        } else {
            timeTableModel.setTeacherId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
        }

        timeTableModel.setInstituteType(tinyDB.getString(LocalConstants.INSTITUTE_TYPE.TYPE));

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = null;
        if (tinyDB.getString(LocalConstants.INSTITUTE_TYPE.TYPE).equalsIgnoreCase(LocalConstants.INSTITUTE_TYPE.SCHOOL)) {
            call = apiService.setTimeTableDays(timeTableModel);
        } else {
            call = apiService.setTimeTableDays(timeTableModel);
        }

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.e(mTAG, "userModelList" + response.body().toString());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void datePicked(int position, int hourOfDayFrom, int minFrom, int hourOfDayTo, int minTo, boolean b) {
        timeTableModelList.get(position).setTimeFrom(hourOfDayFrom + ":" + minFrom);
        timeTableModelList.get(position).setTimeTo(hourOfDayTo + ":" + minTo);
        timeTableModelList.get(position).setIsTimeSet(b);
        isEdit = true;

        TimeTableModel timeTableModel = timeTableModelList.get(position);
        setTimeTable(timeTableModel);
        /*FirebaseFirestore.getInstance()
                .collection("classroom")
                .document(HomeClassroomFragment.classId)
                .update("timetable", data);*/
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        /*if (isEdit){
            List<String> ids = new ArrayList<>();
            for (Map<String, Object> currentStudent : HomeClassroomFragment.model.getCurrentStudents()) {
                ids.add(currentStudent.get("id").toString());
            }
            ExtraFunctions.sendNoti(ids, HomeClassroomFragment.model.getClassName()+" Classroom Update","Time table scheduled of "+ HomeClassroomFragment.model.getClassName()+" Classroom by teacher.",getContext());
        }*/
    }
}