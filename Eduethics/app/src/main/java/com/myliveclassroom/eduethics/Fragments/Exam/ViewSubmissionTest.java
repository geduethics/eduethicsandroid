package com.myliveclassroom.eduethics.Fragments.Exam;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.myliveclassroom.eduethics.Activity.MainActivity;
import com.myliveclassroom.eduethics.Adapter.SubmissionAdapter;
import com.myliveclassroom.eduethics.Fragments.SubFragments.EvaluateFragment;
import com.myliveclassroom.eduethics.Models.HomeWorkModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.TinyDB;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;
import static com.myliveclassroom.eduethics.Utils.LocalConstants.FROM.FROM_ASSIGNMENT_FRAG;
import static com.myliveclassroom.eduethics.Utils.LocalConstants.FROM.FROM_TEST_FRAG;


public class ViewSubmissionTest extends Fragment {

    List<HomeWorkModel> homeWorkModelList=new ArrayList<>();
    HomeWorkModel homeWorkModel=new HomeWorkModel();
    String docId;
    List<Map<String, Object>> finalData = new ArrayList<>();
    List<String> keys = new ArrayList<>();
    int from;
    //Map<String, Object> about;
    Context mContext;
    TinyDB tinyDB;
    RecyclerView recyclerViewSubmission;
    View root;
    SubmissionAdapter submissionAdapter;

    public ViewSubmissionTest(Context context,HomeWorkModel homeWorkModel, int from) {
        this.homeWorkModel=homeWorkModel;
        this.from = from;
        mContext=context;
        tinyDB=new TinyDB(mContext);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
          root = inflater.inflate(R.layout.fragment_view_submission_test, container, false);

        MainActivity.TO_WHICH_FRAG = from == FROM_ASSIGNMENT_FRAG ? LocalConstants.TO_FRAG.TO_ASSIGNMENT_FRAG : LocalConstants.TO_FRAG.TO_TEST_FRAG;



        String collection = "";

        if (from == FROM_TEST_FRAG) {
            collection = "testSub";
            ((TextView) root.findViewById(R.id.duration)).setText("Duration :    minutes");
        } else if (from == FROM_ASSIGNMENT_FRAG) collection = "assignmentSub";

        ((TextView) root.findViewById(R.id.topic)).setText( "topic" );
         ((TextView) root.findViewById(R.id.date)).setText("Due Date :  ") ;


        /*if (homeWorkModel.getIsDone() != null && Integer.parseInt(homeWorkModel.getIsDone()) > 0) {
            String []doneAttachments=homeWorkModel.getDoneAttachment().split(",");
            for (String s:doneAttachments){
                keys.add(s);
            }
        }
        if (finalData.size() == 0) {
            root.findViewById(R.id.empty).setVisibility(View.VISIBLE);
            ((TextView) root.findViewById(R.id.emptyMessage)).setText("No Submission Yet !");
        }*/



        intiRecyclerView();
        return root;
    }

    private void intiRecyclerView(){
        recyclerViewSubmission = root.findViewById(R.id.recyclerViewSubmission);
        recyclerViewSubmission.setLayoutManager(new LinearLayoutManager(getContext()));
        submissionAdapter=new SubmissionAdapter(ViewSubmissionTest.this,mContext,homeWorkModelList);
        recyclerViewSubmission.setAdapter(submissionAdapter);
        getHomeWorkDoneList();
    }

    private void getHomeWorkDoneList(){

        homeWorkModel.setProfileType("Tutor");
            ApiInterface apiService =
                    ApiClient.getClient(mContext).create(ApiInterface.class);

            Call<JsonObject> call = apiService.HomeWorkDoneList(homeWorkModel);
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    try {
                        if (response.body() != null) {

                            Type type = new TypeToken<List<HomeWorkModel>>() {
                            }.getType();
                            homeWorkModelList = new Gson().fromJson(response.body().getAsJsonArray("Response"), type);
                            if (homeWorkModelList.size() == 0) {
                                root.findViewById(R.id.empty).setVisibility(View.VISIBLE);
                                ((TextView) root.findViewById(R.id.emptyMessage)).setText("No Assignment Found!");
                            }else{
                                root.findViewById(R.id.empty).setVisibility(View.GONE);
                            }
                            submissionAdapter.updateList(homeWorkModelList);
                        }

                    } catch (Exception ex) {
                        Toast.makeText(mContext, LocalConstants.APP_MESSAGES.ERROR1, Toast.LENGTH_SHORT).show();
                        Log.e(mTAG, "error =" + ex.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Log.e(mTAG, "error =" + t.toString());
                }
            });
    }

    public void evaluate(HomeWorkModel homeWorkModel) {
        getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(
                R.anim.slide_in,  // enter
                R.anim.fade_out,  // exit
                R.anim.fade_in,   // popEnter
                R.anim.slide_out  // popExit
        ).replace(R.id.fragmentFrame, new EvaluateFragment(mContext,homeWorkModel, from)).commit();
    }
}