package com.myliveclassroom.eduethics.Fragments.TeacherFragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.myliveclassroom.eduethics.Activity.MainActivity;
import com.myliveclassroom.eduethics.Activity.TimeTableTActivity;
import com.myliveclassroom.eduethics.Adapter.TimeTableScheduleViewAdapter;
import com.myliveclassroom.eduethics.Models.TimeTableModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.CommonFunctions;
import com.myliveclassroom.eduethics.classes.TinyDB;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.PROFILE;


public class TimeTableScheduleViewFragment extends Fragment {

    Context context;
    TinyDB tinyDB;
    Context mContext;
    Spinner spinnerClass;
    Spinner spinnerSubject;
    Spinner spinnerTeacher;

    List<TimeTableModel> classList;

    String mClassId = "0", mSubjectId = "0", mTeacherId = "0";
    int mSpinnerPosition = 0;

    LinearLayout layoutSchoolOptions;
    RecyclerView recyclerViewTimeTable;

    List<Map<String, Object>> data = new ArrayList<>();
    List<TimeTableModel> timeTableModelList;
    List<TimeTableModel> timeTableModelFilterList = new ArrayList<>();

    View root;
    TimeTableScheduleViewAdapter dayAdapter;
    Button txtBtnSubmitTimetable;
    final Calendar myCalendar = Calendar.getInstance();
    EditText txtScheduleDate;
    boolean isEdit = false;

    public TimeTableScheduleViewFragment(Context context) {
        mContext = context;
        tinyDB = new TinyDB(mContext);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_time_table_schedule_view, container, false);
        MainActivity.TO_WHICH_FRAG = LocalConstants.TO_FRAG.TO_HOME_CLASSROOM;
        txtBtnSubmitTimetable = root.findViewById(R.id.txtBtnSubmitTimetable);
        if (tinyDB.getString(LocalConstants.INSTITUTE_TYPE.TYPE).equalsIgnoreCase(LocalConstants.INSTITUTE_TYPE.SCHOOL)) {
            root.findViewById(R.id.layoutSchoolOptions).setVisibility(View.VISIBLE);

            txtBtnSubmitTimetable.setVisibility(View.GONE);
        } else {
            root.findViewById(R.id.layoutSchoolOptions).setVisibility(View.GONE);
        }

        init();
        return root;
    }

    private void TeacherOptions() {
        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER_SCHOOL)
                || tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER)) {
            txtBtnSubmitTimetable.setVisibility(View.VISIBLE);
            txtBtnSubmitTimetable.setText("Create New Schedule");
            spinnerClass.setVisibility(View.VISIBLE);

            mClassId = "0";
            spinnerClass.setVisibility(View.GONE);
            root.findViewById(R.id.frameDate).setVisibility(View.GONE);

            txtBtnSubmitTimetable.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tinyDB.putString(LocalConstants.TINYDB_KEYS.TEMP_SCREEN_NAME, "");
                    tinyDB.putString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID, "");
                    getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(
                            R.anim.slide_in,  // enter
                            R.anim.fade_out,  // exit
                            R.anim.fade_in,   // popEnter
                            R.anim.slide_out  // popExit
                    ).replace(R.id.fragmentFrame, new TimeTableScheduleCreateFragment(mContext)).commit();
                }
            });
        }
    }

    private void StudentOptions() {
        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)) {
            if(txtBtnSubmitTimetable!=null) {
                txtBtnSubmitTimetable.setVisibility(View.GONE);
            }
            mClassId = tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID);
            if(spinnerClass!=null) {
                spinnerClass.setVisibility(View.GONE);
            }
            if(root.findViewById(R.id.frameDate)!=null)
            root.findViewById(R.id.frameDate).setVisibility(View.GONE);
        }
    }

    private void init() {
        tinyDB.putString(LocalConstants.TINYDB_KEYS.TEMP_DATE, "");
        bindSpinner();

        TeacherOptions();
        StudentOptions();

        timeTableModelList = new ArrayList<>();
        recyclerViewTimeTable = root.findViewById(R.id.recyclerFeeStudent);
        recyclerViewTimeTable.setLayoutManager(new LinearLayoutManager(context));
        dayAdapter = new TimeTableScheduleViewAdapter(mContext, TimeTableScheduleViewFragment.this);
        recyclerViewTimeTable.setAdapter(dayAdapter);


        setDate();
        updateLabel();

        if (tinyDB.getString(LocalConstants.INSTITUTE_TYPE.TYPE).equalsIgnoreCase(LocalConstants.INSTITUTE_TYPE.SCHOOL)) {
            getClassData();
        } else {
            //getDays();
        }
        //getDays();
    }

    private void bindSpinner() {
        spinnerClass = root.findViewById(R.id.spinnerClass);
        spinnerSubject = root.findViewById(R.id.spinnerSubject);
        spinnerTeacher = root.findViewById(R.id.spinnerTeacher);
    }

    private void setDate() {
        txtScheduleDate = (EditText) root.findViewById(R.id.txtScheduleDate);
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        txtScheduleDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(mContext, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    private void updateLabel() {
        String myFormat = "dd-MMM-yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        txtScheduleDate.setText(sdf.format(myCalendar.getTime()));
        getDaySchedule();
    }

    private void getClassData() {
        TimeTableModel timeTableModel = new TimeTableModel();
        timeTableModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        timeTableModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = apiService.classList(timeTableModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                try {
                    if (response.body() == null) {
                        Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (response.body() != null) {
                        Type type = new TypeToken<List<TimeTableModel>>() {
                        }.getType();
                        classList = new Gson().fromJson(response.body().getAsJsonArray("Response"), type);
                        List<String> spinnerArray = new ArrayList<>();
                        spinnerArray.add("--All Class--");
                       /* for (TimeTableModel str : classList) {
                            spinnerArray.add(str.getClassName());
                        }*/
                        for (int i = 0; i < classList.size(); i++) {
                            spinnerArray.add(classList.get(i).getClassName());
                            if (mClassId.equalsIgnoreCase(classList.get(i).getClassId())) {
                                mSpinnerPosition = i;
                            }
                        }

                        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, spinnerArray);
                        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down vieww
                        spinnerClass.setAdapter(spinnerArrayAdapter);

                        //SET last selected item

                        //spinnerClass.setSelection(mSpinnerPosition);

                        spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int myPosition, long myID) {
                                if (myPosition > 0) {
                                    mSpinnerPosition = myPosition;
                                    mClassId = classList.get(myPosition - 1).getClassId();
                                    //listSubjects();
                                    getDaySchedule();
                                } else {
                                    mSpinnerPosition = myPosition;
                                    mClassId = "0";
                                    //listSubjects();
                                }
                                //Log.i("renderSpinner -> ", "onItemSelected: " + myPosition + "/" + myID);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parentView) {
                                // your code here
                            }
                        });
                    }
                } catch (Exception ex) {
                    Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getDaySchedule() {

        if (txtScheduleDate.getText().toString().trim().equalsIgnoreCase("")) {
            Toast.makeText(mContext, "Please select Schedule Date", Toast.LENGTH_SHORT).show();
            return;
        }

        timeTableModelList = new ArrayList<>();
        TimeTableModel timeTableModel = new TimeTableModel();

        timeTableModel.setUserId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
        timeTableModel.setTeacherId(mTeacherId);
        timeTableModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        timeTableModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));
        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER_SCHOOL)
                || tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER)) {
            timeTableModel.setClassId("0");
        }
        timeTableModel.setSubjectId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SUBJECT_ID));
        timeTableModel.setProfileType(tinyDB.getString(PROFILE));
        timeTableModel.setInstituteType(tinyDB.getString(LocalConstants.INSTITUTE_TYPE.TYPE));

        timeTableModel.setScheduleDate(txtScheduleDate.getText().toString());

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = apiService.timeTable_ScheduleSelect(timeTableModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.e(mTAG, "userModelList" + response.body().toString());
                try {
                    timeTableModelList = new ArrayList<>();

                    if (response.body() != null) {
                        Type type = new TypeToken<List<TimeTableModel>>() {
                        }.getType();
                        timeTableModelList = new Gson().fromJson(response.body().getAsJsonArray("Response"), type);

                        dayAdapter.updateList(timeTableModelList);
                        Log.e(mTAG, "userModelList" + timeTableModelList.toString());
                    }
                } catch (Exception ex) {
                    Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void viewSchedule(String date, String classId) {
        tinyDB.putString(LocalConstants.TINYDB_KEYS.TEMP_DATE, date);
        tinyDB.putString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID, classId);
        getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(
                R.anim.slide_in,  // enter
                R.anim.fade_out,  // exit
                R.anim.fade_in,   // popEnter
                R.anim.slide_out  // popExit
        ).replace(R.id.fragmentFrame, new TimeTableScheduleCreateFragment(mContext)).commit();
    }

    public void viewEditSchedule(String date, String classId) {
        tinyDB.putString(LocalConstants.TINYDB_KEYS.TEMP_DATE, date);
        tinyDB.putString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID, classId);
        getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(
                R.anim.slide_in,  // enter
                R.anim.fade_out,  // exit
                R.anim.fade_in,   // popEnter
                R.anim.slide_out  // popExit
        ).replace(R.id.fragmentFrame, new TimeTableScheduleCreateFragment(mContext)).commit();
    }

    public void sendScheduleOnEmail(TimeTableModel timeTableModel) {

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call =  apiService.sendSheduleEmail(timeTableModel);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Toast.makeText(mContext, "sent successfully", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}