package com.myliveclassroom.eduethics.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager2.widget.ViewPager2;


import com.myliveclassroom.eduethics.Activity.MainActivity;
import com.myliveclassroom.eduethics.Adapter.FragmentAdapter;
import com.myliveclassroom.eduethics.Fragments.HomeFragemtns.AssignmentFragment;
import com.myliveclassroom.eduethics.Fragments.HomeFragemtns.ChatFragment;
import com.myliveclassroom.eduethics.Fragments.HomeFragemtns.NoticeBoardFragment;
import com.myliveclassroom.eduethics.Fragments.HomeFragemtns.StudentFragment;
import com.myliveclassroom.eduethics.Fragments.HomeFragemtns.StudyMaterialFragment;
import com.myliveclassroom.eduethics.Fragments.HomeFragemtns.SummaryFragment;
import com.myliveclassroom.eduethics.Fragments.Exam.TestFragment;
import com.myliveclassroom.eduethics.Models.HomeClassroomWholeModel2;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;

public class HomeClassroomFragment extends Fragment {

    Context context;
    View root;

    TabLayout tabLayout;
    ViewPager2 pager2;
    FragmentAdapter adapter;

    public static HomeClassroomWholeModel2 model2;
    public static String classId;
    public static int from=0;
    public static int toWhichFrag = 0;
    int mTabPosition=0;

    public HomeClassroomFragment(Context context, String classID, int from) {
        this.context = context;
        classId = classID;
        HomeClassroomFragment.from = from;
        mTabPosition=from;
        MainActivity.TO_WHICH_FRAG = LocalConstants.TO_FRAG.TO_CLASSROOM;
        model2=new HomeClassroomWholeModel2();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.fragment_home_classroom, container, false);
        tabLayout = root.findViewById(R.id.tab_layout);
        pager2 = root.findViewById(R.id.pageViewerHomeClassroom);

        //runTask();
        forward();

        MainActivity.actionBarTitle.setText("Classroom");

        return root;
    }



    public void runTask() {
        root.findViewById(R.id.progress).setVisibility(View.VISIBLE);
        FirebaseFirestore.getInstance()
                .collection("classroom")
                .document(classId)
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        //model = documentSnapshot.toObject(HomeClassroomWholeModel.class);
                        forward();
                    }
                });
    }

    private void forward() {

        if (getActivity()==null){
            return;
        }

        FragmentManager fm = getActivity().getSupportFragmentManager();
        ArrayList<Fragment> fragments = new ArrayList<>();

        fragments.add(new SummaryFragment(context));
        fragments.add(new StudentFragment(context));
        fragments.add(new ChatFragment(context));
        fragments.add(new AssignmentFragment(context));
        fragments.add(new TestFragment(context));
        fragments.add(new NoticeBoardFragment(context));
        fragments.add(new StudyMaterialFragment(context));

        adapter = new FragmentAdapter(fm, getLifecycle(), fragments);
        pager2.setAdapter(adapter);


        tabLayout.addTab(tabLayout.newTab().setText("Summary"));
        tabLayout.addTab(tabLayout.newTab().setText("Students"));
        tabLayout.addTab(tabLayout.newTab().setText("Chat"));
        tabLayout.addTab(tabLayout.newTab().setText("Assignments"));
        tabLayout.addTab(tabLayout.newTab().setText("Test"));
        tabLayout.addTab(tabLayout.newTab().setText("Notice Board"));
        tabLayout.addTab(tabLayout.newTab().setText("Study Material"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_CENTER);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);


        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                pager2.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        pager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                tabLayout.selectTab(tabLayout.getTabAt(position));
                Log.e(mTAG,position+"");
                if (position==4){

                }
                if (position==3){

                }
            }
        });
        tabLayout.selectTab(tabLayout.getTabAt(mTabPosition));
        /*new Thread(new Runnable() {
            @Override
            public void run() {
                tabLayout.selectTab(tabLayout.getTabAt(mTabPosition));
                *//*tabLayout.setScrollPosition(mTabPosition,0f,true);
                pager2.setCurrentItem(mTabPosition);*//*
            }
        }).start();*/

        root.findViewById(R.id.progress).setVisibility(View.GONE);
    }
}