package com.myliveclassroom.eduethics.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.myliveclassroom.eduethics.Activity.GalleryImagesActivity;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.classes.TinyDB;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.API_BASE_URL;


public class GalleryImgAdapter extends RecyclerView.Adapter<GalleryImgAdapter.ViewHolder> {

    String []galleryModelList;
    Context mContext;
    TinyDB tinyDB;

    public GalleryImgAdapter(Context context, String [] galleryModels) {
        tinyDB = new TinyDB(context);
        mContext = context;
        galleryModelList = galleryModels;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.gallery_img_adapter, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Glide.with(mContext)
                .asBitmap()
                .load(API_BASE_URL + "Gallery/" + galleryModelList[position])
                .into(holder.img);

        holder.img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mContext instanceof GalleryImagesActivity) {
                    ((GalleryImagesActivity) mContext).startSlider(position);
                }
            }
        });

        holder.txtBtnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mContext instanceof GalleryImagesActivity) {
                    ((GalleryImagesActivity) mContext).deleteImg(position);
                }
            }
        });

        if (tinyDB.getString("IsAdmin").equalsIgnoreCase("Y")) {
            holder.txtBtnDelete.setVisibility(View.VISIBLE);
        }
        else{
            holder.txtBtnDelete.setVisibility(View.GONE);
        }
    }


    @Override
    public int getItemCount() {
        return (galleryModelList == null ? 0 : galleryModelList.length);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img)
        ImageView img;

        @BindView(R.id.txtBtnDelete)
        TextView txtBtnDelete;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
