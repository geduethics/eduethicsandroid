package com.myliveclassroom.eduethics.Services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import com.myliveclassroom.eduethics.R;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;

public class BootBroadcast extends BroadcastReceiver {

    private static final String TAG="BootBroadcast";

    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, "Restarting", Toast.LENGTH_SHORT).show();
        //ctx.startService(new Intent(ctx, BackgroundServiceClass.class));

        Log.e(mTAG+"-"+TAG,"receiver");
        if (Build.VERSION.SDK_INT>= Build.VERSION_CODES.O) {
            if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
                Log.i(mTAG+"-"+TAG,"enque");
                GeneralService.enqueueWork(context,new Intent());
            }
        }
        else {
            Log.e(mTAG+"-"+TAG,"service started again");
            Intent i=new Intent(context, BootBroadcast.class);
            context.startService(i);
        }
    }



}