package com.myliveclassroom.eduethics.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.myliveclassroom.eduethics.DB.DBHelper;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.TinyDB;

import java.util.ArrayList;
import java.util.List;

import static com.myliveclassroom.eduethics.Utils.HomeConstants.ATTENDANCE;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.ATTENDANCE_TEACHER;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.BIRTHDAY;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.CIRCCULARADMIN;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.CIRCULARS;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.DAILY_DOSE;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.DIGITAL_LIBRARY;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.EXAMS;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.FEE;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.GALLERY;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.GALLERYADMIN;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.HEALTH_FEEBACK;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.HOMEWORK;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.HOMEWORK_TEACHER;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.ICARD;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.LEAVE_APP;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.MESSAGES;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.PROFILE;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.REPORTS;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.RESULTS;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.SCHEDULE_STUDENT;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.SCHEDULE_TEACHER;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.SHARE_APP;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.SYLLABUS;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.TEACHERS;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.TIMETABLE;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.TIMETABLE_TEACHER;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.UDAAN;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.VIDEO_LECTURES;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.WORD_OF_THE_DAY;

public class HomeAdapter extends BaseAdapter {
    private final List<Item> mItems = new ArrayList<Item>();
    private final LayoutInflater mInflater;
    Context mContext;
    TinyDB tinyDB;
    boolean mAdminMenu = false;
    DBHelper dbHelper;
    int countHomeworkCount = 0;
    int countCircularCount = 0;

    public HomeAdapter(Context context, Boolean adminMenu) {
        mInflater = LayoutInflater.from(context);
        mContext = context;
        tinyDB = new TinyDB(mContext);
        mAdminMenu = adminMenu;
        dbHelper = new DBHelper(mContext);

        //countHomeworkCount=dbHelper.countNotifications(TINY_CONSTANT_HomeworkNotification);

        //=====================
        //      Profiles
        //      1. Parent
        //      2. Student
        //      3. Teacher
        //      4. Owner
        //=======================

        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.SCHOOL)) {
           // mItems.add(new Item(R.drawable.ic_fee, R.string.home_fee, FEE, true));
           // mItems.add(new Item(R.drawable.ic_user, R.string.home_teachers, TEACHERS, true));
            //mItems.add(new Item(R.drawable.ic_home_cal_png, R.string.home_attendance, ATTENDANCE, true));
            mItems.add(new Item(R.drawable.ic_home_gallery_png, R.string.home_gallery, GALLERY, false));
            mItems.add(new Item(R.drawable.ic_home_birthday, R.string.home_birthday, BIRTHDAY, false));
            mItems.add(new Item(R.drawable.ic_home_share, R.string.home_share, SHARE_APP, false));
        } else if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.PARENT)) {
            mItems.add(new Item(R.drawable.ic_home_cal_png, R.string.home_attendance, ATTENDANCE, true));
            //mItems.add(new Item(R.drawable.ic_exam, R.string.home_exams,EXAMS,true));
            //mItems.add(new Item(R.drawable.ic_results, R.string.home_result,RESULTS,true));
            mItems.add(new Item(R.drawable.ic_home_book_png, R.string.home_homework, HOMEWORK, true));
            mItems.add(new Item(R.drawable.ic_fee, R.string.home_fee, FEE, true));
            //mItems.add(new Item(R.drawable.ic_icard, R.string.home_icard, ICARD, true));
            //mItems.add(new Item(R.drawable.ic_home_timer_png, R.string.home_timetable, TIMETABLE, false));
            mItems.add(new Item(R.drawable.ic_home_timer_png, R.string.home_schedule, SCHEDULE_STUDENT, false));
            //mItems.add(new Item(R.drawable.ic_youtube, R.string.VIDEO_LECTURES, VIDEO_LECTURES, false));

            //mItems.add(new Item(R.drawable.ic_timetable, R.string.home_events,EVENTS,false));
            mItems.add(new Item(R.drawable.ic_home_exam, R.string.home_exams, EXAMS, true));
            mItems.add(new Item(R.drawable.ic_home_result, R.string.home_result, RESULTS, true));
            mItems.add(new Item(R.drawable.ic_home_gallery_png, R.string.home_gallery, GALLERY, false));
            mItems.add(new Item(R.drawable.ic_exam, R.string.home_leave_application, LEAVE_APP, false));
            mItems.add(new Item(R.drawable.ic_home_paperboard_png, R.string.home_circulars, CIRCULARS, false));
            //mItems.add(new Item(R.drawable.ic_home_msg, R.string.home_msg, MESSAGES, false));
            mItems.add(new Item(R.drawable.ic_home_birthday, R.string.home_birthday, BIRTHDAY, false));

            //mItems.add(new Item(R.drawable.ic_home_udan, R.string.home_udaan, UDAAN, false));
            //mItems.add(new Item(R.drawable.ic_home_word_of_the_day, R.string.home_word_of_the_day, WORD_OF_THE_DAY, false));
            //mItems.add(new Item(R.drawable.ic_home_daily_dose, R.string.home_daily_dose, DAILY_DOSE, false));

            //mItems.add(new Item(R.drawable.ic_home_library, R.string.home_library, DIGITAL_LIBRARY, false));
            //mItems.add(new Item(R.drawable.ic_home_syllabus, R.string.home_syllabus, SYLLABUS, false));
            mItems.add(new Item(R.drawable.ic_home_share, R.string.home_share, SHARE_APP, false));
            //  mItems.add(new Item(R.drawable.ic_home_bell_png, R.string.home_notifications, NOTIFICATIONS, false));
        } else if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)) {
            mItems.add(new Item(R.drawable.ic_home_cal_png, R.string.home_attendance, ATTENDANCE, true));
            //mItems.add(new Item(R.drawable.ic_exam, R.string.home_exams,EXAMS,true));
            //mItems.add(new Item(R.drawable.ic_results, R.string.home_result,RESULTS,true));
            mItems.add(new Item(R.drawable.ic_home_book_png, R.string.home_homework, HOMEWORK, true));
            mItems.add(new Item(R.drawable.ic_fee, R.string.home_fee, FEE, true));
            //mItems.add(new Item(R.drawable.ic_icard, R.string.home_icard, ICARD, true));
            //mItems.add(new Item(R.drawable.ic_home_timer_png, R.string.home_timetable, TIMETABLE, false));
            mItems.add(new Item(R.drawable.ic_home_timer_png, R.string.home_schedule, SCHEDULE_STUDENT, false));
            //mItems.add(new Item(R.drawable.ic_youtube, R.string.VIDEO_LECTURES, VIDEO_LECTURES, false));

            //mItems.add(new Item(R.drawable.ic_timetable, R.string.home_events,EVENTS,false));
            mItems.add(new Item(R.drawable.ic_home_exam, R.string.home_exams, EXAMS, true));
            mItems.add(new Item(R.drawable.ic_home_result, R.string.home_result, RESULTS, true));
            mItems.add(new Item(R.drawable.ic_home_gallery_png, R.string.home_gallery, GALLERY, false));
            //mItems.add(new Item(R.drawable.ic_exam, R.string.home_leave_application, LEAVE_APP, false));
            mItems.add(new Item(R.drawable.ic_home_paperboard_png, R.string.home_circulars, CIRCULARS, false));
            //mItems.add(new Item(R.drawable.ic_home_msg, R.string.home_msg, MESSAGES, false));
            mItems.add(new Item(R.drawable.ic_home_birthday, R.string.home_birthday, BIRTHDAY, false));

            //mItems.add(new Item(R.drawable.ic_home_udan, R.string.home_udaan, UDAAN, false));
            //mItems.add(new Item(R.drawable.ic_home_word_of_the_day, R.string.home_word_of_the_day, WORD_OF_THE_DAY, false));
            //mItems.add(new Item(R.drawable.ic_home_daily_dose, R.string.home_daily_dose, DAILY_DOSE, false));

            //mItems.add(new Item(R.drawable.ic_home_library, R.string.home_library, DIGITAL_LIBRARY, false));
            //mItems.add(new Item(R.drawable.ic_home_syllabus, R.string.home_syllabus, SYLLABUS, false));
            mItems.add(new Item(R.drawable.ic_home_share, R.string.home_share, SHARE_APP, false));
            //  mItems.add(new Item(R.drawable.ic_home_bell_png, R.string.home_notifications, NOTIFICATIONS, false));
        }

        if ((tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER) ||
                tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER_SCHOOL)) && mAdminMenu == false) {
            //Teacher menu
            mItems.add(new Item(R.drawable.ic_home_cal_png, R.string.home_teacher_attendance, ATTENDANCE_TEACHER, false));

            mItems.add(new Item(R.drawable.ic_home_book_png, R.string.home_homework, HOMEWORK_TEACHER, false));

            //mItems.add(new Item(R.drawable.ic_home_timer_png, R.string.home_teacher_timetable, TIMETABLE_TEACHER, false));
            mItems.add(new Item(R.drawable.ic_home_timer_png, R.string.home_schedule, SCHEDULE_TEACHER, false));
            mItems.add(new Item(R.drawable.ic_home_timer_png, R.string.home_teacher_healthfeeback, HEALTH_FEEBACK, false));
            //mItems.add(new Item(R.drawable.ic_youtube, R.string.VIDEO_LECTURES, VIDEO_LECTURES, false));
            mItems.add(new Item(R.drawable.ic_home_exam, R.string.home_exams, EXAMS, true));
            mItems.add(new Item(R.drawable.ic_home_result, R.string.home_result, RESULTS, true));
            mItems.add(new Item(R.drawable.ic_fee, R.string.home_fee, FEE, true));
            mItems.add(new Item(R.drawable.ic_home_paperboard_png, R.string.home_circulars, CIRCULARS, false));

            mItems.add(new Item(R.drawable.ic_home_gallery_png, R.string.home_gallery, GALLERY, false));
            //mItems.add(new Item(R.drawable.ic_home_msg, R.string.home_msg, MESSAGES, false));
            mItems.add(new Item(R.drawable.ic_home_birthday, R.string.home_birthday, BIRTHDAY, false));

            //mItems.add(new Item(R.drawable.ic_home_udan, R.string.home_udaan, UDAAN, false));
            //mItems.add(new Item(R.drawable.ic_home_word_of_the_day, R.string.home_word_of_the_day, WORD_OF_THE_DAY, false));
            //mItems.add(new Item(R.drawable.ic_home_daily_dose, R.string.home_daily_dose, DAILY_DOSE, false));
            //mItems.add(new Item(R.drawable.ic_home_library, R.string.home_library, DIGITAL_LIBRARY, false));
            //mItems.add(new Item(R.drawable.ic_home_syllabus, R.string.home_syllabus, SYLLABUS, false));
            mItems.add(new Item(R.drawable.ic_home_result, R.string.home_reports, REPORTS, true));
            mItems.add(new Item(R.drawable.ic_home_share, R.string.home_share, SHARE_APP, false));

            //   mItems.add(new Item(R.drawable.ic_home_bell_png, R.string.home_notifications, NOTIFICATIONS, false));
        }

        if (mAdminMenu) {
            mItems.add(new Item(R.drawable.ic_home_gallery_png, R.string.home_gallery, GALLERYADMIN, false));
            mItems.add(new Item(R.drawable.ic_home_paperboard_png, R.string.home_circulars, CIRCCULARADMIN, false));
        }

        //mItems.add(new Item(R.drawable.ic_profile, R.string.home_profile,PROFILE,false));
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Item getItem(int i) {
        return mItems.get(i);
    }

    @Override
    public long getItemId(int i) {
        return mItems.get(i).drawableId;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        ImageView picture;
        TextView name;
        LinearLayout mMainLayout;
        TextView tvActivity;
        TextView txtPro;
        ImageView imgPro;

        if (v == null) {
            if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.SCHOOL)) {
                v = mInflater.inflate(R.layout.home_admin_adapter, viewGroup, false);
            } else {
                v = mInflater.inflate(R.layout.home_adapter, viewGroup, false);
            }
            v.setTag(R.id.ivHomeImg, v.findViewById(R.id.ivHomeImg));
            v.setTag(R.id.tvHomeTxt, v.findViewById(R.id.tvHomeTxt));
            v.setTag(R.id.tvActivity, v.findViewById(R.id.tvActivity));
            v.setTag(R.id.imgPro, v.findViewById(R.id.imgPro));
            v.setTag(R.id.txtPro, v.findViewById(R.id.txtPro));
        }

        picture = (ImageView) v.getTag(R.id.ivHomeImg);
        name = (TextView) v.getTag(R.id.tvHomeTxt);
        tvActivity = (TextView) v.getTag(R.id.tvActivity);
        mMainLayout = (LinearLayout) v.findViewById(R.id.mMainLayout);

        Item item = getItem(i);

        picture.setImageResource(item.drawableId);
        name.setText(item.name);
        tvActivity.setText(item.activity);
        if (tinyDB.getString("UserType").equalsIgnoreCase("Parent")) {
            if (name.getText().toString().equalsIgnoreCase(mContext.getString(R.string.home_homework)) && countHomeworkCount > 0) {
                ((ImageView) v.getTag(R.id.imgPro)).setVisibility(View.VISIBLE);
                ((TextView) v.getTag(R.id.txtPro)).setVisibility(View.VISIBLE);
                ((TextView) v.getTag(R.id.txtPro)).setText("" + countHomeworkCount);
            }
            if (name.getText().toString().equalsIgnoreCase(mContext.getString(R.string.home_circulars)) && countCircularCount > 0) {
                ((ImageView) v.getTag(R.id.imgPro)).setVisibility(View.VISIBLE);
                ((TextView) v.getTag(R.id.txtPro)).setVisibility(View.VISIBLE);
                ((TextView) v.getTag(R.id.txtPro)).setText("" + countCircularCount);

            }
        }
        return v;
    }

    private static class Item {
        public final int name;
        public final int drawableId;
        public final String activity;
        public final boolean visibility;

        Item(int drawableId, int name, String _activity, boolean _visibility) {
            this.name = name;
            this.drawableId = drawableId;
            visibility = _visibility;
            activity = _activity;
        }
    }
}