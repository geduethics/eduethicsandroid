package com.myliveclassroom.eduethics.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.myliveclassroom.eduethics.Activity.VideoConfrenceActivity;
import com.myliveclassroom.eduethics.Adapter.ClassroomTeachersAdapter2;
import com.myliveclassroom.eduethics.Models.TimeTableModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.TinyDB;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;


public class DynamicFragment extends Fragment {


    private static final String ARG_SECTION_NUMBER = "section_number";
    private int sectionNumber;
    ;

    View root;
    TinyDB tinyDB;
    Context mContext;
    private String mDayId = "0";
    ProgressBar progressBar;


    List<TimeTableModel> timeTableModelList;
    ClassroomTeachersAdapter2 timetableAdapter;

    RecyclerView recyclerViewTimeTable;


    public DynamicFragment() {
    }

    public static DynamicFragment newInstance(int sectionNumber) {
        DynamicFragment fragment = new DynamicFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setHasOptionsMenu(true);
        sectionNumber = getArguments() != null ? getArguments().getInt(ARG_SECTION_NUMBER) : 1;
        Log.e(mTAG,"sectionNumber="+sectionNumber);

        mContext = getContext();
        tinyDB = new TinyDB(getContext());
        mDayId= String.valueOf(sectionNumber+1);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        root = inflater.inflate(R.layout.fragment_one_one, container, false);
        ButterKnife.bind(this, root);
        progressBar=root.findViewById(R.id.progressBar);
        initRecyclerView(root);

        return root;
    }

    private void initRecyclerView(View view) {
        timeTableModelList=new ArrayList<>();
        recyclerViewTimeTable=view.findViewById(R.id.recyclerViewTimeTable);
        recyclerViewTimeTable.setLayoutManager(new LinearLayoutManager(mContext));
        timetableAdapter=new ClassroomTeachersAdapter2(mContext,timeTableModelList,DynamicFragment.this);
        recyclerViewTimeTable.setAdapter(timetableAdapter);
        getTimeTable();
    }

    private void getTimeTable() {
        TimeTableModel listModel = new TimeTableModel();
        listModel.setDay(mDayId);
        listModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));
        listModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        listModel.setClassId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID));

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = apiService.getTimeTable(listModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                try {
                    Type type = new TypeToken<List<TimeTableModel>>() {
                    }.getType();

                    timeTableModelList = new Gson().fromJson(response.body().getAsJsonArray("Response"), type);
                    timetableAdapter=new ClassroomTeachersAdapter2(mContext,timeTableModelList,DynamicFragment.this);
                    recyclerViewTimeTable.setAdapter(timetableAdapter);

                } catch (Exception ex) {
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(mTAG, "error =" + t.toString());
            }
        });
    }

    String mRoomName = "EduEthicsLiveClassRoom";

    public void joinClass() {
        Toast.makeText(mContext, "Go Live", Toast.LENGTH_SHORT).show();

        tinyDB.putString(LocalConstants.Jitsi_CONSTANTS.PROFILE, LocalConstants.Jitsi_CONSTANTS.JOIN_AS_STUDENT);
        root.findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
        startActivity(new Intent(getContext(), VideoConfrenceActivity.class)
                .putExtra("roomId", mRoomName + tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID))
                .putExtra("classId", tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID)));

    }

    @Override
    public void onResume() {
        super.onResume();
        progressBar.setVisibility(View.GONE);
    }

}