package com.myliveclassroom.eduethics.Models;

public class DashboardModel {
    private String SchoolId;
    private String SessionId;
    private String Year;
    private String Month;
    private String TotalStudent;
    private String Paid;
    private String Unpaid;

    private String TotalTeacher;
    private String TeacherPresent,TeacherAbsent,TeacherLeave;
    private String StudentPresent,StudentAbsent,StudentLeave;


    public String getSchoolId() {
        return SchoolId;
    }

    public void setSessionId(String sessionId) {
        SessionId = sessionId;
    }

    public void setSchoolId(String schoolId) {
        SchoolId = schoolId;
    }

    public void setYear(String year) {
        Year = year;
    }

    public void setMonth(String month) {
        Month = month;
    }

    public String getYear() {
        return Year;
    }

    public String getMonth() {
        return Month;
    }

    public String getSessionId() {
        return SessionId;
    }

    public String getPaid() {
        return Paid;
    }

    public String getTotalStudent() {
        return TotalStudent;
    }

    public String getUnpaid() {
        return Unpaid;
    }

    public void setPaid(String paid) {
        Paid = paid;
    }

    public void setUnpaid(String unpaid) {
        Unpaid = unpaid;
    }

    public void setTotalStudent(String totalStudent) {
        TotalStudent = totalStudent;
    }

    public String getStudentAbsent() {
        return StudentAbsent;
    }

    public String getStudentLeave() {
        return StudentLeave;
    }

    public String getTeacherPresent() {
        return TeacherPresent;
    }

    public String getTotalTeacher() {
        return TotalTeacher;
    }

    public String getStudentPresent() {
        return StudentPresent;
    }

    public String getTeacherAbsent() {
        return TeacherAbsent;
    }


    public String getTeacherLeave() {
        return TeacherLeave;
    }

    public void setTotalTeacher(String totalTeacher) {
        TotalTeacher = totalTeacher;
    }


}
