package com.myliveclassroom.eduethics.Fragments.TeacherFragments;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.myliveclassroom.eduethics.Activity.MainActivity;
import com.myliveclassroom.eduethics.Adapter.WebsiteClassroomAdapter;
import com.myliveclassroom.eduethics.Models.WebsiteModel;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.GlobalVariables;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.app.Activity.RESULT_OK;
import static android.content.ClipDescription.MIMETYPE_TEXT_PLAIN;
import static com.myliveclassroom.eduethics.Utils.LocalConstants.PERMISSION_CODE.IMAGE_INS;
import static com.myliveclassroom.eduethics.Utils.LocalConstants.PERMISSION_CODE.IMAGE_PROFILE;

public class WebSitePage extends Fragment {

    View root;
    ImageView insLogo, profilePic;
    TextInputEditText insName, insAbout, insSubject, experiance, address, city, state, youtubeLink, fbLink;
    List<String> selected = new ArrayList<>();
    Button contiBtn;
    List<String> selectedSub = new ArrayList<>();
    List<Map<String, String>> demoLinks = new ArrayList<>();
    Uri profileUri = null, insLogouri = null;
    Map<String, Object> editSiteData = new HashMap<>();

    public String editSite = null;
    StorageReference storageRef = FirebaseStorage.getInstance().getReferenceFromUrl(LocalConstants.STORAGE_REF);    //change the url according to your firebase app

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_web_site_page, container, false);
        MainActivity.TO_WHICH_FRAG = LocalConstants.TO_FRAG.TO_CLASSROOM;

        contiBtn = root.findViewById(R.id.continue_website);


        insLogo = root.findViewById(R.id.instituteLogo);
        profilePic = root.findViewById(R.id.profileLogo);
        insName = root.findViewById(R.id.insName);
        insAbout = root.findViewById(R.id.insAbout);
        insSubject = root.findViewById(R.id.insSubject);
        experiance = root.findViewById(R.id.experiance);
        address = root.findViewById(R.id.address);
        city = root.findViewById(R.id.city);
        state = root.findViewById(R.id.state);
        youtubeLink = root.findViewById(R.id.youtubeLink);
        fbLink = root.findViewById(R.id.fbLink);

        if (editSite != null) {
            FirebaseFirestore.getInstance().collection("webD").document(editSite).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                @Override
                public void onSuccess(DocumentSnapshot documentSnapshot) {
                    if (!documentSnapshot.getString("profileLink").equals(""))
                        FirebaseStorage.getInstance().getReference().child(documentSnapshot.getString("profileLink")).getDownloadUrl().addOnSuccessListener(uri -> {
                            if (getContext() != null)
                                if (uri != null)
                                    Glide.with(getContext()).load(uri).centerCrop().into(profilePic);
                        }).addOnFailureListener(exception -> Toast.makeText(getContext(), "Network Issue !", Toast.LENGTH_SHORT).show());

                    if (!documentSnapshot.getString("insLogo").equals(""))
                        FirebaseStorage.getInstance().getReference().child(documentSnapshot.getString("insLogo")).getDownloadUrl().addOnSuccessListener(uri -> {
                            if (getContext() != null)
                                if (uri != null)
                                    Glide.with(getContext()).load(uri).centerCrop().into(insLogo);
                        }).addOnFailureListener(exception -> Toast.makeText(getContext(), "Network Issue !", Toast.LENGTH_SHORT).show());
                    editSiteData = documentSnapshot.getData();
                    city.setText(documentSnapshot.getString("city"));
                    state.setText(documentSnapshot.getString("state"));
                    youtubeLink.setText(documentSnapshot.getString("youtubeLink"));
                    experiance.setText(documentSnapshot.getString("yearsExp"));
                    insSubject.setText(documentSnapshot.getString("subjects"));
                    fbLink.setText(documentSnapshot.getString("fbLink"));
                    insAbout.setText(documentSnapshot.getString("aboutIns"));
                    insName.setText(documentSnapshot.getString("insName"));

                }
            });
        }

        insLogo.setOnClickListener(v -> {
            Intent i = new Intent(
                    Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(i, IMAGE_INS);
        });

        profilePic.setOnClickListener(v -> {
            Intent i = new Intent(
                    Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(i, IMAGE_PROFILE);
        });

        contiBtn.setOnClickListener(v -> conti1());

        return root;
    }

    private void conti1() {
        boolean allDone = true;
        if (insName.getText().toString().trim().equals("")) {
            allDone = false;
            insName.setError("Required!");
        }
        if (state.getText().toString().trim().equals("")) {
            allDone = false;
            state.setError("Required!");
        }
        if (insAbout.getText().toString().trim().equals("")) {
            allDone = false;
            insAbout.setError("Required!");
        }
        if (insSubject.getText().toString().trim().equals("")) {
            allDone = false;
            insSubject.setError("Required!");
        }
        if (experiance.getText().toString().trim().equals("")) {
            allDone = false;
            experiance.setError("Required!");
        }


        if (allDone) {
            TransitionManager.beginDelayedTransition(root.findViewById(R.id.MAIN));
            root.findViewById(R.id.firstWebsite).setVisibility(View.GONE);
            root.findViewById(R.id.secondWebsite).setVisibility(View.VISIBLE);
            FirebaseFirestore.getInstance()
                    .collection("teacher")
                    .document(GlobalVariables.uid)
                    .get()
                    .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            List<Map<String, Object>> data = (List) documentSnapshot.get("classroom");
                            RecyclerView recyclerView = root.findViewById(R.id.classesRecycler);
                            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                            recyclerView.setAdapter(new WebsiteClassroomAdapter(data, selected, selectedSub));
                            contiBtn.setOnClickListener(v -> conti2());
                        }
                    });

        }

    }

    private void conti2() {

        TransitionManager.beginDelayedTransition(root.findViewById(R.id.MAIN));
        root.findViewById(R.id.secondWebsite).setVisibility(View.GONE);
        root.findViewById(R.id.thiredWebsite).setVisibility(View.VISIBLE);
        RecyclerView recyclerView = root.findViewById(R.id.recyclerYoutube);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        List<String> urlsData = new ArrayList<>();
        List<String> titlesData = new ArrayList<>();
        List<String> desData = new ArrayList<>();
        if (editSite != null) {
            for (Map<String, Object> obj : ((List<Map<String, Object>>) editSiteData.get("demoLinks"))) {
                urlsData.add(obj.get("url").toString());
                desData.add(obj.get("des").toString());
                titlesData.add(obj.get("title").toString());
            }
        }

        innerYOutubeAdapter adapter = new innerYOutubeAdapter(urlsData, titlesData, desData, getContext());
        recyclerView.setAdapter(adapter);

        root.findViewById(R.id.addVideo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BottomSheetDialog dialog = new BottomSheetDialog(getContext());
                dialog.setContentView(R.layout.bottom_addyouitube);
                TextInputEditText desbottom = dialog.findViewById(R.id.desbottom), title = dialog.findViewById(R.id.titlebottom), url = dialog.findViewById(R.id.youtubeUrl);
                dialog.findViewById(R.id.paste).setOnClickListener(v12 -> {
                    ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);

                    if ((clipboard.getPrimaryClipDescription().hasMimeType(MIMETYPE_TEXT_PLAIN)) && (clipboard.hasPrimaryClip())) {
                        ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);
                        url.setText(item.getText().toString());
                    }
                });

                dialog.findViewById(R.id.addBottom).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (url.getText().toString().equals("")) {
                            url.setError("Required !");
                        } else if (title.getText().toString().equals("")) {
                            title.setError("Required !");
                        } else {
                            String t = extractYTId(url.getText().toString());
                            if (t == null) {
                                url.setError("Enter Correct URL !");
                            } else {
                                urlsData.add(t);
                                titlesData.add(title.getText().toString());
                                desData.add(desbottom.getText().toString());
                                adapter.notifyItemInserted(urlsData.size() - 1);
                            }


                        }

                    }
                });

                dialog.show();
            }
        });
        if (editSite != null) {
            contiBtn.setText("Save and edit");
        }
        contiBtn.setOnClickListener(v -> {
            for (int i = 0; i < urlsData.size(); i++) {
                Map<String, String> singleData = new HashMap<>();
                singleData.put("title", titlesData.get(i));
                singleData.put("url", urlsData.get(i));
                singleData.put("des", desData.get(i));
                demoLinks.add(singleData);
            }
            if (editSite == null)
                conti3();
            else {
                WebsiteModel model = getTotalModel();
                FirebaseFirestore.getInstance()
                        .collection("webD")
                        .document(editSite)
                        .set(model).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void unused) {
                        getActivity().onBackPressed();
                    }
                });
            }
        });
    }

    private void conti3() {
        TransitionManager.beginDelayedTransition(root.findViewById(R.id.MAIN));
        root.findViewById(R.id.forthWebsite).setVisibility(View.VISIBLE);
        root.findViewById(R.id.thiredWebsite).setVisibility(View.GONE);
        ConstraintLayout.LayoutParams params = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.topToBottom = R.id.scroll;
        params.setMargins(32, 0, 32, 0);
        TextInputEditText websiteName = root.findViewById(R.id.websiteName);
        contiBtn.setText("Check avaiability");
        websiteName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                contiBtn.setText("Check Availability");
            }
        });

        contiBtn.setLayoutParams(params);

        contiBtn.setOnClickListener(v -> {
            if (contiBtn.getText().toString().equals("Check Availability")) {
                if (websiteName.getText().toString().equals("")) {
                    websiteName.setError("Fill this first");
                } else {
                    root.findViewById(R.id.progress).setVisibility(View.VISIBLE);
                    FirebaseFirestore.getInstance()
                            .collection("utilData")
                            .document("websitesDomain")
                            .get()
                            .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                @Override
                                public void onSuccess(DocumentSnapshot documentSnapshot) {
                                    if (documentSnapshot.exists()) {
                                        if (((List<String>) documentSnapshot.get("data")).contains(websiteName.getText().toString())) {
                                            websiteName.setError("Already Exist");
                                            root.findViewById(R.id.progress).setVisibility(View.GONE);
                                            websiteName.setHintTextColor(getResources().getColor(android.R.color.holo_green_dark));
                                        } else {
                                            contiBtn.setText("Build Now");
                                            root.findViewById(R.id.progress).setVisibility(View.GONE);
                                            Toast.makeText(getContext(), "Available", Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        root.findViewById(R.id.progress).setVisibility(View.GONE);
                                        contiBtn.setText("Build Now");
                                    }
                                }
                            });
                }
            } else {
                websiteUrl = websiteName.getText().toString();
                root.findViewById(R.id.progress).setVisibility(View.VISIBLE);
                if (insLogouri != null || profileUri != null) {
                    if (insLogouri != null) {
                        storageRef.child(("websiteImages/" + GlobalVariables.uid + "/logo.jpeg")).putFile(insLogouri);
                        logoUrl = "websiteImages/" + GlobalVariables.uid + "/logo.jpeg";
                    }
                    checkAndPost();
                    if (profileUri != null) {
                        storageRef.child(("websiteImages/" + GlobalVariables.uid + "/profile.jpeg")).putFile(profileUri);
                        profileUrl = "websiteImages/" + GlobalVariables.uid + "/profile.jpeg";
                    }
                    checkAndPost();
                } else uploadTofireBase();
            }
        });
    }

    int counter = 0;
    String logoUrl = "";
    String profileUrl = "";
    String websiteUrl = "";

    private void checkAndPost() {
        counter++;
        if (counter == 2) {
            uploadTofireBase();
        }
    }

    private void uploadTofireBase() {
        WebsiteModel model = getTotalModel();
        FirebaseFirestore.getInstance()
                .collection("webD")
                .document(websiteUrl)
                .set(model).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void unused) {
                FirebaseFirestore.getInstance().collection("teacher").document(GlobalVariables.uid).update("isSite", websiteUrl);
                FirebaseFirestore.getInstance()
                        .collection("utilData")
                        .document("websitesDomain")
                        .get()
                        .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                List<String> domains = new ArrayList<>();
                                if (documentSnapshot.exists())
                                    if (documentSnapshot.get("data") != null)
                                        domains = (List) documentSnapshot.get("data");
                                domains.add(websiteUrl);
                                Map<String, Object> map = new HashMap<>();
                                map.put("data", domains);
                                FirebaseFirestore.getInstance()
                                        .collection("utilData")
                                        .document("websitesDomain")
                                        .set(map).addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void unused) {
                                        createDomain();
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull @NotNull Exception e) {
                                        Toast.makeText(getContext(), "Failed3", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull @NotNull Exception e) {
                        Toast.makeText(getContext(), "Failed2", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull @NotNull Exception e) {
                Toast.makeText(getContext(), "Failed1", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private WebsiteModel getTotalModel() {
        WebsiteModel model = new WebsiteModel();
        model.setCity(city.getText().toString());
        model.setState(state.getText().toString());
        model.setAboutIns(insAbout.getText().toString());
        model.setAddress(address.getText().toString());
        model.setFbLink(fbLink.getText().toString());

        if (editSite != null && insLogouri != null)
            storageRef.child(("websiteImages/" + GlobalVariables.uid + "/logo.jpeg")).putFile(insLogouri);

        if (editSite != null && profileUri != null)
            storageRef.child(("websiteImages/" + GlobalVariables.uid + "/profile.jpeg")).putFile(profileUri);

        model.setInsLogo("websiteImages/" + GlobalVariables.uid + "/logo.jpeg");
        model.setProfileLink("websiteImages/" + GlobalVariables.uid + "/profile.jpeg");

        model.setInsName(insName.getText().toString());
        model.setSubjects(insSubject.getText().toString());
        model.setTeacherId(GlobalVariables.uid);
        model.setYearsExp(experiance.getText().toString());
        model.setYoutubeLink(youtubeLink.getText().toString());
        List<Map<String, String>> classroom = new ArrayList<>();
        for (int i = 0; i < selected.size(); i++) {
            Map<String, String> map = new HashMap<>();
            map.put("className", selected.get(i));
            map.put("subject", selectedSub.get(i));
            classroom.add(map);
        }
        model.setClassroom(classroom);
        model.setDemoLinks(demoLinks);
        return model;
    }

    private void createDomain() {
        WebView webview = new WebView(getContext());

        webview.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageCommitVisible(WebView view, String url) {
                getActivity().onBackPressed();
                super.onPageCommitVisible(view, url);
            }
        });
        webview.getSettings().setJavaScriptEnabled(true);
        FirebaseFirestore.getInstance().collection("utilData").document("appData").get().addOnSuccessListener(documentSnapshot -> webview.loadUrl(documentSnapshot.getString("createsubdomainsite") + websiteUrl + "/" + GlobalVariables.uid));
    }


    private static String extractYTId(String ytUrl) {
        String vId = null;
        Pattern pattern = Pattern.compile(
                "^https?://.*(?:youtu.be/|v/|u/\\w/|embed/|watch?v=)([^#&?]*).*$",
                Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(ytUrl);
        if (matcher.matches()) {
            vId = matcher.group(1);
        }
        return vId;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMAGE_INS && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            insLogouri = selectedImage;
            Glide.with(getActivity()).load(selectedImage).centerCrop().into(insLogo);
        } else if (requestCode == IMAGE_PROFILE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            profileUri = selectedImage;
            Glide.with(getActivity()).load(selectedImage).centerCrop().into(profilePic);
        }
    }

    public static class innerYOutubeAdapter extends RecyclerView.Adapter<innerYOutubeAdapter.vh> {

        List<String> urlsData, titlesData, data;
        Context context;

        public innerYOutubeAdapter(List<String> urlsData, List<String> titlesData, List<String> data, Context context) {
            this.context = context;
            this.urlsData = urlsData;
            this.titlesData = titlesData;
            this.data = data;
        }

        @NonNull
        @NotNull
        @Override
        public vh onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
            return new vh(LayoutInflater.from(parent.getContext()).inflate(R.layout.youtube_single_layout, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull @NotNull WebSitePage.innerYOutubeAdapter.vh holder, int position) {
            ((TextView) holder.itemView.findViewById(R.id.titleYoutube)).setText(titlesData.get(position));

            holder.itemView.findViewById(R.id.deleteSingleYoutube).setOnClickListener(v -> {
                urlsData.remove(position);
                data.remove(position);
                titlesData.remove(position);
                notifyItemRemoved(position);
                notifyDataSetChanged();

            });

            holder.itemView.findViewById(R.id.editSingleYoutube).setOnClickListener(v -> {
                BottomSheetDialog dialog = new BottomSheetDialog(context);
                dialog.setContentView(R.layout.bottom_addyouitube);
                TextInputEditText desbottom = dialog.findViewById(R.id.desbottom), title = dialog.findViewById(R.id.titlebottom), url = dialog.findViewById(R.id.youtubeUrl);
                url.setText(urlsData.get(position));
                title.setText(titlesData.get(position));
                desbottom.setText(data.get(position));
                dialog.findViewById(R.id.paste).setOnClickListener(v12 -> {
                    ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                    String pasteData = "";
                    // If it does contain data, decide if you can handle the data.
                    if (!(clipboard.hasPrimaryClip())) {

                    } else if (!(clipboard.getPrimaryClipDescription().hasMimeType(MIMETYPE_TEXT_PLAIN))) {
                        // since the clipboard has data but it is not plain text

                    } else {
                        ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);
                        url.setText(item.getText().toString());
                    }
                });
                dialog.findViewById(R.id.addBottom).setOnClickListener(v1 -> {
                    if (url.getText().toString().equals("")) {
                        url.setError("Required !");
                    } else if (title.getText().toString().equals("")) {
                        title.setError("Required !");
                    } else {
                        String t = extractYTId(url.getText().toString());
                        if (t == null) {
                            url.setError("Enter correct URL !");
                        } else {
                            urlsData.set(position, extractYTId(url.getText().toString()));
                            titlesData.set(position, title.getText().toString());
                            data.set(position, desbottom.getText().toString());
                            notifyItemChanged(position);
                            dialog.dismiss();

                        }
                    }
                });
                dialog.show();
            });

        }

        @Override
        public int getItemCount() {
            return urlsData.size();
        }

        static class vh extends RecyclerView.ViewHolder {

            public vh(@NonNull @NotNull View itemView) {
                super(itemView);
            }
        }
    }


}