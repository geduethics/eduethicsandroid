package com.myliveclassroom.eduethics.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.myliveclassroom.eduethics.Activity.GalleryActivity;
import com.myliveclassroom.eduethics.Activity.GalleryImagesActivity;
import com.myliveclassroom.eduethics.Models.GalleryModel;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.classes.TinyDB;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.API_BASE_URL;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {
    List<GalleryModel> galleryModelList;
    Context mContext;
    TinyDB tinyDB;

    public GalleryAdapter(Context context, List<GalleryModel> galleryModels) {
        tinyDB = new TinyDB(context);
        mContext = context;
        galleryModelList = galleryModels;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.gallery_adapter, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtGalleryTitle.setText(galleryModelList.get(position).getGalleryTitle());
        if (!galleryModelList.get(position).getImageName().equalsIgnoreCase("")) {
            holder.txtImageCount.setText(galleryModelList.get(position).getNoOfImages() + " images");
        } else {
            holder.txtImageCount.setText("0 images");
        }
        if (tinyDB.getString("IsAdmin").equalsIgnoreCase("Y")) {
            holder.txtBtnAddMore.setVisibility(View.VISIBLE);
            holder.txtBtnDeleteGallery.setVisibility(View.VISIBLE);
        }
        else{
            holder.txtBtnAddMore.setVisibility(View.GONE);
            holder.txtBtnDeleteGallery.setVisibility(View.GONE);
        }

        Glide.with(mContext)
                .asBitmap()
                .load(API_BASE_URL + "Gallery/" + galleryModelList.get(position).getImageName().split(",")[0])
                .into(holder.thumb);

        holder.thumb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, GalleryImagesActivity.class);
                intent.putExtra("Images", galleryModelList.get(position).getImageName());
                tinyDB.putString("GalleryId", galleryModelList.get(position).getGalleryId());
                mContext.startActivity(intent);
            }
        });

        holder.txtBtnAddMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mContext instanceof GalleryActivity) {
                    ((GalleryActivity) mContext).showAddMoreLayout(galleryModelList.get(position));
                }
            }
        });

        holder.txtBtnDeleteGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mContext instanceof GalleryActivity) {
                    ((GalleryActivity) mContext).deleteGallery(galleryModelList.get(position));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return (galleryModelList == null ? 0 : galleryModelList.size());
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtGalleryTitle)
        TextView txtGalleryTitle;

        @BindView(R.id.txtImageCount)
        TextView txtImageCount;

        @BindView(R.id.txtBtnAddMore)
        TextView txtBtnAddMore;

        @BindView(R.id.txtBtnDeleteGallery)
        TextView txtBtnDeleteGallery;


        @BindView(R.id.thumb)
        ImageView thumb;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
