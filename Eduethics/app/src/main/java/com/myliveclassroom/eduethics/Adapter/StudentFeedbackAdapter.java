package com.myliveclassroom.eduethics.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.myliveclassroom.eduethics.Models.TimeTableModel;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.classes.TinyDB;

import java.util.List;

public class StudentFeedbackAdapter extends RecyclerView.Adapter<StudentFeedbackAdapter.vh> {

    List<TimeTableModel> studentModelList;
    int of;
    Context mContext;
    TinyDB tinyDB;

    public StudentFeedbackAdapter(Context context, List<TimeTableModel> studentModelList, int of) {
        this.studentModelList = studentModelList;
        this.of = of;
        mContext = context;
        tinyDB = new TinyDB(mContext);
    }

    @NonNull
    @Override
    public vh onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new vh(LayoutInflater.from(mContext).inflate(R.layout.student_feedback_adapter, parent, false));
    }


    Boolean isAttentive=false,isHomeWorkDone=false;
    @Override
    public void onBindViewHolder(@NonNull vh holder, int position) {

        studentModelList.get(position).setFeedAttentive("Y");
        studentModelList.get(position).setFeedHomeworkDone("Y");



        holder.txtRoleNo.setText(studentModelList.get(position).getRollNo());
        holder.txtName.setText(studentModelList.get(position).getStudentName());

        holder.chkInAttentive.setChecked(true);
        holder.chkHomeworkNotDone.setChecked(true);

        holder.chkHomeworkNotDone.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                     studentModelList.get(position).setFeedHomeworkDone("Y");
                } else {
                    studentModelList.get(position).setFeedHomeworkDone("N");
                }
            }
        });

        holder.chkInAttentive.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    studentModelList.get(position).setFeedAttentive("Y");
                } else {
                    studentModelList.get(position).setFeedAttentive("N");
                }
            }
        });
    }

    public void updateList(List<TimeTableModel> list) {
        studentModelList = list;
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return studentModelList == null ? 0 : studentModelList.size();
    }

    class vh extends RecyclerView.ViewHolder {


        TextView txtRoleNo, txtName;
        CheckBox chkHomeworkNotDone, chkInAttentive;

        public vh(@NonNull View itemView) {
            super(itemView);

            this.txtName = itemView.findViewById(R.id.txtStudentName);
            this.txtRoleNo = itemView.findViewById(R.id.txtRollNo);
            this.chkHomeworkNotDone = itemView.findViewById(R.id.chkHomeworkNotDone);
            this.chkInAttentive = itemView.findViewById(R.id.chkInAttentive);
        }
    }

}
