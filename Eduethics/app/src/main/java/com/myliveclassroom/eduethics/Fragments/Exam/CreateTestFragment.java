package com.myliveclassroom.eduethics.Fragments.Exam;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.myliveclassroom.eduethics.Activity.MainActivity;
import com.myliveclassroom.eduethics.Adapter.UploadImagesAdapter;
import com.myliveclassroom.eduethics.Fragments.Exam.TestFragment;
import com.myliveclassroom.eduethics.Models.ExamModel;
import com.myliveclassroom.eduethics.Models.TimeTableModel;
import com.myliveclassroom.eduethics.Models.UserModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.ExtraFunctions;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.TinyDB;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.gson.JsonObject;


import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;


public class CreateTestFragment extends Fragment {

    Map<String, Object> data;
    boolean isEdit = false;

    Context mContext;
    TinyDB tinyDB;
    TextView txtlblSubmitdate;
    String mtxtlblSubmitdateOrigionalText;
    RadioButton rdoOrl, rdoMcq, rdoSubjective;
    RadioGroup testTypeRadioGroup;
    String mExamType = LocalConstants.Exam_TYPE.ORAL;
    View pd;

    Spinner spinnerClass;
    Spinner spinnerSubject;
    Spinner spinnerTeacher;

    List<TimeTableModel> classList;
    List<TimeTableModel> subjectList;
    List<TimeTableModel> teacherList;

    LinearLayout layoutSchoolOptions;

    String mClassId = "0", mSubjectId = "0", mTeacherId = "0";

    public CreateTestFragment(Context context) {
        mContext = context;
        tinyDB = new TinyDB(mContext);
    }

    int position;

    public CreateTestFragment(Map<String, Object> data, int position) {
        this.data = data;
        this.isEdit = true;
        this.position = position;

    }

    TextView dateText, timesText;
    int[] t = new int[2];
    int[] d = new int[3];

    boolean isDone = false;

    Calendar dateTime = Calendar.getInstance();

    ArrayList<String> attachments = new ArrayList<>();
    ArrayList<Object> attachmentsData = new ArrayList<>();

    ArrayList<String> attachmentsOnEdit = new ArrayList<>();

    ArrayList<String> forDelete = new ArrayList<>();
    ArrayList<String> forAdd = new ArrayList<>();


    UploadImagesAdapter imagesadapter = new UploadImagesAdapter(attachments, this, LocalConstants.FROM.FROM_CREATE_TEST, null);


    public RecyclerView uploadRecyclerView;


    FirebaseStorage storage = FirebaseStorage.getInstance();
    StorageReference storageRef = storage.getReferenceFromUrl(LocalConstants.STORAGE_REF);    //change the url according to your firebase app

//    Uri selectedDataPath;
    //   Bitmap bitmap = null;


    TextInputEditText topic, description, testDuration, marksPerCorrect, panaltyMarks, maxMarks, txtTotalQuestions, txtQuestionsToAttempt;

    View root;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.fragment_test_create, container, false);
        MainActivity.TO_WHICH_FRAG = LocalConstants.FROM.FROM_EXAM_CREATE_FRAGMENT;

        mClassId = tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID);
        imagesadapter.setProgress(root.findViewById(R.id.progress));
        uploadRecyclerView = root.findViewById(R.id.uploadRecyclerView);
        uploadRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        uploadRecyclerView.setAdapter(imagesadapter);
        pd = root.findViewById(R.id.progress);
        dateText = root.findViewById(R.id.dateText);
        timesText = root.findViewById(R.id.timeText);

        topic = root.findViewById(R.id.topic);
        description = root.findViewById(R.id.description);
        testDuration = root.findViewById(R.id.testDuration);
        marksPerCorrect = root.findViewById(R.id.marksPerCorrect);
        panaltyMarks = root.findViewById(R.id.panaltyMarks);
        maxMarks = root.findViewById(R.id.maxMarks);
        txtTotalQuestions = root.findViewById(R.id.txtTotalQuestions);
        txtQuestionsToAttempt = root.findViewById(R.id.txtQuestionsToAttempt);

        rdoMcq = root.findViewById(R.id.rdoMcq);
        rdoOrl = root.findViewById(R.id.rdoOral);
        rdoSubjective = root.findViewById(R.id.rdoSubjective);

        layoutSchoolOptions = root.findViewById(R.id.layoutSchoolOptions);

        if (tinyDB.getString(LocalConstants.INSTITUTE_TYPE.TYPE).equalsIgnoreCase(LocalConstants.INSTITUTE_TYPE.TUTION)) {
            layoutSchoolOptions.setVisibility(View.GONE);
            mExamType = LocalConstants.Exam_TYPE.MCQ;

        }

        if (isEdit) {
            d[0] = Integer.parseInt(data.get("date").toString().split("-")[2]);
            d[1] = Integer.parseInt(data.get("date").toString().split("-")[1]);
            d[2] = Integer.parseInt(data.get("date").toString().split("-")[0]);

            t[0] = Integer.parseInt(data.get("time").toString().split(":")[0]);
            t[1] = Integer.parseInt(data.get("time").toString().split(":")[1]);

            topic.setText(data.get("topic").toString());
            description.setText(data.get("description").toString());
            testDuration.setText(data.get("testDuration").toString());
            marksPerCorrect.setText(data.get("marksPerCorrect").toString());
            panaltyMarks.setText(data.get("panaltyMarks").toString());
            maxMarks.setText(data.get("maxMarks").toString());

            attachmentsOnEdit.addAll((List) data.get("attachment"));
            urls.addAll(attachmentsOnEdit);
            for (int i = 0; i < attachmentsOnEdit.size(); i++) {
                attachmentsData.add("-");
            }

            attachments.addAll(attachmentsOnEdit);
        } else {
            d[0] = dateTime.get(Calendar.DAY_OF_MONTH);
            d[1] = dateTime.get(Calendar.MONTH) + 1;
            d[2] = dateTime.get(Calendar.YEAR);
            t[0] = dateTime.get(Calendar.HOUR_OF_DAY);
            t[1] = dateTime.get(Calendar.MINUTE);
        }


        timesText.setText(ExtraFunctions.getReadableTime(t[0], t[1]));
        dateText.setText(ExtraFunctions.getReadableDate(d[0], d[1], d[2]));

        root.findViewById(R.id.date).setOnClickListener(v -> ExtraFunctions.datePicker(getContext(), dateText, d));
        root.findViewById(R.id.addAttachment).setOnClickListener(v -> upload());
        root.findViewById(R.id.time).setOnClickListener(v -> ExtraFunctions.timePicker(getContext(), timesText, t));

        root.findViewById(R.id.continue_test).setOnClickListener(v -> {

            if (isDone) {
                //uploadToFirebase();
                //subFunctionUpload();
                // submitExam();
                isDone = false;

            } else {
                boolean allDone = true;
                if (topic.getText().toString().equals("")) {
                    topic.setError("Required !");
                    allDone = false;
                }
                if (description.getText().toString().equals("")) {
                    description.setError("Required !");
                    allDone = false;
                }
                if (testDuration.getText().toString().equals("")) {
                    testDuration.setError("Required !");
                    allDone = false;
                }
                if (marksPerCorrect.getText().toString().equals("")) {
                    marksPerCorrect.setError("Required !");
                    allDone = false;
                }
                if (panaltyMarks.getText().toString().equals("")) {
                    panaltyMarks.setError("Required !");
                    allDone = false;
                }
                if (maxMarks.getText().toString().equals("")) {
                    maxMarks.setError("Required !");
                    allDone = false;
                }
                if (allDone) {
                    if (Integer.parseInt(maxMarks.getText().toString()) < Integer.parseInt(marksPerCorrect.getText().toString())) {
                        marksPerCorrect.setError("Greater than Max. Marks");
                        allDone = false;
                    }

                    if (Integer.parseInt(maxMarks.getText().toString()) < Integer.parseInt(panaltyMarks.getText().toString())) {
                        panaltyMarks.setError("Greater than Max. Marks");
                        allDone = false;
                    }

                    /*if ((d[0] == Calendar.getInstance().get(Calendar.DAY_OF_MONTH)) && (d[1]-1 == Calendar.getInstance().get(Calendar.MONTH)) && (d[2] == Calendar.getInstance().get(Calendar.YEAR)))
                    {
                        if (((Calendar.getInstance().get(Calendar.HOUR_OF_DAY) * 60) + Calendar.getInstance().get(Calendar.MINUTE)) > (t[0] * 60 + t[1])) {
                            ((LinearLayout) root.findViewById(R.id.time)).setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.error_time, null));
                            Toast.makeText(getContext(), "Please Set Valid Time for deadline !", Toast.LENGTH_SHORT).show();
                            allDone = false;
                        }else{
                            ((LinearLayout) root.findViewById(R.id.time)).setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.background_blocks_12dp, null));
                        }
                    }else{
                        //Toast.makeText(getContext(), "not equal"+(d[0] == Calendar.getInstance().get(Calendar.DAY_OF_MONTH))+" : "+(d[1]-1 == Calendar.getInstance().get(Calendar.MONTH))+" : "+(d[2] == Calendar.getInstance().get(Calendar.YEAR)), Toast.LENGTH_SHORT).show();
                    }*/

                    if (allDone) {
                        isDone = true;
                        submitExam();
                    }
                }
            }
        });

        radioSetting();
        getClassData();
        return root;
    }

    private void radioSetting() {
        txtlblSubmitdate = root.findViewById(R.id.txtlblSubmitdate);
        mtxtlblSubmitdateOrigionalText = txtlblSubmitdate.getText().toString();
        if (tinyDB.getString(LocalConstants.INSTITUTE_TYPE.TYPE).equalsIgnoreCase(LocalConstants.INSTITUTE_TYPE.SCHOOL)) {
            rdoOrl.setVisibility(View.VISIBLE);
            rdoOrl.setChecked(true);
            txtlblSubmitdate.setText("Exam Date & Time");
        } else {
            rdoOrl.setVisibility(View.GONE);
            rdoMcq.setChecked(true);
        }
        testTypeRadioGroup = root.findViewById(R.id.testTypeRadioGroup);
        testTypeRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                View radioButton = testTypeRadioGroup.findViewById(checkedId);
                int index = testTypeRadioGroup.indexOfChild(radioButton);
                switch (radioButton.getId()) {
                    case R.id.rdoOral:
                        txtlblSubmitdate.setText("Exam Date & Time");
                        mExamType = LocalConstants.Exam_TYPE.ORAL;
                        break;
                    case R.id.rdoMcq:
                        txtlblSubmitdate.setText(mtxtlblSubmitdateOrigionalText);
                        mExamType = LocalConstants.Exam_TYPE.MCQ;
                        break;
                    case R.id.rdoSubjective:
                        txtlblSubmitdate.setText(mtxtlblSubmitdateOrigionalText);
                        mExamType = LocalConstants.Exam_TYPE.SUBJECTIVE;
                        break;

                }
            }
        });
    }

    Dialog dialog;

    private void upload() {
        dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.upload_dialog);
        dialog.findViewById(R.id.camera).setOnClickListener(v -> {
            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_DENIED)
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, LocalConstants.REQ_CODE.CAMERA_REQUEST);
            else {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, LocalConstants.PERMISSION_CODE.CAMERA_REQUEST);
                dialog.dismiss();
            }
        });

        dialog.findViewById(R.id.pdf).setOnClickListener(v -> {
            Intent chooseFile;
            Intent intent;
            chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
            chooseFile.setType("application/pdf");
            intent = Intent.createChooser(chooseFile, "Choose a file");
            startActivityForResult(intent, LocalConstants.PERMISSION_CODE.PICKFILE_RESULT_CODE);
            dialog.dismiss();
        });

        dialog.findViewById(R.id.image).setOnClickListener(v -> {
            Intent i = new Intent();
            i.setType("image/*");
            i.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(i, "Select Picture"), LocalConstants.PERMISSION_CODE.SELECT_PICTURE);
            dialog.dismiss();
        });

        dialog.findViewById(R.id.upload).setOnClickListener(v -> dialog.dismiss());
        dialog.findViewById(R.id.cancel).setOnClickListener(v -> dialog.dismiss());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    List<String> urls = new ArrayList<>();

    private void submitExam() {

        //=============================================
        //                  Exam Model
        //=============================================
        ExamModel examModel = new ExamModel();
        examModel.setExamTitle(topic.getText().toString());
        examModel.setDescription(description.getText().toString());
        examModel.setMarksPerCorrect(marksPerCorrect.getText().toString());
        examModel.setNegativePerIncorrect(panaltyMarks.getText().toString());
        examModel.setTotalQuestions(txtTotalQuestions.getText().toString());
        examModel.setToAttempt(txtQuestionsToAttempt.getText().toString());

        examModel.setStartDate(d[2] + "-" + (d[1] < 10 ? "0" + d[1] : d[1]) + "-" + (d[0] < 10 ? "0" + d[0] : d[0]) + " " + t[0] + ":" + t[1]);
        examModel.setStartTime(d[2] + "-" + (d[1] < 10 ? "0" + d[1] : d[1]) + "-" + (d[0] < 10 ? "0" + d[0] : d[0]) + " " + t[0] + ":" + t[1]);

        examModel.setMaxMarks(maxMarks.getText().toString());

        String dateString = d[2] + "-" + (d[1] < 10 ? "0" + d[1] : d[1]) + "-" + (d[0] < 10 ? "0" + d[0] : d[0]) + " " + t[0] + ":" + t[1];
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        int minutesToAdd = Integer.parseInt(testDuration.getText().toString());
        String endTime = addHour(dateString, minutesToAdd);
        examModel.setEndTime(endTime);
        examModel.setEndDate(endTime);
        examModel.setCreatedBy(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
        examModel.setExamType(mExamType);
        examModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        examModel.setClassId(mClassId);
        examModel.setSubjectId(mSubjectId);

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = apiService.ExamCreate(examModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                try {
                    if (response.body() == null) {
                        Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        //String mes= new Gson().fromJson(response.body().get("Message"), String.class);
                        Toast.makeText(mContext, "Done", Toast.LENGTH_SHORT).show();
                        getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(
                                R.anim.slide_in,  // enter
                                R.anim.fade_out,  // exit
                                R.anim.fade_in,   // popEnter
                                R.anim.slide_out  // popExit
                        ).replace(R.id.fragmentFrame, new TestFragment(mContext)).commit();
                    }
                } catch (Exception ex) {
                    Toast.makeText(mContext, LocalConstants.APP_MESSAGES.ERROR1, Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, LocalConstants.APP_MESSAGES.ERROR1, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private static String addHour(String myTime, int number) {
        try {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Date d = df.parse(myTime);
            Calendar cal = Calendar.getInstance();
            cal.setTime(d);
            cal.add(Calendar.MINUTE, number);
            String newTime = df.format(cal.getTime());
            return newTime;
        } catch (ParseException e) {
            System.out.println(" Parsing Exception");
        }
        return null;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data == null) return;
        switch (requestCode) {
            case LocalConstants.PERMISSION_CODE.PICKFILE_RESULT_CODE:
                attachmentsData.add(data.getData());
                attachments.add(data.getData().getPath());
                forAdd.add(data.getData().getPath());
                imagesadapter.notifyItemInserted(attachments.size() - 1);
                ((TextView) dialog.findViewById(R.id.pdf)).setBackground(getActivity().getDrawable(R.drawable.second_btn));
                break;
            case LocalConstants.PERMISSION_CODE.SELECT_PICTURE:
                Uri selectedImageUri = data.getData();
                if (null != selectedImageUri) {
                    ((TextView) dialog.findViewById(R.id.image)).setBackground(getActivity().getDrawable(R.drawable.second_btn));
                    attachmentsData.add(selectedImageUri);
                    attachments.add(selectedImageUri.getPath());
                    forAdd.add(selectedImageUri.getPath());
                    imagesadapter.notifyItemInserted(attachments.size() - 1);
                }
                break;
            case LocalConstants.PERMISSION_CODE.CAMERA_REQUEST:
                ((TextView) dialog.findViewById(R.id.camera)).setBackground(getActivity().getDrawable(R.drawable.second_btn));
                attachmentsData.add((Bitmap) data.getExtras().get("data"));
                attachments.add("314" + UUID.randomUUID().toString());
                forAdd.add("314" + UUID.randomUUID().toString());
                imagesadapter.notifyItemInserted(attachments.size() - 1);
                break;
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == LocalConstants.REQ_CODE.CAMERA_REQUEST) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "camera permission granted", Toast.LENGTH_LONG).show();
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, LocalConstants.PERMISSION_CODE.CAMERA_REQUEST);
            } else {
                Toast.makeText(getActivity(), "camera permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void deleteImage(int position) {
        attachmentsData.remove(position);
        attachments.remove(position);
        if (attachmentsData.size() == 0) uploadRecyclerView.setBackground(null);
        imagesadapter = new UploadImagesAdapter(attachments, this, LocalConstants.FROM.FROM_CREATE_TEST, root.findViewById(R.id.progress));
        uploadRecyclerView.setAdapter(imagesadapter);

    }


    private void bindSpinner() {
        spinnerClass = root.findViewById(R.id.spinnerClass);
        spinnerSubject = root.findViewById(R.id.spinnerSubject);
    }

    private void getClassData() {
        bindSpinner();

        TimeTableModel timeTableModel = new TimeTableModel();
        timeTableModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        timeTableModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<JsonObject> call = apiService.classList(timeTableModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                try {
                    if (response.body() == null) {
                        Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (response.body() != null) {
                        Type type = new TypeToken<List<TimeTableModel>>() {
                        }.getType();
                        classList = new Gson().fromJson(response.body().getAsJsonArray("Response"), type);
                        List<String> spinnerArray = new ArrayList<>();
                        spinnerArray.add("--All Class--");
                        for (TimeTableModel str : classList) {
                            spinnerArray.add(str.getClassName());
                        }

                        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, spinnerArray);
                        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down vieww
                        spinnerClass.setAdapter(spinnerArrayAdapter);
                        spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int myPosition, long myID) {
                                if (myPosition > 0) {
                                    mClassId = classList.get(myPosition - 1).getClassId();
                                    tinyDB.putString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID, mClassId);
                                    listSubjects();
                                } else {
                                    mClassId = "0";
                                    tinyDB.putString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID, mClassId);
                                    listSubjects();
                                }
                                //Log.i("renderSpinner -> ", "onItemSelected: " + myPosition + "/" + myID);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parentView) {
                                // your code here
                            }
                        });
                    }
                } catch (Exception ex) {
                    Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void listSubjects() {
        UserModel userModel = new UserModel();
        userModel.setClassId(mClassId);

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<List<TimeTableModel>> call = apiService.sujectList(userModel);
        call.enqueue(new Callback<List<TimeTableModel>>() {
            @Override
            public void onResponse(Call<List<TimeTableModel>> call, Response<List<TimeTableModel>> response) {
                try {
                    if (response.body() != null) {
                        subjectList = response.body();
                        List<String> spinnerArray = new ArrayList<>();
                        spinnerArray.add("--All Subject--");
                        for (TimeTableModel str : subjectList) {
                            spinnerArray.add(str.getSubjectName());
                        }

                        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, spinnerArray);
                        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down vieww
                        spinnerSubject.setAdapter(spinnerArrayAdapter);
                        spinnerSubject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                            @Override
                            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int myPosition, long myID) {
                                if (myPosition > 0) {
                                    mSubjectId = subjectList.get(myPosition - 1).getSubjectId();
                                    tinyDB.putString(LocalConstants.TINYDB_LOGIN_KEYS.SUBJECT_ID, mSubjectId);

                                } else {
                                    mSubjectId = "0";
                                    tinyDB.putString(LocalConstants.TINYDB_LOGIN_KEYS.SUBJECT_ID, mSubjectId);
                                }

                                Log.i("renderSpinner -> ", "onItemSelected: " + myPosition + "/" + myID);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parentView) {
                                // your code here
                            }
                        });
                    }
                } catch (Exception ex) {
                    Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<List<TimeTableModel>> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}