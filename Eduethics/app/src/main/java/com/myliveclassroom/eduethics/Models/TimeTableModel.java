package com.myliveclassroom.eduethics.Models;

import com.google.gson.annotations.SerializedName;

public class TimeTableModel {

    private String TimeTableId;
    private String DayName;
    private String SubjectName;
    private String SubjectId;

    private String TimeFrom;
    private String TimeTo;
    private String TeacherId;
    private String TeacherName;
    private String DayId;

    private String ClassId;
    private String ClassName;
    private String ClassCode;

    private String SchoolId;
    private String SessionId;

    private String StudentId;
    private String StudentName;
    private String RollNo;


    private String PeriodName;
    private String ColorCode;
    private String NoOfDays;

    private String AttTag;
    private String AttTagId;
    private String AttDate;

    private String UserId;

    private String IsStudentActive;
    private String IsClassActive;

    private String Mobile;

    private String ActionType;

    private String ProfileType;
    private String InstituteType;

    private String FeedAttentive;
    private String FeedHomeworkDone;
    private String ScheduleDate;

    private String AboutClass;
    private String AboutTutor;


    private String CourseFee;
    private String MonthlyFee;
    private String StartDate;
    private String NextdueDate;
    private String ReminderDate1;
    private String ReminderDate2;
    private String Advance;

    private String FeeAmt;
    private String AmtHead;
    private String PaymentMode;
    private String PaymentRemarks;

    private String Duration, AdmissionNo;

    private String PresentPer, AbsentPer, LeavePer;

    private String Month, Year;
    private String HolidayName;

    private String Fever,Cold;

    public void setCold(String cold) {
        Cold = cold;
    }

    public void setFever(String fever) {
        Fever = fever;
    }

    public String getHolidayName() {
        return HolidayName;
    }

    public void setMonth(String month) {
        Month = month;
    }

    public void setYear(String year) {
        Year = year;
    }

    public String getAdmissionNo() {
        return AdmissionNo;
    }


    public String getAbsentPer() {
        return AbsentPer;
    }

    public String getLeavePer() {
        return LeavePer;
    }

    public String getPresentPer() {
        return PresentPer;
    }

    public void setDuration(String duration) {
        Duration = duration;
    }

    public String getDuration() {
        return Duration;
    }


    public String getMonth() {
        return Month;
    }

    public String getYear() {
        return Year;
    }

    public void setAboutTutor(String aboutTutor) {
        AboutTutor = aboutTutor;
    }

    public void setAboutClass(String aboutClass) {
        AboutClass = aboutClass;
    }

    public String getAboutTutor() {
        return AboutTutor;
    }

    public String getAboutClass() {
        return AboutClass;
    }

    public String getScheduleDate() {
        return ScheduleDate;
    }

    public void setScheduleDate(String scheduleDate) {
        ScheduleDate = scheduleDate;
    }

    public void setProfileType(String profileType) {
        ProfileType = profileType;
    }

    public void setActionType(String actionType) {
        ActionType = actionType;
    }

    public String getMobile() {
        return Mobile;
    }

    public String getIsStudentActive() {
        return IsStudentActive;
    }

    public String getIsClassActive() {
        return IsClassActive;
    }

    @SerializedName("IsSet")
    private Boolean IsTimeSet;

    public void setDayName(String dayName) {
        DayName = dayName;
    }

    public Boolean getIsTimeSet() {
        return IsTimeSet == null ? false : IsTimeSet;
    }

    public void setIsTimeSet(Boolean timeSet) {
        IsTimeSet = timeSet;
    }

    public String getDayName() {
        return DayName;
    }

    public String getTimeTableId() {
        return TimeTableId;
    }

    public String getDay() {
        return DayId;
    }


    public String getTeacher() {
        return TeacherName;
    }

    public String getTimeFrom() {
        return TimeFrom;
    }


    public String getTimeTo() {
        return TimeTo;
    }

    public void setDay(String day) {
        DayId = day;
    }

    public void setTeacher(String teacher) {
        TeacherName = teacher;
    }

    public void setTimeFrom(String timeFrom) {
        TimeFrom = timeFrom;
    }

    public void setTimeTo(String timeTo) {
        TimeTo = timeTo;
    }

    public String getClassId() {
        return ClassId;
    }

    public String getSchoolId() {
        return SchoolId;
    }

    public String getStudentId() {
        return StudentId;
    }

    public void setClassId(String classId) {
        ClassId = classId;
    }

    public void setTimeTableId(String timeTableId) {
        TimeTableId = timeTableId;
    }

    public void setSchoolId(String schoolId) {
        SchoolId = schoolId;
    }

    public void setStudentId(String studentId) {
        StudentId = studentId;
    }

    public String getColorCode() {
        return ColorCode;
    }

    public String getPeriodName() {
        return PeriodName;
    }

    public void setColorCode(String colorCode) {
        ColorCode = colorCode;
    }

    public void setPeriodName(String periodName) {
        PeriodName = periodName;
    }

    public String getNoOfDays() {
        return NoOfDays;
    }

    public void setNoOfDays(String noOfDays) {
        NoOfDays = noOfDays;
    }

    public String getDayId() {
        return DayId;
    }

    public String getClassName() {
        return ClassName;
    }

    public void setClassName(String className) {
        ClassName = className;
    }

    public void setDayId(String dayId) {
        DayId = dayId;
    }

    public String getStudentName() {
        return StudentName;
    }


    public void setStudentName(String studentName) {
        StudentName = studentName;
    }

    public String getAttTag() {
        return AttTag;
    }

    public void setAttTag(String attTag) {
        AttTag = attTag;
    }

    public String getTeacherId() {
        return TeacherId;
    }

    public String getTeacherName() {
        return TeacherName;
    }

    public void setTeacherId(String teacherId) {
        TeacherId = teacherId;
    }

    public void setTeacherName(String teacherName) {
        TeacherName = teacherName;
    }

    public String getRollNo() {
        return RollNo;
    }

    public void setRollNo(String rollNo) {
        RollNo = rollNo;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getUserId() {
        return UserId;
    }

    public String getAttDate() {
        return AttDate;
    }

    public String getAttTagId() {
        return AttTagId;
    }

    public void setAttDate(String attDate) {
        AttDate = attDate;
    }

    public void setAttTagId(String attTagId) {
        AttTagId = attTagId;
    }

    public void setSubjectId(String subjectId) {
        SubjectId = subjectId;
    }

    public String getSubjectId() {
        return SubjectId;
    }

    public void setSessionId(String sessionId) {
        SessionId = sessionId;
    }

    public String getSessionId() {
        return SessionId;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getClassCode() {
        return ClassCode;
    }

    public void setInstituteType(String instituteType) {
        InstituteType = instituteType;
    }

    public String getFeedAttentive() {
        return FeedAttentive;
    }

    public String getFeedHomeworkDone() {
        return FeedHomeworkDone;
    }

    public void setFeedAttentive(String feedAttentive) {
        FeedAttentive = feedAttentive;
    }

    public void setFeedHomeworkDone(String feedHomeworkDone) {
        FeedHomeworkDone = feedHomeworkDone;
    }


    public String getAmtHead() {
        return AmtHead;
    }

    public String getFeeAmt() {
        return FeeAmt;
    }

    public String getPaymentMode() {
        return PaymentMode;
    }

    public void setAmtHead(String amtHead) {
        AmtHead = amtHead;
    }

    public String getPaymentRemarks() {
        return PaymentRemarks;
    }

    public void setFeeAmt(String feeAmt) {
        FeeAmt = feeAmt;
    }

    public void setPaymentMode(String paymentMode) {
        PaymentMode = paymentMode;
    }

    public void setPaymentRemarks(String paymentRemarks) {
        PaymentRemarks = paymentRemarks;
    }

    public String getAdvance() {
        return Advance;
    }

    public void setAdvance(String advance) {
        Advance = advance;
    }


    public String getCourseFee() {
        return CourseFee;
    }

    public String getMonthlyFee() {
        return MonthlyFee;
    }


    public String getNextdueDate() {
        return NextdueDate;
    }

    public void setCourseFee(String courseFee) {
        CourseFee = courseFee;
    }

    public String getReminderDate1() {
        return ReminderDate1;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setMonthlyFee(String monthlyFee) {
        MonthlyFee = monthlyFee;
    }

    public String getReminderDate2() {
        return ReminderDate2;
    }

    public void setNextdueDate(String nextdueDate) {
        NextdueDate = nextdueDate;
    }

    public void setReminderDate1(String reminderDate1) {
        ReminderDate1 = reminderDate1;
    }

    public void setReminderDate2(String reminderDate2) {
        ReminderDate2 = reminderDate2;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }
}
