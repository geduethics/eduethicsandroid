package com.myliveclassroom.eduethics.Fragments.Exam;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.myliveclassroom.eduethics.Activity.MainActivity;
import com.myliveclassroom.eduethics.Adapter.UploadImagesAdapter;
import com.myliveclassroom.eduethics.Fragments.HomeClassroomFragment;
import com.myliveclassroom.eduethics.Fragments.Exam.AddSolution;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.ExtraFunctions;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.CommonFunctions;
import com.myliveclassroom.eduethics.classes.TinyDB;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.APP_PACKAGE_NAME;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.ATTACHMENT_ARRAY;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.MAIN_FOLDER;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.PICK_CAMERA_REQUEST_CODE;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.PICK_GALLERY_REQUEST_CODE;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.PICK_PDF_REQUEST_CODE;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;
import static com.myliveclassroom.eduethics.classes.CommonFunctions.createImageFileNew;
import static com.myliveclassroom.eduethics.classes.CommonFunctions.getBytes;
import static com.myliveclassroom.eduethics.classes.CommonFunctions.getImageUri;
import static com.myliveclassroom.eduethics.classes.CommonFunctions.getPath;
import static com.myliveclassroom.eduethics.classes.CommonFunctions.saveFile;
import static com.myliveclassroom.eduethics.classes.CommonFunctions.writeToFile;


public class CreateAssignmentFragment extends Fragment {

    Context mContext;
    TinyDB tinyDB;
    TextInputEditText topic, description, maxMarks;
    TextView dateText, timeText;
    int[] d = new int[3];
    int[] t = new int[2];
    Dialog dialog;

    List<String> imagesListArray = new ArrayList<>();
    ArrayList<String> attachments = new ArrayList<>();
    ArrayList<Object> attachmentsData = new ArrayList<>();

    ArrayList<String> attachmentsOnEdit = new ArrayList<>();

    ArrayList<String> forDelete = new ArrayList<>();
    ArrayList<String> forAdd = new ArrayList<>();

    boolean isEdit = false;

    Map<String, Object> data;
    int position;

    public CreateAssignmentFragment(Context context) {
        mContext = context;
        tinyDB = new TinyDB(mContext);
        MainActivity.TO_WHICH_FRAG = LocalConstants.TO_FRAG.TO_ASSIGNMENT_FRAG;
    }


    public CreateAssignmentFragment(Map<String, Object> data, int position) {
        this.data = data;
        this.isEdit = true;
        this.position = position;
    }

    UploadImagesAdapter imagesadapter = new UploadImagesAdapter(imagesListArray, this, LocalConstants.FROM.FROM_CREATE_ASSIGNMENT, null);

    List<String> urls = new ArrayList<>();

    int uploadImageCounter = 0;

    RecyclerView uploadRecyclerView;

    View progressView;

    Calendar dateTime = Calendar.getInstance();

    FirebaseStorage storage = FirebaseStorage.getInstance();
    StorageReference storageRef = storage.getReferenceFromUrl(LocalConstants.STORAGE_REF);    //change the url according to your firebase app
    View root;
    ActivityResultLauncher<Intent> someActivityResultLauncher;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_create_assignment, container, false);

        imagesadapter.setProgress(root.findViewById(R.id.progress));


        progressView = root.findViewById(R.id.progress);

        uploadRecyclerView = root.findViewById(R.id.uploadRecyclerView);
        uploadRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        uploadRecyclerView.setAdapter(imagesadapter);

        d[0] = dateTime.get(Calendar.DAY_OF_MONTH);
        d[1] = dateTime.get(Calendar.MONTH);
        d[2] = dateTime.get(Calendar.YEAR);

        dateText = root.findViewById(R.id.dateText);
        timeText = root.findViewById(R.id.timeText);

        topic = root.findViewById(R.id.topic);
        description = root.findViewById(R.id.description);
        maxMarks = root.findViewById(R.id.maxMarks);

        dateText.setOnClickListener(v -> ExtraFunctions.datePicker(getContext(), dateText, d));
        timeText.setOnClickListener(v -> ExtraFunctions.timePicker(getContext(), timeText, t));

        root.findViewById(R.id.addAttachment).setOnClickListener(v -> filePickDialog());
        Toast.makeText(getContext(), "here", Toast.LENGTH_SHORT).show();

        if (isEdit) {
            d[0] = Integer.parseInt(data.get("date").toString().split("-")[2]);
            d[1] = Integer.parseInt(data.get("date").toString().split("-")[1]);
            d[2] = Integer.parseInt(data.get("date").toString().split("-")[0]);

            t[0] = Integer.parseInt(data.get("time").toString().split(":")[0]);
            t[1] = Integer.parseInt(data.get("time").toString().split(":")[1]);
            Toast.makeText(getContext(), data.get("time").toString(), Toast.LENGTH_SHORT).show();

            topic.setText(data.get("topic").toString());
            description.setText(data.get("description").toString());
            maxMarks.setText(data.get("maxMarks").toString());
            attachmentsOnEdit.addAll((List) data.get("attachment"));
            urls.addAll(attachmentsOnEdit);
            for (int i = 0; i < attachmentsOnEdit.size(); i++) {
                attachmentsData.add("-");
            }

            attachments.addAll(attachmentsOnEdit);
        } else {
            d[0] = dateTime.get(Calendar.DAY_OF_MONTH);
            d[1] = dateTime.get(Calendar.MONTH) + 1;
            d[2] = dateTime.get(Calendar.YEAR);
            t[0] = dateTime.get(Calendar.HOUR_OF_DAY);
            t[1] = dateTime.get(Calendar.MINUTE);
        }

        timeText.setText(ExtraFunctions.getReadableTime(t[0], t[1]));
        dateText.setText(ExtraFunctions.getReadableDate(d[0], d[1], d[2]));

        root.findViewById(R.id.saveCreateAssignment).setOnClickListener(v -> {
             boolean allDone = true;
            if (topic.getText().toString().equals("")) {
                topic.setError("Required !");
                allDone = false;
            }
            if (description.getText().toString().equals("")) {
                description.setError("Required !");
                allDone = false;
            }
            if (maxMarks.getText().toString().equals("")) {
                maxMarks.setError("Required !");
                allDone = false;
            }
            if (allDone) {
                if ((d[0] == Calendar.getInstance().get(Calendar.DAY_OF_MONTH)) && (d[1] - 1 == Calendar.getInstance().get(Calendar.MONTH)) && (d[2] == Calendar.getInstance().get(Calendar.YEAR))) {
                    if (((Calendar.getInstance().get(Calendar.HOUR_OF_DAY) * 60) + Calendar.getInstance().get(Calendar.MINUTE)) > (t[0] * 60 + t[1])) {
                        root.findViewById(R.id.time).setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.error_time, null));
                        Toast.makeText(getContext(), "Please Set Valid Time for deadline !", Toast.LENGTH_SHORT).show();
                        allDone = false;
                    } else {
                        root.findViewById(R.id.time).setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.background_blocks_12dp, null));
                    }
                }
            }


            if (allDone) {

                //TODO
                //uploadToFirebase();
                uploadHomeWork();
            }
           // testUpload();
        });

        return root;
    }


    private void enableSubmitBtn() {
        progressView.setVisibility(View.GONE);
        root.findViewById(R.id.saveCreateAssignment).setEnabled(true);
    }

    private void disableSubmitBtn() {
        progressView.setVisibility(View.VISIBLE);
        root.findViewById(R.id.saveCreateAssignment).setEnabled(false);
    }

    //TODO
    //this is main upload function
    MultipartBody.Part[] body=null;
    private void uploadHomeWork() {
        disableSubmitBtn();
        JSONObject jsonObject = new JSONObject();

        try {
            String randomId = UUID.randomUUID().toString();
            jsonObject.put("CreatedBy", tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));
            jsonObject.put("SchoolId", tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
            jsonObject.put("SessionId", tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));
            jsonObject.put("ClassId", tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID));
            jsonObject.put("SectionId", tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SECTION_ID));
            jsonObject.put("SubjectId", tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SUBJECT_ID));
            jsonObject.put("TeacherId", tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));

            jsonObject.put("WorkHead", topic.getText().toString());
            jsonObject.put("WorkDetail", description.getText().toString());
            jsonObject.put("WorkDate", d[2] + "-" + (d[1] < 10 ? "0" + d[1] : d[1]) + "-" + (d[0] < 10 ? "0" + d[0] : d[0]));
            jsonObject.put("WorkTime", t[0] + ":" + t[1]);
            // jsonObject.put("attachment", urls);
            jsonObject.put("submitted", new ArrayList<>());
            jsonObject.put("maxMarks", maxMarks.getText().toString());
            jsonObject.put("resultId", randomId);
            jsonObject.put("solution", "");

        } catch (JSONException e) {
            e.printStackTrace();
        }
       body = new MultipartBody.Part[imagesListArray.size()];
        //attaching images to multipart
        for (int i = 0; i < imagesListArray.size(); i++) {
            File file =null;
            if (imagesListArray.get(i).toLowerCase().endsWith(".pdf")) {
                file=new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_DOCUMENTS + "/" + MAIN_FOLDER + "/" + "Pdf")+"/"+imagesListArray.get(i));
            }
            else{
                file=new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES + "/" + MAIN_FOLDER + "/" + "Images")+"/"+imagesListArray.get(i));
            }
            RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
            try {
                body[i] = MultipartBody.Part.createFormData("uploaded_file_" +i, file.getName(), requestFile);
            } catch (Exception ex) {
                Log.e(mTAG, "" + ex.getMessage());
            }
        }

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        RequestBody modelParm = RequestBody.create(MediaType.parse("text/plain"), jsonObject.toString());

        Call<JsonObject> call = apiService.HomeWorkSubmitWithFile(body, modelParm);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                enableSubmitBtn();
                try {
                    Log.e("res", response.body().toString());

                } catch (Exception ex) {
                    Log.e(mTAG, "error =" + ex.getMessage());
                    Toast.makeText(mContext, "Something went wrong2!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                // myProgressDialog.hideProgress();
                Toast.makeText(mContext, "Something went wrong1!", Toast.LENGTH_SHORT).show();
                enableSubmitBtn();
            }
        });
    }


    private void testUpload() {
        for (int i = 0; i < imagesListArray.size(); i++) {
            File file =null;
            if (imagesListArray.get(i).toLowerCase().endsWith(".pdf")) {
                file=new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES + "/" + MAIN_FOLDER + "/" + "Pdf")+"/"+imagesListArray.get(i));
            }
            else{
                file=new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES + "/" + MAIN_FOLDER + "/" + "Images")+"/"+imagesListArray.get(i));
            }
            MultipartBody.Part[] body = new MultipartBody.Part[imagesListArray.size()];

            RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
            try {
                body[i] = MultipartBody.Part.createFormData("uploaded_file_" + String.valueOf(i), file.getName(), requestFile);
            } catch (Exception ex) {
                Log.e(mTAG, "" + ex.getMessage());
            }
        }
    }

    private void subFunctionUpload() {
        if (forAdd.size() == 0) {
            uploadToClassroom();
            return;
        }
        for (int i = 0; i < attachments.size(); i++) {
            Bitmap bitmap = null;
            Uri selectedDataPath = null;
            if (forAdd.contains(attachments.get(i))) {
                if (attachments.get(i).startsWith("314")) bitmap = (Bitmap) attachmentsData.get(i);
                else selectedDataPath = (Uri) attachmentsData.get(i);
                Toast.makeText(getContext(), "Tost 1", Toast.LENGTH_SHORT).show();
                if (selectedDataPath == null) {
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    byte[] data = baos.toByteArray();

                    final String randomKey = UUID.randomUUID().toString();

                    UploadTask uploadTask = storageRef.child("assignmentData/" + HomeClassroomFragment.classId + "/" + randomKey + ".jpg").putBytes(data);
                    uploadTask
                            .addOnFailureListener(exception -> Toast.makeText(getContext(), "failed", Toast.LENGTH_SHORT).show())
                            .addOnSuccessListener(taskSnapshot -> {
                                urls.add(taskSnapshot.getMetadata().getPath());
                                uploadImageCounter++;
                                Toast.makeText(getContext(), "Tost", Toast.LENGTH_SHORT).show();
                                checkandpost();
                            });
                } else {
                    final String randomKey = UUID.randomUUID().toString();
                    String extension;
                    if (selectedDataPath.getPath().contains(".")) {
                        extension = selectedDataPath.getPath().substring(selectedDataPath.getPath().lastIndexOf("."));
                    } else {
                        extension = ".pdf";
                    }

                    StorageReference riversRef = storageRef.child(("assignmentData/" + HomeClassroomFragment.classId + "/" + randomKey + extension));

                    riversRef.putFile(selectedDataPath)
                            .addOnSuccessListener(taskSnapshot -> {
                                progressView.setVisibility(View.GONE);
                                String name = riversRef.getDownloadUrl().toString();
                                urls.add(taskSnapshot.getMetadata().getPath());
                                uploadImageCounter++;
                                checkandpost();
                            }).addOnFailureListener(e -> {
                        Toast.makeText(getContext(), "Unable to post your Image !", Toast.LENGTH_SHORT).show();
                        Log.d("TAG", e.getLocalizedMessage() + " " + e.getMessage() + "subFunctionUpload: ");
                        e.printStackTrace();
                    });

                }
            }

        }
    }

    private void checkandpost() {
        if (uploadImageCounter == forAdd.size()) {
            uploadToClassroom();
        }
    }

    private void uploadToClassroom() {
        Map<String, Object> map = new HashMap<>();
        String randomId = isEdit ? data.get("resultId").toString() : UUID.randomUUID().toString();
        map.put("topic", topic.getText().toString());
        map.put("description", description.getText().toString());
        map.put("date", d[2] + "-" + (d[1] < 10 ? "0" + d[1] : d[1]) + "-" + (d[0] < 10 ? "0" + d[0] : d[0]));
        map.put("time", t[0] + ":" + t[1]);
        map.put("attachment", urls);
        map.put("submitted", new ArrayList<>());
        map.put("maxMarks", maxMarks.getText().toString());
        map.put("resultId", randomId);
        map.put("solution", new ArrayList<>());

        Toast.makeText(getContext(), "" + urls.size(), Toast.LENGTH_SHORT).show();

    }

    private void addSolutionDialog() {
        dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.add_solution_dialog);

        dialog.findViewById(R.id.add).setOnClickListener(v -> {
            if (getActivity() != null)
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragmentFrame, new AddSolution("assignments", data, false)).commit();
            dialog.dismiss();
        });

        dialog.findViewById(R.id.cancel).setOnClickListener(v -> {
            if (getActivity() != null)
                getActivity().onBackPressed();
            dialog.dismiss();
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

    }


    private void filePickDialog() {
        dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.upload_dialog);
        dialog.findViewById(R.id.camera).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_DENIED)
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, PICK_CAMERA_REQUEST_CODE);
                else {
                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    /***************************************
                     *      Custom file name for Camera file
                     ***************************************/
                    File photoFile = null;
                    try {
                        photoFile = createImageFileNew(LocalConstants.FILE_TYPE.JPG);
                        mCurrentPhotoPath = photoFile.getAbsolutePath();
                    } catch (IOException ex) {
                        // Error occurred while creating the File
                        Log.i(mTAG, "IOException");
                    }
                    // Continue only if the File was successfully created
                    if (photoFile != null) {
                        Uri imageUri = FileProvider.getUriForFile(
                                mContext,
                                APP_PACKAGE_NAME + ".provider",
                                photoFile);
                        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                        startActivityForResult(cameraIntent, PICK_CAMERA_REQUEST_CODE);
                    }
                }

            }
        });

        dialog.findViewById(R.id.pdf).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent chooseFile;
                Intent intent;
                chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
                chooseFile.setType("application/pdf");
                intent = Intent.createChooser(chooseFile, "Choose a file");
                startActivityForResult(intent, PICK_PDF_REQUEST_CODE);
            }
        });

        dialog.findViewById(R.id.image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                startActivityForResult(intent, PICK_GALLERY_REQUEST_CODE);
            }
        });

        ((TextView) dialog.findViewById(R.id.upload)).setText("Select");
        dialog.findViewById(R.id.upload).setOnClickListener(v -> dialog.dismiss());
        dialog.findViewById(R.id.cancel).setOnClickListener(v -> dialog.dismiss());

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    String mCurrentPhotoPath;
    CommonFunctions comfun = new CommonFunctions();
    List<String> selectFileList = new ArrayList<>();
    ArrayList<String> documentsPathsArray;
    ArrayList<String> attachmentArray = new ArrayList<>();


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PICK_PDF_REQUEST_CODE:
                if (data == null) return;
                File pdfFile = null;
                try {
                    pdfFile = createImageFileNew(LocalConstants.FILE_TYPE.PDF);
                } catch (IOException ex) {
                    // Error occurred while creating the File
                    Log.i(mTAG, "IOException");
                }

                Uri uri = data.getData();
                byte[] inputData = null;
                InputStream iStream = null;
                try {
                    iStream = mContext.getContentResolver().openInputStream(uri);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                try {
                    inputData = getBytes(iStream);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    writeToFile(inputData, pdfFile.getAbsolutePath());
                } catch (IOException e) {
                    e.printStackTrace();
                }

                imagesListArray.add(pdfFile.getName());
                imagesadapter.notifyItemInserted(imagesListArray.size() - 1);
                break;
            case PICK_GALLERY_REQUEST_CODE:
                if (data == null) return;
                //*************OK**************
                if (requestCode == PICK_GALLERY_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
                    if (data == null) {
                        return;
                    }
                    Uri selectedImageUri = data.getData();
                    if (null != selectedImageUri) {
                        Uri imageUri = data.getData();
                        Bitmap bitmap = null;
                        try {
                            bitmap = MediaStore.Images.Media.getBitmap(mContext.getContentResolver(), imageUri);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        Uri u = getImageUri(mContext, bitmap);
                        File finalFile = null;
                        File file = new File(getPath(mContext, u));

                        file = saveFile(file, LocalConstants.FILE_TYPE.JPG);

                        imagesListArray.add(file.getName());
                        imagesadapter.notifyItemInserted(imagesListArray.size() - 1);
                    }
                }

                break;
            case PICK_CAMERA_REQUEST_CODE:
                try {
                    Bitmap mImageBitmap = null;
                    Uri tempUri = Uri.parse(mCurrentPhotoPath);
                    File finalFile = new File(tempUri.getPath());
                    mCurrentPhotoPath = comfun.compressImage(mContext, finalFile.getAbsolutePath(), MAIN_FOLDER + "/Images/");
                    imagesListArray.add(finalFile.getName());
                    imagesadapter.notifyItemInserted(imagesListArray.size() - 1);
                } catch (Exception ex) {
                    Log.e(mTAG, ex.getMessage());
                }
                break;
        }
    }


    public void pickImage() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        intent.setType("image/*");
        intent.putExtra("crop", "true");
        intent.putExtra("scale", true);
        //intent.putExtra("outputX", 512);
        //intent.putExtra("outputY", 512);
        intent.putExtra("aspectX", 0);
        intent.putExtra("aspectY", 0);
        //  intent.putExtra("return-data", true);
        File f = null;
        try {
            f = createImageFileNew(null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, f.toURI());
        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
        startActivityForResult(intent, PICK_GALLERY_REQUEST_CODE);
    }

    private void attachFiles() {
        for (int i = 0; i < documentsPathsArray.size(); i++) {
            if (!attachmentArray.contains(documentsPathsArray.get(i))) {
                attachmentArray.add(documentsPathsArray.get(i));
            }
        }
        String[] tinyArray = tinyDB.getString(ATTACHMENT_ARRAY).split(",");
        for (String str : tinyArray) {
            if (!attachmentArray.contains(str)) {
                attachmentArray.add(str);
            }
        }
        String fileAttachmentArray = "";
        for (String str : attachmentArray) {
            if (!fileAttachmentArray.contains(str)) {
                fileAttachmentArray = str + "," + fileAttachmentArray;
            }
        }
        //String fileAttachmentArray = tinyDB.getString(ATTACHMENT_ARRAY);
        //fileAttachmentArray = (fileAttachmentArray.equalsIgnoreCase("") ? "" : fileAttachmentArray + ",") + documentsPathsArray.get(i);
        tinyDB.putString(ATTACHMENT_ARRAY, fileAttachmentArray);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == LocalConstants.REQ_CODE.CAMERA_REQUEST) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "camera permission granted", Toast.LENGTH_LONG).show();
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, LocalConstants.PERMISSION_CODE.CAMERA_REQUEST);
            } else {
                Toast.makeText(getActivity(), "camera permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void deleteImage(int position) {
        imagesListArray.remove(position);
        imagesadapter.notifyItemRemoved(position);
    }
}