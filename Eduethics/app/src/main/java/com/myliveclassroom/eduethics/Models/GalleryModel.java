package com.myliveclassroom.eduethics.Models;

public class GalleryModel {
    private String GalleryId;
    private String GalleryTitle;
    private String NoOfImages;
    private String ImageName;
    private  String SchoolId;
    private String SessionId;

    public String getSchoolId() {
        return SchoolId;
    }

    public String getGalleryId() {
        return GalleryId;
    }

    public void setGalleryId(String galleryId) {
        GalleryId = galleryId;
    }

    public void setSchoolId(String schoolId) {
        SchoolId = schoolId;
    }

    public void setSessionId(String sessionId) {
        SessionId = sessionId;
    }

    public String getSessionId() {
        return SessionId;
    }

    public String getGalleryTitle() {
        return GalleryTitle;
    }

    public String getImageName() {
        return ImageName;
    }

    public String getNoOfImages() {
        return NoOfImages;
    }

    public void setGalleryTitle(String galleryTitle) {
        GalleryTitle = galleryTitle;
    }

    public void setImageName(String imageName) {
        ImageName = imageName;
    }

    public void setNoOfImages(String noOfImages) {
        NoOfImages = noOfImages;
    }


}
