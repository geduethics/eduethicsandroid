package com.myliveclassroom.eduethics.Models;

import com.google.gson.annotations.SerializedName;

public class FeesModel {

    @SerializedName("RecieptNo")
    private String RecieptNo;

    @SerializedName("RecieptMonth")
    private String RecieptMonth;

    @SerializedName("RecieptDate")
    private String RecieptDate;

    @SerializedName("FeeScheduleName")
    private String FeeScheduleName;

    @SerializedName("FeeHeadName")
    private String FeeHeadName;

    @SerializedName("FeeHeadAmount")
    private String FeeHeadAmount;

    @SerializedName("FeeHeadAmountPaid")
    private String FeeHeadAmountPaid;

    @SerializedName("FeeHeadConcession")
    private String FeeHeadConcession;


    public String getRecieptMonth() {
        return RecieptMonth;
    }

    public void setRecieptMonth(String recieptMonth) {
        RecieptMonth = recieptMonth;
    }

    public String getRecieptNo() {
        return RecieptNo;
    }

    public void setRecieptNo(String recieptNo) {
        RecieptNo = recieptNo;
    }

    public String getFeeHeadAmount() {
        return FeeHeadAmount;
    }

    public String getFeeHeadAmountPaid() {
        return FeeHeadAmountPaid;
    }

    public String getFeeHeadName() {
        return FeeHeadName;
    }

    public String getFeeScheduleName() {
        return FeeScheduleName;
    }

    public String getFeeHeadConcession() {
        return FeeHeadConcession;
    }

    public String getRecieptDate() {
        return RecieptDate;
    }

    public void setFeeHeadAmount(String feeHeadAmount) {
        FeeHeadAmount = feeHeadAmount;
    }

    public void setFeeHeadAmountPaid(String feeHeadAmountPaid) {
        FeeHeadAmountPaid = feeHeadAmountPaid;
    }

    public void setFeeHeadConcession(String feeHeadConcession) {
        FeeHeadConcession = feeHeadConcession;
    }

    public void setFeeHeadName(String feeHeadName) {
        FeeHeadName = feeHeadName;
    }

    public void setFeeScheduleName(String feeScheduleName) {
        FeeScheduleName = feeScheduleName;
    }

    public void setRecieptDate(String recieptDate) {
        RecieptDate = recieptDate;
    }
}
