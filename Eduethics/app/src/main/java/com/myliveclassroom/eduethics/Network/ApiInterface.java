package com.myliveclassroom.eduethics.Network;

//import com.webapptechnology.lalajiand.Services.ProductService;

import com.myliveclassroom.eduethics.Models.ApiResponseModel;
import com.myliveclassroom.eduethics.Models.AreaModel;
import com.myliveclassroom.eduethics.Models.CircularModel;
import com.myliveclassroom.eduethics.Models.DashboardModel;
import com.myliveclassroom.eduethics.Models.ExamModel;
import com.myliveclassroom.eduethics.Models.FeeModel;
import com.myliveclassroom.eduethics.Models.FeeModelTutor;
import com.myliveclassroom.eduethics.Models.FeesModel;
import com.myliveclassroom.eduethics.Models.GalleryModel;
import com.myliveclassroom.eduethics.Models.HomeWorkModel;
import com.myliveclassroom.eduethics.Models.MessageModel;
import com.myliveclassroom.eduethics.Models.QuestionModel;
import com.myliveclassroom.eduethics.Models.TimeTableModel;
import com.myliveclassroom.eduethics.Models.UserModel;
import com.myliveclassroom.eduethics.Models.VideoLectureModel;
import com.google.gson.JsonObject;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiInterface {

    @GET("AppShareText.json")
    Call<JsonObject> getAppShareText();

    @POST("api/PreLogin")
    Call<JsonObject> PreLogin(@Body UserModel user);

    @POST("api/classSave")
    Call<JsonObject> classSave(@Body TimeTableModel user);

    @POST("api/classListWithSubjects")
    Call<JsonObject> classListWithSubjects(@Body TimeTableModel user);

    /*@POST("api/getClassStudent")
    Call<JsonObject> getClassStudent(@Body TimeTableModel user);*/

    @POST("api/getTimeTableDays")
    Call<JsonObject> getTimeTableDays(@Body TimeTableModel timeTableModel);

    @POST("api/getTimeTableDaySchedule")
    Call<JsonObject> getTimeTableDaySchedule(@Body TimeTableModel timeTableModel);

    @POST("api/getTimeTableAll")
    Call<JsonObject> getTimeTableAll(@Body TimeTableModel timeTableModel);

    @POST("api/setTimeTableDays")
    Call<JsonObject> setTimeTableDays(@Body TimeTableModel timeTableModel);



    @POST("api/classEnroll")
    Call<JsonObject> classEnroll(@Body TimeTableModel timeTableModel);

    @POST("api/TutorStudentClassList")
    Call<JsonObject> TutorStudentClassList(@Body TimeTableModel timeTableModel);

    @POST("api/listStudentsToTutor")
    Call<JsonObject> listStudentsToTutor(@Body TimeTableModel timeTableModel);

    @POST("api/tutorStudentApproveReject")
    Call<JsonObject> tutorStudentApproveReject(@Body TimeTableModel timeTableModel);

    @POST("api/tutorHomeWorkList")
    Call<JsonObject> homeWorkList(@Body TimeTableModel timeTableModel);

    @Multipart
    @POST("api/HomeWorkSubmitWithFile")
    Call<JsonObject> HomeWorkSubmitWithFile( @Part MultipartBody.Part[] file, @Part("Model") RequestBody requestBody);


   /* @POST("api/getClassStudent")
    Call<List<TimeTableModel>> getClassStudent(@Body TimeTableModel timeTableModel);*/

    @POST("api/getClassStudent2")
    Call<List<TimeTableModel>> getClassStudent2(@Body TimeTableModel timeTaqbleModel);





    @GET("version.json")
    Call<List<ApiResponseModel>> getAppVersion();



    @GET("HomeworkHead.json")
    Call<List<ApiResponseModel>> getHomeworkHead();

    @POST("api/getNoOfDays")
    Call<TimeTableModel> getNoOfDays();

    @POST("api/timeTable")
    Call<JsonObject> getTimeTable(@Body TimeTableModel timeTableModel);

    @POST("api/getTeacherClasses")
    Call<List<TimeTableModel>> getTeacherClasses(@Body TimeTableModel timeTableModel);

    @POST("api/getClassStudent")
    Call<List<TimeTableModel>> getClassStudent(@Body TimeTableModel timeTableModel);

    @POST("api/AttendanceSubmit")
    Call<JsonObject> attendanceSubmit(@Body  List<TimeTableModel> timeTableModelList);

    @POST("api/AttendanceSummary")
    Call<List<TimeTableModel>> AttendanceSummary(@Body  TimeTableModel listModel);

    @POST("api/AttendanceSelectOnDate")
    Call<List<TimeTableModel>> AttendanceSelectOnDate(@Body TimeTableModel listModel);

    @POST("api/AttendanceSelect")
    Call<List<TimeTableModel>> AttendanceList(@Body TimeTableModel timeTableModel);

    @POST("api/AttendanceSelect")
    Call <List<FeesModel>> feeData();

    @POST("api/HomeWorkList")
    Call<List<HomeWorkModel>> HomeWorkList(@Body HomeWorkModel homeWorkModel);

    @POST("api/getTeacherTimeTable")
    Call<JsonObject> getTeacherTimeTable(@Body TimeTableModel listModel);

    @POST("api/HomeWorkSubmit")
    Call<ApiResponseModel> HomeWorkSubmit(@Body HomeWorkModel timeTableModel);



    @POST("api/Login")
    Call<List<UserModel>> Login(@Body UserModel user);


    @POST("api/ExamRegistration")
    Call<List<UserModel>> ExamRegistration(@Body UserModel user);


    @POST("api/getTeacherTimeTable")
    Call<JsonObject> getTimeTableTeacher(@Body TimeTableModel timeTableModel);

    @Multipart
    @POST("api/ProfilePicUpdate")
    Call<Object> ProfilePicUpdate( @Part MultipartBody.Part[] file, @Part("Model") RequestBody requestBody);

    @POST("api/HomeworkDelete")
    Call<ApiResponseModel> deleteHomework(@Body HomeWorkModel homeWorkModel);

    @POST("api/CircularList")
    Call<List<CircularModel>> CircularList(@Body CircularModel circularModel);

    @Multipart
    @POST("api/CircularSave")
    Call<Object> CircularSave( @Part MultipartBody.Part[] file, @Part("Model") RequestBody requestBody);

    @POST("api/GalleryList")
    Call<List<GalleryModel>> GalleryList(@Body GalleryModel galleryModel);


    @Multipart
    @POST("api/GallerySave")
    Call<Object> GallerySave( @Part MultipartBody.Part[] file, @Part("Model") RequestBody requestBody);

    @POST("api/GalleryImageDelete")
    Call<ApiResponseModel> GalleryImageDelete(@Body GalleryModel galleryModel);

    @POST("api/GalleryDelete")
    Call<ApiResponseModel> GalleryDelete(@Body GalleryModel galleryModel);

    @POST("api/CircularDelete")
    Call<ApiResponseModel> CircularDelete(@Body CircularModel galleryModel);

    @POST("api/NotificationList")
    Call<List<CircularModel>> NotificationList(@Body CircularModel circularModel);

    @POST("api/changePassword")
    Call<ApiResponseModel> changePassword(@Body UserModel userModel);

    @POST("api/BirthdayList")
    Call<List<UserModel>> BirthdayList(@Body UserModel userModel);

    @POST("api/FcmTokenUpdate")
    Call<ApiResponseModel>  FcmTokenUpdate(@Body UserModel user);

    @Multipart
    @POST("api/HomeWorkDoneSubmitWithFile")
    Call<JsonObject> HomeWorkDoneSubmitWithFile(@Part MultipartBody.Part[] file, @Part("Model") RequestBody requestBody);

    /*@POST("api/HomeWorkDoneList")
    Call<List<HomeWorkModel>> HomeWorkDoneList(@Body HomeWorkModel homeWorkModel);*/

    @POST("api/HomeWorkDoneList")
    Call<JsonObject> HomeWorkDoneList(@Body HomeWorkModel homeWorkModel);

    @POST("api/ThoughtSelectOne")
    Call<ApiResponseModel> ThoughtSelectOne();

    @POST("api/HomeWorStausSubmit")
    Call<ApiResponseModel> HomeWorStausSubmit(@Body HomeWorkModel homeWorkModel);

    @POST("api/MessageList")
    Call<List<MessageModel>> MessageList(@Body MessageModel messageModel);

    @POST("api/QuestionList")
    Call<List<QuestionModel>> QuestionList(@Body QuestionModel questionModel);

    @POST("api/DistrictList")
    Call<List<AreaModel>> DistrictList();

    @POST("api/ExamSummary")
    Call<List<ExamModel>> ExamSummary(@Body UserModel questionModel);

    @POST("api/ExamSubmitSingleAns")
    Call<ApiResponseModel> ExamSubmitSingleAns(@Body QuestionModel questionModel);

    @POST("api/ExamFinalSubmit")
    Call<ApiResponseModel> ExamFinalSubmit(@Body QuestionModel questionModel);

    @POST("api/Certificate")
    Call<ApiResponseModel>  Certificate(@Body ExamModel questionModel);

    @POST("api/ResultSummary")
    Call<JsonObject> ResultSummary(@Body ExamModel examModel);

    @POST("api/VideoLectureList")
    Call<List<VideoLectureModel>> VideoLectureList(@Body VideoLectureModel videoLectureModel);

    @POST("api/VideoLectureSave")
    Call<ApiResponseModel> VideoLectureSave(@Body VideoLectureModel videoLectureModel);

    @POST("api/classList")
    Call<JsonObject> classList(@Body TimeTableModel timeTableModel);

    @POST("api/SubjectList")
    Call<List<TimeTableModel>> sujectList(@Body UserModel userModel);

    @POST("api/ExamCreate")
    Call<JsonObject> ExamCreate(@Body ExamModel examModel);

    @POST("api/ExamListByTeacher")
    Call<JsonObject> ExamListByTeacher(@Body ExamModel examModel);

    @POST("api/ExamMcqCreate")
    Call<JsonObject> ExamMcqCreate(@Body List<QuestionModel> questionModelList);

    @POST("api/getTeacherBySubject")
    Call<JsonObject> getTeacherBySubject(@Body TimeTableModel timeTableModel);

    @POST("api/StudentListSchool")
    Call<JsonObject> StudentListSchool(@Body TimeTableModel listModel);

    @POST("api/feeSchoolSelect")
    Call<JsonObject> feeSchoolSelectStudentWise(@Body FeeModel listModel);

    @POST("api/feeSchoolSelectByAdmin")
    Call<JsonObject> feeSchoolSelectByAdmin(@Body FeeModel listModel);

    @POST("api/AttendanceFeedBackSubmit")
    Call<JsonObject> classFeedbackSubmit(@Body List<TimeTableModel> timeTableModelList);

    @POST("api/TimeTableEduEthics_ScheduleSelect")
    Call<JsonObject> timeTable_ScheduleSelect(@Body TimeTableModel  timeTableModelList);

    @POST("api/Dashboard")
    Call<JsonObject> dashboardSelect(@Body DashboardModel dashboardModel);

    @POST("api/TutorFee_SaveClassFee")
    Call<JsonObject> TutorFee_SaveClassFee(@Body FeeModelTutor feeModelTutor);

    @POST("api/ClassAboutUpdate")
    Call<JsonObject> ClassAboutUpdate(@Body TimeTableModel timeTableModel);

    @POST("api/TutorClassList")
    Call<JsonObject> TutorClassList(@Body TimeTableModel timeTableModel);

    @POST("api/TutorFeeListForStudent")
    Call<JsonObject> TutorFeeListForStudent(@Body TimeTableModel timeTableModel);

    @POST("api/TutorFee_SaveStudentWise")
    Call<JsonObject> TutorFee_SaveStudentWise(@Body FeeModelTutor mFeeModelTutor);

    @POST("api/FeeSaveTxn")
    Call<JsonObject> FeeSaveTxn(@Body FeeModel feeModel);

    @POST("api/sendSheduleEmail")
    Call<JsonObject> sendSheduleEmail(@Body TimeTableModel timeTableModel);

    @POST("api/ExamMakeLive")
    Call<JsonObject> ExamMakeLive(@Body ExamModel examModel);

    @POST("api/getTimeTableDayScheduleNextPeriod")
    Call<JsonObject> getTimeTableDayScheduleNextPeriod(@Body TimeTableModel timeTableModel);

    @POST("api/AttendanceReportStudentDateWise2")
    Call<JsonObject> AttendanceReportStudentDateWise(@Body TimeTableModel listModel);

    @POST("api/AttendanceReportStudentDateSubjectWise")
    Call<JsonObject> AttendanceReportStudentDateSubjectWise(@Body  TimeTableModel listModel);

    @POST("api/classHealthSubmit")
    Call<JsonObject> classHealthSubmit(@Body List<TimeTableModel> timeTableModelList);

    @POST("api/AttendanceReport")
    Call<JsonObject> AttendanceReport(@Body TimeTableModel listModel);
}