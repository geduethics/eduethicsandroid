package com.myliveclassroom.eduethics.Activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.NotificationChannel;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.FileProvider;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.myliveclassroom.eduethics.Adapter.CircularAdapter;
import com.myliveclassroom.eduethics.Models.ApiResponseModel;
import com.myliveclassroom.eduethics.Models.CircularModel;
import com.myliveclassroom.eduethics.Network.ApiClient;
import com.myliveclassroom.eduethics.Network.ApiInterface;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.myliveclassroom.eduethics.Utils.GlobalVariables.API_BASE_URL;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.APP_PACKAGE_NAME;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.ATTACHMENT_ARRAY;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.CIRCULAR_CIRCULAR;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.MAIN_FOLDER;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.PICK_PDF_REQUEST_CODE;
import static com.myliveclassroom.eduethics.Utils.GlobalVariables.mTAG;

public class CircularsActivity extends BaseActivity {

    @BindView(R.id.txtPageTitle)
    TextView txtPageTitle;

    @BindView(R.id.imgBack)
    ImageView imgBack;

    @BindView(R.id.imgbtnAdd)
    ImageView imgbtnAdd;

    @BindView(R.id.btnAdd)
    TextView btnAdd;

    @BindView(R.id.txtDetailHead)
    TextView txtDetailHead;

    @BindView(R.id.txtDetailDesc)
    TextView txtDetailDesc;

    @BindView(R.id.iconDownload)
    ImageView iconDownload;

    @BindView(R.id.imgCal)
    ImageView imgCal;

    @BindView(R.id.txtDownloadPer)
    TextView txtDownloadPer;

    @BindView(R.id.layoutCircularDetail)
    RelativeLayout layoutCircularDetail;

    List<CircularModel> circularModelList;
    CircularAdapter circularAdapter;

    RecyclerView recyclerCircular;


    LinearLayout layoutCircularAdd;
    TextView txtBtnCancel, txtBtnSubmit, txtAttachment,editTextDate;
    EditText editTextHead, editTextDetail ;
    ImageView imgAttachFile;
    Date circularDate;
    SimpleDateFormat dateformat = new SimpleDateFormat("dd-MMM-yyyy");
    boolean isAddLayoutOpen;
    protected static TextView displayCurrentDate;
    //List<String> circularHeadList = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.MyThemeNoBar);
        setContentView(R.layout.activity_circulars);
        _BaseActivity(this);
        ButterKnife.bind(this);
        init();
    }

    private void Back() {
            if (!isAddLayoutOpen) {
                super.onBackPressed();
            }
            recyclerCircular.setVisibility(View.VISIBLE);
            layoutCircularAdd.setVisibility(View.GONE);
            layoutCircularDetail.setVisibility(View.GONE);
            isAddLayoutOpen = false;
    }

    @Override
    public void onBackPressed() {
        if (!isAddLayoutOpen) {
            super.onBackPressed();
        }
        recyclerCircular.setVisibility(View.VISIBLE);
        layoutCircularAdd.setVisibility(View.GONE);
        layoutCircularDetail.setVisibility(View.GONE);
        isAddLayoutOpen = false;

    }

    private void init() {
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        layoutCircularAdd = findViewById(R.id.layoutCircularAdd);
        txtBtnCancel = findViewById(R.id.txtBtnCancel);
        txtBtnSubmit = findViewById(R.id.txtBtnSubmit);
        txtAttachment = findViewById(R.id.txtAttachment);

        imgbtnAdd.setVisibility(View.INVISIBLE);
        imgBack.setVisibility(View.VISIBLE);
        btnAdd.setVisibility(View.GONE);
        btnAdd.setText("");

        editTextHead = findViewById(R.id.editTextHead);
        editTextDetail = findViewById(R.id.editTextDetail);
        editTextDate = findViewById(R.id.editTextDate);

        if (tinyDB.getString("IsAdmin").equalsIgnoreCase("Y")) {
            imgbtnAdd.setVisibility(View.VISIBLE);
            btnAdd.setVisibility(View.VISIBLE);
            btnAdd.setText("Add");
        }
        layoutCircularAdd.setVisibility(View.GONE);


        txtPageTitle.setText("Circulars");
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Back();
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recyclerCircular.setVisibility(View.GONE);
                layoutCircularAdd.setVisibility(View.VISIBLE);
                layoutCircularDetail.setVisibility(View.GONE);
            }
        });

        imgbtnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recyclerCircular.setVisibility(View.GONE);
                layoutCircularAdd.setVisibility(View.VISIBLE);
                layoutCircularDetail.setVisibility(View.GONE);
            }
        });

        txtBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerCircular.setVisibility(View.VISIBLE);
                layoutCircularAdd.setVisibility(View.GONE);
                layoutCircularDetail.setVisibility(View.GONE);
            }
        });

        txtBtnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CircularSave();
            }
        });

        imgAttachFile = findViewById(R.id.imgAttachFile);
        imgAttachFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //comfun.open_camera(mContext,101);
                popWindowAtt();

            }
        });

        iconDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadFiles(attachmentArray, txtDownloadPer);
            }
        });
        displayCurrentDate=editTextDate;
        imgCal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerFragment mDatePicker = new DatePickerFragment();
                mDatePicker.show(getSupportFragmentManager(), "Select date");
            }
        });

        initRecyclerView();

        //txtAttachCountHead=findViewById(R.id.txtAttachCountHead);
        //imgAttIconHead=findViewById(R.id.imgAttIconHead);
        //imgAttIconHead.setVisibility(View.GONE);
    }

    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
            Calendar cal = new GregorianCalendar(year, month, day);
            String s = df.format(cal.getTime());
            displayCurrentDate.setText(s);
            //displayCurrentDate.setText( String.valueOf(year) + " - " + String.valueOf(month) + " - " + String.valueOf(day));
        }
    }



    private void initRecyclerView() {
        circularModelList = new ArrayList<>();
        recyclerCircular = findViewById(R.id.recyclerCircular);
        recyclerCircular.setLayoutManager(new LinearLayoutManager(mContext));
        //getCirculars();
    }

    private void getCirculars() {

        showProgress();
        CircularModel circularModel = new CircularModel();
        circularModel.setSchoolId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
        circularModel.setSessionId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));
        circularModel.setClassId(tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID));
        circularModel.setFileType(CIRCULAR_CIRCULAR);

        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<List<CircularModel>> call = apiService.CircularList(circularModel);
        call.enqueue(new Callback<List<CircularModel>>() {
            @Override
            public void onResponse(Call<List<CircularModel>> call, Response<List<CircularModel>> response) {
                hideProgress();
                try {
                    circularModelList = response.body();
                    circularAdapter = new CircularAdapter(mContext, circularModelList);
                    recyclerCircular.setAdapter(circularAdapter);

                } catch (Exception ex) {
                    Log.e(mTAG, "error =" + ex.getMessage());
                }
            }

            @Override
            public void onFailure(Call<List<CircularModel>> call, Throwable t) {
                hideProgress();
                Log.e(mTAG, "error =" + t.toString());
            }
        });
    }

    private void CircularSave() {

        JSONObject jsonObject = new JSONObject();

        if(editTextDate.getText().toString().trim().length()==0){
            Toast.makeText(mContext, "Please enter date", Toast.LENGTH_SHORT).show();
            return;
        }
        if(editTextHead.getText().toString().trim().length()==0){
            Toast.makeText(mContext, "Please enter Heading", Toast.LENGTH_SHORT).show();
            return;
        }
        try {

            try {
                circularDate = dateformat.parse(editTextDate.getText().toString());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            jsonObject.put("CreatedBy", tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.USER_ID));

            jsonObject.put("SchoolId", tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SCHOOL_ID));
            jsonObject.put("SessionId", tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.SESSION_ID));
            jsonObject.put("ClassId", tinyDB.getString(LocalConstants.TINYDB_LOGIN_KEYS.CLASS_ID));
            jsonObject.put("FileType", CIRCULAR_CIRCULAR);

            jsonObject.put("CircularHead", editTextHead.getText().toString());
            jsonObject.put("CircularDetail", editTextDetail.getText().toString());
            jsonObject.put("CircularBy", "");

            String dt = dateformat.format(circularDate.getTime());
            Log.e(mTAG, "date:=" + dt);
            jsonObject.put("CircularDate", dt);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        ApiInterface apiService =
                ApiClient.getClient(mContext).create(ApiInterface.class);


        String ClaimImages = tinyDB.getString(ATTACHMENT_ARRAY);
        String[] claimImagesArry = ClaimImages.split(",");
        MultipartBody.Part[] body = new MultipartBody.Part[claimImagesArry.length];

        int j = 0;
        for (int i = 0; i < claimImagesArry.length; i++) {
            String path = claimImagesArry[i].replace("file:", "");
            if (!path.equalsIgnoreCase("")) {
                File file1 = new File(path);
                RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file1);
                try {
                    body[j] = MultipartBody.Part.createFormData("uploaded_file_" + String.valueOf(j), file1.getName(), requestFile);
                    j++;
                } catch (Exception ex) {
                    Log.e(mTAG, "" + ex.getMessage());
                }
            }
        }
        disableSubmitBtn();
        RequestBody modelParm = RequestBody.create(MediaType.parse("text/plain"), jsonObject.toString());

        Call<Object> call = apiService.CircularSave(body, modelParm);

        // Call<ApiResponse> call = apiService.PostSave(Desc,CreatedBy);
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                //int statusCode = response.code();
                enableSubmitBtn();
                try {
                    Log.e("res", response.body().toString());
                    tinyDB.putString(ATTACHMENT_ARRAY, "");
                    txtAttachment.setText("No attachment");
                    layoutCircularAdd.setVisibility(View.GONE);
                    Toast.makeText(mContext, "Done!", Toast.LENGTH_SHORT).show();
                    recyclerCircular.setVisibility(View.VISIBLE);
                    layoutCircularAdd.setVisibility(View.GONE);
                    layoutCircularDetail.setVisibility(View.GONE);
                    isAddLayoutOpen = false;
                    getCirculars();
                } catch (Exception ex) {
                    Log.e(mTAG, "error =" + ex.getMessage());
                    Toast.makeText(mContext, "Something went wrong2!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                // Log error here since request failed
                Log.e(mTAG, "error =" + t.toString());
                // myProgressDialog.hideProgress();
                Toast.makeText(mContext, "Something went wrong1!", Toast.LENGTH_SHORT).show();
                enableSubmitBtn();
            }
        });
    }

    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    int clickCount = 0;

    private void enableSubmitBtn() {
       hideProgress();

        clickCount = 0;
        txtBtnSubmit.setEnabled(true);
        txtBtnSubmit.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
        txtBtnSubmit.setText("Submit");
    }

    private void disableSubmitBtn() {
         showProgress();
        clickCount = 1;
        txtBtnSubmit.setEnabled(false);
        txtBtnSubmit.setText("Please wait");
        txtBtnSubmit.setBackgroundColor(mContext.getResources().getColor(R.color.grey_att));
    }

    LinearLayout layoutGallery, layoutDocument, layoutCamera;
    AlertDialog alertDialog;

    public void popWindowAtt() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View dialogView = inflater.inflate(R.layout.file_attachment_layout, null);
        dialogBuilder.setView(dialogView);

        layoutCamera = (LinearLayout) dialogView.findViewById(R.id.layoutCamera);
        //layoutGallery = (LinearLayout) dialogView.findViewById(R.id.layoutGallery);
        layoutDocument = (LinearLayout) dialogView.findViewById(R.id.layoutDocument);
        ImageView imgBtnCloseDialog = dialogView.findViewById(R.id.imgBtnCloseDialog);

        imgBtnCloseDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        layoutCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(CircularsActivity.this);
                alertDialog.dismiss();

            }
        });

        layoutDocument.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                openDocExplorer(txtAttachment);
                alertDialog.dismiss();


            }
        });

        alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    public void openDocExplorer(TextView txtAttachment) {
        mtxtAttachment = txtAttachment;
        Intent intent = new Intent();
        intent.setType("application/pdf");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        startActivityForResult(Intent.createChooser(intent, "Select Pdf"), PICK_PDF_REQUEST_CODE);
    }

    TextView mtxtAttachment;
    ArrayList<String> documentsPathsArray;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_CANCELED) {
            //Write your code if there's no result
            mtxtAttachment.setText("No attachment");
            return;
        }
        if (requestCode == PICK_PDF_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            documentsPathsArray = new ArrayList<>();

            //Single file upload code --OK
            if (data.getData() != null) {
                Uri docUri = data.getData();
                File f = null;
                try {
                    f = comfun.saveFile(mContext, docUri, "", MAIN_FOLDER, "doc_");
                    mCurrentPhotoPath = f.getAbsolutePath();
                    documentsPathsArray.add(mCurrentPhotoPath);
                    //new UploadFileToServer().execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else {
                documentsPathsArray = new ArrayList<>();
                ClipData clipData = data.getClipData();
                if (clipData != null) {
                    for (int i = 0; i < clipData.getItemCount(); i++) {
                        ClipData.Item item = clipData.getItemAt(i);
                        Uri uri = item.getUri();
                        File file;
                        try {
                            file = comfun.saveFile(mContext, uri, "", MAIN_FOLDER, "doc_");
                            String path = file.getAbsolutePath();
                            documentsPathsArray.add(path);
                        } catch (IOException e) {
                            Log.e(mTAG, e.getMessage());
                            e.printStackTrace();
                        }
                    }

                }
                Log.e(mTAG, "PICK_PDF_REQUEST -> Enter 2" + documentsPathsArray.toString());
            }

            for (int i = 0; i < documentsPathsArray.size(); i++) {
                String fileAttachmentArray = tinyDB.getString(ATTACHMENT_ARRAY);
                fileAttachmentArray = (fileAttachmentArray.equalsIgnoreCase("") ? "" : fileAttachmentArray + ",") + documentsPathsArray.get(i);
                tinyDB.putString(ATTACHMENT_ARRAY, fileAttachmentArray);
                if (mtxtAttachment != null) {
                    mtxtAttachment.setText(fileAttachmentArray.split(",").length + " attachments");
                }
            }

        } else if (requestCode == MY_CAMERA_PERMISSION_CODE) {
            Uri sourceUri = Uri.parse(mCurrentPhotoPath);
            comfun.openCropActivity(mContext, sourceUri, sourceUri, null);
        } else if (requestCode == PICK_IMAGE) {
            Uri sourceUri = data.getData();
            comfun.openCropActivity(mContext, sourceUri, sourceUri, null);
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                File finalFile = new File(resultUri.getPath());
                String fileAttachmentArray = tinyDB.getString(ATTACHMENT_ARRAY);
                fileAttachmentArray = (fileAttachmentArray.equalsIgnoreCase("") ? "" : fileAttachmentArray + ",") + finalFile.getAbsolutePath();
                tinyDB.putString(ATTACHMENT_ARRAY, fileAttachmentArray);
                mtxtAttachment.setText(fileAttachmentArray.split(",").length + " attachments");

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    String attachmentArray[] = {};

    public void openCircular(CircularModel circularModel) {
        isAddLayoutOpen = true;
        attachmentArray=new String[0];
        recyclerCircular.setVisibility(View.GONE);
        layoutCircularDetail.setVisibility(View.VISIBLE);

        txtDetailHead.setText(circularModel.getCircularHead());
        txtDetailDesc.setText(circularModel.getCircularDetail());
        if (circularModel.getAttachment() != null && !circularModel.getAttachment().isEmpty()) {
            attachmentArray = circularModel.getAttachment().split(",");
        }
        if (attachmentArray.length == 0) {
            txtDownloadPer.setVisibility(View.GONE);
            iconDownload.setVisibility(View.GONE);
        } else {
            txtDownloadPer.setVisibility(View.VISIBLE);
            iconDownload.setVisibility(View.VISIBLE);
        }

    }

    String downloadedFileListArray[];
    TextView mtxtPercentage;
    int fileDownloaded = 0;
    ProgressBar progressBar;
    int mTotalFiles = 0;
    int PROGRESS_MAX = 100;
    int PROGRESS_CURRENT = 0;
    int notificationId = 111000111;
    NotificationChannel channel = null;

    public void downloadFiles(String[] files, TextView txtPercentage) {


        downloadedFileListArray = files;

        //progressBarStyleHorizontal
        mtxtPercentage = txtPercentage;
        progressBar.setProgress(0);
        fileDownloaded = 0;
        mTotalFiles = files.length;
        for (int i = 0; i < files.length; i++) {
            new DownloadFileFromURL().execute(files[i]);
        }
    }

    private class DownloadFileFromURL extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //uploadNotification(notificationId,"Download starting","0% downloaded");
            //progressBar.setProgress(0);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mtxtPercentage.setText("Downloading " + (fileDownloaded == 0 ? 1 : fileDownloaded) + " of " + mTotalFiles + ", 0%");
                }
            });


        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(API_BASE_URL + "Circulars/" + f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // this will be useful so that you can show a tipical 0-100% progress bar
                int lenghtOfFile = conection.getContentLength();
                Log.e(mTAG, "lenghtOfFile =" + lenghtOfFile);
                // download the file
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                // Output stream
                OutputStream output = new FileOutputStream(Environment.getExternalStorageDirectory().toString() + "/" + Environment.DIRECTORY_DOWNLOADS.toString() + "/" + f_url[0]);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }
            return null;
        }

        /**
         * Updating progress bar
         */
        @Override
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            Log.e(mTAG, "progress=" + progress[0]);
            //notifyUploadProgress(notificationId,"Downloading 1 of "+mTotalFiles,progress[0]+"%",100,Integer.valueOf(progress[0]));
            //progressBar.setProgress(Integer.valueOf(progress[0]));
            //progressBar.setVisibility(View.VISIBLE);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mtxtPercentage.setText("Downloading " + (fileDownloaded == 0 ? 1 : fileDownloaded + 1) + " of " + mTotalFiles + ", " + progress[0] + "%");
                }
            });

        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String str) {

            // dismiss the dialog after the file was downloaded
            //Log.e(mTAG,""+file_url);
            //progressBar.setVisibility(View.GONE);
            //notificationManager.cancel(notificationId);
            fileDownloaded = fileDownloaded + 1;

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //Toast.makeText(mContext, "File(s) downloaded successfully", Toast.LENGTH_SHORT).show();
                    if (fileDownloaded == mTotalFiles) {
                        mtxtPercentage.setText("");
                        openDialog(null);
                    }
                }
            });
        }
    }


    public void openDialog(String[] arr) {
        if (downloadedFileListArray == null) {
            downloadedFileListArray = arr;
        }
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        dialogBuilder.setTitle("Open file");
        ListView list = new ListView(mContext);
        list.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, downloadedFileListArray));
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View view, int pos, long id) {
                final String path = Environment.getExternalStorageDirectory().toString() + "/" + Environment.DIRECTORY_DOWNLOADS.toString() + "/" + downloadedFileListArray[pos];
                File file = new File(path);
                //Uri photoURI = FileProvider.getUriForFile(mContext, getApplicationContext().getPackageName() + ".provider", file);
                Uri photoURI = FileProvider.getUriForFile(mContext, APP_PACKAGE_NAME+".provider", file);
                String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension
                        (MimeTypeMap.getFileExtensionFromUrl(path));
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setType(mimeType);
                intent.setDataAndType(photoURI, mimeType);
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    // handle no application here....
                    Toast.makeText(mContext, "Could not open this file", Toast.LENGTH_SHORT).show();
                }
            }
        });
        dialogBuilder.setView(list);

        alertDialog = dialogBuilder.create();
        alertDialog.show();
    }
    private boolean doubleBackToExitPressedOnce = false;
    public void deleteCircular(CircularModel galleryModel) {
        if (doubleBackToExitPressedOnce) {
            showProgress();
            ApiInterface apiService =
                    ApiClient.getClient(mContext).create(ApiInterface.class);

            Call<ApiResponseModel> call = apiService.CircularDelete(galleryModel);
            call.enqueue(new Callback<ApiResponseModel>() {
                @Override
                public void onResponse(Call<ApiResponseModel> call, Response<ApiResponseModel> response) {
                    hideProgress();
                    try {
                        ApiResponseModel apiResponseModel = response.body();
                        Toast.makeText(mContext, apiResponseModel.getMessage(), Toast.LENGTH_SHORT).show();
                        getCirculars();
                    } catch (Exception ex) {
                        Log.e(mTAG, "error =" + ex.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<ApiResponseModel> call, Throwable t) {
                    hideProgress();
                    Log.e(mTAG, "error =" + t.toString());
                }
            });
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        myToast(mContext, "double click to delete", Toast.LENGTH_SHORT);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
    @Override
    public void onResume(){
        super.onResume();
        getCirculars();
    }
}
