package com.myliveclassroom.eduethics.Models;

public class MessageModel {
    private String UserId;
    private String StudentName;
    private String TeacherName;
    private String Message;
    private String CreatedDate;
    private String Status;
    private String UserType;
    private String SessionId;
    private String SchoolId;

    public void setStatus(String status) {
        Status = status;
    }

    public String getStatus() {
        return Status;
    }

    public String getStudentName() {
        return StudentName;
    }

    public String getSessionId() {
        return SessionId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public void setSessionId(String sessionId) {
        SessionId = sessionId;
    }

    public void setSchoolId(String schoolId) {
        SchoolId = schoolId;
    }

    public String getSchoolId() {
        return SchoolId;
    }

    public String getUserId() {
        return UserId;
    }

    public String getTeacherName() {
        return TeacherName;
    }

    public void setUserType(String userType) {
        UserType = userType;
    }

    public String getUserType() {
        return UserType;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public String getMessage() {
        return Message;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public void setStudentName(String studentName) {
        StudentName = studentName;
    }

    public void setTeacherName(String teacherName) {
        TeacherName = teacherName;
    }
}
