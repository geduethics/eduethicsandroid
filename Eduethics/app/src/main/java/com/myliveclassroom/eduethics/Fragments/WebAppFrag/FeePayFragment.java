package com.myliveclassroom.eduethics.Fragments.WebAppFrag;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.myliveclassroom.eduethics.Adapter.FeeSchoolAdapter;
import com.myliveclassroom.eduethics.Adapter.HomeAdapter;
import com.myliveclassroom.eduethics.Adapter.PaymentMethodAdapter;
import com.myliveclassroom.eduethics.Models.PaymentMethodModel;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.classes.ExpandableHeightGridView;

import java.util.ArrayList;
import java.util.List;

public class FeePayFragment extends Fragment {
    Context mContext;
    List<PaymentMethodModel> paymentMethodModelList;
    PaymentMethodAdapter paymentMethodAdapter;
    View root;
    RecyclerView recyclerPaymentMethods;

    TextView txtAmt;

    public FeePayFragment(Context context) {
        // Required empty public constructor
        mContext = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_fee_pay, container, false);

        txtAmt=root.findViewById(R.id.txtAmt);

        initRecyclerView();

        return root;
    }

    private void initRecyclerView() {
        /*gridViewHome = root.findViewById(R.id.gridViewFeePayMethod);
        gridViewHome.setAdapter(new PaymentMethodAdapter(mContext));
        gridViewHome.setExpanded(true);*/

        txtAmt.setText( mContext.getResources().getString(R.string.Rs) +"3000");

        paymentMethodModelList = new ArrayList<>();

        PaymentMethodModel paymentModel = new PaymentMethodModel();
        //1
        paymentModel.setImgPaymentIcon(R.drawable.ic_pay_bhim);
        paymentModel.setPaymentMethod("Bhim Upi");
        paymentMethodModelList.add(paymentModel);

        //2
        paymentModel = new PaymentMethodModel();
        paymentModel.setImgPaymentIcon(R.drawable.ic_pay_card);
        paymentModel.setPaymentMethod("Credit Debit Card");
        paymentMethodModelList.add(paymentModel);

        //3
        paymentModel = new PaymentMethodModel();
        paymentModel.setImgPaymentIcon(R.drawable.ic_pay_paytm);
        paymentModel.setPaymentMethod("Paytm");
        paymentMethodModelList.add(paymentModel);

        //4
        paymentModel = new PaymentMethodModel();
        paymentModel.setImgPaymentIcon(R.drawable.ic_pay_google_pay);
        paymentModel.setPaymentMethod("Google Pay");
        paymentMethodModelList.add(paymentModel);

        //5
        paymentModel = new PaymentMethodModel();
        paymentModel.setImgPaymentIcon(R.drawable.ic_pay_net_banking);
        paymentModel.setPaymentMethod("Net Banking");
        paymentMethodModelList.add(paymentModel);

        recyclerPaymentMethods = root.findViewById(R.id.recyclerPaymentMethods);
        recyclerPaymentMethods.setLayoutManager(new LinearLayoutManager(mContext));
        paymentMethodAdapter = new PaymentMethodAdapter(FeePayFragment.this, mContext, paymentMethodModelList);
        recyclerPaymentMethods.setAdapter(paymentMethodAdapter);
    }
}