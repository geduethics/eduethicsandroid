package com.myliveclassroom.eduethics.Adapter;

import android.app.TimePickerDialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.annimon.stream.Stream;
import com.myliveclassroom.eduethics.Fragments.TeacherFragments.TimeTableScheduleCreateFragment;
import com.myliveclassroom.eduethics.Fragments.TeacherFragments.TimeTableScheduleViewFragment;
import com.myliveclassroom.eduethics.Models.TimeTableModel;
import com.myliveclassroom.eduethics.R;
import com.myliveclassroom.eduethics.Utils.ExtraFunctions;
import com.myliveclassroom.eduethics.Utils.LocalConstants;
import com.myliveclassroom.eduethics.classes.CommonFunctions;
import com.myliveclassroom.eduethics.classes.TinyDB;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.myliveclassroom.eduethics.Utils.HomeConstants.PROFILE;
import static com.myliveclassroom.eduethics.Utils.HomeConstants.SCHEDULE_TEACHER;

public class TimeTableScheduleViewAdapter extends RecyclerView.Adapter<TimeTableScheduleViewAdapter.ViewHolder> {

    TinyDB tinyDB;
    Fragment fragment;
    Context mContext;
    private List<TimeTableModel> timeTableModelListAll2;
    Calendar dateTime = Calendar.getInstance();
    int[] t = new int[4];

    public TimeTableScheduleViewAdapter(Context context, Fragment fragment) {
        timeTableModelListAll2 = null;
        mContext = context;
        this.fragment = fragment;
        tinyDB = new TinyDB(mContext);
    }

    @Override
    public TimeTableScheduleViewAdapter.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new TimeTableScheduleViewAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_schecule_layout, parent, false));
    }


    @Override
    public void onBindViewHolder(@NonNull @NotNull TimeTableScheduleViewAdapter.ViewHolder holder, int position) {
        holder.subjectSingleItem.setVisibility(View.GONE);
        holder.subjectSingleItem.setText("");
        holder.classSingleItem.setText(timeTableModelListAll2.get(position).getClassName());
        holder.txtTimes.setText(timeTableModelListAll2.get(position).getScheduleDate());
        if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.STUDENT)
                || tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.PARENT)) {
            holder.goLiveNow.setText("View Schedule");
            holder.goLiveNow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((TimeTableScheduleViewFragment) fragment).viewSchedule(timeTableModelListAll2.get(position).getScheduleDate(), timeTableModelListAll2.get(position).getClassId());
                }
            });
        } else if (tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER_SCHOOL)
                || tinyDB.getString(PROFILE).equalsIgnoreCase(LocalConstants.PROFILE_TYPE.TEACHER)) {
            if (tinyDB.getString(LocalConstants.TINYDB_KEYS.TEMP_SCREEN_NAME).equalsIgnoreCase(SCHEDULE_TEACHER)) {
                holder.goLiveNow.setText("View");
                holder.btnFwdSchedule.setText("Send");
                holder.btnFwdSchedule.setVisibility(View.VISIBLE);
                holder.btnFwdSchedule.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((TimeTableScheduleViewFragment) fragment).sendScheduleOnEmail(timeTableModelListAll2.get(position));
                    }
                });
            } else {
                holder.goLiveNow.setText("View / Edit");

            }
            holder.goLiveNow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((TimeTableScheduleViewFragment) fragment).viewEditSchedule(timeTableModelListAll2.get(position).getScheduleDate(), timeTableModelListAll2.get(position).getClassId());
                }
            });
        }


    }

    public void updateList(List<TimeTableModel> listAll) {
        timeTableModelListAll2 = listAll;
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return timeTableModelListAll2 == null ? 0 : timeTableModelListAll2.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView classSingleItem, txtTimes, subjectSingleItem, time, goLiveNow, btnFwdSchedule;
        //Spinner spinnerTeacher;

        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);

            this.classSingleItem = itemView.findViewById(R.id.classSingleItem);
            this.txtTimes = itemView.findViewById(R.id.txtTimes);
            this.subjectSingleItem = itemView.findViewById(R.id.time);
            this.time = itemView.findViewById(R.id.time);
            this.btnFwdSchedule = itemView.findViewById(R.id.btnFwdSchedule);
            this.goLiveNow = itemView.findViewById(R.id.goLiveNow);
            this.subjectSingleItem.setVisibility(View.GONE);
            this.btnFwdSchedule.setVisibility(View.GONE);
        }
    }
}
